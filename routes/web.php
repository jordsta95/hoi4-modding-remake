<?php

use Illuminate\Support\Facades\Route;

use App\Http\Middleware\checkUserIsAdmin;
use App\Http\Middleware\CheckAndSetLanguage;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => [App\Http\Middleware\CheckAndSetLanguage::class]], function(){

//Homepage
Route::get('/home', function () {
	return redirect('/');
});
Route::get('/', function () {
	return view('homepage');
});

//Coming soon
Route::get('/decision', function () {
	return view('coming-soon');
});

//Testing page
Route::get('/test', function () {
	return view('tools/research.edit');
});

//Data page
Route::get('/get-data/{type}', [App\Http\Controllers\DataController::class, 'locator']);

//Auth pages
Auth::routes();


//User/account pages
Route::group(['prefix' => 'user'], function(){
	Route::get('/', [App\Http\Controllers\UserController::class, 'index'])->name('account');
	Route::get('/{id}', [App\Http\Controllers\UserController::class, 'getUser'])->name('getUser');
	Route::get('/{id}/media', [App\Http\Controllers\UserController::class, 'getMedia'])->name('getMedia');

	Route::post('/{id}/addToTeam', [App\Http\Controllers\UserController::class, 'addUserToMod'])->name('addUserToMod');
	Route::post('/{id}/media', [App\Http\Controllers\UserController::class, 'updateMedia'])->name('updateMedia');
});
Route::get('/user-search', [App\Http\Controllers\UserController::class, 'findUser'])->name('findUser');

Route::post('/profile/update', [App\Http\Controllers\UserController::class, 'updateUserData'])->name('updateUserData');
Route::post('/profile/delete', [App\Http\Controllers\UserController::class, 'deleteUser'])->name('deleteUser');

//Mod pages
Route::group(['prefix' => 'mod'], function(){
	Route::get('/', [App\Http\Controllers\ModController::class, 'index'])->name('modList');
	Route::get('/{id}', [App\Http\Controllers\ModController::class, 'view'])->name('viewMod');
	Route::get('/{id}/export', [App\Http\Controllers\ModController::class, 'export'])->name('exportMod');
	Route::get('/api/list', [App\Http\Controllers\ModController::class, 'getModsList'])->name('allModsList');

	Route::post('/create', [App\Http\Controllers\ModController::class, 'create'])->name('createMod');
	Route::post('/delete/{id}', [App\Http\Controllers\ModController::class, 'delete'])->name('deleteMod');
	Route::post('/update/{id}', [App\Http\Controllers\ModController::class, 'update'])->name('updateMod');
	Route::post('/{id}/removeFromTeam/{modid}', [App\Http\Controllers\ModController::class, 'removeFromTeam'])->name('removeFromTeam');
});

//Focus tree pages
Route::group(['prefix' => 'focus-tree'], function(){
	Route::get('/', [App\Http\Controllers\FocusTreeController::class, 'index'])->name('focusTreeIndex');
	Route::get('/view/{id}', [App\Http\Controllers\FocusTreeController::class, 'view'])->name('getFocusTree'); //For non-logged in users/users not in mod team
	Route::get('/edit/{id}', [App\Http\Controllers\FocusTreeController::class, 'edit'])->name('editFocusTree');

	Route::post('/create', [App\Http\Controllers\FocusTreeController::class, 'create'])->name('createFocusTree');
	Route::post('/import', [App\Http\Controllers\FocusTreeController::class, 'import'])->name('importFocusTree');
	Route::post('/delete/{id}', [App\Http\Controllers\FocusTreeController::class, 'delete'])->name('deleteFocusTree');
	Route::post('/update/{id}', [App\Http\Controllers\FocusTreeController::class, 'update'])->name('updateFocusTree');
	Route::post('/update-continuous', [App\Http\Controllers\FocusTreeController::class, 'updateContinuousFocusesBox'])->name('updateContinuousFocusesBox');
});
//Individual focus endpoints; pseudo-API calls
Route::group(['prefix' => 'focus'], function(){
	Route::post('/update', [App\Http\Controllers\FocusController::class, 'update'])->name('updateFocus');
	Route::post('/update-media', [App\Http\Controllers\FocusController::class, 'updateMedia'])->name('updateFocusMedia');
	Route::post('/update-position', [App\Http\Controllers\FocusController::class, 'updatePosition'])->name('updateFocusPosition');
	Route::post('/delete', [App\Http\Controllers\FocusController::class, 'delete'])->name('deleteFocus');
});

//Country pages
Route::group(['prefix' => 'country'], function(){
	Route::get('/', [App\Http\Controllers\CountryController::class, 'index'])->name('countryIndex');
	Route::get('/view/{id}', [App\Http\Controllers\CountryController::class, 'view'])->name('getCountry');
	Route::get('/edit/{id}', [App\Http\Controllers\CountryController::class, 'edit'])->name('editCountry');
	Route::get('/api/list', [App\Http\Controllers\CountryController::class, 'list'])->name('countryList');
	Route::get('/api/{id}', [App\Http\Controllers\CountryController::class, 'api'])->name('countryAPI');
	Route::get('/api/{id}/editable', [App\Http\Controllers\CountryController::class, 'checkIfCountryIsEditable'])->name('countryEditable');


	Route::post('/save', [App\Http\Controllers\CountryController::class, 'save'])->name('saveCountryData');

	Route::post('/create', [App\Http\Controllers\CountryController::class, 'create'])->name('createCountry');
	Route::post('/update/{id}', [App\Http\Controllers\CountryController::class, 'update'])->name('updateCountry');
	Route::post('/import/{id}', [App\Http\Controllers\CountryController::class, 'import'])->name('importCountry');
	Route::post('/add-flag', [App\Http\Controllers\CountryController::class, 'addFlag'])->name('addFlagCountry');
	Route::post('/delete/{id}', [App\Http\Controllers\CountryController::class, 'delete'])->name('deleteCountry');
	Route::post('/get-ideologies', [App\Http\Controllers\CountryController::class, 'getListOfAvailableIdeologiesForCountry'])->name('getCountryIdeologies');
	Route::post('/get-start-dates', [App\Http\Controllers\CountryController::class, 'getListOfAvailableStartDatesForCountry'])->name('getCountryStartDates');
	Route::post('/update-ideologies/{id}', [App\Http\Controllers\CountryController::class, 'updateIdeologies'])->name('updateCountryIdeologies');
	Route::post('/update-start-dates/{id}', [App\Http\Controllers\CountryController::class, 'updateStartDates'])->name('updateCountryStartDates');
	Route::post('/update-name/{id}', [App\Http\Controllers\CountryController::class, 'updateName'])->name('updateCountryName');
});


//Ideas pages
Route::group(['prefix' => 'ideas'], function(){
	Route::get('/', [App\Http\Controllers\IdeaController::class, 'index'])->name('ideaIndex');
	Route::get('/view/{id}', [App\Http\Controllers\IdeaController::class, 'view'])->name('getIdea');
	Route::get('/edit/{id}', [App\Http\Controllers\IdeaController::class, 'edit'])->name('editIdea');
	Route::get('/list/{id}', [App\Http\Controllers\IdeaController::class, 'list'])->name('listIdeas');
	Route::get('/api/mod/{id}', [App\Http\Controllers\IdeaController::class, 'getForMod'])->name('getGroupedIdeasForMod');

	Route::post('/create', [App\Http\Controllers\IdeaController::class, 'create'])->name('createIdeaGroup');
	Route::post('/update', [App\Http\Controllers\IdeaController::class, 'update'])->name('updateIdea');
	Route::post('/update-media', [App\Http\Controllers\IdeaController::class, 'updateMedia'])->name('updateIdeaMedia');
	Route::post('/delete', [App\Http\Controllers\IdeaController::class, 'delete'])->name('deleteIdea');
	Route::post('/delete-group/{id}', [App\Http\Controllers\IdeaController::class, 'deleteGroup'])->name('deleteIdeaGroup');
	Route::post('/update-group/{id}', [App\Http\Controllers\IdeaController::class, 'updateGroup'])->name('updateIdeaGroup');
	Route::post('/import', [App\Http\Controllers\IdeaController::class, 'import'])->name('importIdeas');
});

//Events pages
Route::group(['prefix' => 'events'], function(){
	Route::get('/', [App\Http\Controllers\EventController::class, 'index'])->name('eventIndex');
	Route::get('/view/{id}', [App\Http\Controllers\EventController::class, 'view'])->name('getEvent');
	Route::get('/edit/{id}', [App\Http\Controllers\EventController::class, 'edit'])->name('editEvent');

	Route::post('/create', [App\Http\Controllers\EventController::class, 'create'])->name('createEventGroup');
	Route::post('/update', [App\Http\Controllers\EventController::class, 'update'])->name('updateEvent');
	Route::post('/update-option', [App\Http\Controllers\EventController::class, 'updateOption'])->name('updateEventOption');
	Route::post('/update-media', [App\Http\Controllers\EventController::class, 'updateMedia'])->name('updateEventMedia');
	Route::post('/delete', [App\Http\Controllers\EventController::class, 'delete'])->name('deleteEvent');
	Route::post('/delete-group/{id}', [App\Http\Controllers\EventController::class, 'deleteGroup'])->name('deleteEventGroup');
	Route::post('/update-group/{id}', [App\Http\Controllers\EventController::class, 'updateGroup'])->name('updateEventGroup');
	Route::post('/delete-option', [App\Http\Controllers\EventController::class, 'deleteOption'])->name('deleteEventOption');
	Route::post('/import', [App\Http\Controllers\EventController::class, 'import'])->name('importEvents');
});

//Decisions pages
Route::group(['prefix' => 'decision'], function(){
	Route::get('/', [App\Http\Controllers\DecisionController::class, 'index'])->name('decisionIndex');
	Route::get('/view/{id}', [App\Http\Controllers\DecisionController::class, 'view'])->name('getDecision');
	Route::get('/edit/{id}', [App\Http\Controllers\DecisionController::class, 'edit'])->name('editDecision');

	Route::post('/create', [App\Http\Controllers\DecisionController::class, 'create'])->name('createDecisionGroup');
	Route::post('/update', [App\Http\Controllers\DecisionController::class, 'update'])->name('updateDecision');
	Route::post('/update-option', [App\Http\Controllers\DecisionController::class, 'updateOption'])->name('updateDecisionOption');
	Route::post('/update-media', [App\Http\Controllers\DecisionController::class, 'updateMedia'])->name('updateDecisionMedia');
	Route::post('/delete/{id}', [App\Http\Controllers\DecisionController::class, 'delete'])->name('deleteDecision');
	Route::post('/delete-group/{id}', [App\Http\Controllers\DecisionController::class, 'deleteGroup'])->name('deleteDecisionGroup');
	Route::post('/update-group/{id}', [App\Http\Controllers\DecisionController::class, 'updateGroup'])->name('updateDecisionGroup');
	Route::post('/delete-option', [App\Http\Controllers\DecisionController::class, 'deleteOption'])->name('deleteDecisionOption');
	Route::post('/import', [App\Http\Controllers\DecisionController::class, 'import'])->name('importDecisions');
});

//Start Dates pages
Route::group(['prefix' => 'start-date'], function(){
	Route::get('/', [App\Http\Controllers\StartDateController::class, 'index'])->name('startDateIndex');
	Route::get('/view/{id}', [App\Http\Controllers\StartDateController::class, 'view'])->name('getStartDate');
	Route::get('/edit/{id}', [App\Http\Controllers\StartDateController::class, 'edit'])->name('editStartDate');

	Route::post('/create', [App\Http\Controllers\StartDateController::class, 'create'])->name('createStartDate');
	Route::post('/update', [App\Http\Controllers\StartDateController::class, 'update'])->name('updateStartDate');
	Route::post('/update-option', [App\Http\Controllers\StartDateController::class, 'updateOption'])->name('updateStartDateOption');
	Route::post('/update-media', [App\Http\Controllers\StartDateController::class, 'updateMedia'])->name('updateStartDateMedia');
	Route::post('/delete/{id}', [App\Http\Controllers\StartDateController::class, 'delete'])->name('deleteStartDate');
	Route::post('/import', [App\Http\Controllers\StartDateController::class, 'import'])->name('importStartDate');
});

//Ideology pages
Route::group(['prefix' => 'ideology'], function(){
	Route::get('/', [App\Http\Controllers\IdeologyController::class, 'index'])->name('ideologyIndex');
	Route::get('/view/{id}', [App\Http\Controllers\IdeologyController::class, 'view'])->name('getIdeology');
	Route::get('/edit/{id}', [App\Http\Controllers\IdeologyController::class, 'edit'])->name('editIdeology');
	Route::get('/api/ideology-list/{id}', [App\Http\Controllers\IdeologyController::class, 'getIdeologyList'])->name('getIdeologyList');


	Route::post('/create', [App\Http\Controllers\IdeologyController::class, 'create'])->name('createIdeology');
	Route::post('/update', [App\Http\Controllers\IdeologyController::class, 'update'])->name('updateIdeology');
	Route::post('/update-option', [App\Http\Controllers\IdeologyController::class, 'updateOption'])->name('updateIdeologyOption');
	Route::post('/update-media', [App\Http\Controllers\IdeologyController::class, 'updateMedia'])->name('updateIdeologyMedia');
	Route::post('/delete/{id}', [App\Http\Controllers\IdeologyController::class, 'delete'])->name('deleteIdeology');
	Route::post('/import', [App\Http\Controllers\IdeologyController::class, 'import'])->name('importIdeology');
});

//Character pages
Route::group(['prefix' => 'character'], function(){
	Route::get('/list', [App\Http\Controllers\CharacterListController::class, 'list'])->name('allCharacterLists');
	Route::get('/api/list', [App\Http\Controllers\CharacterListController::class, 'getLists'])->name('getAllCharacterLists');
	Route::get('/list/{id}', [App\Http\Controllers\CharacterListController::class, 'view'])->name('viewCharacterList');
	Route::get('/api/list/mod/{id}', [App\Http\Controllers\CharacterListController::class, 'getListsInMod'])->name('getCharacterListsForMod');
	Route::get('/api/list/{id}', [App\Http\Controllers\CharacterListController::class, 'getList'])->name('getAllCharactersInList');
	Route::get('/api/list/{id}/editable', [App\Http\Controllers\CharacterListController::class, 'checkIfListIsEditable'])->name('checkCharacterListEditable');

	Route::post('/list/create', [App\Http\Controllers\CharacterListController::class, 'createList'])->name('createCharacterList');
	Route::post('/api/update', [App\Http\Controllers\CharacterController::class, 'addOrUpdate'])->name('addOrUpdateCharacter');
	Route::post('/api/delete', [App\Http\Controllers\CharacterController::class, 'remove'])->name('removeCharacter');
	Route::post('/delete/{id}', [App\Http\Controllers\CharacterListController::class, 'deleteList'])->name('deleteCharacterList');
});

//Traits pages
Route::group(['prefix' => 'traits'], function(){
	Route::get('/list', [App\Http\Controllers\TraitController::class, 'list'])->name('allTraitLists');
	Route::get('/api/list', [App\Http\Controllers\TraitController::class, 'getLists'])->name('getAllTraitLists');
	Route::get('/api/editable/{id}', [App\Http\Controllers\TraitController::class, 'checkTraitsEditable'])->name('checkTraitsEditable');
	Route::get('/data/{id}/{type}', [App\Http\Controllers\TraitController::class, 'getData'])->name('getTraitsData');
	Route::get('/view/{id}/{type}', [App\Http\Controllers\TraitController::class, 'view'])->name('viewTraits');
});


//Forum pages
Route::group(['prefix' => 'forum'], function(){
	Route::get('/', [App\Http\Controllers\ForumController::class, 'index'])->name('forumIndex');
	Route::get('/{group}', [App\Http\Controllers\ForumController::class, 'category'])->name('forumGroup'); //Bug Reports/Team Search/Etc.
	Route::get('/{group}/{id}-{slug}', [App\Http\Controllers\ForumController::class, 'post'])->name('forumPost'); //Actual forum post

	Route::post('/new', [App\Http\Controllers\ForumController::class, 'create'])->name('forumPostCreate');
	Route::post('/{group}/{id}-{slug}', [App\Http\Controllers\ForumController::class, 'respond'])->name('forumAddResponse');
	Route::post('/comment/{id}', [App\Http\Controllers\ForumController::class, 'editComment'])->name('forumEditResponse');
	Route::post('/bug-report', [App\Http\Controllers\ForumController::class, 'bugReport'])->name('forumBugReportCreate');
});

//Map stuff
Route::get('/map/{file}.{extension}', function($file, $extension){
	if(Storage::has('map-svgs/'.$file.'.'.$extension)){
		// if(env('APP_ENV') == 'local'){
		// 	echo '<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>';
		// }
		return Storage::get('map-svgs/'.$file.'.'.$extension);
	}
});


//Media pages
Route::group(['prefix' => 'media'], function(){
	Route::get('/spritemap/{tool}', function($tool){
		return generateSpriteMap($tool);
	})->name('media.spriteMap');
	Route::get('/gfx-list/{tool}', function($tool){
		return getMedia($tool);
	});
	Route::get('/trait-sprite/{num}', [App\Http\Controllers\MediaController::class, 'getTraitSprite'])->name('media.traitSprite');
	// Route::get('/manager', ['as' => 'mediamanager.render', 'uses' => 'MediaController@renderMediaManager']);
	// Route::get('/manager/{path}', ['as' => 'mediamanager.renderPath', 'uses' => 'MediaController@renderMediaManagerPath']);
	Route::post('/delete/{id}', [App\Http\Controllers\MediaController::class, 'deleteImage'])->name('media.delete');

	Route::post('/upload', [App\Http\Controllers\MediaController::class,'upload'])->name('media.upload');
	Route::post('/upload-custom-gfx', [App\Http\Controllers\MediaController::class,'uploadCustomGFX'])->name('media.uploadCustomGFX');
	// Route::post('/upload-media-manager', ['as' => 'media.upload-media-manager', 'uses' => 'MediaController@mediaManagerUpload']);
	Route::get('/find/{id}', [App\Http\Controllers\MediaController::class,'find'])->name('media.find');
	Route::get('/find/{id}/{size}', [App\Http\Controllers\MediaController::class,'findSized'])->name('media.findSized');
	Route::get('/{path}/{filename}.{extension}', [App\Http\Controllers\MediaController::class,'findByName'])->name('media.findByName');
});


//Admin pages
Route::group(['prefix' => 'admin', 'middleware' => [App\Http\Middleware\checkUserIsAdmin::class]], function(){
	Route::get('/', function(){
		return view('admin.index');
	});

	Route::post('/upload-files', [App\Http\Controllers\Admin::class, 'uploadFiles'])->name('adminUploadFiles');
	Route::post('/create-svg-map', [App\Http\Controllers\Admin::class, 'createMapSVG'])->name('adminCreateMapSVG');
});

});