<p style="text-align: center;">
	<a href="{{ $link }}" style="background: #637FC1; text-decoration: none; color: #FFFFFF; padding-left: {{ space(2) }}  padding-right: {{ space(2) }} padding-top: {{ space() }}  padding-bottom: {{ space() }}">{{ $title }}</a>
</p>