@extends('emails.template')

@section('title')
	<h1 style="line-height: 1; margin: 0;">{{ translate('tools/forum/emails.newReply.title', ['name' => $user->username, 'title' => $title]) }}</h1>
@endsection

@section('content')
	<p style="text-align: center;">{{ translate('tools/forum/emails.newReply.content') }}</p>
	{!! $emailContent !!}
	<p style="margin-top: 30px;"> @include('emails.button', ['title' => translate('tools/forum/emails.newReply.button_text'), 'link' => $link]) </p>
@endsection