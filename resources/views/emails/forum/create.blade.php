@extends('emails.template')

@section('title')
	<h1 style="line-height: 1; margin: 0;">New Post Created on the HOI4 Forums</h1>
@endsection

@section('content')
	{!! $content !!}
	<p style="margin-top: 30px; text-align: center;"> @include('emails.button', ['title' => 'View Post', 'link' => $link]) </p>
@endsection