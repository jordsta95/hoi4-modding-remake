<?php
$blue = '#637FC1';
$text = '#232E21';
?>
<table align="center" border="0" cellpadding="0" cellspacing="0" style="max-width: 640px; font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Oxygen-Sans,Ubuntu,Cantarell,'Helvetica Neue',sans-serif">
	<tr>
		<td style="background: {{ $blue }}; padding: {{ space() }} color: #ffffff; text-align: center;">
			@yield('title')
		</td>
	</tr>
	<tr>
		<td style="padding: {{ space(2) . space() }}">
			<p style="font-size: 12px; text-align: center;">{{ translate('tools/forum/emails.times') }}</p>
			@yield('content')
		</td>
	</tr>
	<tr>
		<td style="background: #333333; font-size: 14px; text-align: center; color: #FFFFFF; padding: {{ space(0.5) . space() }}">
			<p>{{ translate('tools/forum/emails.generated_time') }} {{ date('d/M/Y') }} - {{ date("h:ia") }}</p>
			<p style="font-size: 12px;">HOI4modding.com</p>
		</td>
	</tr>
</table>