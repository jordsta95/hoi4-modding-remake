@extends('layouts.app')

@section('sidebar')
	@include('sections.menus.start-dates')
@endsection
@section('content')
<div class="modal import-modal">
	<div class="modal-header">
		{{ translate('tools/general-help.import_title') }}
		<div class="close" onclick="toggleImportModal()">{{ featherIcon('x') }}</div>
	</div>
	<div class="modal-content">
		<p>{{ translate('tools/general-help.import_instruction') }}</p>
		<div class="grid">
			<?php $files = ['bookmark', 'localisation']; ?>
			@foreach($files as $file)
				<div class="grid-col-span-6">
					{{ translate('tools/start-date/help.import_'.$file) }}
				</div>
				<div class="grid-col-span-6">
					<div class="input-with-help always-show-help">
						<input type="file" name="{{ $file }}" id="import-{{ $file }}">
						<div class="help-text">
							{{ featherIcon('info') }}
							<div class="help-text-info">
							{!! translate('tools/start-date/help.import_'.$file.'_help') !!}
							</div>
						</div>
					</div>
				</div>
			@endforeach
		</div>
		<p>&nbsp;</p>
		<p><button class="button full-width importBookmark">{{ translate('tools/general-help.import_now') }}</button></p>
	</div>
</div>
	<div id="{{ $date->id }}" class="start-date edit page-content">
		<start-date-form id="{{ $date->id }}" :data="startDate" :newCountry="newCountry"></start-date-form>
		<div class="help-text-file" id="start-date-data" v-pre>
			{{ $date }}
		</div>
		<div class="help-text-file" id="help-text-file" v-pre>
			{{ getLangArray('tools/start-date/help') }}
		</div>
		<div class="help-text-file" id="tool-help-file" v-pre>
			{{ getLangArray('tools/general-help') }}
		</div>
		<div class="help-text-file" id="gfx-list" v-pre>
			{{ getMedia('start-date') }}
		</div>

	</div>
@endsection
