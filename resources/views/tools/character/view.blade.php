@extends('layouts.app')
@if(Auth::check())
	@section('content')
		<div class="page-content">
			<character-list id="{{ $list->id }}" mod-id="{{ $list->mod_id }}"></character-list>
		</div>
	@endsection
@else
	@section('content')
		<div class="page-content">
			<h3>{{ translate('tools/mod/index.not_logged_in') }}</h3>
		</div>
	@endsection
@endif