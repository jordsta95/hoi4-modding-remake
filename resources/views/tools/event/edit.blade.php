@extends('layouts.app')

@section('sidebar')
	@include('sections.menus.events')
@endsection
@section('content')
<div class="modal import-modal">
	<div class="modal-header">
		{{ translate('tools/general-help.import_title') }}
		<div class="close" onclick="toggleImportModal()">{{ featherIcon('x') }}</div>
	</div>
	<div class="modal-content">
		<p>{{ translate('tools/general-help.import_instruction') }}</p>
		<div class="grid">
			<?php $files = ['events', 'localisation']; ?>
			@foreach($files as $file)
				<div class="grid-col-span-6">
					{{ translate('tools/general-help.import_events.'.$file) }}
				</div>
				<div class="grid-col-span-6">
					<div class="input-with-help always-show-help">
						<input type="file" name="{{ $file }}" id="import-{{ $file }}">
						<div class="help-text">
							{{ featherIcon('info') }}
							<div class="help-text-info">
							{!! translate('tools/general-help.import_events.'.$file.'_help') !!}
							</div>
						</div>
					</div>
				</div>
			@endforeach
		</div>
		<p>&nbsp;</p>
		<p><button class="button full-width importEvents">{{ translate('tools/general-help.import_now') }}</button></p>
	</div>
</div>
	<div id="{{ $events->id }}" class="events edit page-content">
		<div class="event-list">
			<event :json="item" :type="item.type" :id="item.id" v-bind:key="item.id" :listindex="key" v-for="(item, key) in eventList" @removeeventfromlist="removeeventfromlist"></event>
		</div>
		<div class="help-text-file" id="event-game-id"><?php
			$return = '';
			if(!empty($events->prefix)){
				$return .= $events->prefix.'_';
			}
			$return .= $events->filename;
			echo $return;
			?></div>
		<div class="help-text-file" id="event-list">
			<?php
			$list = [];
			if(!empty($events->events)){
				foreach($events->events as $event){
					$f = [];
					$f['id'] = $event->id;
					$f['name'] = $event->name;
					$f['gfx'] = $event->gfx;
					$f['type'] = $event->type;
					$f['data'] = $event->data;
					if(empty($f['data'])){
						$f['data'] = [];
					}
					$f['options'] = $event->options;
					if(empty($f['options'])){
						$f['options'] = [];
					}

					$list[] = $f;
				}
			}
			echo json_encode($list);
			?>
		</div>
		<div class="help-text-file" id="help-text-file" v-pre>
			{{ getLangArray('tools/events/help') }}
		</div>
		<div class="help-text-file" id="tool-help-file" v-pre>
			{{ getLangArray('tools/general-help') }}
		</div>
		<div class="help-text-file" id="gfx-list" v-pre>
			{{ getMedia('event') }}
		</div>

	</div>
@endsection