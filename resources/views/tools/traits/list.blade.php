@extends('layouts.app')
@if(Auth::check())
	@section('content')
		@if(!empty($mods) || $mods->count() > 0)
			<all-traits-lists></all-traits-lists>
		@else
			
		@endif
	@endsection
@else
	@section('content')
		<div class="page-content">
			<h3>{{ translate('tools/mod/index.not_logged_in') }}</h3>
		</div>
	@endsection
@endif