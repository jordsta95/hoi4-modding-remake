@extends('layouts.app')
@if(Auth::check())
	@section('content')
		<div class="page-content">
			@if($type == "country")
				<country-leader-traits id="{{ $mod->id }}" type="{{ $type }}"></country-leader-traits>
			@else
				<h4>Only country leader traits are available right now</h4>
			@endif
		</div>
	@endsection
@else
	@section('content')
		<div class="page-content">
			<h3>{{ translate('tools/mod/index.not_logged_in') }}</h3>
		</div>
	@endsection
@endif