@extends('layouts.app')
@if(Auth::check())
	@if(!empty($mods) || $mods->count() > 0)
		@section('sidebar')
			@include('sections.menus.focus-tree-index')
		@endsection
	@endif
	@section('content')
		<div class="page-content">
			@if(empty($mods) || $mods->count() == 0)
				<h1>{{ translate('tools/mod/index.no_mods_header') }}</h1>
				<p>{!! translate('tools/focus-tree/help.no_mods') !!}</p>
			@else
				<div class="grid">
					@foreach($mods as $mod)
						<div class="grid-col-span-6">
							<div class="mod-preview">
								<div class="mod-image" style="background-image: url('/media/find/{{ $mod->media_id }}');"></div>
								<div class="mod-details">
									<h3>{{ $mod->name }}</h3>
									@if($mod->focus_trees->count() > 0)
										<div class="grid">
											@foreach($mod->focus_trees as $tree)
												<div class="grid-col-span-6">
													<a href="/focus-tree/edit/{{ $tree->id }}">[{{ $tree->tag }}] {{ $tree->name }} ({{ $tree->focuses->count() }})</a>
												</div>
											@endforeach
										</div>
									@else
										<p>{{ translate('tools/focus-tree/help.no_focus_tree') }}</p>
									@endif
									
								</div>
							</div>
						</div>
					@endforeach
				</div>
			@endif
			<div class="help-text-file" id="modal-help-text">
				{{ getLangArray('tools/focus-tree/help') }}
			</div>
			<div class="help-text-file" id="country-list">
				{{ getCountryArrayForVue() }}
			</div>
			<div class="help-text-file" id="mod-list">
				{{ json_encode(getUniqueModList()) }}
			</div>
		</div>
	@endsection
@else
	@section('content')
		<div class="page-content">
			<h3>{{ translate('tools/mod/index.not_logged_in') }}</h3>
		</div>
	@endsection
@endif

