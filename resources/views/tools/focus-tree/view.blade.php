@extends('layouts.app')

@section('content')
	<h4 style="position: fixed; top: 0; left: 50%; background: var(--white); color: var(--blue); padding: var(--space); transform: translateX(-50%); z-index: 99;">Focus connections disabled in this view</h4>
	<div class="national-focus">
		<?php
		$json = [];
		foreach($focustree->focuses as $focus){
			$json[] = [
				'id' => $focus->id,
				'name' => $focus->name,
				'description' => $focus->description,
				'gfx' => $focus->gfx,
				'x' => $focus->x,
				'y' => $focus->y,
				'data' => $focus->data
			];
		}
		?>
		<div id="all-focuses" style="">
			@foreach($focustree->focuses as $focus)
				<div class="focus-item" data-id="{{ $focus->id }}" style="top: {{ $focus->y * 190 }}px; left:{{ $focus->x * 200 }}px;">
					<img class="focus-image" src="/media/find/{{ $focus->gfx }}" loading="lazy">
					<div class="focus-title" >
						{{ $focus->name }}
					</div>
				</div>
			@endforeach
		</div>
	</div>
	<div class="focus-zoom-controls">
		<div class="decrease">-</div>
		<div class="current-zoom">
			100
		</div>
		<div class="increase">+</div>
	</div>
	<div class="help-text-file" id="help-text-file" v-pre>
			{{ getLangArray('tools/focus-tree/help') }}
		</div>
		<div class="help-text-file" id="tool-help-file" v-pre>
			{{ getLangArray('tools/general-help') }}
		</div>
		<div class="help-text-file" id="gfx-list" v-pre>
			{{ getMedia('focus') }}
		</div>
@endsection
@section('footer-script')
<style id="focusStyle"></style>
@endsection