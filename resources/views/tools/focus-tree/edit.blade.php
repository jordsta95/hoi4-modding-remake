@extends('layouts.app')

@section('sidebar')
	@include('sections.menus.focus-tree')
@endsection
@section('content')
<div class="modal import-modal">
	<div class="modal-header">
		{{ translate('tools/general-help.import_title') }}
		<div class="close" onclick="toggleImportModal()">{{ featherIcon('x') }}</div>
	</div>
	<div class="modal-content">
		<p>{{ translate('tools/general-help.import_instruction') }}</p>
		<div class="grid">
			<?php $files = ['focus_tree', 'localisation']; ?>
			@foreach($files as $file)
				<div class="grid-col-span-6">
					{{ translate('tools/general-help.import_focus.'.$file) }}
				</div>
				<div class="grid-col-span-6">
					<div class="input-with-help always-show-help">
						<input type="file" name="{{ $file }}" id="import-{{ $file }}">
						<div class="help-text">
							{{ featherIcon('info') }}
							<div class="help-text-info">
							{!! translate('tools/general-help.import_focus.'.$file.'_help') !!}
							</div>
						</div>
					</div>
				</div>
			@endforeach
		</div>
		<p>&nbsp;</p>
		<p><button class="button full-width importFocusTree">{{ translate('tools/general-help.import_now') }}</button></p>
	</div>
</div>
	<div id="{{ $focustree->id }}" class="national-focus edit">
		<div class="focus-zoom-controls">
			<div class="decrease">-</div>
			<div class="current-zoom">
				100
			</div>
			<div class="increase">+</div>
		</div>
		<div id="all-focuses" style="">
			<div class="focus-connector-container"></div>
			<focus :json="item" :id="item.id" v-bind:key="item.id" v-for="(item, key) in focusList" @removefocusfromlist="removefocusfromlist"></focus>
			<div class="continuous-focuses" data-x="{{ $focustree->x  }}" data-y="{{ $focustree->y }}">{{ translate('tools/focus-tree/help.continuous_focuses') }}</div>
		</div>
		<div class="help-text-file focus-tree-list-data" id="focus-tree-list-0" v-pre>
		<?php 
			$list = [];
			$loopIndex = 0;
			$listNumber = 1;
			
			foreach($focustree->focuses as $focus){
				$loopIndex++;
				if($loopIndex > 80){
					echo json_encode($list);
					$list = [];
					echo '</div><div class="help-text-file focus-tree-list-data" id="focus-tree-list-'.$listNumber.'" v-pre>';
					$listNumber++;
					$loopIndex = 0;
				}
				
					$f = [];
					$f['id'] = $focus->id;
					$f['name'] = $focus->name;
					$f['description'] = $focus->description;
					$f['x'] = $focus->x;
					$f['y'] = $focus->y;
					$f['gfx'] = $focus->gfx;
					$f['data'] = json_decode($focus->data);
					if(empty($f['data'])){
						$f['data'] = [];
					}
					$list[] = $f;
				}
				
				
			echo json_encode($list);

		?>
		</div>

		{{-- TODO - Continuous focus box 
		<div class="continuous-focus" style="position: absolute; width: 100px; height: 100px; background: red; top: {{ $focustree->y * 190 }}px; left: {{ $focustree->x * 200 }}px; transform: translate(50px, 100px);"></div>
		--}}
		<div class="help-text-file" id="help-text-file" v-pre>
			{{ getLangArray('tools/focus-tree/help') }}
		</div>
		<div class="help-text-file" id="tool-help-file" v-pre>
			{{ getLangArray('tools/general-help') }}
		</div>
		<div class="help-text-file" id="gfx-list" v-pre>
			{{ getMedia('focus') }}
		</div>

	</div>
@endsection

@section('footer-script')
<style id="focusStyle"></style>
@endsection