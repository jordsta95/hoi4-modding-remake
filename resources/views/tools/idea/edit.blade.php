@extends('layouts.app')

@section('sidebar')
	@include('sections.menus.ideas')
@endsection
@section('content')
<div id="traits-lang" data-lang="{{ $ideas->language }}"></div>
<div class="modal import-modal">
	<div class="modal-header">
		{{ translate('tools/general-help.import_title') }}
		<div class="close" onclick="toggleImportModal()">{{ featherIcon('x') }}</div>
	</div>
	<div class="modal-content">
		<p>{{ translate('tools/general-help.import_instruction') }}</p>
		<div class="grid">
			<?php $files = ['ideas', 'localisation']; ?>
			@foreach($files as $file)
				<div class="grid-col-span-6">
					{{ translate('tools/general-help.import_ideas.'.$file) }}
				</div>
				<div class="grid-col-span-6">
					<div class="input-with-help always-show-help">
						<input type="file" name="{{ $file }}" id="import-{{ $file }}">
						<div class="help-text">
							{{ featherIcon('info') }}
							<div class="help-text-info">
							{!! translate('tools/general-help.import_ideas.'.$file.'_help') !!}
							</div>
						</div>
					</div>
				</div>
			@endforeach
		</div>
		<p>&nbsp;</p>
		<p><button class="button full-width importIdeas">{{ translate('tools/general-help.import_now') }}</button></p>
	</div>
</div>
	<div id="{{ $ideas->id }}" class="ideas edit page-content">
		<div class="idea-list">
			@foreach(Lang::get('tools/ideas/help.groups') as $key => $label)
				<h4 data-group="{{ $key }}">{{ $label }}</h4>
			@endforeach
			<idea :json="item" :group="item.group" :id="item.id" v-bind:key="item.id" v-for="(item, key) in ideaList" @removeideafromlist="removeideafromlist"></idea>
		</div>
		
		<div class="help-text-file" id="idea-list">
			<?php
			$list = [];
			if(!empty($ideas->ideas)){
				foreach($ideas->ideas as $idea){
					$f = [];
					$f['id'] = $idea->id;
					$f['name'] = $idea->name;
					$f['gfx'] = $idea->gfx;
					$f['group'] = $idea->group;
					$f['data'] = $idea->options;
					if(empty($f['data'])){
						$f['data'] = [];
					}
					$list[] = $f;
				}
			}
			echo json_encode($list);
			?>
		</div>
		<div class="help-text-file" id="help-text-file" v-pre>
			{{ getLangArray('tools/ideas/help') }}
		</div>
		<div class="help-text-file" id="tool-help-file" v-pre>
			{{ getLangArray('tools/general-help') }}
		</div>
		<div class="help-text-file" id="gfx-list" v-pre>
			{{ getMedia('ideas') }}
		</div>

	</div>
@endsection
