@extends('layouts.app')
@if(Auth::check())
	@if(!empty($mods) || $mods->count() > 0)
		@section('sidebar')
			@include('sections.menus.decision-index')
		@endsection
	@endif
	@section('content')
		<div class="page-content">
			@if(empty($mods) || $mods->count() == 0)
				<h1>{{ translate('tools/mod/index.no_mods_header') }}</h1>
				<p>{!! translate('tools/focus-tree/help.no_mods') !!}</p>
			@else
				<div class="grid">
					@foreach($mods as $mod)
						<div class="grid-col-span-6">
							<div class="mod-preview">
								<div class="mod-image" style="background-image: url('/media/find/{{ $mod->media_id }}');"></div>
								<div class="mod-details">
									<h3>{{ $mod->name }}</h3>
									@if($mod->decisions->count() > 0)
										<div class="grid">
											@foreach($mod->decisions as $decision)
												<div class="grid-col-span-6">
													<a href="/decision/edit/{{ $decision->id }}">{{ $decision->name }}</a>
												</div>
											@endforeach
										</div>
									@else
										<p>{{ translate('tools/decision/index.no_decision') }}</p>
									@endif
									
								</div>
							</div>
						</div>
					@endforeach
				</div>
			@endif
			<div class="help-text-file" id="modal-help-text">
				{{ getLangArray('tools/decision/index') }}
			</div>
			<div class="help-text-file" id="country-list">
				{{ getCountryArrayForVue() }}
			</div>
			<div class="help-text-file" id="mod-list">
				{{ json_encode(getUniqueModList()) }}
			</div>
		</div>
	@endsection
@else
	@section('content')
		<div class="page-content">
			<h3>{{ translate('tools/mod/index.not_logged_in') }}</h3>
		</div>
	@endsection
@endif

