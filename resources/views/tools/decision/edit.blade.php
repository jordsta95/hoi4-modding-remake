@extends('layouts.app')

@section('sidebar')
	@include('sections.menus.decision')
@endsection
@section('content')
<div class="modal import-modal">
	<div class="modal-header">
		{{ translate('tools/general-help.import_title') }}
		<div class="close" onclick="toggleImportModal()">{{ featherIcon('x') }}</div>
	</div>
	<div class="modal-content">
		<p>{{ translate('tools/general-help.import_instruction') }}</p>
		<div class="grid">
			<?php $files = ['decision', 'localisation']; ?>
			@foreach($files as $file)
				<div class="grid-col-span-6">
					{{ translate('tools/general-help.import_decision.'.$file) }}
				</div>
				<div class="grid-col-span-6">
					<div class="input-with-help always-show-help">
						<input type="file" name="{{ $file }}" id="import-{{ $file }}">
						<div class="help-text">
							{{ featherIcon('info') }}
							<div class="help-text-info">
							{!! translate('tools/general-help.import_decision.'.$file.'_help') !!}
							</div>
						</div>
					</div>
				</div>
			@endforeach
		</div>
		<p>&nbsp;</p>
		<p><button class="button full-width importDecisions">{{ translate('tools/general-help.import_now') }}</button></p>
	</div>
	<span class="import-title help-text-file">{{ translate('tools/decision/help.import_select_category') }}</span>
	<span class="import-desc help-text-file">{{ translate('tools/decision/help.import_select_category_help') }}</span>
</div>
	<div id="{{ $category->id }}" class="decision edit page-content">
		<decision-list :category="decisionCategory" :decisions="decisionList" :helpText="helpText" :toolHelp="toolHelp" @removedecisionfromlist="removedecisionfromlist"></decision-list>
		<div class="help-text-file" id="decision-list" v-pre>
			<?php
			$list = [];
			if(!empty($category->decisions)){
				foreach($category->decisions as $decision){
					$list[] = $decision;
				}
			}
			echo json_encode($list);
			?>
		</div>
		<div class="help-text-file" id="decision-category" v-pre>{{ $category }}</div>
		<div class="help-text-file" id="help-text-file" v-pre>
			{{ getLangArray('tools/decision/help') }}
		</div>
		<div class="help-text-file" id="tool-help-file" v-pre>
			{{ getLangArray('tools/general-help') }}
		</div>
		<div class="help-text-file" id="gfx-list" v-pre>
			{{ getMedia('decision') }}
		</div>

	</div>
@endsection