@extends('layouts.app')
@if(Auth::check())
	@section('sidebar')
		@include('sections.menus.mod')
	@endsection
	@section('content')
		<div class="page-content">
			@if(empty($mods) || $mods->count() == 0)
				<h1>{{ translate('tools/mod/index.no_mods_header') }}</h1>
				<p>{{ translate('tools/mod/index.no_mods') }}</p>
			@else
				<div class="grid">
					@foreach($mods as $mod)
						<div class="grid-col-span-6">
							@include('components/tools/mod/preview')
						</div>
					@endforeach
				</div>
			@endif
			<div class="help-text-file" id="mod-help-text">
				{{ getLangArray('tools/mod/index') }}
			</div>
		</div>
	@endsection
@else
	@section('content')
		<div class="page-content">
			<h3>{{ translate('tools/mod/index.not_logged_in') }}</h3>
		</div>
	@endsection
@endif

