@extends('layouts.app')
@if(Auth::check())
	@section('sidebar')
		@include('sections.menus.mod-view')
	@endsection
@endif
@section('content')
<?php 
$canEdit = false;
$isAdmin = false;
if(Auth::check()){
	foreach(Auth::user()->mods as $m){
		if($m->id == $mod->id){
			$canEdit = true;
		}
	}
	foreach($mod->roles as $role){
		if($role->administrator){
			$isAdmin = true;
		}
	}
	if(Auth::user()->role_id == 2){
		$canEdit = true;
		$isAdmin = true;
	}
}
$json = json_decode($mod->data);
?>
	<div class="page-content">
		<div class="grid align-centre">
			<div class="grid-col-span-2">
				<div class="circle-image" style="background-image: url('/media/find/{{ $mod->media_id }}');"></div>
			</div>
			<div class="grid-col-span-laptop-6 grid-col-span-3">
				<h1>{{ $mod->name }}</h1>
				<p>{{ $mod->description }}</p>
				@if($json)
				<div class="grid">
					<div class="grid-col-span-2 grid-col-span-tablet-6 mod-icon-marker">
						<span>{{ translate('tools/mod/view.ideology_overhaul') }}</span>
						<span class="icon">
							@if(isset($json->ideology_overhaul) && !empty($json->ideology_overhaul))
								{{ featherIcon('check') }}
							@else
								{{ featherIcon('x') }}
							@endif
						</span>	
					</div>
					<div class="grid-col-span-2 grid-col-span-tablet-6 mod-icon-marker">
						<span>{{ translate('tools/mod/view.start_date_overhaul') }}</span>
						<span class="icon">
							@if(isset($json->start_date_overhaul) && !empty($json->start_date_overhaul))
								{{ featherIcon('check') }}
							@else
								{{ featherIcon('x') }}
							@endif
						</span>		
					</div>
					<div class="grid-col-span-2 grid-col-span-tablet-6 mod-icon-marker">
						<span>{{ translate('tools/mod/view.country_overhaul') }}</span>
						<span class="icon">
							@if(isset($json->country_overhaul) && !empty($json->country_overhaul))
								{{ featherIcon('check') }}
							@else
								{{ featherIcon('x') }}
							@endif
						</span>		
					</div>
				</div>
				@endif
				@if($canEdit)
					<p><button class="button delete invert" id="modDelete" data-id="{{ $mod->id }}">{{ translate('tools/mod/view.delete_mod') }}</button></p>
					<div class="help-text-file" aria-hidden="true">
						<span class="mod-delete-title">{{ translate('tools/mod/view.confirm_delete_box.title') }}</span>
						<span class="mod-delete-text">{{ translate('tools/mod/view.confirm_delete_box.content') }}</span>
						<span class="mod-delete-confirm">{{ translate('tools/mod/view.confirm_delete_box.confirm') }}</span>
						<span class="mod-delete-cancel">{{ translate('tools/mod/view.confirm_delete_box.cancel') }}</span>
					</div>
				@endif
			</div>
			<div class="grid-col-span-laptop-4 grid-col-span-6">
				<h3>{{ translate('tools/mod/view.team') }}</h3>
				<div class="grid">
					@foreach($mod->users as $user)
						<div class="grid-col-span-6 flex-at-end">
							<p>
								{{ $user->pivot->role }}<br>
								<small><a href="/user/{{ $user->id }}">{{ $user->username }}</a></small>
							</p>
							@if($isAdmin && count($mod->users) > 1)
								<form method="POST" action="/mod/{{ $mod->id }}/removeFromTeam/{{ $user->id }}">
									@csrf
									<button class="ignore">{{ featherIcon('x') }}</button>
								</form>
							@endif
						</div>
					@endforeach
				</div>
			</div>
		</div>
		<section class="mod-tabs">
			<section class="mod-details">
				<div class="tabble-content">
					<div class="tab-headers">
						<a href="#focus-tree" class="tab-header active">{{ translate('tools/mod/view.tabs.focus_trees') }}</a>
						<a href="#events" class="tab-header">{{ translate('tools/mod/view.tabs.events') }}</a>
						<a href="#ideas" class="tab-header">{{ translate('tools/mod/view.tabs.ideas') }}</a>
						<a href="#countries" class="tab-header">{{ translate('tools/mod/view.tabs.countries') }}</a>
						<a href="#decisions" class="tab-header">{{ translate('tools/mod/view.tabs.decisions') }}</a>
						<a href="#bookmark" class="tab-header">{{ translate('tools/mod/view.tabs.bookmark') }}</a>
						<a href="#ideology" class="tab-header">{{ translate('tools/mod/view.tabs.ideology') }}</a>
						<a href="#characters" class="tab-header">Characters</a>
					</div>
					<div class="tab-content">
						<div class="help-text-file" aria-hidden="true">
							<span class="delete-title">{{ translate('tools/mod/view.delete') }}</span>
							<span class="delete-text">{{ translate('tools/mod/view.delete_extra') }}</span>
							<span class="delete-confirm">{{ translate('tools/mod/view.delete_confirm') }}</span>
							<span class="delete-cancel">{{ translate('tools/mod/view.delete_cancel') }}</span>
						</div>
						<div class="tab-details no-pad active" id="focus-tree">
							@include('components/tools/mod/tab-sections/focus-tree')
						</div>
						<div class="tab-details no-pad" id="events">
							@include('components/tools/mod/tab-sections/events')
						</div>
						<div class="tab-details no-pad" id="ideas">
							@include('components/tools/mod/tab-sections/ideas')
						</div>
						<div class="tab-details no-pad" id="countries">
							@include('components/tools/mod/tab-sections/country')
						</div>
						<div class="tab-details no-pad" id="decisions">
							@include('components/tools/mod/tab-sections/decision')
						</div>
						<div class="tab-details no-pad" id="bookmark">
							@include('components/tools/mod/tab-sections/bookmark')
						</div>
						<div class="tab-details no-pad" id="ideology">
							@include('components/tools/mod/tab-sections/ideology')
						</div>
						<div class="tab-details no-pad" id="characters">
							@include('components/tools/mod/tab-sections/character')
						</div>
					</div>
				</div>
			</section>
		</section>
	</div>
	<div class="help-text-file" id="tool-help-file" v-pre>
			{{ getLangArray('tools/general-help') }}
		</div>
@endsection


