@extends('layouts.app')

@section('content')
	<div class="page-content">
		<country id="{{ $country->id }}" mod-id="{{ $country->mt->id }}" map-url="/map/{{ getMapSVGURL($country->mod) }}"></country>
	</div>
@endsection