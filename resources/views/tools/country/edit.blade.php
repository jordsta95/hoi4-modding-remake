@extends('layouts.app')

@section('sidebar')
	@include('sections/menus.country')
@endsection

@section('content')
<div id="traits-lang" data-lang="{{ $country->language }}"></div>
	<div class="page-content" data-tag="{{ $country->tag }}" data-id="{{ $country->id }}" data-mod="{{ $country->mod }}">
		<div class="modal import-modal">
			<div class="modal-header">
				{{ translate('tools/general-help.import_title') }}
				<div class="close" onclick="toggleImportModal()">{{ featherIcon('x') }}</div>
			</div>
			<div class="modal-content">
				<p>{{ translate('tools/general-help.import_instruction') }}</p>
				<div>
					<div class="grid">
						<?php $files = ['history', 'localisation', 'units', 'common']; ?>
						@foreach($files as $file)
							<div class="grid-col-span-6">
								{{ translate('tools/general-help.import_country.'.$file) }}
							</div>
							<div class="grid-col-span-6">
								<div class="input-with-help always-show-help">
									<input type="file" name="{{ $file }}" id="import-{{ $file }}" <?php if(in_array($file, ['units', 'localisation'])){ echo 'multiple'; } ?>>
									<div class="help-text">
										{{ featherIcon('info') }}
										<div class="help-text-info">
										{!! translate('tools/general-help.import_country.'.$file.'_help') !!}
										</div>
									</div>
								</div>
							</div>
						@endforeach
					</div>
				</div>
				<p>&nbsp;</p>
				<p><button class="button full-width importCountry">{{ translate('tools/general-help.import_now') }}</button></p>
			</div>
		</div>
		<country-editor :data="countryData" :countrytab="countryTab" :startdate="startDate"></country-editor>
		<div id="province-map" class="help-text-file">
			{{-- getMapSVG($country->mod) !!--}}
		</div>
		<div id="state-list" class="help-text-file" v-pre>
			{{ getStateList($country->mod) }}
		</div>
		<div id="idea-lists" class="help-text-file">
			<?php 
			$ideaLists = [];
			foreach($ideas as $idea){
				$hasNS = false;
				foreach($idea->ideas as $i){
					if($i->group == 'country'){
						$hasNS = true;
						continue;
					}
				}
				if($hasNS){
					$ideaLists[$idea->id] = ['name' => $idea->name, 'prefix' => $idea->prefix];
				}
			}
			?>
			{{ json_encode($ideaLists)}}
		</div>
		<div id="country-data" class="help-text-file" v-pre>{{ $country }}</div>
		<div id="help-ideologies-text" class="help-text-file" v-pre>{{ getSubIdeologies($mod, $country->mod) }}</div>
		<div id="help-military-text" class="help-text-file" v-pre>{{ getLangArray('tools/country/military') }}</div>
		<div id="help-divisions-text" class="help-text-file" v-pre>{{ getLangArray('tools/country/divisions') }}</div>
		<div id="help-states-text" class="help-text-file" v-pre>{{ getLangArray('tools/country/states') }}</div>
		<div id="help-misc-text" class="help-text-file" v-pre>{{ getLangArray('tools/country/misc') }}</div>
		<div id="help-start-dates-text" class="help-text-file" v-pre>{{ getLangArray('tools/country/start-dates') }}</div>
		<div id="tool-help-file" class="help-text-file" v-pre>{{ getLangArray('tools/general-help') }}</div>
		<div id="help-research-text" class="help-text-file" v-pre>{{ getLangArray('tools/country/research') }}</div>
		<div class="help-text-file" id="gfx-list" v-pre>
			{{ getMedia('ideas') }}
		</div>
		<div class="help-text-file" id="leader-gfx-list" v-pre>
			{{ getMedia('country') }}
		</div>
		<div id="division-breakdown" class="help-text-file" v-pre>
			<?php $unitDelegation = getLandUnitDivisionBreakdown('land.json') ?>
			{{ json_encode($unitDelegation) }}
		</div>
		<div id="division-data" class="help-text-file" v-pre>
			{{ getHOILocalisationData('unit', $country->language, $unitDelegation->all_units) }}
		</div>
		<div id="researchable" class="help-text-file" v-pre>
			<?php $research = getResearch($country->mod) ?>
			{{ json_encode($research) }}
		</div>
		<div id="research-data" class="help-text-file" v-pre>
			{{ getHOILocalisationData('research', $country->language, $research->research) }}
		</div>
		<div id="autonomy-data" class="help-text-file" vpre>{{ getHOILocalisationData('autonomy', $country->language, ['autonomy_dominion', 'autonomy_supervised_state', 'autonomy_colony', 'autonomy_puppet', 'autonomy_integrated_puppet', 'autonomy_annex', 'autonomy_free', 'autonomy_reichskommissariat', 'autonomy_reichsprotectorate', 'autonomy_satellite', 'autonomy_wtt_imperial_associate', 'autonomy_wtt_imperial_protectorate', 'autonomy_wtt_imperial_subject']) }}</div>

		{{--<p id="fullsize-image" style="display: none;"><img style="max-width:5632px" id="imageToCheck" src="/img/provinces.bmp"></p>--}}

		<div class="load-maps"><span class="button white invert">{{ featherIcon('map') }} <span class="text">{{ translate('tools/country/help.map_load') }}</span></span></div>

		<div class="country-data-save">
			{{-- TODO: Lang --}}
			<span class="button button-blue"><span class="text">SAVE</span><span class="saving">{{ featherIcon('loader') }}</span></span>
		</div>
	</div>
@endsection

@section('footer-script')
	<script defer>
		var loadMapMapLoaded = false;
		$('body').on('click', '.load-maps', function(){
			$('.load-maps').hide();
			if(!loadMapMapLoaded){
				var savedStateData = getStateResults();
				var stateData = {}
				$.each(savedStateData, function(index, data){
					if(data.hasOwnProperty('colour')){
						stateData[data.id] = data.colour;
					}else{
						stateData[data.id] = '#FFFFFF';
					}
				});

				$('#pageloader').removeClass('loaded');
				$.get('/map/{{ getMapSVGURL($country->mod) }}', function(data){
					var $map = $('#province-map');
					$map.html(data);
					setTimeout(function(){
						$map.html($('#province-map').html());
						//$('body').append('<div class="loader" id="progress"><div class="inner"><progress id="loaderProgress" max="'+(JSON.parse($('#state-list').text()).states.length)+'" value="0"></progress></div></div>');
						//var progress = 0;
						$.each(JSON.parse($('#state-list').text()).states, function(stateID, provinces){
							$.each(provinces, function(key, id){
								//if($map.find('[data-province-id="'+id+'"]').attr('data-state-id') != stateID && $map.find('[data-province-id="'+id+'"]').attr('data-state-id') != undefined){
									//console.log('data-province-id="'+id+'" - data-state-id="'+stateID+'" - '+$map.find('[data-province-id="'+id+'"]').attr('data-state-id'));
								//}
								$map.find('[data-province-id="'+id+'"]').attr('data-state-id', stateID);
								if(stateData.hasOwnProperty(stateID)){
									$map.find('[data-province-id="'+id+'"]').attr('fill', stateData[stateID]);
									$map.find('[data-province-id="'+id+'"]').css('fill', stateData[stateID]);
								}
							});
							// progress++;
							// $('#loaderProgress').val(progress);
						});
						$map.find('[data-state-id]').each(function(){
							var id = $(this).attr('data-state-id');
							if(!$map.find('#state-'+id).length){
								$map.find('svg').append('<g id="state-'+id+'"></g>');
							}
							$map.find('#state-'+id).append($(this));
						});
						$map.html($map.html());
						$('#pageloader').addClass('loaded');
						loadMapMapLoaded = true;
						$('.requires-maps').removeClass('requires-maps');

					}, 200);
				});
			}
		});
	</script>
	<style>
		.requires-maps{ display: none; }
		.load-maps{position: fixed; left: 1rem; bottom: 3rem; z-index: 99999;}
		.load-maps .text{display: none;}
		.load-maps:hover .text{display: inline;}
		.load-maps .button{display: flex; align-items: center;}
	</style>
@endsection