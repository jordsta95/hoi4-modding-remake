@extends('layouts.app')
@if(Auth::check())
	@if(!empty($mods) || $mods->count() > 0)
		@section('sidebar')
			<span class="sidebar-menu-item" @click="setGlobalModal('create-country-modal')">{{ featherIcon('plus') }}Create Country</span>
		@endsection
	@endif
	@section('content')
		@if(!empty($mods) || $mods->count() > 0)
			<all-countries></all-countries>
		@else
			
		@endif
	@endsection
@else
	@section('content')
		<div class="page-content">
			<h3>{{ translate('tools/mod/index.not_logged_in') }}</h3>
		</div>
	@endsection
@endif

