@extends('layouts.app')

@section('sidebar')
	@include('sections.menus.ideology')
@endsection
@section('content')
<div class="modal import-modal">
	<div class="modal-header">
		{{ translate('tools/general-help.import_title') }}
		<div class="close" onclick="toggleImportModal()">{{ featherIcon('x') }}</div>
	</div>
	<div class="modal-content">
		<p>{{ translate('tools/general-help.import_instruction') }}</p>
		<div class="grid">
			<?php $files = ['ideology', 'localisation', 'factions']; ?>
			@foreach($files as $file)
				<div class="grid-col-span-6">
					{{ translate('tools/ideologies/help.import_'.$file) }}
				</div>
				<div class="grid-col-span-6">
					<div class="input-with-help always-show-help">
						<input type="file" name="{{ $file }}" id="import-{{ $file }}">
						<div class="help-text">
							{{ featherIcon('info') }}
							<div class="help-text-info">
							{!! translate('tools/ideologies/help.import_'.$file.'_help') !!}
							</div>
						</div>
					</div>
				</div>
			@endforeach
			<span class="import-title help-text-file">{{ translate('tools/ideologies/help.import_select_ideology') }}</span>
			<span class="import-desc help-text-file">{{ translate('tools/ideologies/help.import_select_ideology_help') }}</span>
		</div>
		<p>&nbsp;</p>
		<p><button class="button full-width importIdeology">{{ translate('tools/general-help.import_now') }}</button></p>
	</div>
</div>
	<div id="{{ $ideology->id }}" class="ideology edit page-content">
		<ideology-form :helptext="helpText" :toolhelp="toolHelp"  :ideology="ideology"></ideology-form>
		<div class="help-text-file" id="ideology-data" v-pre>
			{{ $ideology }}
		</div>
		<div class="help-text-file" id="help-text-file" v-pre>
			{{ getLangArray('tools/ideologies/help') }}
		</div>
		<div class="help-text-file" id="tool-help-file" v-pre>
			{{ getLangArray('tools/general-help') }}
		</div>
		<div class="help-text-file" id="gfx-list" v-pre>
			{{ getMedia('ideology') }}
		</div>
	</div>
@endsection
