@extends('layouts.app')

@section('content')
	<div class="homepage grid" style="height: 80%; align-items: center;">
		<div class="grid-col-start-1 grid-col-start-tablet-4 grid-col-span-tablet-6 grid-col-span-12 text-centre">
			<h1>{{ translate('pages/home.coming_soon.title') }}</h1>
			<p>{{ translate('pages/home.coming_soon.text') }}</p>
		</div>
	</div>
@endsection
