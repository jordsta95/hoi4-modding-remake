@extends('layouts.app')

@section('content')
	<div class="homepage grid">
		<div class="title grid-col-start-1 grid-col-span-tablet-6 grid-col-span-12 grid-row-start-tablet-1">
			<h1>{{ translate('pages/home.title') }}</h1>
			<h6>{{ translate('pages/home.version.'.env('APP_ENV')) }}</h6>
		</div>
		<div class="about grid-col-start-1 grid-row-start-tablet-2 grid-col-span-tablet-6 grid-col-span-12">
			<h2>{{ translate('pages/home.about.title') }}</h2>
			<p>{{ translate('pages/home.about.content_intro') }}</p>
			<p>{{ translate('pages/home.about.content_description') }}</p>
			<p><a href="https://discord.gg/qaWe7GNJPV" target="_blank" style="display:flex;"><span style="margin-right: 10px;align-self: center;font-size: 30px;transform: translateY(-5px);">{{ featherIcon('discord') }}</span>Click here to access the Community Discord server</a></p>
		</div>
		<div class="grid-col-span-tablet-6 grid-col-span-12 grid-col-start-tablet-7 grid-col-start-1 grid-row-start-tablet-1 grid-row-span-tablet-2">
			<h2>{{ translate('pages/home.updates.title') }}</h2>
			<h6>{{ translate('pages/home.updates.subtitle') }}</h6>
			<div class="home-forum-updates">
				@foreach(getSiteUpdates() as $post)
					<div class="home-update">
						<h4><a href="/forum/updates/{{ $post->id }}-{{ $post->slug }}">{{ $post->title }}</a></h4>
						<p>
							<?php 
							$ex = explode(' ', $post->initialComment->content);
							$words = 0;
							while($words < 20){
								if(isset($ex[$words])){
									echo $ex[$words].' ';
								}else{
									$words = 100;
								}
								$words++;
							}
							if($words < 80){
								echo '...';
							}
							?>
						</p>
					</div>
				@endforeach
			</div>
		</div>
		<div class="how grid-col-start-1 grid-col-span-tablet-7 grid-col-span-12">
			<h2>{{ translate('pages/home.how.title') }}</h2>
			<p>{{ translate('pages/home.how.description') }}</p>
		</div>
		<div class="patreons grid-col-start-tablet-8 grid-col-span-tablet-5 grid-col-start-1 grid-col-span-12">
			<h3>{{ translate('pages/home.patreon.title') }}</h3>
			<p>{{ translate('pages/home.patreon.description') }}</p>
			{{--
			<div class="patrons">
				<ul class="patron-list">
					@foreach(getPatrons() as $id => $data)
						<li><a href="/user/{{ $id }}"><span class="image-wrapper"><img src="/media/find/{{ $data->avatar }}/focus"></span>{{ $data->username }}</a></li>
					@endforeach
				</ul>
			</div>
			<p><a href="https://www.patreon.com/hoi4moddingtools" target="_blank" class="button">{{ translate('pages/home.patreon.help_out') }}</a></p>
			--}}
		</div>
	</div>
@endsection
