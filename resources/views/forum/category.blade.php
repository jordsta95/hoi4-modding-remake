@extends('layouts.app')

@if($forum->id != 3)
	@section('sidebar')
		@include('forum.menu')
	@endsection
@endif

@section('content')
	<div class="page-content forum-posts-page" data-forum="{{ $forum->id }}">
		<p class="forum-header-link"><a href="/forum/">{{ featherIcon('arrow-left') }} {{ translate('tools/forum/forums.forum') }}</a></p>
		
		@if($forum->slug != 'international')
			<h6>{{ translate('forum.language_message') }}</h6>
		@endif
		

		<h3>{{ translate('tools/forum/forums.'.$forum->localisation_key.'.title') }}</h3>
		<p>{{ translate('tools/forum/forums.'.$forum->localisation_key.'.description') }}</p>
		<?php $posts = $forum->posts()->orderBy('updated_at', 'DESC')->paginate(10); ?>
		<div>
			@foreach($posts as $post)
				<?php $link = '/forum/'.$forum->slug .'/'. $post->id .'-'.$post->slug; ?>
				<section class="forum-post-preview">
					<div class="post-details grid-col-span-2 grid-col-start-tablet-2 grid-col-span-tablet-1 grid-row-start-1">
						<h3><a href="{{ $link }}">{{ $post->title }}</a></h3>
						<blockquote>
							{!! ltrim(explode('</p>', explode('<hr>',(isset($post->initialComment) ? $post->initialComment->content : ''))[0])[0], '<p>') !!}...
						</blockquote>
						<p><a href="{{ $link }}" class="button">{{ translate('tools/forum/forums.read_more') }}</a></p>
					</div>
					<div class="poster text-centre created-by grid-col-start-1 grid-row-start-2 grid-row-start-tablet-1">
						<span class="poster-title">{{ translate('tools/forum/forums.started_by') }}</span>
						<div class="user-avatar" style="background-image: url('/media/find/{{ $post->user->media_id }}');"></div>
						<p>{{ $post->user->username }}</p>
						<p><small><datetime datetime="{{ date('d/M/Y H:i', strtotime($post->created_at)) }}">{{ date('d/M/Y H:i', strtotime($post->created_at)) }}</datetime></small></p>
					</div>
					<div class="poster text-centre updated-by grid-col-start-2 grid-col-start-tablet-3 grid-row-start-2 grid-row-start-tablet-1">
						<?php $lastPost = $post->latestComment(); if(isset($lastPost)): $lastUser = $lastPost->user; ?>
						<span class="poster-title">{{ translate('tools/forum/forums.last_reply') }}</span>
						<div class="user-avatar" style="background-image: url('/media/find/{{ $lastUser->media_id }}');"></div>
						<p>{{ $lastUser->username }}</p>
						<p><small><datetime datetime="{{ date('d/M/Y H:i', strtotime($lastPost->created_at)) }}">{{ date('d/M/Y H:i', strtotime($lastPost->created_at)) }}</datetime></small></p>
						<?php endif; ?>
					</div>
				</section>
			@endforeach
		</div>
		{{ $posts->links() }}
	</div>
@endsection
