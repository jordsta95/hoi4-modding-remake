@extends('layouts.app')

@section('sidebar')
	@include('forum.menu')
@endsection

@section('content')
	<div class="page-content">
		<p><a href="https://discord.gg/qaWe7GNJPV" target="_blank" style="display:flex;"><span style="margin-right: 10px;align-self: center;font-size: 30px;transform: translateY(-5px);">{{ featherIcon('discord') }}</span>Click here to access the Community Discord server</a></p>
		
		<h6>{{ translate('forum.language_message') }}</h6>
		@foreach($categories as $cat)
			<div class="forum-category">
				<div class="forum-title">
					<h3>{{ translate('tools/forum/categories.'.$cat->localisation_key.'.title') }}</h3>
					<p>{{ translate('tools/forum/categories.'.$cat->localisation_key.'.description') }}</p>
				</div>
				<div class="forum-list">
					@foreach($cat->forums as $forum)
						<div class="forum-item grid">
							<div class="grid-col-span-laptop-3 grid-col-span-12">
								<a href="/forum/{{ $forum->slug }}">{{ translate('tools/forum/forums.'.$forum->localisation_key.'.title') }}</a>
							</div>
							<div class="grid-col-span-laptop-2 grid-col-span-12">
								<?php $total = count($forum->posts); ?>
								{{ translate('tools/forum/forums.post_count', ['count' => $total]) }}
							</div>
							<div class="grid-col-span-laptop-7 grid-col-span-12 text-left">
								<div>
									@if($total > 0)
										<?php $latest = $forum->latestPost(); ?>
										{!! translate('tools/forum/forums.latest_post', ['post' => '<a href="/forum/'.$forum->slug.'/'.$latest->id.'-'.$latest->slug.'">'.$latest->title.'</a>']) !!}
										<br>
										<small>{{ translate('forum.updated_at', ['time' => date('d/M/Y H:i', strtotime($latest->updated_at))])}} - {{ $latest->latestComment()->user->username }}</small>
									@else
										{{ translate('tools/forum/forums.no_posts') }}
									@endif
								</div>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		@endforeach
		<p>&nbsp;</p>
		<p><a href="https://discord.gg/qaWe7GNJPV" target="_blank" style="display:flex;"><span style="margin-right: 10px;align-self: center;font-size: 30px;transform: translateY(-5px);">{{ featherIcon('discord') }}</span>Click here to access the Community Discord server</a></p>
	</div>

@endsection
