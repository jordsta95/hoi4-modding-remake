@extends('layouts.app')

@section('sidebar')
	@include('forum.menu')
@endsection

@section('content')
	<div class="page-content">
		<p class="forum-header-link"><a href="/forum/{{ $post->forum->slug }}">{{ featherIcon('arrow-left') }} {{ translate('tools/forum/forums.'.$post->forum->localisation_key.'.title') }}</a></p>
		<section class="forum-post">
			<header>
				<h5>{{ $post->title }}</h5>
				<span class="text-right">
					<time datetime="{{ $post->created_at }}">{{ date('d/M/Y', strtotime($post->created_at)) }}</time>
				</span>
			</header>
			<?php $comments = $post->comments()->orderBy('id', 'ASC')->paginate(10); ?>
			@foreach($comments as $comment)
				<section class="forum-comment" id="{{ $comment->id }}">
					<div class="profile">
						<?php $user = $comment->user; ?>
						<img src="/media/find/{{ $user->media_id }}">
						<h4><a href="/user/{{ $user->id }}">{{ $user->username }}</a></h4>
						@if($user->role_id == 3)
							<p style="font-size:1rem;color:var(--green);">-= Helpful user =-</p>
						@endif
					</div>
					<div class="comment">
						{!! $comment->content !!}
					</div>
					<span></span>
					<div class="forum-post-footer">
						<div class="post-controls">
							{{-- Edit post button goes here --}}
							@if(Auth::check())
								@if($comment->user_id == Auth::user()->id || Auth::user()->role_id == 2)
									<a class="forum-control edit-post" href="#" data-id="{{ $comment->id }}">{{ translate('forum.controls.edit') }}</a>
								@endif
								<span class="forum-control quote-post">{{ translate('forum.controls.quote') }}</span>
							@endif
						</div>
						<time datetime="{{ date('d/M/Y H:i', strtotime($comment->created_at)) }}" class="text-right date">
							{{ date('d/M/Y H:i', strtotime($comment->created_at)) }}
							@if($comment->created_at != $comment->updated_at)
							<br>
							{{ translate('forum.updated_at', ['time' => date('d/M/Y H:i', strtotime($comment->updated_at))]) }}
							@endif
						</time>
					</div>
				</section>
			@endforeach
		</section>
		{{ $comments->links() }}
		@if(Auth::check() && $post->open == 1)
			<section class="add-reply">
				<form action="" method="POST">
					@csrf
					<h3>{{ translate('forum.reply') }}</h3>
					<textarea name="text" class="wysiwyg"></textarea>
					<p class="text-right"><button class="button">{{ translate('forum.submit_reply') }}</button></p>
				</form>
			</section>
		@endif
	</div>
@endsection
