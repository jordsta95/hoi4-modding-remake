@extends('layouts.app')


@section('content')
	<div class="page-content forum-posts-page" data-forum="search">
		<p class="forum-header-link"><a href="/forum/">{{ featherIcon('arrow-left') }} {{ translate('tools/forum/forums.forum') }}</a></p>
		
		

		<form action="/forum">
			<div class="grid">
				<div class="grid-col-span-2 grid-col-span-tablet-10">
					<div class="input-with-help">
						<div class="input-wrapper">
							<input class="default-input" type="text" name="search" required="required" value="{{ isset($_GET['search']) ? $_GET['search'] : '' }}" placeholder="...">
							<label class="input-label" :for="name" style="background: var(--pageBackground)">...</label>
						</div>
					</div>
				</div>
				<div class="grid-col-span-2">
					<button class="button button-blue" style="width: 100%;">{{ featherIcon('search') }}</button>
				</div>
			</div>
		</form>
		<div>
			@foreach($posts as $post)
				<?php $link = '/forum/'.$post->forum->slug .'/'. $post->id .'-'.$post->slug; ?>
				<section class="forum-post-preview">
					<div class="post-details grid-col-span-2 grid-col-start-tablet-2 grid-col-span-tablet-1 grid-row-start-1">
						<h3><a href="{{ $link }}">{{ $post->title }}</a></h3>
						<blockquote>
							{!! ltrim(explode('</p>', explode('<hr>',(isset($post->initialComment) ? $post->initialComment->content : ''))[0])[0], '<p>') !!}...
						</blockquote>
						<p><a href="{{ $link }}" class="button">{{ translate('tools/forum/forums.read_more') }}</a></p>
					</div>
					<div class="poster text-centre created-by grid-col-start-1 grid-row-start-2 grid-row-start-tablet-1">
						<span class="poster-title">{{ translate('tools/forum/forums.started_by') }}</span>
						<div class="user-avatar" style="background-image: url('/media/find/{{ $post->user->media_id }}');"></div>
						<p>{{ $post->user->username }}</p>
						<p><small><datetime datetime="{{ date('d/M/Y H:i', strtotime($post->created_at)) }}">{{ date('d/M/Y H:i', strtotime($post->created_at)) }}</datetime></small></p>
					</div>
					<div class="poster text-centre updated-by grid-col-start-2 grid-col-start-tablet-3 grid-row-start-2 grid-row-start-tablet-1">
						<?php $lastPost = $post->latestComment(); if(isset($lastPost)): $lastUser = $lastPost->user; ?>
						<span class="poster-title">{{ translate('tools/forum/forums.last_reply') }}</span>
						<div class="user-avatar" style="background-image: url('/media/find/{{ $lastUser->media_id }}');"></div>
						<p>{{ $lastUser->username }}</p>
						<p><small><datetime datetime="{{ date('d/M/Y H:i', strtotime($lastPost->created_at)) }}">{{ date('d/M/Y H:i', strtotime($lastPost->created_at)) }}</datetime></small></p>
						<?php endif; ?>
					</div>
				</section>
			@endforeach
		</div>
		{{ $posts->links() }}
	</div>
@endsection
