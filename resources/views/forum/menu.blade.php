<menu-item onclick="showForumPostCreator()" icon="plus" title="{{ translate('tools/forum/forums.menu.add_post')  }}"></menu-item>

<div>
	<a href="/forum?search=" class="sidebar-menu-item">{{ featherIcon('search') }} Search</a>
</div>

@include('components/tools/forum.new-post')