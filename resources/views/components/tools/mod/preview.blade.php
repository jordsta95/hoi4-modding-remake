<div class="mod-preview">
	<a href="/mod/{{ $mod->id }}" class="mod-link"></a>
	<div class="mod-image" style="background-image: url('/media/find/{{ $mod->media_id }}');"></div>
	<div class="mod-details">
		<h3>{{ $mod->name }}</h3>
		<div class="grid grid-tight">
			<div class="grid-col-span-6">
				{{ translate('tools/mod/index.preview.focus_trees', ['num' => count($mod->focus_trees)]) }}
			</div>
			<div class="grid-col-span-6">
				{{ translate('tools/mod/index.preview.events', ['num' => count($mod->events)]) }}
			</div>
			<div class="grid-col-span-6">
				{{ translate('tools/mod/index.preview.ideas', ['num' => count($mod->ideas)]) }}
			</div>
			<div class="grid-col-span-6">
				{{ translate('tools/mod/index.preview.countries', ['num' => count($mod->countries)]) }}
			</div>
			<div class="grid-col-span-6">
				{{ translate('tools/mod/index.preview.decisions', ['num' => count($mod->decisions)]) }}
			</div>
			<div class="grid-col-span-6">
				{{ translate('tools/mod/index.preview.bookmarks', ['num' => count($mod->startDates)]) }}
			</div>
			<div class="grid-col-span-6">
				{{ translate('tools/mod/index.preview.ideologies', ['num' => count($mod->ideologies)]) }}
			</div>
			<div class="grid-col-span-6">
				{{ count($mod->characters) }} Character Lists
			</div>
		</div>
	</div>
</div>