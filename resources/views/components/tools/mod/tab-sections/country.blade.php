@foreach($mod->countries as $country)
	<div class="mod-focus-list-item">
		@if($canEdit)
			<a href="/country/edit/{{ $country->id }}">[<span class="tag">{{ $country->tag }}</span>] <span class="name">{{ $country->name }}</span></a>
			<div class="controls">
				<span class="edit focus-tree-edit" data-id="country-{{ $country->id }}">{{ featherIcon('settings') }}</span>
				<span class="edit country-edit-ideologies" data-id="{{ $country->id }}">{{ featherIcon('pie-chart') }}</span>
				<span class="edit country-edit-start-dates" data-id="{{ $country->id }}">{{ featherIcon('calendar') }}</span>
				<span class="delete country-delete" data-id="{{ $country->id }}">{{ featherIcon('trash-2') }}</span>
			</div>
			<div class="help-box" id="help-tree-country-{{ $country->id }}">
				<div class="help-box-header">
					<h5>Edit</h5>
					<div class="close" data-help="tree-country-{{ $country->id }}"><svg class="feather-icon icon-x"><use xlink:href="#x"></use></svg></div>
				</div>
				<div class="help-box-content">
					<form method="POST" action="/country/update-name/{{ $country->id }}">
						@csrf
						<div class="grid" style="font-size: 1rem;">
							<div class="grid-col-span-12">
								<input-help label="{{ translate('tools/mod/view.name') }}" value="{{ $country->name }}" helptext="" name="name"></input-help>
							</div>
						</div>
						<p>&nbsp;</p>
						<p><button class="button">{{ translate('tools/mod/view.update_focus_tree') }}</button></p>
					</form>
				</div>
			</div>
			{{--
			Don't think this is needed
			<div class="help-box" id="help-tree-{{ $tree->id }}">
				<div class="help-box-header">
					<h5>{{ translate('tools/mod/view.edit_focus_tree', ['tag' => $tree->tag]) }}</h5>
					<div class="close" data-help="tree-{{ $tree->id }}"><svg class="feather-icon icon-x"><use xlink:href="#x"></use></svg></div>
				</div>
				<div class="help-box-content">
					<form method="POST" action="/focus-tree/update/{{ $tree->id }}">
						@csrf
						<div class="grid" style="font-size: 1rem;">
							<div class="grid-col-span-6">
								<input-help label="{{ translate('tools/mod/view.tag') }}" value="{{ $tree->tag }}" helptext="" name="tag"></input-help>
							</div>
							<div class="grid-col-span-6">
								<input-help label="{{ translate('tools/mod/view.name') }}" value="{{ $tree->name }}" helptext="" name="name"></input-help>
							</div>
							<div class="grid-col-span-6">
								<input-help label="{{ translate('tools/mod/view.tree_id') }}" value="{{ $tree->tree_id }}" helptext="" name="tree_id"></input-help>
							</div>
							<div class="grid-col-span-6 emoji-select">
								<select-help label="{{ translate('tools/mod/view.language') }}" helptext="" name="lang" values="{{ json_encode(getGameLanuageOptions()) }}" value="{{ $tree->lang }}"></select-help>
							</div>
						</div>
						<p>&nbsp;</p>
						<p><button class="button">{{ translate('tools/mod/view.update_focus_tree') }}</button></p>
					</form>
				</div>
			</div>--}}
		@else
			<a href="/country/view/{{ $country->id }}">[<span class="tag">{{ $country->tag }}</span>] <span class="name">{{ $country->name }}</span></a>
		@endif
	</div>
@endforeach

@if($canEdit)
<div class="modal wide import-modal" id="country-ideology-edit" data-mod-id="{{ $mod->id }}">
	<div class="modal-header">
		<h5>{{ translate('tools/mod/view.country_edit.edit_ideology') }}</h5>
		<div class="close"><svg class="feather-icon icon-x"><use xlink:href="#x"></use></svg></div>
	</div>
	<div class="modal-content">
		<form method="POST" action="" data-src="/country/update-ideologies/" id="country-ideology-form">
			@csrf
			<div class="country-popup-edit title">
				<div class="name">{{ translate('tools/mod/view.country_edit.name') }}</div>
				<div class="add">{{ translate('tools/mod/view.country_edit.add') }}</div>
				<div class="remove">{{ translate('tools/mod/view.country_edit.remove') }}</div>
			</div>
			<div class="ideology-list"></div>
			<p>&nbsp;</p>
			<div class="text-centre">
				<button class="button">{{ translate('tools/mod/view.country_edit.submit') }}</button>
			</div>
		</form>
	</div>
</div>
<span></span>
<div class="modal wide import-modal" id="country-start-date-edit" data-mod-id="{{ $mod->id }}">
	<div class="modal-header">
		<h5>{{ translate('tools/mod/view.country_edit.edit_start_date') }}</h5>
		<div class="close"><svg class="feather-icon icon-x"><use xlink:href="#x"></use></svg></div>
	</div>
	<div class="modal-content">
		<form method="POST" action="" data-src="/country/update-start-dates/" id="country-start-date-form">
			@csrf
			<div class="country-popup-edit title">
				<div class="name">{{ translate('tools/mod/view.country_edit.date') }}</div>
				<div class="add">{{ translate('tools/mod/view.country_edit.add') }}</div>
				<div class="remove">{{ translate('tools/mod/view.country_edit.remove') }}</div>
			</div>
			<div class="ideology-list"></div>
			<p>&nbsp;</p>
			<div class="text-centre">
				<button class="button">{{ translate('tools/mod/view.country_edit.submit') }}</button>
			</div>
		</form>
	</div>
</div>
@endif