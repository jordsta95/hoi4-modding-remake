@foreach($mod->focus_trees as $tree)
	<div class="mod-focus-list-item">
		@if($canEdit)
			<a href="/focus-tree/edit/{{ $tree->id }}">[<span class="tag">{{ $tree->tag }}</span>] <span class="name">{{ $tree->name }} ({{ $tree->focuses->count() }})</span></a>
			<div class="controls">
				<span class="edit focus-tree-edit" data-id="{{ $tree->id }}" data-focus-id="{{ $tree->tree_id }}" data-lang="{{ $tree->lang }}">{{ featherIcon('settings') }}</span>
				<span class="delete focus-tree-delete" data-id="{{ $tree->id }}">{{ featherIcon('trash-2') }}</span>
			</div>
			<div class="help-box" id="help-tree-{{ $tree->id }}">
				<div class="help-box-header">
					<h5>{{ translate('tools/mod/view.edit_focus_tree', ['tag' => $tree->tag]) }}</h5>
					<div class="close" data-help="tree-{{ $tree->id }}"><svg class="feather-icon icon-x"><use xlink:href="#x"></use></svg></div>
				</div>
				<div class="help-box-content">
					<form method="POST" action="/focus-tree/update/{{ $tree->id }}">
						@csrf
						<div class="grid" style="font-size: 1rem;">
							<div class="grid-col-span-6">
								<input-help label="{{ translate('tools/mod/view.tag') }}" value="{{ $tree->tag }}" helptext="" name="tag"></input-help>
							</div>
							<div class="grid-col-span-6">
								<input-help label="{{ translate('tools/mod/view.name') }}" value="{{ $tree->name }}" helptext="" name="name"></input-help>
							</div>
							<div class="grid-col-span-6">
								<input-help label="{{ translate('tools/mod/view.tree_id') }}" value="{{ $tree->tree_id }}" helptext="" name="tree_id"></input-help>
							</div>
							<div class="grid-col-span-6 emoji-select">
								<select-help label="{{ translate('tools/mod/view.language') }}" helptext="" name="lang" values="{{ json_encode(getGameLanuageOptions()) }}" value="{{ $tree->lang }}"></select-help>
							</div>
						</div>
						<p>&nbsp;</p>
						<p><button class="button">{{ translate('tools/mod/view.update_focus_tree') }}</button></p>
					</form>
				</div>
			</div>
		@else
			<a href="/focus-tree/view/{{ $tree->id }}">[<span class="tag">{{ $tree->tag }}</span>] <span class="name">{{ $tree->name }}</span></a>
		@endif
	</div>
@endforeach