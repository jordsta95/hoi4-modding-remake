@foreach($mod->characters as $group)
	<div class="mod-focus-list-item">
		@if($canEdit)
			<a href="/character/list/{{ $group->id }}"><span class="name">{{ $group->name }}</span></a>
			<div class="controls">
				<span class="delete character-delete" data-id="{{ $group->id }}">{{ featherIcon('trash-2') }}</span>
			</div>
		@else
			<a href="/character/list/{{ $group->id }}"><span class="name">{{ $group->name }}</span></a>
		@endif
	</div>
@endforeach