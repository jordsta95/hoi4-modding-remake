@foreach($mod->ideologies as $group)
	<div class="mod-focus-list-item">
		@if($canEdit)
			<a href="/ideology/edit/{{ $group->id }}"><span class="name">{{ $group->name }}</span></a>
			<div class="controls">
				<span class="delete ideology-delete" data-id="{{ $group->id }}">{{ featherIcon('trash-2') }}</span>
			</div>
		@else
			<a href="/ideology/view/{{ $group->id }}"><span class="name">{{ $group->name }}</span></a>
		@endif
	</div>
@endforeach