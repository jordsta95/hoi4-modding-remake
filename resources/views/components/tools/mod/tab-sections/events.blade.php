@foreach($mod->events as $group)
	<div class="mod-focus-list-item">
		@if($canEdit)
			<a href="/events/edit/{{ $group->id }}"> @if($group->prefix) [<span class="tag">{{ $group->prefix }}</span>] @endif <span class="name">{{ $group->name }}</span></a>
			<div class="controls">
				<span class="edit event-group-edit" data-id="{{ $group->id }}" data-lang="{{ $group->lang }}">{{ featherIcon('settings') }}</span>
				<span class="delete event-group-delete" data-id="{{ $group->id }}">{{ featherIcon('trash-2') }}</span>
			</div>
			<div class="help-box" id="help-event-{{ $group->id }}">
				<div class="help-box-header">
					<h5>{{ translate('tools/mod/view.edit_event_group', ['name' => $group->name]) }}</h5>
					<div class="close" data-help="event-{{ $group->id }}"><svg class="feather-icon icon-x"><use xlink:href="#x"></use></svg></div>
				</div>
				<div class="help-box-content">
					<form method="POST" action="/events/update-group/{{ $group->id }}">
						@csrf
						<div class="grid" style="font-size: 1rem;">
							<div class="grid-col-span-6">
								<input-help label="{{ translate('tools/mod/view.prefix') }}" value="{{ $group->prefix }}" helptext="" name="prefix"></input-help>
							</div>
							<div class="grid-col-span-6">
								<input-help label="{{ translate('tools/mod/view.name') }}" value="{{ $group->name }}" helptext="" name="name"></input-help>
							</div>
							<div class="grid-col-span-6 emoji-select">
								<select-help label="{{ translate('tools/mod/view.language') }}" helptext="" name="lang" values="{{ json_encode(getGameLanguageOptions()) }}" value="{{ $group->language }}"></select-help>
							</div>
							<div class="grid-col-span-6">
								<p><button class="button">{{ translate('tools/mod/view.update_focus_tree') }}</button></p>
							</div>
						</div>
					</form>
				</div>
			</div>
		@else
			<a href="/events/view/{{ $group->id }}"> @if($group->prefix) [<span class="tag">{{ $group->prefix }}</span>] @endif <span class="name">{{ $group->name }}</span></a>
		@endif
	</div>
@endforeach