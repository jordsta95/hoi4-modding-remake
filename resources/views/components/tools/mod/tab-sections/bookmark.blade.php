@foreach($mod->startDates as $group)
	<div class="mod-focus-list-item">
		@if($canEdit)
			<a href="/start-date/edit/{{ $group->id }}"> @if($group->date) [<span class="tag">{{ $group->date }}</span>] @endif <span class="name">{{ $group->name }}</span></a>
			<div class="controls">
				<span class="delete start-date-delete" data-id="{{ $group->id }}">{{ featherIcon('trash-2') }}</span>
			</div>
		@else
			<a href="/start-date/view/{{ $group->id }}"> @if($group->date) [<span class="tag">{{ $group->date }}</span>] @endif <span class="name">{{ $group->name }}</span></a>
		@endif
	</div>
@endforeach