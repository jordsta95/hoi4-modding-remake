@foreach($mod->decisions as $decision)
	<div class="mod-focus-list-item">
		@if($canEdit)
			<a href="/decision/edit/{{ $decision->id }}"><span class="name">{{ $decision->name }}</span></a>
			<div class="controls">
				<span class="delete decision-delete" data-id="{{ $decision->id }}">{{ featherIcon('trash-2') }}</span>
			</div>
		@else
			<a href="/decision/view/{{ $decision->id }}"><span class="name">{{ $decision->name }}</span></a>
		@endif
	</div>
@endforeach