<div class="create-forum-post">
	<div class="modal-header">
		<h5>{{ translate('tools/forum/forums.create_post.header') }}</h5>
		<div class="close">{{ featherIcon('x') }}</div>
	</div>
	<div class="create-forum-post-inner">
		<form action="/forum/new" method="POST" entype="multipart/form-data">
			@csrf
			<div class="grid">
				<div class="grid-col-span-12 grid-col-span-laptop-3">
					<select-help name="forum" value="" values="{{ getForumList() }}" label="{{ translate('tools/forum/forums.create_post.forum') }}">
				</div>
				<div class="grid-col-span-12 grid-col-span-laptop-9">
					<input-help name="title" label="{{ translate('tools/forum/forums.create_post.title') }}" required>
				</div>
				<div class="grid-col-span-12 has-wysiwyg">
					<textarea-help name="text" label="{{ translate('tools/forum/forums.create_post.text') }}" required>
				</div>
				<div class="grid-col-span-12 grid-col-span-laptop-6">
					{{-- <div class="custom-image-upload">
						<div class="icon">{{ featherIcon('image') }}</div>
						<label>{{ translate('tools/forum/forums.create_post.files') }}<span class="number" id="forumFileCount"></span></label>
						<input type="file" name="files[]" multiple accept="image/*,.txt,.yml" id="forumFiles">
					</div> --}}
				</div>
				<div class="grid-col-span-12 grid-col-span-laptop-6 text-right">
					<button class="button">{{ translate('tools/forum/forums.create_post.submit') }}</button>
				</div>
			</div>
		</form>
	</div>
</div>