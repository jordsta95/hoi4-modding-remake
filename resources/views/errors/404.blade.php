@extends('layouts.app')

@section('content')
	<div class="error-wrapper">
		<span class="error-number">404</span>
		<div class="error-wrapper-inner">
			<h1 class="error-title">{{ translate('pages/errors.404.title') }}</h1>
			<p class="error-content">{{ translate('pages/errors.404.content') }}</p>
		</div>
	</div>
@endsection
