{{-- Use menu-item Vue component when bringing in a menu item which requires a help box --}}

<div>
	<span @click="addNewIdea()" class="sidebar-menu-item"><svg class="feather-icon icon-plus"><use xlink:href="#plus"></use></svg>{{ translate('tools/ideas/help.create_idea') }}</span>

</div>
<menu-item onclick="toggleImportModal()" icon="download" title="{{ translate('menus/tools.focus_tree_menu.import') }}"></menu-item>
<span class="sidebar-menu-item" id="reportBug">{{ featherIcon('life-buoy') }}{{ translate('menus/tools.focus_tree_menu.report_bug') }}</span>
{{--<span class="sidebar-menu-item" id="toggleHelp">{{ featherIcon('help-circle') }}{{ translate('menus/tools.focus_tree_menu.help') }}</span>--}}