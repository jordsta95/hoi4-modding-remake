<?php
	$isAdmin = false;
	foreach($mod->users as $user){
		if($user->id == Auth::user()->id){
			if($user->pivot->administrator){
				$isAdmin = true;
			}
		}
	}
	
?>

<div>
	<a href="/mod/{{ $mod->id }}/export" class="sidebar-menu-item downloadMod">{{ featherIcon('upload') }} {{ translate('menus/tools.mod_menu.export_mod') }}</a>
</div>
@if(Auth::user()->role_id == 2 || $isAdmin)
	{{-- TODO: Edit mod/delete mod --}}
	{{-- <menu-item icon="edit-2" title="{{ translate('menus/tools.mod_menu.edit_details') }}" helptext="edit_mod" help_header="{{ translate('tools/mod/help.edit_details.title') }}" help_text="{{ translate('tools/mod/help.edit_details.description') }}"></menu-item>

	<menu-item icon="trash-2" title="{{ translate('menus/tools.mod_menu.delete_mod') }}" helptext="delete_mod" help_header="{{ translate('tools/mod/help.delete_mod.title') }}" help_text="{{ translate('tools/mod/help.delete_mod.description') }}"></menu-item> --}}
	<span class="sidebar-menu-item" @click="setGlobalModal('edit-mod-modal')">{{ featherIcon('edit-2') }}{{ translate('menus/tools.mod_menu.edit') }}</span>
	<div class="help-text-file" id="mod-edit-data">
		{{$mod}}
	</div>
	<div class="help-text-file" id="mod-help-text">
		{{ getLangArray('tools/mod/index') }}
	</div>
@endif
{{-- <span class="sidebar-menu-item" id="toggleHelp">{{ featherIcon('help-circle') }}{{ translate('menus/tools.focus_tree_menu.help') }}</span>--}}