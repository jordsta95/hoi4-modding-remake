{{-- Use menu-item Vue component when bringing in a menu item which requires a help box --}}

<div>
	<span @click="addNewFocus()" class="sidebar-menu-item showHelpText" data-help="create_focus"><svg class="feather-icon icon-plus"><use xlink:href="#plus"></use></svg>{{ translate('menus/tools.focus_tree_menu.create_focus') }}</span>

	
</div>

<div>
	<label class="sidebar-menu-item">
		<span class="icon"><input type="checkbox" id="focus-multi-select"></span>
		{{ translate('menus/tools.focus_tree_menu.multi_select') }}
	</label>
</div>

<div>
	<span class="sidebar-menu-item showHelpText enterDeleteMode" data-help="delete_focus"><svg class="feather-icon icon-trash-2"><use xlink:href="#trash-2"></use></svg>{{ translate('menus/tools.focus_tree_menu.delete') }}</span>
</div>

<menu-item onclick="toggleImportModal()" icon="download" title="{{ translate('menus/tools.focus_tree_menu.import') }}" helptext="import_focus" help_header="{{ translate('tools/focus-tree/help.import_focus.title') }}" help_text="{{ translate('tools/focus-tree/help.import_focus.description') }}"></menu-item>
<span class="sidebar-menu-item" id="reportBug">{{ featherIcon('life-buoy') }}{{ translate('menus/tools.focus_tree_menu.report_bug') }}</span>
<span class="sidebar-menu-item" id="toggleHelp">{{ featherIcon('help-circle') }}{{ translate('menus/tools.focus_tree_menu.help') }}</span>