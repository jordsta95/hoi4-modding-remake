{{-- TODO: UPDATE THIS --}}
<span class="sidebar-menu-item" @click="updateCountryTab('start-dates')">{{ featherIcon('calendar') }}{{ translate('menus/country.start_date') }}</span>
<span class="sidebar-menu-item" @click="updateCountryTab('ideologies')">{{ featherIcon('pie-chart') }}{{ translate('menus/country.ideologies') }}</span>
<span class="sidebar-menu-item requires-maps" @click="updateCountryTab('states')">{{ featherIcon('map') }}{{ translate('menus/country.states') }}</span>
<span class="sidebar-menu-item" @click="updateCountryTab('government')">{{ featherIcon('briefcase') }}{{ translate('menus/country.government') }}</span>
<span class="sidebar-menu-item" @click="updateCountryTab('military')">{{ featherIcon('anchor') }}{{ translate('menus/country.military') }}</span>
<span class="sidebar-menu-item requires-maps" @click="updateCountryTab('divisions')">{{ featherIcon('shield') }}{{ translate('menus/country.divisions') }}</span>
<span class="sidebar-menu-item" @click="updateCountryTab('research')">{{ featherIcon('book') }}{{ translate('menus/country.research') }}</span>
<span class="sidebar-menu-item" @click="updateCountryTab('misc')">{{ featherIcon('globe') }}{{ translate('menus/country.misc') }}</span>
<menu-item onclick="toggleImportModal()" icon="download" title="{{ translate('menus/tools.focus_tree_menu.import') }}" helptext="import_focus" help_header="{{ translate('tools/focus-tree/help.import_focus.title') }}" help_text="{{ translate('tools/focus-tree/help.import_focus.description') }}"></menu-item>
<span class="sidebar-menu-item" id="reportBug">{{ featherIcon('life-buoy') }}{{ translate('menus/tools.focus_tree_menu.report_bug') }}</span>
<span class="sidebar-menu-item">{{ featherIcon('help-circle') }}Help</span>