<div>
	<span @click="addNewCountryToStartDate()" class="sidebar-menu-item showHelpText"><svg class="feather-icon icon-plus"><use xlink:href="#plus"></use></svg>{{ translate('tools/start-date/help.menu.add_country') }}</span>

	
</div>

<menu-item onclick="toggleImportModal()" icon="download" title="{{ translate('menus/tools.focus_tree_menu.import') }}"></menu-item>
<span class="sidebar-menu-item" id="reportBug">{{ featherIcon('life-buoy') }}{{ translate('menus/tools.focus_tree_menu.report_bug') }}</span>