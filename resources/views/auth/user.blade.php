@extends('layouts.app')

@if(Auth::check())
	@section('sidebar')
		@if(Auth::user()->id == $user->id || Auth::user()->role_id == 2)
			<div>
				<span class="sidebar-menu-item"  @click="setGlobalModal('user-form')">{{ featherIcon('edit') }} {{ translate('tools/user/profile.menu.edit') }}</span>
			</div>
			<div>
				<span class="sidebar-menu-item"  @click="setGlobalModal('media-manager')">{{ featherIcon('image') }} {{ translate('tools/user/profile.menu.gfx') }}</span>
			</div>
			<div id="user-data" class="help-text-file">
				{{ $user }}
			</div>
			<div id="user-help-text" class="help-text-file">
				{{ getLangArray('tools/user/profile.edit_box') }}
			</div>
			<div id="user-gfx-help-text" class="help-text-file">
				{{ getLangArray('tools/user/profile.gfx_edit') }}
			</div>
			@if(Auth::user()->role_id == 2)
				<menu-item icon="plus" title="{{ translate('tools/user/profile.menu.add_to_mod') }}" onclick="$('#help-addToTeam').toggleClass('open')"></menu-item>
				<div class="help-box" id="help-addToTeam">
					<form method="POST" action="/user/{{ $user->id }}/addToTeam">
						@csrf
						<div class="help-box-header">
							<h5>{{ translate('tools/user/profile.team.title', ['member' => $user->username]) }}</h5>
							<div class="close" data-help="addToTeam"><svg class="feather-icon icon-x"><use xlink:href="#x"></use></svg></div>
						</div>
						<div class="help-box-content">
							<p>{{ translate('tools/user/profile.team.content', ['member' => $user->username]) }}</p>
							<select-help label="{{ translate('tools/user/profile.team.choose_mod') }}" value="" name="mod_id" values="{{ json_encode(getAdminModList()) }}"></select-help>
							<input-help label="{{ translate('tools/user/profile.team.role') }}" name="role" helptext="{{ translate('tools/user/profile.team.role_help') }}"></input-help>
							<button class="button">{{ translate('tools/user/profile.team.submit') }}</button>
						</div>
					</form>
				</div>
			@endif
			<div>
				<span class="sidebar-menu-item"  @click="setGlobalModal('user-delete')">{{ featherIcon('trash-2') }} {{ translate('tools/user/profile.menu.delete') }}</span>
			</div>
		@else
			<menu-item icon="plus" title="{{ translate('tools/user/profile.menu.add_to_mod') }}" onclick="$('#help-addToTeam').toggleClass('open')"></menu-item>
			<div class="help-box" id="help-addToTeam">
				<form method="POST" action="/user/{{ $user->id }}/addToTeam">
					@csrf
					<div class="help-box-header">
						<h5>{{ translate('tools/user/profile.team.title', ['member' => $user->username]) }}</h5>
						<div class="close" data-help="addToTeam"><svg class="feather-icon icon-x"><use xlink:href="#x"></use></svg></div>
					</div>
					<div class="help-box-content">
						<p>{{ translate('tools/user/profile.team.content', ['member' => $user->username]) }}</p>
						<select-help label="{{ translate('tools/user/profile.team.choose_mod') }}" value="" name="mod_id" values="{{ json_encode(getAdminModList()) }}"></select-help>
						<input-help label="{{ translate('tools/user/profile.team.role') }}" name="role" helptext="{{ translate('tools/user/profile.team.role_help') }}"></input-help>
						<button class="button">{{ translate('tools/user/profile.team.submit') }}</button>
					</div>
				</form>
			</div>
		@endif
	@endsection
@endif

@section('content')
	<div class="user-wrapper page-content">
		<section class="user-header">
			<div class="grid">
				<div class="grid-col-span-laptop-3 grid-col-span-12">
					<div class="user-avatar" style="background-image: url('/media/find/{{ $user->media_id }}');"></div>
				</div>
				<div class="grid-col-span-laptop-4 grid-col-span-12">
					<h1>{{ $user->username }}</h1>
					<p>{{ $user->description }}</p>
				</div>
				<div class="grid-col-span-laptop-5 grid-col-span-12">
					<?php $links = json_decode($user->links); ?>
					@if($links)
						<div class="grid user-social-links">
							@foreach($links as $id => $link)
								@if(!empty($link))
									<div class="grid-col-span-laptop-6 grid-col-span-12 icon-input">
										{{ featherIcon($id) }}
										@if($id == 'twitter')
											<a href="//twitter.com/{{ ltrim($link, '@') }}" target="_blank" rel="nofollow">{{ $link }}</a>
										@elseif($id == 'twitch')
											<a href="//twitch.tv/{{ strtolower($link) }}" target="_blank" rel="nofollow">{{ $link }}</a>
										@elseif($id == 'youtube')
											<a href="{{ $link }}" target="_blank" rel="nofollow">{{ $link }}</a>
										@else
											{{ $link }}
										@endif
									</div>
								@endif
							@endforeach
						</div>
					@endif
				</div>
			</div>
		</section>
		<section class="mod-details">
			<div class="tabble-content">
				<div class="tab-headers">
					<a href="#mod-info" class="tab-header active">{{ translate('tools/user/profile.tabs.mods') }}</a>
					<a href="#forum-threads" class="tab-header">{{ translate('tools/user/profile.tabs.forum_threads') }}</a>
					<a href="#forum-posts" class="tab-header">{{ translate('tools/user/profile.tabs.forum_posts') }}</a>
				</div>
				<div class="tab-content">
					<div class="tab-details active" id="mod-info">
						<div class="grid">
							@foreach($user->mods as $mod)
								<div class="grid-col-span-6">
									@include('components/tools/mod/preview')
								</div>
							@endforeach
						</div>
					</div>
					<div class="tab-details" id="forum-threads">
						<div class="grid">
							@foreach($user->forum_posts()->orderBy('created_at', 'DESC')->limit(10)->get() as $thread)
								<div class="grid-col-span-12 grid-col-span-tablet-10">
									<h4><a href="/forum/{{ $thread->forum->slug }}/{{ $thread->id }}-{{ $thread->slug }}">{{ $thread->title }}</a></h4>
								</div>
								<div class="grid-col-span-12 grid-col-span-tablet-2 text-right">
									{{ date('d/M/Y', strtotime($thread->created_at)) }}
								</div>
							@endforeach
						</div>
					</div>
					<div class="tab-details" id="forum-posts">
						@foreach($user->forum_comments()->orderBy('created_at', 'DESC')->limit(10)->get() as $post)
							<?php $thread = $post->post; ?>
							@if(!empty($thread->forum))
								<div class="user-forum-post">
									<div class="grid">
										<div class="grid-col-span-12 grid-col-span-tablet-10">
											<h4><a href="/forum/{{ $thread->forum->slug }}/{{ $thread->id }}-{{ $thread->slug }}">{{ $thread->title }}</a></h4>
										</div>
										<div class="grid-col-span-12 grid-col-span-tablet-2 text-right">
											{{ date('d/M/Y', strtotime($post->created_at)) }}
										</div>
									</div>
									<blockquote>{!! strip_tags($post->content, '<p>') !!}</blockquote>
								</div>
							@endif
						@endforeach
					</div>
				</div>
			</div>
		</section>
	</div>
@endsection
