@extends('layouts.app')

@section('content')
<div class="modal centred wide show">
	<div class="modal-header">
		<h5>{{ translate('auth.register') }}</h5>
		<a href="{{ url()->previous() }}" class="close"><svg class="feather-icon icon-x"><use xlink:href="#x"></use></svg></a>
	</div>
	<div class="modal-content">
		<form method="POST" enctype="multipart/form-data"> 
			@csrf
			<div class="grid">
				<div class="grid-col-span-desktop-9 grid-col-span-12">
					<input-help label="{{ translate('auth.email') }}" value="{{ old('email') }}" helptext="" name="email"></input-help>
					@error('email')
						<div class="form-error-message" role="alert">
							{{ $message }}
						</div>
					@enderror
					<input-help label="{{ translate('auth.username') }}" value="{{ old('username') }}" helptext="" name="username"></input-help>
					@error('username')
						<div class="form-error-message" role="alert">
							{{ $message }}
						</div>
					@enderror
					<div class="grid">
						<div class="grid-col-span-laptop-6 grid-col-span-12">
							<div class="input-wrapper data-field">
								<input type="password" name="password" placeholder="{{ translate('auth.password') }}" class="default-input">
								<label for="password">{{ translate('auth.password') }}</label>
							</div>
							@error('password')
								<div class="form-error-message" role="alert">
									{{ $message }}
								</div>
							@enderror
						</div>
						<div class="grid-col-span-laptop-6 grid-col-span-12">
							<div class="input-wrapper data-field">
								<input type="password" name="password_confirmation" placeholder="{{ translate('auth.confirm_password') }}" class="default-input">
								<label for="password_confirmation">{{ translate('auth.confirm_password') }}</label>
							</div>
							@error('password_confirmation')
								<div class="form-error-message" role="alert">
									{{ $message }}
								</div>
							@enderror
						</div>
					</div>
					<div class="emoji-select data-field">
						<select-help label="{{ translate('auth.language') }}" value="{{ old('language') }}" helptext="" name="language" values="{{ json_encode(getSiteLanguageOptions()) }}"></select-help>
					</div>
					@error('language')
						<div class="form-error-message" role="alert">
							{{ $message }}
						</div>
					@enderror
				</div>
				<div class="grid-col-span-desktop-3 grid-col-span-12 text-centre">
					<div class="image-upload data-field">
						<div class="user-image">
							<img id="base64" src="/img/placeholder-image.png">
						</div>
						<input type="file" class="toBase64" data-alters="base64" name="avatar">
						<p class="text-centre">{{ translate('auth.avatar') }}</p>
					</div>
					<button class="button">Submit</button>
				</div>
				<div class="grid-col-span-3">
					
				</div>
			</div>
		</form>
	</div>
</div>
{{--
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">{{ __('Register') }}</div>

				<div class="card-body">
					<form method="POST" action="{{ route('register') }}">
						@csrf

						<div class="form-group row">
							<label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

							<div class="col-md-6">
								<input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

								@error('name')
									<span class="invalid-feedback" role="alert">
										<strong>{{ $message }}</strong>
									</span>
								@enderror
							</div>
						</div>

						<div class="form-group row">
							<label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

							<div class="col-md-6">
								<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

								@error('email')
									<span class="invalid-feedback" role="alert">
										<strong>{{ $message }}</strong>
									</span>
								@enderror
							</div>
						</div>

						<div class="form-group row">
							<label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

							<div class="col-md-6">
								<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

								@error('password')
									<span class="invalid-feedback" role="alert">
										<strong>{{ $message }}</strong>
									</span>
								@enderror
							</div>
						</div>

						<div class="form-group row">
							<label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

							<div class="col-md-6">
								<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
							</div>
						</div>

						<div class="form-group row mb-0">
							<div class="col-md-6 offset-md-4">
								<button type="submit" class="btn btn-primary">
									{{ __('Register') }}
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
--}}
@endsection
