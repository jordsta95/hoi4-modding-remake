@extends('layouts.app')

@section('content')
	<div class="page-content">
		<h1 class="text-centre">{{ translate('tools/user/search.title') }}</h1>
		<div>
			<form>
				<ul class="user-name-select">
					<?php 
					$letter = 'a';
					while($letter != 'aa'){
						echo '
							<li>
								<label><input onchange="this.form.submit();" type="radio" value="'.$letter.'" name="first_letter" '.((isset($_GET["first_letter"]) && $_GET["first_letter"] == $letter) ? 'checked="checked"' : '') .'>
								<span>'.$letter.'</span></label>
							</li>';
						
						$letter++;
					}
					?>
					<li>
						<label>
							<input onchange="this.form.submit();" type="radio" value="0" name="first_letter">
							<span>0-9</span>
						</label>
					</li>
					<li>
						<label>
							<input onchange="this.form.submit();" type="radio" value="-" name="first_letter">
							<span>?</span>
						</label>
					</li>
				</ul>
				<div class="user-search-box">
					<div class="input-wrapper">
						<input type="text" name="search" class="default-input" placeholder="Search users">
						<label>Search users...</label>
					</div>
					<button>Search</button>
				</div>
			</form>
			<div class="user-search-list">
				@foreach($users as $user)
					<a class="user-search-item" href="/user/{{ $user->id }}">
						<span class="image" style="background-image: url('/media/find/{{ $user->media_id }}');"></span>
						<span class="details">
							<span class="username">{{ $user->username }}</span>
							<span class="description">{{ $user->description }}</span>
						</span>
					</a>
				@endforeach
			</div>
			{{ $users->links() }}
		</div>
	</div>
@endsection
