@extends('layouts.app')

@section('content')
<div class="modal centred show">
	<div class="modal-header">
		<h5>{{ translate('auth.reset_password') }}</h5>
		<a href="/" class="close"><svg class="feather-icon icon-x"><use xlink:href="#x"></use></svg></a>
	</div>
	<div class="modal-content">
		<form method="POST" action="{{ route('password.update') }}"> 
			@csrf
			<input type="hidden" name="token" value="{{ $token }}">
			<div class="input-wrapper data-field">
				<input type="email" name="email" placeholder="{{ translate('auth.email') }}" class="default-input">
				<label for="email">{{ translate('auth.email') }}</label>
			</div>
			<div class="input-wrapper data-field">
				<input type="password" name="password" placeholder="{{ translate('auth.password') }}" class="default-input">
				<label for="password">{{ translate('auth.password') }}</label>
			</div>
			<div class="input-wrapper data-field">
				<input type="password" name="password_confirmation" placeholder="{{ translate('auth.confirm_password') }}" class="default-input">
				<label for="password_confirmation">{{ translate('auth.confirm_password') }}</label>
			</div>
			<div class="data-field text-centre">
				<button class="button">{{ translate('auth.submit') }}</button>
			</div>
		</form>
	</div>
</div>
{{--
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">{{ __('Reset Password') }}</div>

				<div class="card-body">
					<form method="POST" action="{{ route('password.update') }}">
						@csrf

						<input type="hidden" name="token" value="{{ $token }}">

						<div class="form-group row">
							<label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

							<div class="col-md-6">
								<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

								@error('email')
									<span class="invalid-feedback" role="alert">
										<strong>{{ $message }}</strong>
									</span>
								@enderror
							</div>
						</div>

						<div class="form-group row">
							<label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

							<div class="col-md-6">
								<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

								@error('password')
									<span class="invalid-feedback" role="alert">
										<strong>{{ $message }}</strong>
									</span>
								@enderror
							</div>
						</div>

						<div class="form-group row">
							<label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

							<div class="col-md-6">
								<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
							</div>
						</div>

						<div class="form-group row mb-0">
							<div class="col-md-6 offset-md-4">
								<button type="submit" class="btn btn-primary">
									{{ __('Reset Password') }}
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
--}}
@endsection
