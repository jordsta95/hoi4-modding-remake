<!doctype html>
<?php $userGlobalStyle = null; ?>
@if(Auth::check())
	@if(isset(Auth::user()->styles))
		@if(!empty(Auth::user()->styles))
			<?php $userGlobalStyle = json_decode(Auth::user()->styles); ?>
		@endif
	@endif
@endif
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Fonts -->
	<link rel="dns-prefetch" href="//fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;400;500&family=Raleway:wght@400;600;700&display=swap" rel="stylesheet">

	<!-- Styles -->
	<link href="{{ asset('css/app.css') }}?v={{ filemtime(public_path() . '/css/app.css') }}" rel="stylesheet">
	<link rel="stylesheet" href="/trumbowyg/dist/ui/trumbowyg.min.css">

	<!-- Scripts -->
	<script>window.jQuery || document.write('<script src="//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"><\/script>')</script>
	@if(!empty($userGlobalStyle))
		<style>
			body{
				@if(isset($userGlobalStyle->colour))
					@foreach($userGlobalStyle->colour as $c => $v)
						--{{ $c }} : {{ $v }};
					@endforeach
				@endif
				@if(isset($userGlobalStyle->space_size))
					--space: {{ $userGlobalStyle->space_size }}rem;
				@endif
			}
			html{
				@if(isset($userGlobalStyle->font_size))
					font-size: {{ $userGlobalStyle->font_size }}px !important;
				@endif
			}
		</style>
	@endif
</head>
<body class=" @if(!empty($userGlobalStyle)) @if(isset($userGlobalStyle->class)) {{$userGlobalStyle->class}} @endif @endif ">
<span class="loader" id="pageloader">{{ featherIcon('loader') }}</span>
	@if(\Session::has('success'))
		<div class="alert-message success">
			{!! \Session::get('success') !!}
		</div>
	@endif
	@if(\Session::has('error'))
		<div class="alert-message error">
			{!! \Session::get('error') !!}
			@if(gettype(\Session::get('error')) != 'string')
				@foreach(\Session::get('error')->getMessages() as $this_error)
					{{$this_error[0]}}
				@endforeach
			@endif
		</div>
	@endif
	{{--
	<div id="app">
		<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
			<div class="container">
				<a class="navbar-brand" href="{{ url('/') }}">
					{{ config('app.name', 'Laravel') }}
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<!-- Left Side Of Navbar -->
					<ul class="navbar-nav mr-auto">

					</ul>

					<!-- Right Side Of Navbar -->
					<ul class="navbar-nav ml-auto">
						<!-- Authentication Links -->
						@guest
							<li class="nav-item">
								<a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
							</li>
							@if (Route::has('register'))
								<li class="nav-item">
									<a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
								</li>
							@endif
						@else
							<li class="nav-item dropdown">
								<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
									{{ Auth::user()->name }}
								</a>

								<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
									<a class="dropdown-item" href="{{ route('logout') }}"
									   onclick="event.preventDefault();
													 document.getElementById('logout-form').submit();">
										{{ __('Logout') }}
									</a>

									<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
										@csrf
									</form>
								</div>
							</li>
						@endguest
					</ul>
				</div>
			</div>
		</nav>
		--}}
	<div @if(View::hasSection('sidebar')) class="has-sidebar {{ (isset($_COOKIE['sidebarClosed']) && $_COOKIE['sidebarClosed']== "yes") ? '' : 'open' }}" @endif id="app">
		@if(View::hasSection('sidebar'))
			<section class="sidebar-menu">
				<div class="sidebar-toggle">{{ featherIcon('menu') }}</div>
				<div class="sidebar-menu-items">
					@yield('sidebar')
				</div>
			</section>
		@endif

		<main class="main-content">
			@yield('content')
		</main>

		<section class="main-menu">
			<span class="main-menu-toggle">{{ featherIcon('menu') }} {{ translate('menus/generic.menu') }}</span>
			<nav class="menu">
				<a href="/" class="menu-item">{{ featherIcon('home') }} {{ translate('menus/generic.home') }}</a>
				@auth
					<span class="menu-item has-children">
						{{ featherIcon('user') }} {{ translate('menus/generic.account') }}
						<span class="children">
							<a href="/user">
								<span class="title">{{ translate('menus/generic.your_account') }}</span>
								<div class="description">{{ translate('menus/generic.your_account_text') }}</div>
							</a>
							<a href="/user-search">
								<span class="title">{{ translate('menus/generic.user_search') }}</span>
								<div class="description">{{ translate('menus/generic.user_search_text') }}</div>
							</a>
							<a href="{{ route('logout') }}" class="custom-action" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
								<span class="title">{{ translate('auth.logout') }}</span>
								<span class="description">{{ translate('auth.logout_text') }}</span>
							</a>
							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								@csrf
							</form>
						</span>
					</span>
				@endauth
				@guest
					<span class="menu-item has-children">
						{{ featherIcon('user') }} {{ translate('menus/generic.account') }}
						<span class="children">
							<a href="/login" style="width: calc(50% - 0.75rem)">
								<span class="title">{{ translate('auth.login') }}</span>
								<span class="description">{{ translate('auth.login_text') }}</span>
							</a>
							<a href="/register" style="width: calc(50% - 0.75rem)">
								<span class="title">{{ translate('auth.register') }}</span>
								<span class="description">{{ translate('auth.register_text') }}</span>
							</a>
						</span>
					</span>
				@endguest
				@auth
					<span class="menu-item @if(Auth::user()->unreadNotifications->count()) has-children @endif ">
						<a href="/forum">{{ featherIcon('message-square') }} {{ translate('menus/generic.forum') }}</a>
							@if(Auth::user()->unreadNotifications->count())
								<span class="menu-item-notification"></span>
								<span class="children">
									<span class="small-text text-white">{{ translate('menus/generic.unread_notifications') }}</span>
									<span></span>
									<a href="/forum?markAllNotificationsAsRead=true" class="small-text text-right underline">
										{{ translate('menus/generic.mark_notifications_as_read') }}
									</a>
									@foreach(Auth::user()->unreadNotifications as $n)
										<?php 
										$nData = json_decode(json_encode($n->data)); 
										$linkParts = explode('#', $nData->link);
										$link = $linkParts[0];
										if(strpos($link, '?') === false){
											$link .= '?';
										}else{
											$link .= '&';
										}
										$link .= 'notification='.$n->id;
										$link .= '#'.$linkParts[1];
										?>
										<a href="{{ $link }}">
											<span class="title">{{ $nData->title }}</span>
											<span class="description">{{ $nData->username }}</span>
										</a>
									@endforeach
								</span>
							@endif
					</span>
				@endauth
				@guest
					<a href="/forum" class="menu-item">
						{{ featherIcon('message-square') }} {{ translate('menus/generic.forum') }}
					</a>
				@endguest
				<span class="menu-item has-children">
					{{ featherIcon('code') }} {{ translate('menus/generic.modding_tools') }}
					<span class="children">
						<a href="/focus-tree">
							<span class="title">{{ translate('menus/tools.focus_tree') }}</span>
							<span class="description">{{ translate('menus/tools.focus_tree_description') }}</span>
						</a>
						<a href="/events">
							<span class="title">{{ translate('menus/tools.event') }}</span>
							<span class="description">{{ translate('menus/tools.event_description') }}</span>
						</a>
						<a href="/ideas">
							<span class="title">{{ translate('menus/tools.idea') }}</span>
							<span class="description">{{ translate('menus/tools.idea_description') }}</span>
						</a>
						<a href="/country">
							<span class="title">{{ translate('menus/tools.country') }}</span>
							<span class="description">{{ translate('menus/tools.country_description') }}</span>
						</a>
						<a href="/decision">
							<span class="title">{{ translate('menus/tools.decision') }}</span>
							<span class="description">{{ translate('menus/tools.decision_description') }}</span>
						</a>
						<a href="/ideology">
							<span class="title">{{ translate('menus/tools.ideology') }}</span>
							<span class="description">{{ translate('menus/tools.ideology_description') }}</span>
						</a>
						<a href="/start-date">
							<span class="title">{{ translate('menus/tools.start_dates') }}</span>
							<span class="description">{{ translate('menus/tools.start_dates_description') }}</span>
						</a>
						<a href="/character/list">
							<span class="title">Characters</span>
							<span class="description">Create generals/leaders/etc. for your countries</span>
						</a>
						<span></span>
					</span>
				</span>
				<a href="/mod" class="menu-item">{{ featherIcon('layers') }} {{ translate('menus/generic.your_mods') }}</a>
				{{-- <a href="https://www.patreon.com/hoi4moddingtools" target="_blank" class="menu-item support">{{ translate('menus/generic.support') }}</a> --}}
			</nav>
		</section>
		<component v-bind:is="globalModal" @closeglobalmodal="closeglobalmodal"></component>
	</div>
	<div style="display: none">
		@include('components/assets/feather-icons')
	</div>
	<noscript><style>.loader{opacity: 0;pointer-events: none;}</style></noscript>
	<!-- Scripts -->
	<script src="{{ asset('js/app.js') }}?v={{ filemtime(public_path() . '/js/app.js') }}"></script>
	<script src="{{ asset('js/wysiwyg.js') }}"></script>
	<script>
		function makeWYSIWYG(){
			$('.has-wysiwyg textarea,.wysiwyg').trumbowyg({
				@if(Auth::check())
					@if(Auth::user()->role_id != 2) 
					tagsToRemove: [
						'script', 'style', 'iframe', 'input', 'textarea' , 'button'
					],
					@endif 
				@endif
				btnsDef: {
					// Create a new dropdown
					image: {
						dropdown: ['insertImage', 'upload'],
						ico: 'insertImage'
					},
					justify: {
						dropdown: ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
						ico: 'justifyLeft'
					}
				},
				btns: [
					@if(Auth::check())
						@if(Auth::user()->role_id == 2)
							['viewHTML'],
						@endif
					@endif
					['undo', 'redo'], 
					['formatting', 'strong', 'em', 'link'], 
					['unorderedList', 'orderedList'],
					['justify'], 
					['image'], 
					['emoji']
				],
				plugins: {
					upload: {
						serverPath: '/media/upload?url=true&asJson=true',
						fileFieldName: 'image',
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						},
						urlPropertyName: "url"
					}
				}
			});
		}
		makeWYSIWYG();
		@if(\Request::route())
			@if(\Request::route()->getName() == 'forumPost')
				$('.edit-post').on('click', function(){
					$(this).hide();
					var $content = $(this).closest('.forum-comment').find('.comment');
					var action = '/forum/comment/'+$(this).attr('data-id');
					$content.html('<form method="POST" action="'+action+'"> @csrf <textarea class="wysiwyg" name="text" required>'+ $content.html()+'</textarea><p><button class="button">Edit Post</button></p></form>');
					
					makeWYSIWYG();
				});
			@endif
		@endif

		$('.sidebar-toggle').on('click', function(){
			$('.has-sidebar').toggleClass('open');
			var isOpen = $('.has-siderbar').hasClass('open');
			setCookie('sidebarClosed', (isOpen ? 'no' : 'yes'), 1);
		});
	</script>
	<script>
	window.getSiteLanguages = function(){ return {!! json_encode(getSiteLanguageOptions()) !!}; }
	window.getSiteLanguage = function(){ return '{{ app()->getLocale() }}'; }
	</script>
	<style id="themeFontSize"></style>
	<style id="themeSpace"></style>
	<style id="themeColours"></style>

	@yield('footer-script')

	@if(\Session::has('exportErrors'))
		{{ dd(\Session::get('exportErrors'))}}
	@endif
	
</body>
</html>
