@if ($paginator->hasPages())
	<nav role="navigation" aria-label="Pagination Navigation" class="pagination-nav">
		<div class="pagination-links">
			{{-- Previous Page Link --}}
			@if ($paginator->onFirstPage())
				<span aria-disabled="true" aria-label="{{ __('pagination.previous') }}" class="arrow">
					{{ featherIcon('chevron-left') }}
				</span>
			@else
				<a href="{{ $paginator->previousPageUrl() }}" rel="prev" class="arrow" aria-label="{{ __('pagination.previous') }}">
				  {{ featherIcon('chevron-left') }}
				</a>
			@endif

			{{-- Pagination Elements --}}

			@foreach ($elements as $element)
				{{-- "Three Dots" Separator --}}
				@if (is_string($element))
					<span aria-disabled="true">
						{{ $element }}
					</span>
				@endif

				{{-- Array Of Links --}}
				@if (is_array($element))
					@foreach ($element as $page => $url)
						@if ($page == $paginator->currentPage())
							<span aria-current="page">
								{{ $page }}
							</span>
						@else
							<a href="{{ $url }}" class="" aria-label="{{ __('Go to page :page', ['page' => $page]) }}">
								{{ $page }}
							</a>
						@endif
					@endforeach
				@endif
			@endforeach

			{{-- Next Page Link --}}
			@if ($paginator->hasMorePages())
				<a href="{{ $paginator->nextPageUrl() }}" rel="next" class="arrow" aria-label="{{ __('pagination.next') }}">
					{{ featherIcon('chevron-right') }}
				</a>
			@else
				<span aria-disabled="true" aria-label="{{ __('pagination.next') }}" class="arrow">
				   {{ featherIcon('chevron-right') }}
				</span>
			@endif
		</div>
	</nav>
@endif
