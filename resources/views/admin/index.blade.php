<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Scripts -->
	<script src="{{ asset('js/app.js') }}" defer></script>

	<!-- Fonts -->
	<link rel="dns-prefetch" href="//fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;400;500&family=Raleway:wght@400;600;700&display=swap" rel="stylesheet">

	<!-- Styles -->
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
	<div id="svg-image" style="display: none;"><div style="width:5632px">{!! Storage::get('map-svgs/vanilla.svg') !!}</div></div>

	@if(\Session::has('success'))
		<div class="alert-message success">
			{!! \Session::get('success') !!}
		</div>
	@endif
	@if(\Session::has('error'))
		<div class="alert-message error">
			{!! \Session::get('error') !!}
			@if(gettype(\Session::get('error')) != 'string')
				@foreach(\Session::get('error')->getMessages() as $this_error)
					{{$this_error[0]}}
				@endforeach
			@endif
		</div>
	@endif
	<div id="app">
		<div class="page-content">
			<div>
				<h1>Admin panel</h1>
				<p>Stuff in borders submit separately</p>
				<section id="upload-file" style="margin-top: 2rem;border: 1px solid black; padding: 1rem; display: inline-block; vertical-align: top;">
					<h3 style="margin-bottom: 1rem;">Upload HOI4 file(s)</h3>
					<form action="/admin/upload-files" method="POST" enctype="multipart/form-data">
						@csrf
						<p>Upload location</p>
						<p class="input-wrapper"><input type="text" name="location" class="default-input" value="/hoi4-files"></p>
						<p>Files to upload</p>
						<p><input type="file" multiple name="file[]" id="import-files"></p>
						<p onclick="adminConvertFileToJson()">CONVERT!</p>
						<p>If no textboxes are here, then there's an issue...</p>
						<section id="output" class="input-wrapper" style="max-height: 5rem; overflow: auto;">
					
						</section>
						<p><button class="button" id="uploadButton" style="display: none;">Submit</button></p>
					</form>

					<h3 style="margin-bottom: 1rem;">Upload HOI4 language file(s)</h3>
					<form action="/admin/upload-files" method="POST" enctype="multipart/form-data">
						@csrf
						<p>Upload location</p>
						<p class="input-wrapper"><input type="text" name="location" class="default-input" value="/hoi4-files"></p>
						<p>Files to upload</p>
						<p><input type="file" multiple name="file[]" id="import-lang-files"></p>
						<p onclick="adminConvertLanguageFileToJson()">CONVERT!</p>
						<p>If no textboxes are here, then there's an issue...</p>
						<section id="langoutput" class="input-wrapper" style="max-height: 5rem; overflow: auto;">
					
						</section>
						<p><button class="button" id="uploadLangButton" style="display: none;">Submit</button></p>
					</form>
				</section>
				<section style="margin-top: 2rem;border: 1px solid black; padding: 1rem; display: inline-block;" id="state-map-maker">
					<h3>Add provinces to map</h3>
					<form action="/admin/create-svg-map" method="POST" enctype="multipart/form-data">
						@csrf
						<input placeholder="delimiter" value=";" type="text" id="delimiter">
						<p>Upload location</p>
						<p class="input-wrapper"><input type="text" name="location" class="default-input" value="/map-svgs"></p>
						<p>Definitions CSV for map</p>
						<p><input type="file" name="csv" id="definitions-csv"></p>
						<p>SVG of map</p>
						<p><input type="file" name="svg" id="definitions-svg"></p>
						<p onclick="adminPreviewSVG()">PREVIEW!</p>
						<textarea name="svg_code" class="svg-code" placeholder="Do not submit if empty" style="width: 100%;"></textarea>
						<button>Submit</button>
					</form>
					<div id="svgPreview" style="margin-top: 1rem;"></div>
				</section>
				
				<section style="margin-top: 2rem;border: 1px solid black; padding: 1rem; display: inline-block; vertical-align: top;" id="map-pixel-worker">
					<p>Definitions CSV for map</p>
					<p><input type="file" name="csv" id="pixel-definitions-csv"></p>
					<p id="fullsize-image" style="overflow: hidden; height: 10px;width: 300px;"><img style="max-width:5632px" id="imageToCheck" src="/get-data/mapBMP"></p>
					<p><button id="getPixels" onclick="getPixels()">Get Pixels</button></p>
					{{--<div id="pixelData" style="display: none;">{!! Storage::get('map-svgs/map-centre-positions.json') !!}</div>--}}
				</section>

				<section style="margin-top: 2rem;border: 1px solid black; padding: 1rem; display: inline-block;" id="state-json">
					<p>State JSON</p>
					@php
					function HSVtoRGB(array $hsv) {
						list($H,$S,$V) = $hsv;
						//1
						$H *= 6;
						//2
						$I = floor($H);
						$F = $H - $I;
						//3
						$M = $V * (1 - $S);
						$N = $V * (1 - $S * $F);
						$K = $V * (1 - $S * (1 - $F));
						//4
						switch ($I) {
							case 0:
								list($R,$G,$B) = array($V,$K,$M);
								break;
							case 1:
								list($R,$G,$B) = array($N,$V,$M);
								break;
							case 2:
								list($R,$G,$B) = array($M,$V,$K);
								break;
							case 3:
								list($R,$G,$B) = array($M,$N,$V);
								break;
							case 4:
								list($R,$G,$B) = array($K,$M,$V);
								break;
							case 5:
							case 6: //for when $H=1 is given
								list($R,$G,$B) = array($V,$M,$N);
								break;
						}
						return array($R, $G, $B);
					}

					$states = Storage::files('/hoi4-files/history/states');
					$stateFiles = [];
					$colours = Storage::get('/hoi4-files/common/countries/colors.txt');
					$file = explode("\n", $colours);
					$colourData = '{ "country_tag":"color"';
					$newTag = true;
					$finishedColours = false;
					$colourSet = false;
					foreach($file as $line){
						if(substr(trim($line), 0, 1) == '#'){
							continue; //It's a comment line
						}
						$ex = explode('#', $line); //Remove comment
						
						if(empty(trim($ex[0]))){
							continue;
						}
						if($newTag){
							$tag = trim(explode('=', $ex[0])[0]);
							$colourData .= ', "'.$tag.'":';
							$newTag = false;
							$finishedColours = false;
							$colourSet = false;
							if(strpos($ex[0], 'color') === false){
								continue;
							}else{
								$n = str_replace('color', '| color', $ex[0]);
								$l = explode('|', $n);
								$ex = explode('#', $l[1]);
							}
						}
						if(!$newTag && !$finishedColours){
							$finishedColours = true;
							if(strpos($ex[0], 'color') != false){
								$finishedColours = false;
								$c = explode('=', $ex[0]);
								if(trim($c[0]) == 'color'){
									$col = trim(str_replace('{', '(', str_replace('}', ')', $c[1])));
									$col = str_replace(' ( ', '(', $col);
									$col = str_replace(' ) ', ')', $col);
									$col = str_replace(' ', ',', $col);
									$col = str_replace(',,', ',', $col);
									$col = str_replace(',)', ')', $col);
									if(strpos($c[1], 'HSV') !== false || strpos($c[1], 'hsv') !== false){
										$n = str_replace(['HSV(','hsv('], '', $col);
										$n = str_replace(')', '', $n);
										$n = explode(',', $n);
										$rgb = HSVtoRGB($n);
										$nums = [];
										foreach($rgb as $i){
											$nums[] = $i * 100;
										}
										$col = 'rgb('.implode(',', $nums).')';
									}else{
										$n = str_replace(['rgb(','RGB('], '', $col);
										$n = str_replace(')', '', $n);
										$col = 'rgb('.$n.')';
									}
									$colourData .= '"'.$col.'"';
									$colourSet = true;
								}
							}else{
								$newTag = true;
								if(!$colourSet){
									$colourData .= '"#FFFFFF"';
								}
							}
						}
					}
					$colourData .= '}';
					echo '<textarea>'.$colourData.'</textarea>';
					$colourData = json_decode($colourData);



					foreach($states as $filePath){
						$arr = explode('-', str_replace('hoi4-files', 'hoi4files', $filePath), 2);
						$file = Storage::get($filePath);
						$json = json_decode($file);
						$colour = '#FFFFFF';
						if(isset($json->state)){
							if(isset($json->state->history)){
								if(isset($json->state->history->owner)){
									if(isset($colourData->{$json->state->history->owner})){
										$colour = $colourData->{$json->state->history->owner};
									}
								}elseif(isset($json->state->history->controller)){
									if(isset($colourData->{$json->state->history->controller})){
										$colour = $colourData->{$json->state->history->controller};
									}
								}
							}
						}


						$stateFiles[] = ['id' => trim(str_replace('hoi4files/history/states/','',$arr[0])), 'name' => str_replace(['hoi4-files/history/states/', '.txt'],'',$arr[1]), 'colour' => $colour];
					}
					@endphp
					<textarea>{{ json_encode($stateFiles) }}</textarea>
				</section>

				<div id="generatedSVG"></div>
				
			</div>
		</div>
	</div>
	<div style="display: none">
		@include('components/assets/feather-icons')
	</div>
	<script>
		var definitions = [];
		window.adminPreviewSVG = function(){
			definitions = [];

			let csvreader = new FileReader();
			let csvfilename = document.getElementById('definitions-csv').files[0].name;
			csvreader.readAsText(document.getElementById('definitions-csv').files[0], "UTF-8");
			csvreader.onload = function(evt){
				var lines = evt.target.result.split("\n");
				$.each(lines, function(key, line){
					var data = line.split($('#delimiter').val()); 
					definitions[data[0]] = {'r': data[1], 'g': data[2], 'b':data[3]};
				});
			};

			let svgreader = new FileReader();
			let svgfilename = document.getElementById('definitions-svg').files[0].name;
			svgreader.readAsText(document.getElementById('definitions-svg').files[0], "UTF-8");
			svgreader.onload = function(evt){
				$('#svgPreview').html(evt.target.result);
				
				getSVGProvinces();
				
			};
		}

		window.getSVGProvinces = function(){
			$('#svgPreview').find('[fill*="#"],[style*="fill"]').each(function(){
				if($(this).attr('fill')){
					var rgb = hexToRgb($(this).attr('fill'));
				}else{
					var style = $(this).attr('style');
					var styles = style.split(';');
					var fill = null;
					$.each(styles, function(index, s){
						var n = s.split(':');
						if(n[0] == 'fill'){
							fill = n[1];
						}
					});
					var rgb = hexToRgb(fill);
				}
				if(rgb){
					var hasShown = false;
					if(rgb.hasOwnProperty('r') && rgb.hasOwnProperty('g') && rgb.hasOwnProperty('b')){
						var shape = $(this);
						$.each(definitions, function(key, value){
							if((value.r == rgb.r) && (value.g == rgb.g) && (value.b == rgb.b)){
								//Perfect match
								shape.attr('data-province-id', key);
							}
						});
					}
				}
			});
			$('#svgPreview').find('[fill*="#"],[style*="fill"]').each(function(){
				if($(this).attr('fill')){
					var rgb = hexToRgb($(this).attr('fill'));
				}else{
					var style = $(this).attr('style');
					var styles = style.split(';');
					var fill = null;
					$.each(styles, function(index, s){
						var n = s.split(':');
						if(n[0] == 'fill'){
							fill = n[1];
						}
					});
					var rgb = hexToRgb(fill);
				}
				if(rgb){
					var hasShown = false;
					if(rgb.hasOwnProperty('r') && rgb.hasOwnProperty('g') && rgb.hasOwnProperty('b')){
						var shape = $(this);
						$.each(definitions, function(key, value){
							if((value.r == rgb.r + 1 || value.r == rgb.r - 1 || value.r == rgb.r) && (value.g == rgb.g + 1 || value.g == rgb.g - 1 || value.g == rgb.g) && (value.b == rgb.b + 1 || value.b == rgb.b - 1 || value.b == rgb.b)){
								//Is 1 RGB value out
								if(!$('#svgPreview').find('[data-province-id="'+key+'"]')){
									shape.attr('data-province-id', key);
								}
							}
						});
					}
				}
			});
			$('.svg-code').val($('#svgPreview').html());
		}

		function hexToRgb(hex) {
		  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
		  return result ? {
			r: parseInt(result[1], 16),
			g: parseInt(result[2], 16),
			b: parseInt(result[3], 16)
		  } : null;
		}
		function componentToHex(c) {
			var hex = c.toString(16);
			return hex.length == 1 ? "0" + hex : hex;
		}

		function rgbToHex(r, g, b) {
			const rgb = (r << 16) | (g << 8) | (b << 0);
			return '#' + (0x1000000 + rgb).toString(16).slice(1);
			return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
		}

		window.adminConvertFileToJson = function(){
			var len = document.getElementById('import-files').files.length;
			for(var i = 0; i < document.getElementById('import-files').files.length; ++i) {
				let reader = new FileReader();
				let filename = document.getElementById('import-files').files[i].name;
				reader.readAsText(document.getElementById('import-files').files[i], "UTF-8");
				reader.onload = function(evt){
					let json = hoiFileToJSONWithGlobalKey(evt.target.result, 0);
					if(json.hasOwnProperty('state')){
						json.state.provinces = json.state.provinces.provinces;
					}
					$('#output').append('<textarea class="default-input" name="fileJson['+filename+']">'+JSON.stringify(json)+'</textarea>');
				};
			}

			if(i == len){
				$('#uploadButton').show();
			}
				
		}
		window.adminConvertLanguageFileToJson = function(){
			var len = document.getElementById('import-lang-files').files.length;
			for(var i = 0; i < document.getElementById('import-lang-files').files.length; ++i) {
				let reader = new FileReader();
				let filename = document.getElementById('import-lang-files').files[i].name;
				reader.readAsText(document.getElementById('import-lang-files').files[i], "UTF-8");
				reader.onload = function(evt){
					let json = localisationToJSON(evt.target.result);
					$('#langoutput').append('<textarea class="default-input" name="fileJson['+filename+']">'+JSON.stringify(json)+'</textarea>');
				};
			}

			if(i == len){
				$('#uploadLangButton').show();
			}
				
		}

		var pxDefinitions = [];
		let rgbDefs = {};
		window.getPixels = function(){
			$('#svg-image').show();
			setTimeout(function(){
				let csvreader = new FileReader();
				let csvfilename = document.getElementById('pixel-definitions-csv').files[0].name;
				csvreader.readAsText(document.getElementById('pixel-definitions-csv').files[0], "UTF-8");
				csvreader.onload = function(evt){
					var lines = evt.target.result.split("\n");
					$.each(lines, function(key, line){
						var data = line.split($('#delimiter').val()); 
						pxDefinitions.push(data[1]+','+data[2]+','+data[3]+','+data[0]);
						let rgb = "rgb("+data[1]+","+data[2]+","+data[3]+")";
						rgbDefs[rgb] = data[0];
					});
					setTimeout(function(){
						colourCheck();
					}, 200);
					//setNewColours();
				};
			}, 500);
		}

		let pixels = {};
		window.colourCheck = function(){
			console.log('Getting colours from map');
			var img = document.getElementById('imageToCheck'); 
			var base_image = new Image();
			base_image.src = $('#imageToCheck').attr('src');



			if(!canvas) {
				var canvas = document.createElement('canvas');
				canvas.width = img.width;
				canvas.height = img.height;
				canvas.getContext('2d').drawImage(base_image, 0, 0, img.width, img.height);

				let startX = 0;
				let startY = 0;
				let maxX = img.width;
				let maxY = img.height;
				let testing = false;

				if(testing){
					maxY = 190;
					maxX = 190;
				}
				while(maxY >= startY){
					while(maxX >= startX){
						let rgb = canvas.getContext('2d').getImageData(startX, startY, 1, 1).data;
						let key = "rgb("+rgb[0]+","+rgb[1]+","+rgb[2]+")";

						if(!pixels.hasOwnProperty(key)){
							pixels[key] = [];
						}
						pixels[key].push([startX, startY]);

						// let elem = document.elementFromPoint(startX, startY);
						// let el = $(elem);
						// if(el.attr('fill')){
						// 	if(el.attr('fill').includes('rgb')){

						// 	}else{
						// 		el.attr('fill', key);
						// 	}
						// }
						
						startX++;
					}
					startX = 0;
					startY++;
				}

				$('#generatedSVG').html(generateSVG(pixels, maxX, maxY));
				console.log("Completed colour check");

				// let unassigned = [];
				// $.each(pxDefinitions, function(index, value){
					
				// 	var spl = value.split(','); //R,G,B,ID
				// 	let rgb = "rgb("+spl[0]+","+spl[1]+","+spl[2]+")";

				// 	console.log('Assigning province: '+spl[3]);
				// 	if($("[fill='"+rgb+"']").length){
				// 		$("[fill='"+rgb+"']").attr('data-province-id', spl[3]);
				// 	}else{
				// 		unassigned.push(spl[3]);
				// 	}
				// });
				// console.log("Unassigned provinces:");
				// console.log(unassigned);
			}

			$('#svg-image').hide();
		}

		window.generateSVG = function(pixelsData, maxX, maxY) {
		  // Initialize the SVG string
		  let svgContent = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 '+maxX+' '+maxY+'">';

			// Loop through each color and its coordinates
		  for (const [color, coordinates] of Object.entries(pixelsData)) {
			// Group coordinates by rows
			const rows = {};
			coordinates.forEach(([x, y]) => {
			  if (!rows[y]) rows[y] = [];
			  rows[y].push(x);
			});

			// Build the `d` attribute for the path
			let pathD = '';
			for (const [y, xs] of Object.entries(rows)) {
			  // Sort the x-coordinates for easier processing
			  const sortedXs = xs.sort((a, b) => a - b);

			  // Find contiguous segments in the row
			  let startX = sortedXs[0];
			  let prevX = sortedXs[0];
			  for (let i = 1; i <= sortedXs.length; i++) {
				if (sortedXs[i] !== prevX + 1) {
				  // End of a segment
				  const width = prevX - startX + 1;
				  pathD += `M${startX} ${y}h${width}v1h-${width}z `;
				  // Start a new segment
				  startX = sortedXs[i];
				}
				prevX = sortedXs[i];
			  }
			}

			// Add the path element for this color
			svgContent += `<path d="${pathD.trim()}" fill="${color}" data-province-id="${rgbDefs[color]}"/>`;
		  }

		  // Close the SVG
		  svgContent += `</svg>`;
		  return svgContent;
		}

		window.colourCheckOld = function(){
			console.log('Start colour check');
			var img = document.getElementById('imageToCheck'); 
			var base_image = new Image();
			base_image.src = $('#imageToCheck').attr('src');
			if(!canvas) {

				var canvas = document.createElement('canvas');
				canvas.width = img.width;
				canvas.height = img.height;
				canvas.getContext('2d').drawImage(base_image, 0, 0, img.width, img.height);
				$.each(pxDefinitions, function(index, value){
					console.log('Colour: '+value);
					if(value != '0,0,0'){
						var rgb = value.split(','); //R,G,B,ID
						var hex = rgbToHex(rgb[0],rgb[1],rgb[2]);

						var everyPixelChecked = false;
						var currLoop = 0;
						

						var width = img.width;
						var height = img.height;

						var offset = 50;
						var offsetX = -1;
						var maxXOff = -1;
						var minXOff = -1;

						var x = 0;
						var y = 0;
						var maxY = -1;

						var pixelData = [];
						while(!everyPixelChecked){
							var data = canvas.getContext('2d').getImageData(x, y, 1, 1).data;
							if(rgb[0] == data[0] && rgb[1] == data[1] && rgb[2] == data[2]){
								pixelData.push([x,y]);
								if(offsetX == -1){
									offsetX = x;
									maxXOff = (offsetX + offset);
									minXOff = (offsetX - offset);
									maxY = (y + (offset * 2));
									if(minXOff < 0){
										minXOff = 0;
									}
									if(maxY > height){
										maxY = height;
									}
								}
							}
							
							if((x == width && offsetX == -1) || (offsetX != -1 && maxXOff === x)){
								//It'll still check all Y values, but it turns 2048 * 5000 to 2048 * 100
								if(y == height){
									everyPixelChecked = true;
								}
								y++;
								if(height > y){
									y++;
								}
								x=0;
								if(offsetX != -1){
									x = minXOff;
								}
								
							}else{
								x++;
								if(width > x){
									x++;
								}
							}
							currLoop++;
						}

						var xMin = 99999999;
						var xMax = 0; 
						var yMin = 99999999;
						var yMax = 0; 
						var location = [];
						$.each(pixelData, function(i, l){
							x = l[0];
							y = l[1];

							if(x > xMax){
								xMax = x;
							}
							if(x < xMin){
								xMin = x;
							}

							if(y > yMax){
								yMax = y;
							}
							if(y < yMin){
								yMin = y;
							}
							
						});

						location = [Math.ceil((((xMin + xMax) / 2))), Math.ceil((((yMin + yMax) / 2)))];
						//console.log(location);
						var elem = document.elementFromPoint(location[0], location[1]);
						$(elem).attr('style', "fill:"+hex);
						$(elem).attr('data-province-id', data[3]);
					}
					console.log("Complete CSV row: "+index);
				});

				setTimeout(function(){
					//$('#svg-image').hide();
					alert('Complete, changed: '+changed+'/'+total);
				}, 500);
			}
		}

		window.setNewColours = function(){
			var json = JSON.parse($('#pixelData').text());
			var total = 0;
			var changed = 0;
			$.each(pxDefinitions, function(index, value){
				if(json.hasOwnProperty(value)){
					var location = json[value];
					var rgb = value.split(',');
					var hex = rgbToHex(rgb[0],rgb[1],rgb[2]);

					var elem = document.elementFromPoint(location[0], location[1]);
					
					if($(elem).attr('fill')){
						var ex = $(elem).attr('fill');
					}else{
						var style = $(elem).attr('style');
						if(style){
							var styles = style.split(';');
							var fill = null;
							$.each(styles, function(index, s){
								var n = s.split(':');
								if(n[0] == 'fill'){
									fill = n[1];
								}
							});
							var ex = fill;
						}else{
							var ex = '';
						}
					}
					if(ex != hex){
						changed++;
					}
					$(elem).attr('style', "fill:"+hex);
					total++;
				}
			});

			setTimeout(function(){
				$('#svg-image').hide();
				alert('Complete, changed: '+changed+'/'+total);
			}, 500);
		}
	</script>
	<style>
		body{
			overflow: scroll;
		}
	</style>
</body>
</html>
