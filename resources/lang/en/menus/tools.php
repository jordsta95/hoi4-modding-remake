<?php

return[
	'focus_tree' => 'Focus Tree',
	'focus_tree_description' => 'Build your own focus tree to decide the fate of any nation',
	'event' => 'Events',
	'event_description' => 'Create events to change the course of history',
	'idea' => 'Ideas',
	'idea_description' => 'Create ideas and national spirits to enhance a country',
	'country' => 'Country',
	'country_description' => 'Create your own, or edit an existing, country',
	'decision' => 'Decision',
	'decision_description' => 'Create decisions to shape the path a nation takes',
	'ideology' => 'Ideology',
	'ideology_description' => 'Create custom ideologies for all nations to subscribe to',
	'start_dates' => 'Start Dates',
	'start_dates_description' => 'Create custom start dates for when players can start your mod in',
	
	'focus_tree_menu' => [
		'create_focus_tree' => 'Create Focus Tree',

		'create_focus' => 'Create Focus',
		'delete' => 'Delete',
		'import' => 'Import',
		'report_bug' => 'Report Bug',
		'help' => 'Help',
		'multi_select' => 'Multi-select'
	],


	'mod_menu' => [
		'create' => 'Create Mod',
		'add_member' => 'Add Member',
		'edit_details' => 'Edit Details',
		'delete_mod' => 'Delete Mod',
		'export_mod' => 'Export Mod',
		'edit' => 'Edit Mod'
	]
];