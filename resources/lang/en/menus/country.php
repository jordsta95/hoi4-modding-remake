<?php

return [
	'start_date' => 'Start Date',
	'ideologies' => 'Ideologies',
	'government' => 'Government',
	'military' => 'Military',
	'misc' => 'Misc.',
	'states' => 'States',
	'divisions' => 'Divisions',
	'research' => 'Research'
];