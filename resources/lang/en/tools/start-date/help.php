<?php

return [
	'no_start_date' => 'No start dates created for this mod',
	'create' => 'Create Start Date',
	'create_modal' => [
		'title' => 'Create Start Date',
		'mod' => 'Mod',
		'mod_help' => 'Which mod are you adding this start date to?',
		'name' => 'Name',
		'name_help' => 'Name for the start date to show in the menu.',
		'date' => 'Date',
		'date_help' => 'Date to start the game at in YYYY.MM.DD format.',
		'lang' => 'Language',
		'lang_help' => 'Which game language version is this for?',
		'submit' => 'Submit'
	],
	'import_bookmark' => 'Bookmark',
	'import_bookmark_help' => 'The bookmark file contains all necessary info for your start date to be added. It can be found at: <small>C:\Program Files (x86)\Steam\steamapps\common\Hearts of Iron IV\common\bookmarks</small> for default install on Windows.',
	'import_localisation' => 'Localisation',
	'import_localisation_help' => 'Localisation file can be found at: <small>C:\Program Files (x86)\Steam\steamapps\common\Hearts of Iron IV\localisation\bookmarks_l_[language].yml</small>, where [language] is the language of the mod, for default install on Windows.',
	'menu' => [
		'add_country' => 'Add Country'
	],
	'edit' => [
		'title' => 'Edit Start Date',
		'name' => 'Name',
		'name_help' => 'Name for the start date.',
		'description' => 'Description',
		'description_help' => 'The description for the start date.',
		'date' => 'Date',
		'date_help' => 'Date to start the game at in YYYY.MM.DD format.',
		'default' => 'Default',
		'default_help' => 'Is this the start date the game should automatically select when starting a new game?',
		'default_country' => 'Default Country',
		'default_country_help' => 'This is the tag for the country which will be selected by default when this start date is selected.',
		'countries' => 'Countries'
	],
	'country' => [
		'edit_country' => 'Edit Country',
		'tag' => 'Tag',
		'tag_help' => 'Country code, usually 3 characters (e.g. GER for Germany), that you want to appear on the country select screen',
		'history' => 'History',
		'history_help' => 'A brief description of the history of the nation',
		'ideology' => 'Ideology',
		'ideology_help' => 'What ideology should show under the nation on the country select screen',
		'available' => 'Available',
		'available' => 'When should this country appear on the country select screen?',
		'ideas' => 'Ideas',
		'ideas_help' => 'Add up to 3 idea IDs (comma separated) to show as a preview for the nation.',
		'focuses' => 'Focuses',
		'focuses_help' => 'Add up to 3 focus IDs (comma separated) to show as a preview for the nation.',
		'minor' => 'Minor',
		'minor_help' => 'The nation will appear as a minor nation on the select screen',
		'ideologies' => [
			'democratic' => 'Democracy',
			'communism' => 'Communism',
			'fascism' => 'Fascism',
			'neutrality' => 'Non-Aligned'
		]
	]
];