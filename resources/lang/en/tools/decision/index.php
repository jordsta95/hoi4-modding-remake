<?php

return [
	'create_decision' => 'Create Decision',
	'no_decision' => 'No decision categories created for this mod',
	'create_modal' => [
		'title' => 'Create New Decisions',
		'name' => 'Name',
		'name_help' => 'The name of the category the decisions should fall under, e.g. "Prospect for Resources"',
		'mod' => 'Mod',
		'mod_help' => 'Which mod will this decision category be assigned to?',
		'language' => 'Language',
		'language_help' => 'Which game language should these files be assigned to?',
		'submit' => 'Submit'
	]
];