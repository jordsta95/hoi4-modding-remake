<?php
return [
	'edit_military' => 'Edit Military', //Title for edit box
	'army' => 'Army', //Whether they are creating an army/navy military leader
	'navy' => 'Navy',
	'name' => 'Name', //Name of the military leader they are creating
	'traits' => 'Traits', //Traits the leader has
	'overall' => 'Overall', //Overall level
	'attack' => 'Attack', //Attack skill level
	'defence' => 'Defence', //Defence skill level
	'logistics' => 'Logistics', //Logistics skill level,
	'planning' => 'Planning', //Planning skill level
	'coordination' => 'Coordination', //Coordination skill level
	'manuevering' => 'Manuevering', //Manuevering skill level
	'is_field_marshal' => 'Is field marshal?', //If the army leader is a field marshal [checkbox]
	'submit' => 'Submit',
];