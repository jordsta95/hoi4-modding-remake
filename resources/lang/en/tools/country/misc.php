<?php

return [
	'names' => 'Names', //Title for names
	//Names
	'male_name' => 'Male names',
	'female_name' => 'Female names',
	'surname' => 'Surnames',
	'callsign' => 'Callsigns',
	'name_help' => 'List of names that have been comma-separated. E.g. "John, Alex, David, ..."',

	//Tag
	'tag' => 'Tag',
	'tag_help' => 'Unique code that identifies your country. Must be alphanumeric using valid ASCII (English) characters only; ABC, BA1, etc.',

	//Colour
	'colour' => 'Colour', //Colour title
	'red' => 'R',
	'green' => 'G',
	'blue' => 'B',

	//Culture/Capital
	'culture_capital' => 'Culture/Capital', //Title for section

	//Culture
	'culture' => 'Culture',
	'culture_help' => 'This determines the ethnicity of the generals the game will generate when the AI/player has to use Political Power to get more.',

	'cultures' => [
		'western_european' => 'European',
		'commonwealth' => 'Commonwealth',
		'african' => 'African',
		'asian' => 'Asian',
		'southamerican' => 'South American' 
	],



	//Capital
	'capital' => 'Capital',
	'capital_help' => 'Select which state contains your nation\'s capital.',

	//Misc Data
	'war_support' => 'War Support',
	'war_support_help' => 'Starting amount of war support, as a percentage (0.1 = 10%)',
	'stability' => 'Stability',
	'stability_help' => 'Starting stability, as a percentage (0.1 = 10%)',
	'research_slots' => 'Research Slots',
	'research_slots_help' => 'Number of research slots to start with',
	'convoys' => 'Convoys',
	'convoys_help' => 'Number of convoys to start with',
	'faction' => 'Create Faction',
	'faction_help' => 'Create this faction, as the leader, at the start of the game.',
	'add_to_faction' => 'Add to Faction',
	'add_to_faction_help' => 'Comma-separated list of nations which should join the faction, if one is created. E.g. GER, FRA, BEL',
	'give_military_access' => 'Give Military Access',
	'give_military_access_help' => 'Comma-separated list of nations which should be given military access. E.g. GER, FRA, BEL',

	'politic_title' => 'Politics', //Title for section
	//Politics
	'politics' => [
		'last_election' => 'Last Election',
		'last_election_help' => 'When was the last election in the country in YYYY.MM.DD. E.g. 1920.12.01',
		'election_frequency' => 'Election Frequency',
		'election_frequency_help' => 'How many months between elections, if the ideology allows them.',
		'elections_allowed' => 'Elections Allowed?',
		'elections_allowed_help' => 'Are elections allowed by default?',
		'ruling_party' => 'Ruling Party',
		'ruling_party_help' => 'Which party is ruling the nation at the start of the game?'
	],

	'foreign_relations' => 'Foreign Relations', //Title for add to faction/allow other nations military access
	'misc' => 'Misc.', //Title for Misc. section
	'national_spirits' => 'National Spirits',
	'national_spirits_desc' => 'These are the national spirits the country should start the game with.',
	'national_spirit_choose_list' => 'Select list to choose from:',
	'national_spirit_no_options' => 'You have no idea lists with a National Spirit in it.',
	'national_spirit_title' => 'Select your national spirits',
	'autonomy' => 'Autonomy',
	'autonomy_desc' => 'Choose nations to be your subjects along with their autonomy level.',

	'subject_tag' => 'Tag',
	'subject_tag_help' => 'The tag of the nation you want as a subject',
	'autonomy_level' => 'Autonomy Level',
	'autonomy_level_help' => 'How autonomous this nation should be.',
	'add_subject' => 'Add Subject'
];