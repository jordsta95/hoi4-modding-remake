<?php
return [
	'title' => 'Choose start date',
	'info' => 'Select start date below. Data which is not added/edited in a later start date, will use info from prior start date(s)'
];