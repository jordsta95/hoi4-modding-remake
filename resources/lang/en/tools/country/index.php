<?php

return [
	'no_country' => 'No countries created for this mod',
	'no_start_date' => 'Unable to create country as mod has start date overhaul set, but no start dates could be found',
	'no_ideology' => 'Unable to create country as mod has ideology overhaul set, but no ideologies could be found',
];