<?php
return [
	'create_modal' => [
		'title' => 'Create new country',
		'submit' => 'Create Country',
		'mod' => 'Mod',
		'mod_help' => 'Select which mod the country should be assigned to.',
		'tag' => 'Country Tag',
		'tag_help' => 'The tag for this nation. Select from the list below, or type a unique tag for a new nation.',
		'name' => 'Name',
		'name_help' => 'This is the name used to refer to country generally; you will be able to edit the name for each ideology later.',
		'lang' => 'Language',
		'lang_help' => 'Which language should the exported files for the country be assigned to?',
		'type' => 'Mod type',
		'type_id' => 'Changing this will define which ideologies and start dates you have available. Only change this if you are creating a sub-mod.',
		'modTypes' => [
			'vanilla' => 'Vanilla', //ONLY EDIT THIS ONE,
		]
		
	],
	'country_deleted' => 'Your country has successfully been deleted',
	'map_load' => 'Load Map - Large file download'
];