<?php

return [
	'control' => 'Control this state?', //[checkbox] - Checking the box will cause the current country to become the owner of the state
	'core' => 'Core this state?', //[checkbox] - Checking the box will cause the current country to claim to own the territory (e.g. Ireland claim Northern Ireland)
	'submit' => 'Submit'
];