<?php
return [
	'division_designer' => 'Division designer', //Title for division designer
	'unit_name' => 'Unit name', //Name for unit type (e.g. Panzer Brigade)
	'save' => 'Save', //Save button
	'deployed' => 'Divisions deployed', //Title for division list for deployement
	'division_location' => 'Select division locations', //Location selector title
	'update' => 'Update', //Update locations
];