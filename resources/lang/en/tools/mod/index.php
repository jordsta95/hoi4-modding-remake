<?php 

return [
	'not_logged_in' => 'Create an account or login to start creating mods',
	'no_mods_header' => 'Nothing to see here',
	'no_mods' => "Looks like you haven't created any mods yet. Press 'Create mod' in the menu to start.",

	'create_mod' => 'Create a mod',
	'name' => 'Mod name',
	'name_help' => 'Name used to identify the mod on this site. The name given here does not affect the mod in the game.',
	'description' => 'Description',
	'description_help' => 'Description of the mod. The description given here does not affect the mod in the game.',
	'submit' => 'Submit',
	'created' => 'Mod successfully created',

	'creator' => 'Creator',

	'preview' => [
		'focus_trees' => ':num Focus Trees',
		'events' => ':num Event Lists',
		'ideas' => ':num Idea Lists',
		'countries' => ':num Countries',
		'decisions' => ':num Decision Categories',
		'bookmarks' => ':num Start Dates',
		'ideologies' => ':num Ideologies'
	],

	'ideology_overhaul' => 'Ideology Overhaul',
	'ideology_overhaul_help' => 'Do you want this mod to replace the default ideologies in game?',
	'start_date_overhaul' => 'Start Date Overhaul',
	'start_date_overhaul_help' => 'Do you want this mod to replace the default start dates in game?',
	'country_overhaul' => 'Country Overhaul',
	'country_overhaul_help' => 'Do you want this mod to remove all the default countries in the game?',

	'deleted' => 'Mod successfully deleted',

	'export' => [
		'error' => [
			'focus_tree' => 'Error exporting focus tree with the tag :tag',
			'country' => 'Error exporting country with the tag :tag',
			'ideas' => 'Error exporting idea lists',
			'events' => 'Error exporting events lists',
			'start_date' => 'Error exporting start dates',
			'ideology' => 'Error exporting ideologies',
			'country_state' => 'Error exporting states',
			'country_colour' => 'Error exporting country colours',

			'country_history' => 'Error exporting country history file :tag',
			'decision' => 'Error exporting decision files'
		]
	]
];