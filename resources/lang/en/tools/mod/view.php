<?php


return [
	'nothing' => 'Nothing available to download',
	'team' => 'Team:',

	'tabs' => [
		'focus_trees' => 'Focus Trees',
		'events' => 'Events',
		'ideas' => 'Ideas',
		'countries' => 'Countries',
		'decisions' => 'Decisions',
		'bookmark' => 'Start Dates',
		'ideology' => 'Ideologies'
	],

	'coming_soon' => 'Coming soon!',
	'coming_soon_text' => 'This section is currently in development, and will be updated soon.',

	'delete' => 'Are you sure you want to delete this item?',
	'delete_extra' => 'This action is irreversable, and you will not be able to retrieve the data.',
	'delete_confirm' => 'Delete!',
	'delete_cancel' => 'Cancel',

	'edit_focus_tree' => 'Edit Focus Tree - :tag',
	'update_focus_tree' => 'Update',
	'tag' => 'Tag',
	'prefix' => 'Prefix',
	'name' => 'Name',
	'tree_id' => 'Focus Tree ID',
	'language' => 'Language',

	'ideology_overhaul' => 'Ideology Overhaul',
	'start_date_overhaul' => 'Start Date Overhaul',
	'country_overhaul' => 'Country Overhaul',

	'delete' => 'Delete Item?',
	'delete_mod' => 'Delete Mod?',
	'confirm_delete_box' => [
		'title' => 'Are you sure you want to delete this mod?',
		'content' => 'This action is permanent. Your mod, and all related content to it, will no longer be accessible.',
		'confirm' => 'Yes, delete!',
		'cancel' => 'No, keep mod'
	],
	'edit_ideas_group' => 'Edit Ideas Group - :name',
	'updated_ideas_group' => 'Successfully updated group - :name',
	'delete_ideas_group' => 'Successfully deleted group - :name',
	'edit_event_group' => 'Edit Event Group - :name',
	'updated_single' => 'Successfully updated :name',
	'delete_single' => 'Successfully deleted :name',

	'country_edit' => [
		'edit_ideology' => 'Edit available ideologies',
		'edit_start_date' => 'Edit available start dates',
		'name' => 'Name',
		'add' => 'Add',
		'remove' => 'Remove',
		'submit' => 'Submit',
		'date' => 'Date'
	]
];