<?php

return[
	'no_ideas' => 'No ideas created for this mod',
	'create_ideas' => 'Create Ideas',
	'create_idea' => 'Create Idea',

	'create_modal' => [
		'title' => 'Create Ideas',
		'name' => 'Name',
		'name_help' => 'This field will be used in the filename, when you export your mod, and across the site.',
		'prefix' => 'Prefix',
		'prefix_help' => 'Changing this will prefix all your ideas\' ids, allowing you to have two ideas with the same name, but different prefixes.',
		'lang' => 'Language',
		'lang_help' => 'Which language should the exported files be assigned to?',
		'mod' => 'Mod',
		'mod_help' => 'Select which mod these ideas should be assigned to.',
		'submit' => 'Submit'
	],
	'groups' => [
		'economy' => 'Economy',
		'mobilization_laws' => 'Mobilization Laws',
		'political_advisor' => 'Political Advisor',
		'theorist' => 'Theorist',
		'high_command' => 'High Command',
		'army_chief' => 'Army Chief',
		'air_chief' => 'Air Chief',
		'navy_chief' => 'Navy Chief',
		'country' => 'Country (National Spirit)',
		'trade_laws' => 'Trade Laws',
		'tank_manufacturer' => 'Tank Manufacturer',
		'naval_manufacturer' => 'Naval Manufacturer',
		'aircraft_manufacturer' => 'Aircraft Manufacturer',
		'industrial_concern' => 'Industrial Concern',
		'materiel_manufacturer' => 'Weapons Manufacturer',
	],
	'ledger' => [
		'army' => 'Army',
		'navy' => 'Navy',
		'air' => 'Air Force'
	],
	'edit' => [
		'add' => 'Add Idea',
		'edit' => 'Edit Idea',
		'name' => 'Name',
		'name_help' => 'The name which should show for this idea when in game',
		'group' => 'Group',
		'group_help' => 'Where this idea should be found in government. If not in government, then select "Country".',
		'options' => [
			'description' => 'Description',
			'description_help' => 'This will show when hovering over the idea in game',
			'ledger' => 'Ledger',
			'ledger_help' => 'Which branch of the armed forces does this relate to?',
			'traits' => 'Traits',
			'traits_help' => 'Determines which traits this leader has.',
			'cost' => 'Cost',
			'cost_help' => 'How much political power this idea costs to add to a nation.',
			'removal_cost' => 'Removal Cost',
			'removal_cost_help' => 'Cost to remove this idea from a country.',
			'ai_will_do' => 'AI Will Do',
			'ai_will_do_help' => 'How likely the AI is to pick this idea. Higher number = greater chance to pick.',
			'allowed' => 'Allowed',
			'allowed_help' => 'Determines whether or not a country is able to get this idea at any point based on what is set here. If empty, all countries will be allowed.',
			'allowed_civil_war' => 'Allowed During Civil War',
			'allowed_civil_war_help' => 'Same as allowed, but determines what happens with the idea in the case of a civil war. E.g. if an idea goes to the communist side in a civil war.',
			'available' => 'Available',
			'available_help' => 'Determines when the idea is available to be selected/added to a country.',
			'visible' => 'Visible',
			'visible_help' => 'Determines whether an idea is visible to the player.',
			'cancel' => 'Cancel',
			'cancel_help' => 'Remove the idea if these conditions are met.',
			
			'equipment_bonus' => 'Equipment Bonus',
			'equipment_bonus_help' => 'Adds a modifier to the specificed equipment.',
			'research_bonus' => 'Research Bonus',
			'research_bonus_help' => 'Adds a modifier to the research speed for selected technology category(ies).',
			'targeted_modifier' => 'Targeted Modifier',
			'targeted_modifier_help' => 'Specific modifiers with target relationship.',
			'modifier' => 'Modifier',
			'modifier_help' => 'Full list of modifiers can be found on the Paradox Wiki',//Do no translate "Paradox Wiki"
			'on_add' => 'On Add',
			'on_add_help' => 'Effects which should take place when this idea is added to a country.',
			'on_remove' => 'On Remove',
			'on_remove_help' => 'Effects which should take place when this idea is removed from a country.'
		],
		'game_rules' => [
			'can_be_called_to_war' => '',
			'can_boost_other_ideologies' => '',
			'can_create_factions' => '',
			'can_declare_war_on_same_ideology' => '',
			'can_declare_war_without_wargoal_when_in_war' => '',
			'can_decline_call_to_war' => '',
			'can_force_government' => '',
			'can_generate_female_aces' => '',
			'can_guarantee_other_ideologies' => '',
			'can_join_factions' => '',
			'can_join_factions_not_allowed_diplomacy' => '',
			'can_join_opposite_factions' => '',
			'can_lower_tension' => '',
			'can_not_declare_war' => '',
			'can_occupy_non_war' => '',
			'can_only_justify_war_on_threat_country' => '',
			'can_use_kamikaze_pilots' => '',
			'units_deployed_to_overlord' => ''
		],
		'targeted_modifiers' => [ //Only change those labelled "name"
			'cic_to_target_factor' => [
				'name' => 'Gives a portion of the country\'s civilian industry to the specified target.',
				'example' => 'cic_to_target_factor = 0.5'
			],
			'extra_trade_to_target_factor' => [
				'name' => 'Adds extra produced recourses available for trade to target country.',
				'example' => 'extra_trade_to_target_factor = 0.5'
			],
			'generate_wargoal_tension_against' => [
				'name' => 'Changes world tension necessary for us to justify against the target country.',
				'example' => 'generate_wargoal_tension_against = 0.5'
			],
			'mic_to_target_factor' => [
				'name' => 'Gives a portion of the country\'s military industry to the specified target.',
				'example' => 'mic_to_target_factor = 0.5'
			],
			'trade_cost_for_target_factor' => [
				'name' => 'The cost for the targeted country to purchase this country\'s resources.',
				'example' => 'trade_cost_for_target_factor = 0.5'
			],
			'targeted_legitimacy_daily' => [
				'name' => 'Changes daily gain of legitimacy of the target country.',
				'example' => 'targeted_legitimacy_daily = 0.5'
			],
			'attack_bonus_against' => [
				'name' => 'Gives an attack bonus against the armies of the specified country.',
				'example' => 'attack_bonus_against = 0.5'
			],
			'attack_bonus_against_cores' => [
				'name' => 'Gives an attack bonus against the armies of the specified country on its core territory.',
				'example' => 'attack_bonus_against_cores = 0.5'
			],
			'breakthrough_bonus_against' => [
				'name' => 'Gives a breakthrough bonus against the armies of the specified country.',
				'example' => 'breakthrough_bonus_against = 0.5'
			],
			'defense_bonus_against' => [
				'name' => 'Gives a defense bonus against the armies of the specified country.',
				'example' => 'defense_bonus_against = 0.5'
			]
		]
	]
];