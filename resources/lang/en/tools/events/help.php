<?php

return [
	'no_events' => 'No events created for this mod',
	'create_events' => 'Create Events',
	'create_event' => 'Create Event',

	'create_modal' => [
		'title' => 'Create Events',
		'name' => 'Name',
		'name_help' => 'This field will be used in the filename, when you export your mod, and across the site.',
		'prefix' => 'Prefix',
		'prefix_help' => 'Changing this will prefix all your events\' ids, allowing you to have two ideas with the same name, but different prefixes.',
		'lang' => 'Language',
		'lang_help' => 'Which language should the exported files be assigned to?',
		'mod' => 'Mod',
		'mod_help' => 'Select which mod these events should be assigned to.',
		'submit' => 'Submit'
	],

	'news_title' => 'World News',

	'edit' => [
		'add' => 'Add event',
		'edit' => 'Edit event',
		'country' => 'Country event',
		'news' => 'News event',
		'name' => 'Name',
		'name_help' => 'Title to appear when event is triggered.',
		'type' => 'Event Type',
		'type_help' => 'Which style the event should show as in game.',
		'options' => [
			'description' => 'Description',
			'description_help' => 'Extra text to show under the title when the event has been triggered.',
			'fire_only_once' => 'Fire Only Once?',
			'fire_only_once_help' => 'If set the event can only occur once.', 
			'is_triggered_only' => 'Is Triggered Only?',
			'is_triggered_only_help' => 'If set, the event can only be triggered through other events, focuses, or decisions.',
			'trigger' => 'Trigger',
			'trigger_help' => 'What conditions must be met for this event to be triggered automatically.',
			'mean_time_to_happen' => 'Mean Time to Happen',
			'mean_time_to_happen_help' => 'How many days, on average, should it take for the event to trigger when all conditions are met?',
			'timeout_days' => 'Timeout Days',
			'timeout_days_help' => 'How many days can the player wait before the first option is automatically chosen for them?',
			'fire_for_sender' => 'Fire For Sender?',
			'fire_for_sender_help' => 'If set, the country which triggered the event will not be able to see it.',
			'hidden' => 'Hidden?',
			'hidden_help' => 'The event will not be shown to the target country, but the effects will still take place.',
			'major' => 'Major?',
			'major_help' => 'If set, the event will be shown to all countries.'
		],

		'option_edit' => [
			'add' => 'Add option',
			'edit' => 'Edit option',
			'name' => 'Name',
			'name_help' => 'What should appear in the button the player presses?',
			'options' => [
				'trigger' => 'Trigger',
				'trigger_help' => 'What conditions must be met for this option to be available to the player?',
				'ai_chance' => 'AI Chance',
				'ai_chance_help' => 'What is the chance the AI will choose this option. Higher number = more likely.',
				'effect' => 'Effect',
				'effect_help' => 'What should happen when this option is chosen. Anything in here will be visible to the player.',
				'hidden_effect' => 'Hidden effect',
				'hidden_effect_help' => 'What should happen when this option is chosen. Anything in here will not be visible to the player.' 
			]
		]
	]
];