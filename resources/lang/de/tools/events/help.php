<?php

return [
	'no_events' => 'Noch keine Events für diesen Mod erstellt',
	'create_events' => 'Events erstellen',
	'create_event' => 'Event erstellen',

	'create_modal' => [
		'title' => 'Events erstellen',
		'name' => 'Name',
		'name_help' => 'Dieses Feld wird in dem Dateinamen, wenn du deinen Mod exportierst, und überall auf der Seite verwendet.',
		'prefix' => 'Vorsilbe',
		'prefix_help' => 'Wenn dies geändert wird, wird eine Vorsilben an die IDs von Events hinzugefügt. Dies erlaubt dir zwei Ideen mit gleichem Namen, aber mit verschiedenen Vorsilben zu haben.',
		'lang' => 'Sprache',
		'lang_help' => 'Zu welcher Sprache sollen die exportierten Datein hinzugefügt werden?',
		'mod' => 'Mod',
		'mod_help' => 'Wähle aus zu welchem Mod diese Events hinzugefügt werden sollen.',
		'submit' => 'Erstellen'
	],

	'news_title' => 'Weltnachrichten',

	'edit' => [
		'add' => 'Event hinzufügen',
		'edit' => 'Event bearbeiten',
		'country' => 'Land Event',
		'news' => 'Nachrichten Event',
		'name' => 'Name',
		'name_help' => 'Titel der erscheint wenn dieses Event ausgelöst wird.',
		'type' => 'Event Typ',
		'type_help' => 'Welche Art von Event im Spiel angezeigt werden soll.',
		'options' => [
			'description' => 'Beschreibung',
			'description_help' => 'Zusätzlicher Text der unter dem Titel gezeigt wird wenn das Event ausgelöst wird.',
			'fire_only_once' => 'Nur einmal auslösen?',
			'fire_only_once_help' => 'Sorgt dafür, dass das Event nur einmal ausgelöst wird.', 
			'is_triggered_only' => 'Nur wenn ausgelöst?',
			'is_triggered_only_help' => 'Sorgt dafür, dass das Event nur durch andere Events, Schwerpunkte, oder Entscheidungen ausgelöst wird.',
			'trigger' => 'Auslöser',
			'trigger_help' => 'Welche Bedingungen erfüllt werden müssen, damit das Event automatisch ausgelöst wird.',
			'mean_time_to_happen' => 'Zwischenzeit',
			'mean_time_to_happen_help' => 'Wie viele Tage sollte es, im Durchschnitt, dauern bis das Event ausgelöst wird, wenn alle Bedingungen erfüllt sind?',
			'timeout_days' => 'Timeout-Tage',
			'timeout_days_help' => 'Wie viele Tage kann der Spieler warten bevor die erste Option automatisch für ihn gewählt wird?',
			'fire_for_sender' => 'Versteckt für Auslöser?',
			'fire_for_sender_help' => 'Das Land, welches das Event ausgelöst hat, ist nicht in der Lage es zu sehen.',
			'hidden' => 'Versteckt?',
			'hidden_help' => 'Das Event wird nicht für das Zielland sichtbar sein, die Effekte werden jedoch dennoch stattfinden.
',
			'major' => 'Großes Ereignis?',
			'major_help' => 'Das Event wird in allen Ländern zu sehen sein'
		],

		'option_edit' => [
			'add' => 'Option hinzufügen',
			'edit' => 'Option bearbeiten',
			'name' => 'Name',
			'name_help' => 'Was soll in der Schaltfläche, die der Spieler drückt, erscheinen?',
			'options' => [
				'trigger' => 'Auslöser',
				'trigger_help' => 'Welche Bedingungen müssen erfüllt werden, bevor diese Option für den Spieler verfügbar ist?',
				'ai_chance' => 'KI Chance',
				'ai_chance_help' => 'Wie hoch ist die Chance, dass die KI diese Option auswählt? Höhere Zahl = höhere Chance.',
				'effect' => 'Effekt',
				'effect_help' => 'Was passieren soll, wenn diese Option ausgewählt ist. Alles in hier ist sichtbar für den Spieler.',
				'hidden_effect' => 'Versteckter Effekt',
				'hidden_effect_help' => 'Was passieren soll, wenn diese Option ausgewählt ist. Alles in hier ist sichtbar für den Spieler.' 
			]
		]
	]
];