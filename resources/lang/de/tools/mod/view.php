<?php


return [
	'nothing' => 'Nichts zum herunterladen verfügbar',
	'team' => 'Team:',

	'tabs' => [
		'focus_trees' => 'Schwerpunktbäume',
		'events' => 'Ereignisse',
		'ideas' => 'Ideen',
		'countries' => 'Länder',
		'decisions' => 'Entscheidungen'
	],

	'coming_soon' => 'Kommt bald!',
	'coming_soon_text' => 'Dieser Abschnitt wird im Moment entwickelt und wird bald aktualisiert.',

	'delete' => 'Bist du sicher, dass du dieses Element löschen willst?',
	'delete_extra' => 'Diese Aktion kann nicht rückgängig gemacht werden und es wird unmöglich sein die Daten zurückzuholen.',
	'delete_confirm' => 'Löschen!',
	'delete_cancel' => 'Abbrechen',

	'edit_focus_tree' => 'Bearbeite den Schwerpunktbaum - :tag',
	'update_focus_tree' => 'Aktualisieren',
	'tag' => '\'Tag\'',
	'name' => 'Name',
	'tree_id' => 'Schwerpunktbaum ID',
	'language' => 'Sprache',

	'delete' => 'Diesen Mod löschen?',
	'confirm_delete_box' => [
		'title' => 'Bist du sicher, dass du diesen Mod löschen willst?',
		'content' => 'Diese Aktion ist permanent. Dein Mod und alle bezogenen Inhalte werden nicht länger zur Verfügung stehen.',
		'confirm' => 'Ja, löschen!',
		'cancel' => 'Nein, Mod behalten'
	],
	'edit_ideas_group' => 'Ideen Gruppe bearbeiten - :name',
	'updated_ideas_group' => 'Gruppe erfolgreich aktualisiert - :name', //Ideas & Events both use this, as they are stored as groups.
	'delete_ideas_group' => 'Gruppe erfolgreich gelöscht - :name',
	'edit_event_group' => 'Event Gruppe bearbeiten - :name',
];