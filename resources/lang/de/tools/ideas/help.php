<?php

return[
	'no_ideas' => 'Keine Ideen für diesen Mod erstellt',
	'create_ideas' => 'Ideen erstellen',
	'create_idea' => 'Idee erstellen',

	'create_modal' => [
		'title' => 'Ideen erstellen',
		'name' => 'Name',
		'name_help' => 'Dieses Feld wird als Dateiname, wenn du deinen Mod exportierst, und überall auf der Seite verwendet.',
		'prefix' => 'Vorsilbe',
		'prefix_help' => 'Wenn dies geändert wird, wird eine Vorsilben an die IDs von Events hinzugefügt. Dies erlaubt dir zwei Ideen mit gleichem Namen, aber mit verschiedenen Vorsilben zu haben.',
		'lang' => 'Sprache',
		'lang_help' => 'Zu welcher Sprache sollen die exportierten Datein hinzugefügt werden?',
		'mod' => 'Mod',
		'mod_help' => 'Wähle aus zu welchem Mod diese Events hinzugefügt werden sollen.',
		'submit' => 'Erstellen'
	],
	'groups' => [
		'economy' => 'Wirtschaft',
		'mobilization_laws' => 'Mobilisierungsgesetze',
		'political_advisor' => 'Politischer Berater',
		'theorist' => 'Theoretiker',
		'high_command' => 'Oberkommando',
		'army_chief' => 'Armeechef',
		'air_chief' => 'Luftwaffenchef',
		'navy_chief' => 'Marinechef',
		'country' => 'Land (Nationalgeist)'
	],
	'ledger' => [
		'army' => 'Armee',
		'navy' => 'Marine',
		'air' => 'Luftwaffe'
	],
	'edit' => [
		'add' => 'Idee hinzufügen',
		'edit' => 'Idee bearbeiten',
		'name' => 'Name',
		'name_help' => 'Welcher Name für diese Idee im Spiel angezeigt werden soll.',
		'group' => 'Gruppe',
		'group_help' => 'Wo diese Idee in der Regierung zu finden sein soll. Wenn sie nicht in der Regierung ist, wähle “Land” aus.',
		'options' => [
			'ledger' => 'Hauptbuch',
			'ledger_help' => 'Welchen Teil des Militär betrifft es?',
			'traits' => 'Merkmale',
			'traits_help' => 'Bestimmt welche Merkmale dieser Führer hat.',
			'cost' => 'Kosten',
			'cost_help' => 'Wie viel politische Krankt es kostet, diese Idee zu einer Nation hinzuzufügen.',
			'removal_cost' => 'Entfernungskosten',
			'removal_cost_help' => 'Wie viel politische Krankt es kostet, diese Idee zu einer Nation hinzuzufügen.',
			'ai_will_do' => 'KI Chance',
			'ai_will_do_help' => 'Wie hoch die Chance ist, dass die KI diese Idee auswählt. Höhere Nummer = höhere Chance ausgewählt zu werden.',
			'allowed' => 'Erlaubt',
			'allowed_help' => 'Bestimmt ob ein Land diese Idee bekommen kann oder nicht. Wenn dieses Feld leer ist, haben alle Länder Zugang zu der Idee.',
			'allowed_civil_war' => 'Während dem Bürgerkrieg erlaubt',
			'allowed_civil_war_help' => 'Das selbe wie “Erlaubt”, aber bestimmt was mit der Idee im Falle eines Bürgerkriegs passiert. z.B. falls eine Idee im Bürgerkrieg zu der kommunistischen Seite fällt.',
			'available' => 'Verfügbar',
			'available_help' => 'Bestimmt, ob die Idee zum auswählen/hinzufügen zu eine mLand verfügbar ist.',
			'visible' => 'Sichtbar',
			'visible_help' => 'Bestimmt, ob die Idee für den Spieler sichtbar ist oder nicht.',
			'cancel' => 'Abbrechen',
			'cancel_help' => 'Entfernung der Idee falls diese Bedingungen erfüllt werden.',
			
			'equipment_bonus' => 'Ausrüstungsbonus',
			'equipment_bonus_help' => 'Fügt einen Modifikator zu der ausgewählten Ausrüstung hinzu.',
			'research_bonus' => 'Nachforschungsbonus',
			'research_bonus_help' => 'Fügt einen Modifikator zum Nachforschungstempo für die ausgewählte(n) technologische(n) Kategorie(n) hinzu.',
			'targeted_modifier' => 'Gezielter Modifikator',
			'targeted_modifier_help' => 'Bestimmte Modifikatoren mit einer Zielbeziehung.',
			'modifier' => 'Modifikator',
			'modifier_help' => 'Komplette Liste der Modifikatoren findest du auf der Paradox Wiki',//Do no translate "Paradox Wiki"
			'on_add' => 'Beim hinzufügen',
			'on_add_help' => 'Welche Effekte in Kraft treten sollten wenn diese Idee zu einem Land hinzugefügt wird.',
			'on_remove' => 'Beim Entfernen',
			'on_remove_help' => 'Welche Effekte in Kraft treten sollten wenn diese Idee von einem Land entfernt wird.'
		],
		'game_rules' => [
			'can_be_called_to_war' => '',
			'can_boost_other_ideologies' => '',
			'can_create_factions' => '',
			'can_declare_war_on_same_ideology' => '',
			'can_declare_war_without_wargoal_when_in_war' => '',
			'can_decline_call_to_war' => '',
			'can_force_government' => '',
			'can_generate_female_aces' => '',
			'can_guarantee_other_ideologies' => '',
			'can_join_factions' => '',
			'can_join_factions_not_allowed_diplomacy' => '',
			'can_join_opposite_factions' => '',
			'can_lower_tension' => '',
			'can_not_declare_war' => '',
			'can_occupy_non_war' => '',
			'can_only_justify_war_on_threat_country' => '',
			'can_use_kamikaze_pilots' => '',
			'units_deployed_to_overlord' => ''
		],
		'targeted_modifiers' => [ //Only change those labelled "name"
			'cic_to_target_factor' => [
				'name' => 'Gibt einen Teil der Zivilindustrie an das festgelegte Ziel.',
				'example' => 'cic_to_target_factor = 0.5'
			],
			'extra_trade_to_target_factor' => [
				'name' => 'Überschüssige verfügbare Ressourcen werden für den Handel mit dem Zielland hinzugefügt.',
				'example' => 'extra_trade_to_target_factor = 0.5'
			],
			'generate_wargoal_tension_against' => [
				'name' => 'Ändert die notwendige Weltspannung um ein Kriegsziel gegen das Zielland zu rechtfertigen.',
				'example' => 'generate_wargoal_tension_against = 0.5'
			],
			'mic_to_target_factor' => [
				'name' => 'Gibt einen Teil der Militärindustrie des Landes an das festgelegte Ziel.',
				'example' => 'mic_to_target_factor = 0.5'
			],
			'trade_cost_for_target_factor' => [
				'name' => 'Die Kosten, für das Zielland, um die Ressourcen des Landes zu kaufen.',
				'example' => 'trade_cost_for_target_factor = 0.5'
			],
			'targeted_legitimacy_daily' => [
				'name' => 'Ändert den täglichen Legitimitätsgewinn des Ziellandes.',
				'example' => 'targeted_legitimacy_daily = 0.5'
			],
			'attack_bonus_against' => [
				'name' => 'Gibt einen Angriffsbonus gegen die Armeen des ausgewählten Landes.',
				'example' => 'attack_bonus_against = 0.5'
			],
			'attack_bonus_against_cores' => [
				'name' => 'Gibt einen Angriffsbonus gegen die Armeen des ausgewählten Landes in dessen Kerngebiet.',
				'example' => 'attack_bonus_against_cores = 0.5'
			],
			'breakthrough_bonus_against' => [
				'name' => 'Gibt einen Durchbruchbonus gegen die Armeen des ausgewählten Landes.',
				'example' => 'breakthrough_bonus_against = 0.5'
			],
			'defense_bonus_against' => [
				'name' => 'Gibt einen Verteidigungsbonus gegen die Armeen des ausgewählten Landes.',
				'example' => 'defense_bonus_against = 0.5'
			]
		]
	]
];