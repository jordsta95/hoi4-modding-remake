<?php

return [
	//Custom GFX modal
	'choose_gfx' => 'Wähle GFX',
	'custom_gfx' => 'Benutzerdefinierte GFX',
	'default_gfx' => 'Standart GFX',
	'search_gfx' => 'Suche nach GFX',
	'upload_custom_gfx' => 'Wähle eine neue benutzerdefinierte GFX',
	'submit_gfx' => 'Lade eine GFX hoch',

	//Confirm Delete
	'delete_title' => 'Bist du sicher, dass du das löschen willst?',
	'delete_text' => 'Sobald sie gelöscht wurden, können deine Datein nicht mehr zurückgeholt werden.', 
	'delete_confirm' => 'Ja, löschen!',
	'delete_cancel' => 'Abbrechen',

	//Import
	'import_title' => 'Importieren',
	'import_instruction' => 'Füge die unten stehenden Dateien hinzu und drücke \'Jetzt importieren\' darunter um die Dateien in deinen Mod zu importieren.',
	'import_now' => 'Jetzt importieren',
	'import_focus' => [
		'focus_tree' => 'Schwerpunktbaum Ordner',
		'focus_tree_help' => 'Für einen Standartspiel Schwerpunktbaum findest du den Schwerpunktbaum hier: <small>C:\Program Files (x86)\Steam\steamapps\common\Hearts of Iron IV\common\national_focus</small> basierend auf dem Standartspeicherplatz für Steam',
		'localisation' => 'Localisation Ordner',
		'localisation_help' => 'Für einen Standartspiel Schwerpunktbaum findest du den Lokalisierung Ordner in: <small>C:\Program Files (x86)\Steam\steamapps\common\Hearts of Iron IV\localisation</small>. Der Ordner focus_l_[language of the mod].yml ist für einen nicht-DLC Schwerpunktbaum, oder für ein Präfix mit dem DLC für einen DLC Baum, zB.: lar_focus_l_english.yml für einen La Resistance Schwerpunktbaum, in Englisch.'
	],

	//Builder
	'builder_title' => 'Bau-Tool',
	'builder_description' => 'Klicke auf \'+\' um einen neuen Artikel zu deinem Ergebnis hinzuzuzfügen. Wenn der Artikel ein \'scope\' und noch nicht fertig ist, wird ein neues \'+\' erscheinen. Wenn du dein Ergebnis fertig gebaut hast, klicke \'Erstellen\'.',
	'builder_submit' => 'Erstellen',
	'builder_cancel' => 'Abbrechen',
	'builder_search' => 'Suche Ergebnisse',

	//Bug Report
	'bug_report' => [
		'title' => 'Melde einen Bug',
		'page_info' => 'Webseiten Information',
		'additional_files' => 'Zusätzliche Dateien',
		'bug_title' => 'Kurze Beschreibung des Bugs',
		'bug_description' => 'Lange Beschreibung des Bugs',
		'report_bug' => 'Melde einen Bug',
		'close' => [
			'title' => 'Bug Meldung abbrechen',
			'description' => 'Text verwerfen und Bug Meldung abbrechen',
			'confirm' => 'Bestätigen',
			'cancel' => "Ich bin nicht fertig!"
		]
	],


	//Country import
	'import_country' => [
		'history' => 'Ländergeschichte Order',
		'history_help' => 'Für ein Standartspiel Land, kannst du die Datei, basierend auf deinem Standart Installations Standort für Steam, hier finden: <small>C:\Program Files (x86)\Steam\steamapps\common\Hearts of Iron IV\history\countries</small>',
		'localisation' => 'Lokalisation Datei (optional)',
		'localisation_help' => 'Alle Lokalisations Dateien die mit dem Land verbunden sind; countries_l_[country].yml gemeinsam mit allen die eine Beschreibung für die Anführer der Nation haben',
		'units' => 'Einheiten Datei (optional)',
		'units_help' => 'Alle Land-Einheiten Dateien die su importieren willst',//'All unit files, including "Naval" & "Naval_Legacy", for your country. For base game nation: <small>C:\Program Files (x86)\Steam\steamapps\common\Hearts of Iron IV\history\units</small>.',
		'common' => 'Gemeinsame Länder Datei (optional)',
		'common_help' => 'Für ein Standartspiel Land, kannst du die Datei, basierend auf deinem Standart Installations Standort für Steam, hier finden: <small>C:\Program Files (x86)\Steam\steamapps\common\Hearts of Iron IV\common\countries</small>'
	],

	'import_events' => [
		'events' => 'Events Datei',
		'events_help' => 'Für ein Standartspiel Land, kannst du die Datei, basierend auf deinem Standart Installations Standort für Steam, hier finden: <small>C:\Program Files (x86)\Steam\steamapps\common\Hearts of Iron IV\events</small>',
		'localisation' => 'Lokalisation Datei',
		'localisation_help' => 'Für ein Standartspiel Event, kannst du die Datei hier finden: <small>C:\Program Files (x86)\Steam\steamapps\common\Hearts of Iron IV\localisation</small>. Die Datei wird events_l_[language of the mod].yml, für Ideen ohne DLC sein und mit dem Vorwort DLC für jene die über ein DLC hinzugefügt wurden, z.B. lar_ideas_l_english.yml für La Resistance Events, in Englisch.'
	],
];