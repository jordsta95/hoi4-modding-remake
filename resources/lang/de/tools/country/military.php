<?php
return [
	'edit_military' => 'Militär bearbeiten', //Title for edit box
	'army' => 'Armee', //Whether they are creating an army/navy military leader
	'navy' => 'Marine',
	'name' => 'Name', //Name of the military leader they are creating
	'traits' => 'Eigenschaften', //Traits the leader has
	'overall' => 'Gesamtlevel', //Overall level
	'attack' => 'Attacke', //Attack skill level
	'defence' => 'Verteidigung', //Defence skill level
	'logistics' => 'Logistik', //Logistics skill level,
	'planning' => 'Planung', //Planning skill level
	'coordination' => 'Koordinierung', //Coordination skill level
	'manuevering' => 'Manövrieren', //Manuevering skill level
	'is_field_marshal' => 'Ist Feldmarschall?', //If the army leader is a field marshal [checkbox]
	'submit' => 'Hinzufügen',
];