<?php

return [
	'control' => 'Diesen Staat kontrollieren?', //[checkbox] - Checking the box will cause the current country to become the owner of the state
	'core' => 'Diesen Staat übernehmen?', //[checkbox] - Checking the box will cause the current country to claim to own the territory (e.g. Ireland claim Northern Ireland)
	'submit' => 'Hinzufügen'
];