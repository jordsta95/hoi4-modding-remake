<?php
return [
	'create_modal' => [
		'title' => 'Neues Land erstellen',
		'submit' => 'Land erstellen',
		'mod' => 'Mod',
		'mod_help' => 'Wähle aus zu welchem Mod dieses Land gehören soll.',
		'tag' => 'Länder-Tag',
		'tag_help' => 'Das Tag dieser Nation. Wähle aus der unterem Liste aus, oder tippe ein einzigartiges Tag für eine neue Nation ein.',
		'name' => 'Name',
		'name_help' => 'Dies ist der Name der generell benutzt wird um das Land zu erwähnen. Du kannst den Namen später für jede Ideologie bearbeiten.',
		'lang' => 'Sprache',
		'lang_help' => 'Welcher Sprache sollen die exportierten Datein hinzugefügt werden?',
		'type' => 'Mod Typ',
		'type_id' => 'Bestimmt welche Ideologien und Anfangsdatums verfügbar sind. Bearbeite dies nur, wenn du einen Sub-Mod erstellst.',
		'modTypes' => [
			'vanilla' => 'Vanilla',
		]
		
	],
	'country_deleted' => 'Dein Land wurde erfolgreich gelöscht.',
	'map_load' => 'Karte laden - große Datei runterladen'
];