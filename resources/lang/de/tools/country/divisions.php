<?php
return [
	'division_designer' => 'Divisionsdesigner', //Title for division designer
	'unit_name' => 'Einheitenname', //Name for unit type (e.g. Panzer Brigade)
	'save' => 'Speichern', //Save button
	'deployed' => 'Divisionen bereitgestellt', //Title for division list for deployement
	'division_location' => 'Wähle Divisionsorte', //Location selector title
	'update' => 'Aktualisieren', //Update locations
];