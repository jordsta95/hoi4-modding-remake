<?php

return [
	'name' => 'Country Name',
	'name_help' => 'Name of the country',
	'definition' => 'Country Definition Name',
	'definition_help' => 'Full name of the country, with definite article. E.g. "Russia" becomes "The Russian Federation".',
	'nationality' => 'Nationality Name',
	'nationality_help' => 'The name for people of the nation; E.g. "British" for United Kingdom.',
	'popularity' => 'Popularity',
	'popularity_help' => 'Popularity as a percentage of total for this ideology.',
	'flag' => [
		'title' => 'Flag',
		'text' => 'Upload PNG file for the flag you wish to use for this country.',
		'subtext' => 'Leave blank if editing an existing country and wish to use the default flag.',
	],
	'leader' => [
		'title' => 'Leader',
		'name' => 'Name',
		'name_help' => 'Name for the leader.',
		'traits' => 'Traits',
		'traits_help' => 'Optional. Traits that this leader has when in power.',
		'sub_ideology' => 'Sub-ideology',
		'sub_ideology_help' => 'Which variant of the ideology the leader aligns to.',
		'description' => 'Description',
		'description_help' => 'Add some backstory to your leader which can be viewed when hovering over them in game.',
		'expire' => 'Expire Date',
		'expire_help' => 'Date in YYYY.MM.DD format the leader should expire from useage'
	],

	'culture' => [
		'western_european' => 'European',
		'commonwealth' => 'Commonwealth',
		'african' => 'African',
		'asian' => 'Asian',
		'southamerican' => 'South American' 
	],

	/*
	mod_name => [
		<ideology> => nicename,
		<ideology>_sub =>[
			<subideology> => nicename
		]
	]
	*/
	'vanilla' => [
		'democratic' => 'Democracy',
		'democratic_sub' => [
			'liberalism' => 'Liberal',
			'conservatism' => 'Conservatist',
			'socialism' => 'Socialism'
		],
		'communism' => 'Communism',
		'communism_sub' => [
			'stalinism' => 'Stalinism',
			'leninism' => 'Leninism',
			'marxism' => 'Marxism',
			'anti_revisionism' => 'Anti-Revisionism',
			'anarchist_communism' => 'Anarchist'
		],
		'fascism' => 'Fascism',
		'fascism_sub' => [
			'nazism' => 'Nazism',
			'fascism_ideology' => 'Fascist',
			'gen_nazism' => 'Nazism',
			'falangism' => 'Falangism',
			'rexism' => 'Rexism'
		],
		'neutrality' => 'Non-Aligned',
		'neutrality_sub' => [
			'despotism' => 'Despotism',
			'oligarchism' => 'Oligarchism',
			'anarchism' => 'Anarchism',
			'moderatism' => 'Moderatism',
			'centrism' => 'Centrism'
		]
	]
];