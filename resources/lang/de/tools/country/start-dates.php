<?php
return [
	'title' => 'Wähle ein Startdatum',
	'info' => 'Wähle ein Startdatum. Daten, welche nicht in einem späteren Startdatum geändert/hinzugefügt werden, werden Informationen von früheren Startdaten verwenden'
];