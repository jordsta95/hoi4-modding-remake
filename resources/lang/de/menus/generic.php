<?php

return [
	'menu' => 'Menü',
	'home' => 'Home',
	'account' => 'Account',
	'forum' => 'Forum',
	'modding_tools' => 'Modding Werkzeuge',
	'your_mods' => 'Deine Mods',
	'support' => 'Unterstütze uns auf Patreon!',
	'your_account' => 'Dein Account',
	'your_account_text' => 'Gehe zu deinem Account, und bearbeite deine Einstellungen',
	'user_search' => 'Benutzer Suche', // <--
	'user_search_text' => 'Suche nach anderen Benutzern auf dieser Webseite',

	'unread_notifications' => 'Ungelesene Nachrichten',
	'mark_notifications_as_read' => 'Alle als gelesen markieren'
];