browserCheck();

window.globalTraitSelectorList = null;

//Add stuff for builder to local storage
$(document).ready(function(){
	if(!getCookie('CountryTags')){
		resetCountryTags();
	}

	if(!getCookie('BuilderResults') || !getBuilderResults().hasOwnProperty('version') || getBuilderResults().version != 1.1){
		if(!getBuilderResults() || !getBuilderResults().hasOwnProperty('version') || getBuilderResults().version != 1.1){
			resetBuilderResults();
		}
	}


	if(!getCookie('StateResults')){
		resetStateResults();
	}

	if(!getCookie('TraitsResults')){
		resetTraitsResults();
	}

	window.resetLocalData = function(){
		resetCountryTags();
		resetStateResults();
		resetTraitsResults();
		resetBuilderResults();
		alert('Reset cached data');
	}

	function resetCountryTags(){
		$.getJSON("/get-data/tags", function(data) {
			window.localStorage.setItem('CountryTags', JSON.stringify(data));
			setCookie('CountryTags', 'Set', 5);
		});
	}

	function resetBuilderResults(){
		$.getJSON("/get-data/builder", function(data) {
			window.localStorage.setItem('BuilderResults', JSON.stringify(data));
			setCookie('BuilderResults', 'Set', 5);
		});
	}

	function resetStateResults(){
		$.getJSON("/get-data/states", function(data) {
			window.localStorage.setItem('StateResults', JSON.stringify(data));
			setCookie('StateResults', 'Set', 5);
		});
	}

	function resetTraitsResults(){
		$.getJSON("/get-data/traits", function(data) {
			window.localStorage.setItem('TraitsResults', JSON.stringify(data));
			setCookie('TraitsResults', 'Set', 5);
		});
	}
});

window.getCountryTags = function(){
	return JSON.parse(window.localStorage.getItem('CountryTags'));
}

window.getBuilderResults = function(){
	return JSON.parse(window.localStorage.getItem('BuilderResults'));
}

window.getStateResults = function(){
	return JSON.parse(window.localStorage.getItem('StateResults'));
}

window.getTraitsResults = function(){
	return JSON.parse(window.localStorage.getItem('TraitsResults'));
}

window.setCookie = function(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays*24*60*60*1000));
	var expires = "expires="+ d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

window.getCookie = function(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for(var i = 0; i <ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return false;
}

if($('#pageloader').length){
	$(window).on('load', function(){
		$('#pageloader').addClass('loaded');
	});
}

$('a').on('click', function(e){
	if($(this).attr('target') || $(this).hasClass('custom-action')){
		return;
	}
	if($(this).attr('href') && ($(this).attr('href').charAt(0) != '#' || $(this).attr('href').charAt(0) != '?')){
		e.preventDefault();
		$('#loader').addClass('loading');
		var goToLink = $(this).attr('href');
		setTimeout(function(){
			window.location.href = goToLink;
		}, 120);
	}
});



window.getFileLanguages = function(){
	var lang = [
		{
			'value': 'english',
			'label': '🇬🇧 English'
		},
		{
			'value': 'french',
			'label': '🇫🇷 Français'
		},
		{
			'value': 'german',
			'label': '🇩🇪 Deutsch'
		},
		{
			'value': 'polish',
			'label': '🇵🇱 Polskie'
		},
		{
			'value': 'russian',
			'label': '🇷🇺 русский'
		},
		{
			'value': 'spanish',
			'label': '🇪🇸 Español'
		},
		{
			'value': 'braz_por',
			'label': '🇧🇷 Português'
		},
		{
			'value': 'simp_chinese',
			'label': '🇨🇳 中文'
		}
	];
	return lang;
};

function browserCheck(){
	var ua = window.navigator.userAgent;
	var msie = ua.indexOf("MSIE ");
	if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)){
		browserAlert('Outdated Browser', 'You are using an insecure browser, and should update to ensure you stay safe whilst navigating the web.<br><em>Some features on this site will not work on your browser!</em>');
		return;
	}
}


//Create a browser alert
function browserAlert(title, content, id){
	var dataId = '';
	if(id){
		dataId = id;
	}

	var string = '<div class="help-box browser-alert" data-id="'+dataId+'">';
		if(title){
			string += '<div class="help-box-header">'+title+'</div>';
		}
		if(content){
			string += '<div class="help-box-content'+content;
		}
		string += '<p class="browser-alert-button"><button class="button" onclick="removeAlert(this)">Ok</button></p></div>';
	string += '</div>';

	var div = document.createElement('div');
	div.innerHTML = string;
	document.body.appendChild(div);
}

//Remove browser alert
function removeAlert(elem){
	elem.parentNode.parentNode.parentNode.parentNode.removeChild(elem.parentNode.parentNode.parentNode);
}


document.body.addEventListener('click', function (event){
	//Show/Hide a Help Box
	if(document.getElementById('app').classList.contains('showHelpData')){
		if(event.srcElement.getAttribute('data-help')){
			document.getElementById('help-'+event.srcElement.getAttribute('data-help')).classList.toggle('open');
		}
	}
});

//Toggle whether help boxes should show
$('#toggleHelp').on('click', function(){
	$('#app').toggleClass('showHelpData');
	if(!$('#app').hasClass('showHelpData')){
		$('.help-box').removeClass('open');
	}
});

$('.help-box .close').on('click', function(){
	$(this).closest('.help-box').removeClass('open');
});

//Images to Base64
File.prototype.convertToBase64 = function(callback){
	var reader = new FileReader();
	reader.onload = function(e) {
		 callback(e.target.result)
	};
	reader.onerror = function(e) {
		 callback(null, e);
	};        
	reader.readAsDataURL(this);
};

$(".toBase64").on('change',function(){
	var alters = '#'+$(this).attr('data-alters');
	var selectedFile = this.files[0];
	selectedFile.convertToBase64(function(base64){
		$(alters).attr('src', base64);
	});
});
setTimeout(function(){
	$('.alert-message').each(function(){
		$(this).remove();
	});
}, 5000);


window.getInputObject = function(name, key) {
	if(key.includes(name)) {
		var splitStr = key.split(/\[|\]/g);
		 return {
			index: splitStr[1],
			key: splitStr[3],
		}
	}
	return null;
}

window.getFormDataByInputName = function(formID, name) {
	var formData = new FormData( document.getElementById(formID));
	var results = {};
	for (var key of formData.keys()) {
		var obj = getInputObject(name, key);
		if (obj) {
			if (results[obj.index]) {
				results[obj.index][obj.key] = formData.get(key);
			}else{
				results[obj.index] = formData.get(key);
			}
		}
	}
	return results;
}

$('.enterDeleteMode').on('click', function(){
	$('.enterDeleteMode').toggleClass('active');
	$('body').toggleClass('delete-mode');
});


window.preprocess = function(s) {
	let lines = s.split(/\r?\n/);
	let result = "";
	for(var i = 0; i < lines.length; ++i) {
		var line = lines[i];
		var index = line.indexOf("#");
		if(index != -1) {
			line = line.substring(0, index);
		}
		result += line + "\n";
	}
	return result;
}


window.hoiFileToJSONWithGlobalKey = function(s, level){
	window.hoiJsonImportGlobalKey = '';
	return hoiFileToJSON(s, level);
}


window.hoiFileToJSON = function(s, level, ml = 2) {
	let maxLevel = ml;
	s = preprocess(s);
	var key = "";
	var value = "";
	var buildingKey = true;
	var braceCount = 0; 
	var hadBraces = false;
	var json = {};
	if(window.hoiJsonImportGlobalKey == undefined){
		window.hoiJsonImportGlobalKey = '';
	}

	//For fucky keys
	if(['provinces', 'color', 'traits', 'dynamic_faction_names'].includes(hoiJsonImportGlobalKey)){
		buildingKey = false;
		key = hoiJsonImportGlobalKey;
	}

	for(var i = 0; i < s.length; ++i) {
		let c = s.charAt(i);
		
		
		//If the parser is currently expecting a key
		if(buildingKey) {
			//Ignore these characters because they won't be part of a key
			if(c === "{" || c === "}") {
				continue;
			}
			
			//As long as we don't hit the = character, we are still building a key.
			//The assumption here is that keys can contain space characters
			if(c !== '=') {
				key += c;
			}
			else {
				buildingKey = false;
				key = key.trim();
				window.hoiJsonImportGlobalKey = key;
			}
		}
		//The parser is expecting a value
		else {
			value += c;
			//if only whitespace
			//  continue
			if(c === "{") {
				++braceCount;
				hadBraces = true;
			}
			else if(c === "}") {
				--braceCount;
			}
			
			//If the braces are evenly matched,
			//and our value string is not just whitespace characters
			//and the next character to add is whitespace,
			//then we are done building the value string
			let isNoSpaceCheck = (['name', 'surnames'].includes(key));
			if(braceCount === 0 && /\S/.test(value) && (((/"/.test(c) || /\t/.test(c)) && value.length > 2 && isNoSpaceCheck) || /\s/.test(c) && !isNoSpaceCheck)) {
				value = value.trim();
				
				//In the stupid format, the same key can appear multiple times.
				//If this happens, then what we really want is to treat that key
				//as an array
				if(key in json) {
					
					//Convert value stored at that key to
					//an array if it isn't already one
					if(!Array.isArray(json[key])) {
						var obj = json[key];
						json[key] = [obj];
					}

					//If the value had {} characters, then
					//it will consist of other key/value pairs.
					if(hadBraces && level < maxLevel) {                    
						json[key] = json[key].concat(hoiFileToJSON(value, level+1, maxLevel));
						hadBraces = false;
					}
					else {
						json[key] = json[key].concat(value);
					} 
				}
				
				else {
					if(hadBraces && level < maxLevel) {                    
						json[key] = hoiFileToJSON(value, level+1, maxLevel);
						hadBraces = false;
					}
					else {
						json[key] = value;
					} 
				}
				
				
				buildingKey = true;
				key = "";
				value = "";
			}
		}
	}

	return json;
}

window.localisationToJSON = function(data){
	var lines = data.split("\n");
	var json = {};

	var attempts = 0;
	//Do twice, just in case the desc appears before the name in the file
	while(attempts < 1){
		lines.forEach(function(line){
			var split = line.split(":");
			//If there's no second object, it's probably the language declaration
			if(split[1]){
				var key = split[0].replace(/ /g, '');
				var second = split.slice(1).join(':');

				var value = '';
				var valueAppend = false;

				//Loop over chars, just to skip out on '0 "' - or any varient
				for(var i = 0; i < second.length; ++i) {
					let c = second.charAt(i);
					if(valueAppend){
						value += c;
					}
					//Once we hit ", then everything after is description
					if(c == '"'){
						valueAppend = true;
					}
				}
				//Replace last " in description
				value = value.replace(/"([^"]*)$/, '$1');

				var descKey = key.replace('_desc', '');
				if(!json.hasOwnProperty(key) && !json.hasOwnProperty(descKey)){
					json[key] = {'name':value};
				}else{
					if(json.hasOwnProperty(descKey)){
						json[descKey]['description'] = value;
					}
				}
			}
		});

		attempts++;
	}

	return json;
}

window.toggleImportModal = function(){
	$('.import-modal').toggleClass('open');
}

window.isJSON = function(str) {
	try {
		return JSON.parse(str);
	} catch (e) {
		return false;
	}
}

import domtoimage from 'dom-to-image';
$('#reportBug').on('click', function(e){
	e.stopPropagation();
	generateBugReportModal();
});

window.generateBugReportModalImageGenerated = false;
window.generateBugReportModal = function(){
	generateBugReportModalImageGenerated = false;
	$('body').append('<div class="loader-overlay"><svg class="feather-icon icon-loader"><use xlink:href="#loader"></use></svg></div>');
	domtoimage.toPng(document.getElementById('app')).then(function(url) {
		generateBugReportModalImageGenerated = true;
		var localisation = JSON.parse($('#tool-help-file').text());
		var bugModal = '<div class="bug-modal-wrapper">';
			bugModal += '<div class="bug-modal-inner">';
				bugModal += '<div class="help-box static full-width">';
					bugModal += '<div class="help-box-header">';
						bugModal += '<h5>'+localisation.bug_report.title+'</h5>';
						bugModal += '<div class="close" id="closeBugReport"><svg class="feather-icon icon-x"><use xlink:href="#x"></use></svg></div>';
					bugModal += '</div>';
					bugModal += '<div class="help-box-content">';
						bugModal += '<form method="POST" action="/forum/bug-report" enctype="multipart/form-data">';
							bugModal += '<input type="hidden" name="_token" value="'+$('[name="csrf-token"]').attr('content')+'">';
							bugModal += '<div class="grid">';
								bugModal += '<div class="grid-col-span-12 grid-col-span-laptop-4">';
									bugModal += '<h4>'+localisation.bug_report.page_info+'</h4>';
									bugModal += '<p><img src="'+url+'"><input style="display: none;" value="'+url+'" name="bug_image" readonly></p>';
									bugModal += '<p style="display:none;"><input value="'+window.location.href+'" readonly name="url"></p>';
									bugModal += '<h4>'+localisation.bug_report.additional_files+'</h4>';
									bugModal += '<p><input type="file" multiple name="files[]" accept=".txt,.yml,.png,.jpg,.jpeg,.gif"></p>';
								bugModal += '</div>';
								bugModal += '<div class="grid-col-span-12 grid-col-span-laptop-8">';
									bugModal += '<div class="input-wrapper data-field">';
										bugModal += '<input class="default-input" type="text" name="title" placeholder="'+localisation.bug_report.bug_title+'">';
										bugModal += '<label class="input-label" for="title">'+localisation.bug_report.bug_title+'</label>';
									bugModal += '</div>';
									bugModal += '<div class="input-wrapper data-field">';
										bugModal += '<textarea class="default-input" name="description" placeholder="'+localisation.bug_report.bug_description+'"></textarea>';
										bugModal += '<label class="input-label" for="description">'+localisation.bug_report.bug_description+'</label>';
									bugModal += '</div>';
									bugModal += '<div class="data-field text-right">';
										bugModal += '<button class="button">'+localisation.bug_report.report_bug+'</button>';
									bugModal += '</div>';
								bugModal += '</div>';
							bugModal += '</div>';
						bugModal += '</div>';
					bugModal += '</div>';
				bugModal += '</div>';
			bugModal += '</div>';
		bugModal += '</div>';
		$('body').append(bugModal);
		$('body').find('.loader-overlay').remove();
	});
}

$('body').on('click', '#closeBugReport', function(){
	var localisation = JSON.parse($('#tool-help-file').text());
	jconfirm({
		title: localisation.bug_report.close.title,
		content: localisation.bug_report.close.description,
		buttons: {   
			ok: {
				text: localisation.bug_report.close.confirm,
				btnClass: 'button',
				keys: ['enter'],
				action: function(){
					$('.bug-modal-wrapper').remove();					
				}
			},
			cancel: {
				text: localisation.bug_report.close.cancel,
				btnClass: 'button invert white'
			}
		}
	});
});

$('.tab-header').on('click', function(){
	var id = $(this).attr('href');
	$(this).closest('.tabble-content').find('.active').removeClass('active');
	$(this).addClass('active');
	$(this).closest('.tabble-content').find(id).addClass('active');
});

if(window.location.hash){
	if($('.tab-header').length){
		if($('.tab-header[href="'+window.location.hash+'"]').length){
			$('.tab-header[href="'+window.location.hash+'"]').trigger('click');
		}
	}
}

window.builderEntryChooser = null;
window.setupOutcomeBuilderSearch = function(builderData){
	//Add click event to all addToBuilder buttons
	$('.addToBuilder').on('click', function(){
		var tabs = parseInt($(this).attr('data-tabs'));
		var text = '<div class="builderSearch">';
		text += '<p class="input-wrapper"><input class="default-input builderSearchInput" placeholder="Search outcomes"><label class="input-label">Search outcomes</label></p>';
		//Loop over builderData as that contains the key for lang file, and data for the stuff
		Object.keys(builderData).forEach(function(key){
			var obj = builderData[key];
			text += '<div class="builder-output" contenteditable="false" data-gameid="'+obj.game_id+'" data-outcome="'+obj.default_outcome+'">';
				text += '<p class="output-title">'+obj.description+'</p>';
				text += '<pre class="output-preview">'+obj.example+'</pre>';
			text += '</div>';
		});
		text += '</div>';
		text += '<button class="addToBuilder" contenteditable="false" data-tabs="'+tabs+'">+</button>';
		//Replace the button with the search area
		$(this).replaceWith(text);

		//Add keyup handler for search input
		$('.builderSearchInput').on('keyup', function(){
			var val = $(this).val();
			var re = new RegExp(val, 'i');
			if(val == ''){
				$(this).parent().parent().find('.builder-output').show();
			}else{
				$(this).parent().parent().find('.builder-output').each(function(){
					var gameID = $(this).attr('data-gameid');
					var text = $(this).find('.output-title').text();

					if(gameID.match(re) || text.match(re)){
						$(this).show();
					}else{
						$(this).hide();
					}
				});
			}
		});

		//Add click handler for builder output
		$('.builder-output').on('click', function(){
			//Get tabs number of nearest + 
			var tabs = parseInt($(this).parent().next().attr('data-tabs'));
			var tab = "\t";
			var tabStr = tab.repeat(tabs);

			var outcome = tabStr;
			var step = $(this).attr('data-outcome');

			if(step){
				outcome += step+"\n";
			}else{
				outcome += "?\n";
			}
			outcome = outcome.replace('TAG', '<span contenteditable="false" class="choose-country">TAG</span>').replace('state_id', '<span contenteditable="false" class="choose-state">STATE</span>').replace('STATEID', '<span contenteditable="false" class="choose-state">STATE</span>').replace('NEWLEVEL', tabStr+'<button class="addToBuilder" contenteditable="false" data-tabs="'+(tabs + 1)+'">+</button>');
			$(this).parent().replaceWith(outcome);

			//Slight delay, as no delay caused issues
			setTimeout(function(){
				//Add the click handler again for new button added 
				window.setupOutcomeBuilderSearch(builderData)
			}, 100);
		});

		//Add the click handler again for new button added 
		window.setupOutcomeBuilderSearch(builderData)
	});

	$('.choose-country').on('click', function(){
		if(!$('.modal-cover').length){
			window.builderEntryChooser = $(this);
			var content = '<div class="modal-cover" contenteditable="false">';
				content += '<div class="input-wrapper">';
					content += '<input class="default-input" id="extra-list" type="text" list="builder-extra-list">';
					content += '<datalist id="builder-extra-list">';
						content += '<option></option>';
						var entryList = getCountryTags();
						Object.keys(entryList).forEach(function(key){
							content += '<option value="'+key+'">'+entryList[key]+'</option>';
						});
					content += '</datalist>';
				content += '</div>';
				content += '<p>&nbsp;</p>';
				content += '<p><button type="button" onclick="window.chooseBuilderEntryChooser()" class="button invert" id="swapOutEntryChooser">Submit</button></p>';
			content += '</div>';
			$('.builder-modal-content').append(content);
		}
	});
	$('.choose-state').on('click', function(){
		if(!$('.modal-cover').length){
			window.builderEntryChooser = $(this);
			var content = '<div class="modal-cover" contenteditable="false">';
				content += '<div class="input-wrapper">';
					content += '<input class="default-input" id="extra-list" type="text" list="builder-extra-list">';
					content += '<datalist id="builder-extra-list">';
						content += '<option></option>';
						var entryList = getStateResults();
						Object.keys(entryList).forEach(function(key){
							content += '<option value="'+entryList[key].id+'">'+entryList[key].name+'</option>';
						});
					content += '</datalist>';
				content += '</div>';
				content += '<p>&nbsp;</p>';
				content += '<p class="text-center"><button onclick="window.chooseBuilderEntryChooser()" type="button" class="button invert" id="swapOutEntryChooser">Submit</button></p>';
			content += '</div>';
			$('.builder-modal-content').append(content);
		}
	});
}

window.chooseBuilderEntryChooser = function(){
	window.builderEntryChooser.replaceWith(window.builderEntryChooser.closest('.dialog-modal').find('#extra-list').val());
	window.builderEntryChooser = null;
	$('.builder-modal-content').find('.modal-cover').remove();
}