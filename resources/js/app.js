/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');

window.Vue = require('vue');


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('placeholder', require('./components/Placeholder.vue').default);

Vue.component('gfx-select', require('./components/GFXSelect.vue').default);
Vue.component('gfx-modal', require('./components/GFXModal.vue').default);

Vue.component('outcome-builder', require('./components/OutcomeBuilder.vue').default);
Vue.component('trait-selector', require('./components/TraitSelector.vue').default);

Vue.component('help-box', require('./components/HelpBox.vue').default);
Vue.component('menu-item', require('./components/MenuItem.vue').default);

Vue.component('create-mod-modal', require('./components/Mod/CreateMod.vue').default);
Vue.component('edit-mod-modal', require('./components/Mod/EditMod.vue').default);

Vue.component('user-form', require('./components/UserForm.vue').default);
Vue.component('user-delete', require('./components/UserDelete.vue').default);

Vue.component('asset-stain', require('./components/Assets/Stain.vue').default);

Vue.component('media-manager', require('./components/User/MediaManager.vue').default);

/*
National Focus
*/
Vue.component('create-focus-tree-modal', require('./components/NationalFocus/CreateFocusTree.vue').default);
Vue.component('focus', require('./components/NationalFocus/NationalFocus.vue').default);
Vue.component('focus-edit-menu', require('./components/NationalFocus/FocusEditMenu.vue').default);

/*
Country
*/
Vue.component('all-countries', require('./components/Country/AllList.vue').default);
Vue.component('country', require('./components/Country/Country.vue').default);
Vue.component('ideologies', require('./components/Country/sections/Ideologies.vue').default);
Vue.component('politics', require('./components/Country/sections/Politics.vue').default);
Vue.component('government', require('./components/Country/sections/Government.vue').default);
Vue.component('military', require('./components/Country/sections/Military.vue').default);
Vue.component('divisions', require('./components/Country/sections/Divisions.vue').default);
Vue.component('research', require('./components/Country/sections/Research.vue').default);
Vue.component('states', require('./components/Country/sections/States.vue').default);
Vue.component('misc', require('./components/Country/sections/Data.vue').default);
Vue.component('national-spirits', require('./components/Country/sections/NationalSpirits.vue').default);

Vue.component('create-country-modal', require('./components/Country/CreateCountryModal.vue').default);
Vue.component('country-editor', require('./components/Country/CountryEditor.vue').default);
//Vue.component('ideologies', require('./components/Country/tabs/Ideologies.vue').default);
//Vue.component('states', require('./components/Country/tabs/States.vue').default);
//Vue.component('misc', require('./components/Country/tabs/Misc.vue').default);
//Vue.component('start-dates', require('./components/Country/tabs/StartDates.vue').default);
//Vue.component('divisions', require('./components/Country/tabs/Divisions.vue').default);
//Vue.component('government', require('./components/Country/tabs/Government.vue').default);
//Vue.component('military', require('./components/Country/tabs/Military.vue').default);
//Vue.component('research', require('./components/Country/tabs/Research.vue').default);

Vue.component('division-province', require('./components/Country/DivisionProvinces.vue').default);
Vue.component('military-editor', require('./components/Country/MilitaryEditor.vue').default);
Vue.component('government-editor', require('./components/Country/GovernmentEditor.vue').default);

/*
Ideas
*/
Vue.component('create-ideas-modal', require('./components/Ideas/CreateIdeas.vue').default);
Vue.component('idea', require('./components/Ideas/Idea.vue').default);
Vue.component('edit-idea', require('./components/Ideas/EditIdea.vue').default);

/*
Events
*/
Vue.component('create-events-modal', require('./components/Events/CreateEvents.vue').default);
Vue.component('event', require('./components/Events/Event.vue').default);
Vue.component('edit-event', require('./components/Events/EditEvent.vue').default);
Vue.component('event-option', require('./components/Events/EventOption.vue').default);

/*
Decision
*/
Vue.component('create-decision-modal', require('./components/Decision/CreateCategory.vue').default);
Vue.component('decision-list', require('./components/Decision/DecisionList.vue').default);
Vue.component('edit-decision-category', require('./components/Decision/EditCategory.vue').default);
Vue.component('edit-decision', require('./components/Decision/EditDecision.vue').default);


/*
Start Dates
*/
Vue.component('create-start-date-modal', require('./components/StartDate/CreateStartDate.vue').default);
Vue.component('start-date-form', require('./components/StartDate/StartDate.vue').default);
Vue.component('start-date-country', require('./components/StartDate/EditCountry.vue').default);

/*
Ideologies
*/
Vue.component('create-ideology-modal', require('./components/Ideology/CreateIdeology.vue').default);
Vue.component('ideology-form', require('./components/Ideology/EditIdeology.vue').default);

/*
Research
*/
//Vue.component('research-index', require('./components/Research/Index.vue').default);


/* 
Character
*/
Vue.component('all-character-lists', require('./components/Character/AllList.vue').default);
Vue.component('create-character-list', require('./components/Character/CreateListModal.vue').default);
Vue.component('character-list', require('./components/Character/List.vue').default);
Vue.component('character', require('./components/Character/Item.vue').default);

/*
Traits
*/
Vue.component('all-traits-lists', require('./components/Traits/AllList.vue').default);
Vue.component('country-leader-traits', require('./components/Traits/CountryLeaderTraits.vue').default);

/*
Components
*/
Vue.component('mod-preview', require('./components/Components/ToolModPreview.vue').default);
Vue.component('modal', require('./components/Components/Modal.vue').default);

/*
Form fields/input
*/
Vue.component('btn', require('./components/Input/Button.vue').default);
Vue.component('link-button', require('./components/Input/Link.vue').default);
Vue.component('input-help', require('./components/Input/InputHelp.vue').default);
Vue.component('textarea-help', require('./components/Input/TextareaHelp.vue').default);
Vue.component('select-help', require('./components/Input/SelectHelp.vue').default);
Vue.component('checkbox-help', require('./components/Input/CheckboxHelp.vue').default);
Vue.component('datalist-help', require('./components/Input/DataListHelp.vue').default);
Vue.component('builder', require('./components/Input/Builder.vue').default);
Vue.component('traits', require('./components/Input/Traits.vue').default);
Vue.component('mod-list', require('./components/Input/ModList.vue').default);
Vue.component('tag-list', require('./components/Input/TagList.vue').default);
Vue.component('csrf', require('./components/Input/CSRF.vue').default);
Vue.component('gfx-input', require('./components/Input/GFXInput.vue').default);
Vue.component('trait-input', require('./components/Input/TraitSelector.vue').default);
Vue.component('builder-input', require('./components/Input/OutcomeBuilder.vue').default);

/*
Global help text files, if they exist
*/
var helpText = null;
var toolHelp = null;
if(document.getElementById("help-text-file")){
	helpText = JSON.parse(document.getElementById("help-text-file").innerHTML);
}
if(document.getElementById("tool-help-file")){
	toolHelp = JSON.parse(document.getElementById("tool-help-file").innerHTML);
}

var globalAppData = { helpText: helpText, toolHelp: toolHelp, globMod: "placeholder" };

if($('.focus-tree-list-data').length){
	var focusList = [];
	$('.focus-tree-list-data').each(function(index){
		var json = JSON.parse($(this).text());
		$.each(json, function(key, value){
			focusList.push(value);
		});
	});
	globalAppData['focusList'] = focusList;
}

if(document.getElementById('country-data')){
	globalAppData['countryData'] = JSON.parse(document.getElementById("country-data").innerHTML);
	globalAppData['countryTab'] = 'ideologies';
	globalAppData['startDate'] = 0;
}
if(document.getElementById('idea-list')){
	globalAppData['ideaList'] = JSON.parse(document.getElementById("idea-list").innerHTML);
}
if(document.getElementById('event-list')){
	globalAppData['eventList'] = JSON.parse(document.getElementById("event-list").innerHTML);
}
if(document.getElementById('start-date-data')){
	globalAppData['startDate'] = JSON.parse(document.getElementById("start-date-data").innerHTML);
	globalAppData['newCountry'] = false;
}
if(document.getElementById('ideology-data')){
	globalAppData['ideology'] = JSON.parse(document.getElementById("ideology-data").innerHTML);
}

if(document.getElementById('mod-edit-data')){
	globalAppData['mod'] = JSON.parse(document.getElementById('mod-edit-data').innerHTML);
}
if(document.getElementById('decision-list')){
	globalAppData['decisionList'] = JSON.parse(document.getElementById("decision-list").innerHTML);
}
if(document.getElementById('decision-category')){
	globalAppData['decisionCategory'] = JSON.parse(document.getElementById("decision-category").innerHTML);
}

var globalVueApp = new Vue({
	el: '#app',
	data: globalAppData,
	methods:{
		setGlobalModal(modal){
			this.globMod = modal;
		},
		closeglobalmodal(){
			this.globMod = 'placeholder';
		},
		addNewFocus(){
			this.focusList.push({'id' : 'new', 'name' : '', 'description': '', 'x' : '', 'y' : '', 'gfx' : 445, 'data' : {}});
			$('.focus-item [data-id="new"]').addClass('active');
		},
		addNewIdea(){
			this.ideaList.push({'id' : 'new', 'name' : '', 'gfx' : 1, 'data' : {}});
		},
		addNewEvent(){
			this.eventList.push({'id' : 'new', 'name' : '', 'gfx' : 1, 'data' : {}});
			this.globalAppData.newCountry = true;
		},
		addNewDecision(){
			var timestamp = Date.now();
			this.decisionList.push({'id' : 'new-'+timestamp, 'name' : '', 'gfx' : 1, 'data' : {}});
		},
		addNewCountryToStartDate(){
			if(this.startDate.countries == null){
				this.startDate.countries = [];
			}
			this.newCountry = this.startDate.countries.length;
			this.startDate.countries.push({tag : '', data : {ideology: '', history: '', ideas: '', focuses: ''}, order: this.startDate.countries.length});
		},
		removefocusfromlist(focusID){
			let env = this;
			this.focusList.forEach(function(val, key){
				if(val.id === focusID){
					env.focusList.splice(env.focusList.indexOf(val), 1);
				}
			});
		},
		removeideafromlist(ID){
			let env = this;
			this.ideaList.forEach(function(val, key){
				if(val.id === ID){
					env.ideaList.splice(env.ideaList.indexOf(val), 1);
				}
			});
		},
		removeeventfromlist(ID){
			let env = this;
			this.eventList.forEach(function(val, key){
				if(val.id === ID){
					env.eventList.splice(env.eventList.indexOf(val), 1);
				}
			});
		},
		removedecisionfromlist(ID){
			let env = this;
			this.decisionList.forEach(function(val, key){
				if(val.id === ID){
					env.decisionList.splice(env.decisionList.indexOf(val), 1);
				}
			});
		},
		updateCountryTab(modal){
			this.countryTab = modal;
		},
		updateCountryStartDate(index){
			this.startDate = index;
		},
	},
	computed: {
		globalModal: function(){
			return this.globMod.toLowerCase();
		}
	}
});

window.getVueApp = function(){
	return globalVueApp;
}

window.setVueApp = function(data){
	globalVueApp = data;
}

/* Has to go here, so globalVueApp is available */
$('.country-data-save').on('click', function(){
	updateCountryData(globalVueApp.countryData);
});

//Custom JS Files
require('./jConfirm');
require('./dragscroll');
require('./jQuery-ui');
require('./site-wide');

//Tools
require('./tools/forum');
require('./tools/national-focus');
require('./tools/country');
require('./tools/mod');
require('./tools/ideas');
require('./tools/events');
require('./tools/start-dates');
require('./tools/ideology');
require('./tools/decisions');