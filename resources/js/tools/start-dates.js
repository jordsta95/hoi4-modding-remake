
var startDateImportFile = null;
var localisationStartDatesImportFile = null;
$('.importBookmark').on('click', function(){
	readStartDateFiles();
});

//Read the events file, and parse it to JSON
function readStartDateFiles(){
	var reader = new FileReader();
	reader.readAsText(document.getElementById('import-bookmark').files[0], "UTF-8");
	reader.onload = function(evt){
		startDateImportFile = hoiFileToJSON(evt.target.result, 0, 2);
		if(startDateImportFile.hasOwnProperty('bookmarks')){
			getStartDateLocalisationData();
		}else{
			alert('Invalid start date file');
		}
	};
}

function getStartDateLocalisationData(){
	var reader = new FileReader();
	reader.readAsText(document.getElementById('import-localisation').files[0], "UTF-8");
	reader.onload = function(evt){
		localisationStartDatesImportFile = localisationToJSON(evt.target.result, 0);
		attachLocalisationToImportedStartDate();
	};
}

var startDateImportJSON = {data: {}, countries: []};
function attachLocalisationToImportedStartDate(){
	$.each(startDateImportFile.bookmarks, function(index, data){
		var order = 0;
		$.each(data, function(key, value){
			if(key == 'name' || key == 'desc'){
				var v = value.replace(/"/g, '');
				if(key == 'desc'){
					key = 'description';
				}
				startDateImportJSON[key] = localisationStartDatesImportFile[v].name;
			}
			else if(key == 'picture'){
				startDateImportJSON.gfx = 1;
			}
			else if(key == 'date'){
				startDateImportJSON.date = value;
			}
			else if(key == 'default_country'){
				var v = value.replace(/"/g, '');
				startDateImportJSON.data.default_country = v;
			}
			else if(key == 'default'){
				if(value == 'yes'){
					startDateImportJSON.data.default = 1;
				}else{
					startDateImportJSON.data.default = 0;
				}
			}
			else if(key == 'effect'){
				
			}
			else{
				var country = value;
				if(Array.isArray(country)){
					country = value[0];
				}

				country = hoiFileToJSON(key+' = '+country, 0, 1);
				country = country[key];

				var c = {tag: key.replace(/"/g, ''), order: order, data:{}};
				if(country.hasOwnProperty('ideology')){
					c.data.ideology = country.ideology;
				}
				if(country.hasOwnProperty('history')){
					c.data.history = localisationStartDatesImportFile[country.history.replace(/"/g, '')].name;
				}
				c.data.focuses = '';
				c.data.ideas = '';

				if(country.hasOwnProperty('focuses')){
					var focuses = country.focuses.replace(/\{/g, '').replace(/\}/g, '').trim().split('\n');
					if(focuses){
						var f = '';
						$.each(focuses, function(a,b){
							if(b){
								f += ','+b.trim();
							}
						});
						if(f){
							c.data.focuses = f.substring(1);
						}
					}
				}

				if(country.hasOwnProperty('ideas')){
					var ideas = country.ideas.replace(/{/g, '').replace(/}/g, '').trim().split('\n');
					if(ideas){
						var i = '';
						$.each(ideas, function(a,b){
							if(b){
								i += ','+b.trim();
							}
						});
						if(i){
							c.data.ideas = i.substring(1);
						}
					}
				}

				if(c.hasOwnProperty('minor')){
					c.data.minor = true;
				}

				startDateImportJSON.countries.push(c);
				order++;
			}
		});
		return false;
	});
	console.log(startDateImportJSON);
	importStartDates();
}

function importStartDates(){
	const config = {
		headers: {
			'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content,
		}
	}
	let formData = new FormData();
	formData.append('id', $('.start-date').attr('id'));
	formData.append('import', JSON.stringify(startDateImportJSON));
	axios.post('/start-date/import', formData, config).then(function (response) {
		window.location.href = window.location.href;
	});
}

$('.start-date-delete').on('click', function(){
	var id = $(this).attr('data-id');
	var list = $(this).closest('.mod-focus-list-item');
	jconfirm({
		title: $('.delete-title').text(),
		content: $('.delete-text').text(),
		buttons: {   
			ok: {
				text: $('.delete-confirm').text(),
				btnClass: 'button',
				keys: ['enter'],
				action: function(){
					const config = {
						headers: {
							'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content,
						}
					}
					let formData = new FormData();
					axios.post('/start-date/delete/'+id, formData, config);
					list.remove();
				}
			},
			cancel: {
				text: $('.delete-cancel').text(),
				btnClass: 'button invert white'
			}
		}
	});
});