window.showForumPostCreator = function(){
	toggleForumPostCreate();
}

$('.create-forum-post .close').on('click', function(){
	toggleForumPostCreate();
});

function toggleForumPostCreate(){
	if($('.forum-posts-page').length){
		$('.create-forum-post [name="forum"]').val($('.forum-posts-page').attr('data-forum'));
	}
	$('.create-forum-post').toggleClass('open');
}

$('#forumFiles').on('change', function(){
	var fLen = document.getElementById('forumFiles').files.length;
	if(fLen > 0){
		$('#forumFileCount').text(' - ('+fLen+')');
	}else{
		$('#forumFileCount').text('');
	}
});

$('.quote-post').on('click', function(){
	var textarea = $('.add-reply').find('textarea');
	var author = $(this).closest('.forum-comment').find('.profile h4').text();
	var comment = $(this).closest('.forum-comment').find('.comment').html();

	var quote = textarea.val() + '<blockquote>'+ comment +'<p>-'+author+'</p></blockquote><p>&nbsp</p>';
	textarea.val();

	$('.add-reply').find('.trumbowyg-editor').html(quote);
	$('.add-reply').find('.trumbowyg-editor img').remove();
	$('.add-reply').find('.trumbowyg-editor hr').remove();
	textarea.val(quote);
	$('.page-content').animate({
        scrollTop: $('.add-reply').offset().top
    }, 500);
});