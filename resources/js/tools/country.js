$('.importCountry').on('click', function(){
	readCountryFile();
});

//Read the History file. Check if it's valid - File is mandatory
function readCountryFile(){
	var reader = new FileReader();
	reader.readAsText(document.getElementById('import-history').files[0], "UTF-8");
	reader.onload = function(evt){
		countryFile = hoiFileToJSON(evt.target.result, 0, 3);
		//If it's valid check other files
		if(countryFile.hasOwnProperty('capital')){
			importCountryFile(countryFile);
		}else{
			alert('Cannot find Capital in "Country History" file. This file will be ignored, as it appears invalid.');
		}

		readLocalisationFile();
		readUnitsFile();
		readCommonFile();

		$('.country-data-save').addClass('saving');
		const config = {
			headers: {
				'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content,
			}
		}
		let formData = new FormData();
		formData.append('data', JSON.stringify(getVueApp().countryData));

		axios.post('/country/import/'+$('.page-content').attr('data-id'), formData, config).then(function (response) {
			countryDataEdited = false;
			$('.country-data-save').removeClass('saving');
			$('.country-data-save').hide();
			$(window).unbind('beforeunload', leaveFunction);
			window.location.href = window.location.href;
		});
		
	};
}

//Importing the country file
function importCountryFile(file){
	var v = getVueApp();
	data = v.countryData;
	data.capital = file.capital;

	if(!data.misc_data){
		data.misc_data = {};
	}

	if(file.hasOwnProperty('set_politics')){
		data.misc_data['set_politics'] = file.set_politics;
		//Remove " from the last election date
		data.misc_data['set_politics'].last_election = data.misc_data['set_politics'].last_election.replace(/"/g, '');
	}
	if(file.hasOwnProperty('set_stability')){
		data.misc_data['set_stability'] = file.set_stability;
	}
	if(file.hasOwnProperty('set_war_support')){
		data.misc_data['set_war_support'] = file.set_war_support;
	}
	if(file.hasOwnProperty('add_to_faction')){
		data.misc_data['add_to_faction'] = file.add_to_faction;
	}
	if(file.hasOwnProperty('set_convoys')){
		data.misc_data['set_convoys'] = file.set_convoys;
	}
	if(file.hasOwnProperty('set_research_slots')){
		data.misc_data['set_research_slots'] = file.set_research_slots;
	}
	if(file.hasOwnProperty('create_faction')){
		data.misc_data['create_faction'] = file.create_faction;
	}

	if(file.hasOwnProperty('set_popularities')){
		$.each(data.dates, function(index, date){
			$.each(date.ideologies, function(index, ideology){
				if(file.set_popularities.hasOwnProperty(ideology.ideology)){
					ideology.popularity = file.set_popularities[ideology.ideology];
				}else{
					ideology.popularity = 0;
				}
			});
		});
	}

	//For leader, loop over the localisation files for the ideologies of the mod. See if they match that sub ideology, profit!
	var ideologies = JSON.parse($('#help-ideologies-text').text());
	if(ideologies.hasOwnProperty($('.page-content').attr('data-mod'))){
		if(file.hasOwnProperty('create_country_leader')){
			$.each(file.create_country_leader, function(index, leader){
				if(leader.hasOwnProperty('ideology')){
					var ideology = '';
					var subIdeology = '';
					$.each(ideologies[$('.page-content').attr('data-mod')], function(key, value){
						if(subIdeology != ''){
							return true;
						}
						if(typeof value === 'object' && value !== null){
							//Sub ideology
							$.each(value, function(name, label){
								if(name == leader.ideology){
									subIdeology = ideology;
									return true;
								}
							});
						}else{
							ideology = key;
						}
					});
					var toCheck = ideology;
					$.each(data.dates, function(i, date){
						$.each(date.ideologies, function(index, ideology){
							if(ideology.ideology == toCheck){
								ideology.leader = leader.name.replace(/"/g, "");
								ideology.sub_ideology = subIdeology;
								ideology.getLeaderGFX = leader.picture.replace(/"/g, "");
								if(leader.hasOwnProperty('desc')){
									ideology.leader_description = 'GET_LOC_DESC||'+leader.desc;
								}
								if(leader.hasOwnProperty('traits')){
									var t = leader.traits.traits.traits;
									t = t.replace('{', '').replace('}', '').trim();
									if(t){
										ideology.traits = t;
									}
								}
							}
						});
					});
				}
			});
		}
	}


	if(file.hasOwnProperty('create_corps_commander')){
		if(!data.general_data){
			data.general_data = [];
		}
		$.each(file.create_corps_commander, function(key, general){
			if(general.hasOwnProperty('name')){
				var gD = {"is_field_marshal": false, "name": general.name.replace(/"/g, "")};
				gD = getGeneralData(gD, general);
				data.general_data.push(gD);
			}
		});
	}

	if(file.hasOwnProperty('create_field_marshal')){
		if(!data.general_data){
			data.general_data = [];
		}
		$.each(file.create_field_marshal, function(key, general){
			if(general.hasOwnProperty('name')){
				var gD = {"is_field_marshal": true, "name": general.name.replace(/"/g, "")};
				gD = getGeneralData(gD, general);
				data.general_data.push(gD);
			}
		});
	}

	if(file.hasOwnProperty('create_navy_leader')){
		if(!data.admiral_data){
			data.admiral_data = [];
		}
		$.each(file.create_navy_leader, function(key, general){
			if(general.hasOwnProperty('name')){
				var gD = {"name": general.name.replace(/"/g, "")};
				if(general.hasOwnProperty('attack_skill')){
					gD['attack'] = general.attack_skill;
				}else{
					gD['attack'] = 1;
				}
				if(general.hasOwnProperty('defense_skill')){
					gD['defence'] = general.defense_skill;
				}else{
					gD['defence'] = 1;
				}
				if(general.hasOwnProperty('coordination_skill')){
					gD['coordination'] = general.coordination_skill;
				}else{
					gD['coordination'] = 1;
				}
				if(general.hasOwnProperty('maneuvering_skill')){
					gD['maneuvering'] = general.maneuvering_skill;
				}else{
					gD['maneuvering'] = 1;
				}
				if(general.hasOwnProperty('picture')){
					gD['getGFX'] = general.picture;
				}else{
					gD['gfx'] = 1;
				}
				if(general.hasOwnProperty('skill')){
					gD['overall'] = general.skill;
				}else{
					gD['overall'] = 1;
				}
				if(general.hasOwnProperty('traits')){
					var t = general.traits.traits.traits; //Traits are fucked
					t = t.replace('{', '').replace('}', '');
					t = t.split(' ');
					traits = [];
					$.each(t, function(key, value){
						if(value){
							traits.push(value);
						}
					});
					gD['traits'] = traits;
				}else{
					gD['traits'] = [];
				}
				gD['picture'] = general.picture.replace(/"/g, "");
				data.admiral_data.push(gD);
			}
		});
	}

	console.log(v.countryData);
	console.log(data);
	v.countryData = data;
	setVueApp(v);
}

function getGeneralData(gD, general){
	if(general.hasOwnProperty('attack_skill')){
		gD['attack'] = general.attack_skill;
	}else{
		gD['attack'] = 1;
	}
	if(general.hasOwnProperty('defense_skill')){
		gD['defence'] = general.defense_skill;
	}else{
		gD['defence'] = 1;
	}
	if(general.hasOwnProperty('logistics_skill')){
		gD['logistics'] = general.logistics_skill;
	}else{
		gD['logistics'] = 1;
	}
	if(general.hasOwnProperty('planning_skill')){
		gD['planning'] = general.planning_skill;
	}else{
		gD['planning'] = 1;
	}
	if(general.hasOwnProperty('picture')){
		gD['getGFX'] = general.picture;
	}else{
		gD['gfx'] = 1;
	}
	if(general.hasOwnProperty('skill')){
		gD['overall'] = general.skill;
	}else{
		gD['overall'] = 1;
	}
	if(general.hasOwnProperty('traits')){
		var t = general.traits.traits.traits; //Traits are fucked
		t = t.replace('{', '').replace('}', '');
		t = t.split(' ');
		traits = [];
		$.each(t, function(key, value){
			if(value){
				traits.push(value);
			}
		});
		gD['traits'] = traits;
	}else{
		gD['traits'] = [];
	}
	gD['picture'] = general.picture.replace(/"/g, "");
	return gD;
}

//Read file containing localisation. This file is optional, no "No Import" errors if fails
function readLocalisationFile(){
	if(document.getElementById('import-localisation').files.length){
		var len = document.getElementById('import-localisation').files.length;
		var tag = $('.page-content').attr('data-tag').toUpperCase();
		var v = getVueApp();
		data = v.countryData;

		for(var i = 0; i < len; ++i) {
			let reader = new FileReader();
			reader.readAsText(document.getElementById('import-localisation').files[i], "UTF-8");
			reader.onload = function(evt){
				localisationImportFile = localisationToJSON(evt.target.result, 0);
				$.each(data.dates, function(i, date){
					$.each(data.ideologies, function(n, ideology){
						var leaderDesc = ideology.leader_description.split('||');
						$.each(localisationImportFile, function(name, value){
							if(name == tag){
								ideology.name = value;
							}
							if(name+'_ADJ' == tag){
								ideology.nationality = value;
							}
							if(name+'_DEF' == tag){
								ideology.definition = value;
							}

							if(name+'_'+ideology.ideology == tag){
								ideology.name = value;
							}
							if(name+'_'+ideology.ideology+'_ADJ' == tag){
								ideology.nationality = value;
							}
							if(name+'_'+ideology.ideology+'_DEF' == tag){
								ideology.definition = value;
							}
							if(leaderDesc > 1){
								if(name == leaderDesc[1]){
									ideology.leader_description = value;
								}
							}
						});
						var leaderDesc = ideology.leader_description.split('||');
						if(leaderDesc > 1){
							if(leaderDesc[0] == "GET_LOC_DESC"){
								ideology.leader_description = "";
							}
						}
					});
				});
			};
		}

		v.countryData = data;
		setVueApp(v);
	}else{
		var v = getVueApp();
		data = v.countryData;
		$.each(data.dates, function(i, date){
			$.each(data.ideologies, function(n, ideology){
				var leaderDesc = ideology.leader_description.split('||');
				$.each(localisationImportFile, function(name, value){
					if(leaderDesc > 1){
						if(leaderDesc[0] == "GET_LOC_DESC"){
							ideology.leader_description = "";
						}
					}
				});
			});
		});
		v.countryData = data;
		setVueApp(v);
	}
}

//Read file containing units. This file is optional, no "No Import" errors if fails
function readUnitsFile(){
	if(document.getElementById('import-units').files.length){
		

		var len = document.getElementById('import-units').files.length;
		for(var i = 0; i < len; ++i) {
			let reader = new FileReader();
			let filename = document.getElementById('import-units').files[i].name;
			reader.readAsText(document.getElementById('import-units').files[i], "UTF-8");
			let fileDate = filename.replace('.txt', '').split('_');
			let date = fileDate[fileDate.length];
			reader.onload = function(evt){
				var v = getVueApp();
				data = v.countryData;
				
				//TODO: Use filename to get date to append to
				let json = hoiFileToJSON(evt.target.result, 0);
				
				if(json.hasOwnProperty('division_template')){
					$.each(json.division_template, function(index, template){
						var t = {};
						t['name'] = template.name.replace(/"/g, "");
						t['regiments'] = {};
						t['support'] = {};
						t['locations'] = [];

						$.each(template.support, function(type, position){
							var xy = position.replace('{', '').replace('}', '').replace(/ /g, '').replace('y', ' y').split(' ');
							var y = 0;
							$.each(xy, function(index, arr){
								var a = arr.split('=');
								if(a[0] == 'y'){
									y = a[1];
								}
							});
							t['support'][y] = type;
						});

						$.each(template.regiments, function(type, positions){
							//If there's just 1 of this type
							if(!Array.isArray(positions)){
								positions = [positions];
							}
							$.each(positions, function(key, position){
								var xy = position.replace('{', '').replace('}', '').replace(/ /g, '').replace('y', ' y').split(' ');
								var x = 0;
								var y = 0;
								$.each(xy, function(index, arr){
									var a = arr.split('=');
									if(a[0] == 'x'){
										x = a[1];
									}
									if(a[0] == 'y'){
										y = a[1];
									}
								});
								if(!t['regiments'].hasOwnProperty(x)){
									t['regiments'][x] = {};
								}
								t['regiments'][x][y] = type;
							});

						});

						//TODO: push to correct date
					});
				}

				if(json.hasOwnProperty('units')){
					//Army
					if(json.units.hasOwnProperty('division')){
						if(!Array.isArray(json.units.division)){
							json.units.division = [json.units.division];
						}
						$.each(json.units.division, function(key, div){
							var temp = div.division_template.replace(/"/g, "");
							$.each(data.dates, function(index, d){
								if(d.year == date){
									$.each(d.divisions, function(i, di){
										if(di.name == temp){
											if(!di.hasOwnProperty('locations')){
												di['locations'] = [];
											}
											if(!Array.isArray(di['locations'])){
												di['locations'] = [];
											}
											di['locations'].push(div.location);
										}
									});
								}
							});

						});
					}
					//Navy - Unused; ignore
					if(json.units.hasOwnProperty('fleet')){

					}
				}
				if(json.hasOwnProperty('instant_effect')){

				}
				if(json.hasOwnProperty('air_wings')){

				}

				v.countryData = data;
				setVueApp(v);
			};
		}
	}
}

//Read file containing common. This file is optional, no "No Import" errors if fails
function readCommonFile(){
	if(document.getElementById('import-common').files.length){
		var reader = new FileReader();
		reader.readAsText(document.getElementById('import-common').files[0], "UTF-8");
		reader.onload = function(evt){
			commonFile = hoiFileToJSON(evt.target.result, 0, 1);
			if(commonFile.hasOwnProperty('color')){
				var v = getVueApp();
				data = v.countryData;
				var col = commonFile.color.color.replace('{', '').replace('}', '').trim().split(' ');
				data.colour = {"r":col[0], "g":col[1], "b":col[2]};

				if(commonFile.hasOwnProperty('graphical_culture')){
					data.culture = commonFile.graphical_culture.replace('_gfx', '');
				}

				v.countryData = data;
				setVueApp(v);
			}
		};
	}
}

var countryDataEdited = false;

var leaveFunction = function(){
  return "You have unsaved changes, do you want to leave?";
};

window.resetCountrySaveUpdateEvents = function(){
	//Let it load Vue components
	setTimeout(function(){
		$('#country-editor-tabs input, #country-editor-tabs textarea, #country-editor-tabs select').on('change', function(){
			countryDataEdited = true;
			$('.country-data-save').show();
			$(window).bind('beforeunload', leaveFunction);
		});
	}, 500);
}

resetCountrySaveUpdateEvents();

var currentEditingDivisionIDIndex = 0;
var currentEditingDivisionDateIndex = 0;

window.setEditingDivisionProvince = function(date, id){
	currentEditingDivisionDateIndex = date;
	currentEditingDivisionIDIndex = id;
}

window.getCurrentEditingDivision = function(){
	return [currentEditingDivisionDateIndex, currentEditingDivisionIDIndex];
}

window.updateCountryData = function(countryData){
	$('.country-data-save').addClass('saving');
	const config = {
		headers: {
			'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content,
		}
	}
	let formData = new FormData();
	formData.append('data', JSON.stringify(countryData));

	axios.post('/country/update/'+$('.page-content').attr('data-id'), formData, config).then(function (response) {
		countryDataEdited = false;
		$('.country-data-save').removeClass('saving');
		$('.country-data-save').hide();
		$(window).unbind('beforeunload', leaveFunction);
	});
	
}


$('.map-wrapper .increase').on('click', function(){
	zoomInMap();
});

$('.map-wrapper .decrease').on('click', function(){
	zoomOutMap();
});

function calculateDistance(elem, mouseX, mouseY) {
    return Math.floor(Math.sqrt(Math.pow(mouseX - (elem.offset().left+(elem.width()/2)), 2) + Math.pow(mouseY - (elem.offset().top+(elem.height()/2)), 2)));
}

var mX = 0;
var mY = 0;

$('body').parent().mousemove(function(e) {  
	if($('.map-wrapper svg').length){
	    mX = e.pageX;
	    mY = e.pageY;
	    var $element = $('body').find('.map-wrapper');
	    distance = calculateDistance($element, mX, mY);   
	}     
});

window.zoomInMap = function(){
	var f = parseFloat($('.map-wrapper').attr('data-zoom'));
	var zoomable = false;
	if(f < 16){
		zoomable = true;
		f = f * 2;
	}
	var sl = $('.map-wrapper svg').parent().scrollLeft();
	var st = $('.map-wrapper svg').parent().scrollTop();
	$('.map-wrapper').attr('data-zoom', f);
	$('.map-wrapper svg').css('height', (f * 100) + '%');

	if(zoomable){
		$('.map-wrapper svg').parent().scrollLeft((sl * 2) + mX);
		$('.map-wrapper svg').parent().scrollTop((st * 2) + mY);
	}

}

window.zoomOutMap = function(){
	var f = parseFloat($('.map-wrapper').attr('data-zoom'));
	if(f > 0.5){
		f = f / 2;
	}
	var sl = $('.map-wrapper svg').parent().scrollLeft();
	var st = $('.map-wrapper svg').parent().scrollTop();
	$('.map-wrapper').attr('data-zoom', f);
	$('.map-wrapper svg').css('height', (f * 100) + '%');
	if(f > 0.5){
		$('.map-wrapper svg').parent().scrollLeft((sl / 2) - mX);
		$('.map-wrapper svg').parent().scrollTop((st / 2) - mY);
	}
}

$('.country-delete').on('click', function(){
	var id = $(this).attr('data-id');
	var list = $(this).closest('.mod-focus-list-item');
	jconfirm({
		title: $('.delete-title').text(),
		content: $('.delete-text').text(),
		buttons: {   
			ok: {
				text: $('.delete-confirm').text(),
				btnClass: 'button',
				keys: ['enter'],
				action: function(){
					const config = {
						headers: {
							'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content,
						}
					}
					let formData = new FormData();
					axios.post('/country/delete/'+id, formData, config);
					list.remove();
				}
			},
			cancel: {
				text: $('.delete-cancel').text(),
				btnClass: 'button invert white'
			}
		}
	});
});

$('.character-delete').on('click', function(){
	var id = $(this).attr('data-id');
	var list = $(this).closest('.mod-focus-list-item');
	jconfirm({
		title: $('.delete-title').text(),
		content: $('.delete-text').text(),
		buttons: {   
			ok: {
				text: $('.delete-confirm').text(),
				btnClass: 'button',
				keys: ['enter'],
				action: function(){
					const config = {
						headers: {
							'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content,
						}
					}
					let formData = new FormData();
					axios.post('/character/delete/'+id, formData, config);
					list.remove();
				}
			},
			cancel: {
				text: $('.delete-cancel').text(),
				btnClass: 'button invert white'
			}
		}
	});
});

$('.country-edit-ideologies').on('click', function(){
	const config = {
		headers: {
			'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content,
		}
	}
	let formData = new FormData();
	formData.append('mod_id', $('#country-ideology-edit').attr('data-mod-id'));
	formData.append('country_id', $(this).attr('data-id'));
	$('#country-ideology-form').attr('action', $('#country-ideology-form').attr('data-src')+$(this).attr('data-id'));
	axios.post('/country/get-ideologies', formData, config).then(function (response) {
		var data = response.data;
		var all = [];
		var available = [];
		var newIdeologies = [];
		var existingIdeologies = [];
		var nonExistentIdeologies = [];
		$.each(data.available, function(index, item){
			all.push(item.overall);
			available.push(item.overall);
		});
		$.each(data.current, function(index, item){
			if(all.includes(item.overall)){
				existingIdeologies.push(item.overall);
			}
			else{
				nonExistentIdeologies.push(item.overall);
			}
		});
		$.each(data.available, function(index, item){
			if(!existingIdeologies.includes(item.overall)){
				newIdeologies.push(item.overall);
			}
		});
		var output = '';
		$.each(data.available, function(index, item){
			output += '<div class="country-popup-edit">';
				output += '<div class="name">'+item.overall+'</div>';
				output += '<div class="add"><label class="fakebox"><input type="checkbox" '+(newIdeologies.includes(item.overall) ? '' : 'disabled')+' name="add['+item.overall+']"><div class="checkbox"><svg class="feather-icon icon-check"><use xlink:href="#check"></use></svg></div></label></div>';
				output += '<div class="remove"><label class="fakebox"><input type="checkbox" '+(newIdeologies.includes(item.overall) ? 'disabled' : '')+' name="remove['+item.overall+']"><div class="checkbox"><svg class="feather-icon icon-check"><use xlink:href="#check"></use></svg></div></label></div>';
			output += '</div>';
		});
		$.each(data.current, function(index, item){
			if(!available.includes(item.overall)){
				output += '<div class="country-popup-edit">';
					output += '<div class="name">'+item.overall+'</div>';
					output += '<div class="add"><label class="fakebox"><input type="checkbox" '+(newIdeologies.includes(item.overall) ? '' : 'disabled')+' name="add['+item.overall+']"><div class="checkbox"><svg class="feather-icon icon-check"><use xlink:href="#check"></use></svg></div></label></div>';
					output += '<div class="remove"><label class="fakebox"><input type="checkbox" '+(newIdeologies.includes(item.overall) ? 'disabled' : '')+' name="remove['+item.overall+']"><div class="checkbox"><svg class="feather-icon icon-check"><use xlink:href="#check"></use></svg></div></label></div>';
				output += '</div>';
			}
		});
		$('#country-ideology-edit .ideology-list').html(output);
		$('#country-ideology-edit').addClass('open');
	});
});

$('#country-ideology-edit .close').on('click', function(){
	$('#country-ideology-edit').removeClass('open');
});



$('.country-edit-start-dates').on('click', function(){
	const config = {
		headers: {
			'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content,
		}
	}
	let formData = new FormData();
	formData.append('mod_id', $('#country-start-date-edit').attr('data-mod-id'));
	formData.append('country_id', $(this).attr('data-id'));
	$('#country-start-date-form').attr('action', $('#country-start-date-form').attr('data-src')+$(this).attr('data-id'));
	axios.post('/country/get-start-dates', formData, config).then(function (response) {
		var data = response.data;
		var all = [];
		var available = [];
		var newDates = [];
		var existingDates = [];
		var nonExistentDates = [];
		$.each(data.available, function(index, item){
			all.push(item.year+'.'+item.month+'.'+item.day);
			available.push(item.year+'.'+item.month+'.'+item.day);
		});
		$.each(data.current, function(index, item){
			if(all.includes(item.year+'.'+item.month+'.'+item.day)){
				existingDates.push(item.year+'.'+item.month+'.'+item.day);
			}
			else{
				nonExistentDates.push(item.year+'.'+item.month+'.'+item.day);
			}
		});
		$.each(data.available, function(index, item){
			if(!existingDates.includes(item.year+'.'+item.month+'.'+item.day)){
				newDates.push(item.year+'.'+item.month+'.'+item.day);
			}
		});
		var output = '';
		$.each(data.available, function(index, item){
			output += '<div class="country-popup-edit">';
				output += '<div class="name">'+item.year+'.'+item.month+'.'+item.day+'</div>';
				output += '<div class="add"><label class="fakebox"><input type="checkbox" '+(newDates.includes(item.year+'.'+item.month+'.'+item.day) ? '' : 'disabled')+' name="add['+item.year+'.'+item.month+'.'+item.day+']"><div class="checkbox"><svg class="feather-icon icon-check"><use xlink:href="#check"></use></svg></div></label></div>';
				output += '<div class="remove"><label class="fakebox"><input type="checkbox" '+(newDates.includes(item.year+'.'+item.month+'.'+item.day) ? 'disabled' : '')+' name="remove['+item.year+'.'+item.month+'.'+item.day+']"><div class="checkbox"><svg class="feather-icon icon-check"><use xlink:href="#check"></use></svg></div></label></div>';
			output += '</div>';
		});
		$.each(data.current, function(index, item){
			if(!available.includes(item.year+'.'+item.month+'.'+item.day)){
				output += '<div class="country-popup-edit">';
					output += '<div class="name">'+item.year+'.'+item.month+'.'+item.day+'</div>';
					output += '<div class="add"><label class="fakebox"><input type="checkbox" '+(newDates.includes(item.year+'.'+item.month+'.'+item.day) ? '' : 'disabled')+' name="add['+item.year+'.'+item.month+'.'+item.day+']"><div class="checkbox"><svg class="feather-icon icon-check"><use xlink:href="#check"></use></svg></div></label></div>';
					output += '<div class="remove"><label class="fakebox"><input type="checkbox" '+(newDates.includes(item.year+'.'+item.month+'.'+item.day) ? 'disabled' : '')+' name="remove['+item.year+'.'+item.month+'.'+item.day+']"><div class="checkbox"><svg class="feather-icon icon-check"><use xlink:href="#check"></use></svg></div></label></div>';
				output += '</div>';
			}
		});
		$('#country-start-date-edit .ideology-list').html(output);
		$('#country-start-date-edit').addClass('open');
	});
});

$('#country-start-date-edit .close').on('click', function(){
	$('#country-start-date-edit').removeClass('open');
});



// window.addColoursToUnfinishedMap = function(){
// 	var img = document.getElementById('imageToCheck'); 
// 	var base_image = new Image();
// 	base_image.src = $('#imageToCheck').attr('src');
// 	var canvas = document.createElement('canvas');
// 	canvas.width = img.width;
// 	canvas.height = img.height;
// 	canvas.getContext('2d').drawImage(base_image, 0, 0, img.width, img.height);
// 	console.log('<- checked');
// 	$('#state-map svg > *').each(function(){
// 		if(!$(this).attr('data-province-id')){

// 			var f = parseFloat($('.map-wrapper').attr('data-zoom'));
// 			var x = ($(this).offset().left - 25);
// 			var y = ($(this).offset().top + 1);

// 			var elem = document.elementFromPoint(x, y);
// 			if($(elem) != $(this)){
// 				var changedElem = false;
// 				var w = $(this).width();
// 				var h = $(this).height();
// 				var incW = 0;
// 				var incH = 0;
// 				while(!changedElem){
// 					var elem = document.elementFromPoint(x + incW, y + incH);
// 					if($(elem) != $(this)){
// 						if(incW < w){
// 							incW++;
// 						}
// 						if(incH < h){
// 							incH++;
// 						}
// 					}else{
// 						changedElem = true;
// 					}

// 					if(incH == h && incW == w){
// 						changedElem = true;
// 					}
// 				}
// 			}

// 			console.log('<- checked');

// 			var rgb = canvas.getContext('2d').getImageData((x + 2), (y + 2), 1, 1).data;
// 			var hex = rgbToHex(rgb[0],rgb[1],rgb[2]);

// 			$(this).attr('style', 'fill:'+hex);

// 		}
// 	});
// 	console.log('DONE!!!');
// };


// window.componentToHex = function(c) {
// 	var hex = c.toString(16);
// 	return hex.length == 1 ? "0" + hex : hex;
// }

// window.rgbToHex = function(r, g, b) {
// 	const rgb = (r << 16) | (g << 8) | (b << 0);
// 		return '#' + (0x1000000 + rgb).toString(16).slice(1);
// 	return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
// }