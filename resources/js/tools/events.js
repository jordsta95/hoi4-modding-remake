$('.event-group-edit').on('click', function(){
	$('#help-event-'+$(this).attr('data-id')).toggleClass('open');
});

$('.event-group-delete').on('click', function(){
	var id = $(this).attr('data-id');
	var list = $(this).closest('.mod-focus-list-item');
	jconfirm({
		title: $('.delete-title').text(),
		content: $('.delete-text').text(),
		buttons: {   
			ok: {
				text: $('.delete-confirm').text(),
				btnClass: 'button',
				keys: ['enter'],
				action: function(){
					const config = {
						headers: {
							'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content,
						}
					}
					let formData = new FormData();
					axios.post('/events/delete-group/'+id, formData, config);
					list.remove();
				}
			},
			cancel: {
				text: $('.delete-cancel').text(),
				btnClass: 'button invert white'
			}
		}
	});
});


var eventsImportFile = null;
var localisationEventsImportFile = null;
$('.importEvents').on('click', function(){
	readEventsFiles();
});

//Read the events file, and parse it to JSON
function readEventsFiles(){
	var reader = new FileReader();
	reader.readAsText(document.getElementById('import-events').files[0], "UTF-8");
	reader.onload = function(evt){
		eventsImportFile = hoiFileToJSON(evt.target.result, 0);
		if(eventsImportFile.hasOwnProperty('country_event') || eventsImportFile.hasOwnProperty('news_event')){
			getEventsLocalisationData();
		}else{
			alert('Invalid events file');
		}
	};
}

function getEventsLocalisationData(){
	var reader = new FileReader();
	reader.readAsText(document.getElementById('import-localisation').files[0], "UTF-8");
	reader.onload = function(evt){
		localisationEventsImportFile = localisationToJSON(evt.target.result, 0);
		attachLocalisationToImportedEvents();
	};
}

var eventImportJSON = null;
function attachLocalisationToImportedEvents(){
	eventImportJSON = {};
	var types = ['country_event', 'news_event'];
	$.each(types, function(key, type){
		if(eventsImportFile.hasOwnProperty(type)){
			Object.keys(eventsImportFile[type]).forEach(function(key){
				var category = eventsImportFile[type][key];
				var data = {options: {}, data: {}, type: ((type == 'country_event') ? 'country' : 'news')};
				$.each(category, function(k, v){
					if(k == 'title'){
						data.name = localisationEventsImportFile[v]
					}
					else if(k == 'picture'){
						data.picture = v;
					}
					else if(k == 'id'){

					}
					else if(k == 'option'){
						if(!Array.isArray(v)){
							v = [v];
						}
						$.each(v, function(n, opt){
							var option = {data: {effect:""}};
							$.each(opt, function(o,f){
								if(o == 'name'){
									option.name = localisationEventsImportFile[opt.name];
								}
								else if(o == 'ai_chance'){
									var chance = 1;
									var rep = f.replace(/\t/g, '');
									var split = rep.split(/\n/);
									$.each(split, function(l, line){
										if(chance == 1){
											var next = false;
											var parts = line.split('=', function(q,w){
												if(chance == 1){
													if(w == 'factor'){
														next = true;
													}
													if(next){
														chance = w;
													}
												}
											});
										}
									});
									option.data.ai_chance = chance;
								}
								else if(o == 'trigger'){
									option.data.trigger = f;
								}
								else if(o == 'hidden_effect'){
									option.data.hidden_effect = f;
								}
								else{
									option.data.effect += "\n"+f;
								}
							});
							data.options[n] = option;
						});
					}
					else if(k == 'desc'){
						data.data['description'] = localisationEventsImportFile[v];
					}
					else{
						data.data[k] = v;
					}
				});	
				eventImportJSON[type+'_'+key] = data;
			});
		}
	});

	importEvents();
}

function importEvents(){
	const config = {
		headers: {
			'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content,
		}
	}
	let formData = new FormData();
	formData.append('id', $('.events').attr('id'));
	formData.append('import', JSON.stringify(eventImportJSON));
	axios.post('/events/import', formData, config).then(function (response) {
		window.location.href = window.location.href;
	});
}