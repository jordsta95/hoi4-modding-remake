var decisionImportFile = null;
var localisationDecisionImportFile = null;
$('.importDecisions').on('click', function(){
	readDecisionFiles();
});

//Read the decision file, and parse it to JSON
function readDecisionFiles(){
	var reader = new FileReader();
	reader.readAsText(document.getElementById('import-decision').files[0], "UTF-8");
	reader.onload = function(evt){
		decisionImportFile = hoiFileToJSON(evt.target.result, 0, 3);
		if(Object.keys(decisionImportFile).length > 1){
			//TODO! USE THIS CHECK!
		}else{
			
		}
		getDecisionLocalisationData();
	};
}

function getDecisionLocalisationData(){
	var reader = new FileReader();
	reader.readAsText(document.getElementById('import-localisation').files[0], "UTF-8");
	reader.onload = function(evt){
		localisationDecisionImportFile = localisationToJSON(evt.target.result, 0);
		console.log(localisationDecisionImportFile);
		attachLocalisationToImportedDecisions();
	};
}

var decisionImportJSON = null;
function attachLocalisationToImportedDecisions(){
	decisionImportJSON = {};
	Object.keys(decisionImportFile).forEach(function(key){
		var category = decisionImportFile[key];
		var catData = {category: {}, list: {}};
		if(localisationDecisionImportFile.hasOwnProperty(key)){
			if(localisationDecisionImportFile[key].hasOwnProperty('name')){
				catData.category.name = localisationDecisionImportFile[key].name;
			}
			if(localisationDecisionImportFile[key].hasOwnProperty('description')){
				catData.category.description = localisationDecisionImportFile[key].description;
			}
		}else{

		}
		$.each(category, function(id, vals){
			var data = {data: {}};
			$.each(vals, function(n,v){
				if(n != 'icon'){
					data.data[n] = v;
				}else{
					//This will be handled on upload
					data.icon = v;
				}
			});
			if(localisationDecisionImportFile.hasOwnProperty(id)){
				if(localisationDecisionImportFile[id].hasOwnProperty('name')){
					data.name = localisationDecisionImportFile[id].name;
				}
				if(localisationDecisionImportFile[id].hasOwnProperty('description')){
					data.description = localisationDecisionImportFile[id].description;
				}
			}else{
				data.name = id;
			}
			
			catData.list[id] = data;
		});
		decisionImportJSON[key] = catData;
	});
	importDecisionCategoryCheck()
}

function importDecisionCategoryCheck(){
	var buttons = {};
	$.each(decisionImportJSON, function(key, value){
		buttons[key] = { text: value.category.name, btnClass: 'button', action: function(){ importDecisions(key) } };
	});

	jconfirm({
		title: $('.import-title').text(),
		content: $('.import-desc').text(),
		buttons: buttons
	});
	
}

function importDecisions(key){
	const config = {
		headers: {
			'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content,
		}
	}
	let formData = new FormData();
	formData.append('id', $('.decision').attr('id'));
	formData.append('import', JSON.stringify(decisionImportJSON[key]));
	axios.post('/decision/import', formData, config).then(function (response) {
		window.location.href = window.location.href;
	});
}

$('.mod-focus-list-item .decision-delete').on('click', function(){
	var id = $(this).attr('data-id');
	var list = $(this).closest('.mod-focus-list-item');
	jconfirm({
		title: $('.delete-title').text(),
		content: $('.delete-text').text(),
		buttons: {   
			ok: {
				text: $('.delete-confirm').text(),
				btnClass: 'button',
				keys: ['enter'],
				action: function(){
					const config = {
						headers: {
							'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content,
						}
					}
					let formData = new FormData();
					axios.post('/decision/delete/'+id, formData, config);
					list.remove();
				}
			},
			cancel: {
				text: $('.delete-cancel').text(),
				btnClass: 'button invert white'
			}
		}
	});
});