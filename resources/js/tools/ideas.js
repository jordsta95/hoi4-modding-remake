$('.idea-group-edit').on('click', function(){
	$('#help-idea-'+$(this).attr('data-id')).toggleClass('open');
});

$('.idea-group-delete').on('click', function(){
	var id = $(this).attr('data-id');
	var list = $(this).closest('.mod-focus-list-item');
	jconfirm({
		title: $('.delete-title').text(),
		content: $('.delete-text').text(),
		buttons: {   
			ok: {
				text: $('.delete-confirm').text(),
				btnClass: 'button',
				keys: ['enter'],
				action: function(){
					const config = {
						headers: {
							'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content,
						}
					}
					let formData = new FormData();
					axios.post('/ideas/delete-group/'+id, formData, config);
					list.remove();
				}
			},
			cancel: {
				text: $('.delete-cancel').text(),
				btnClass: 'button invert white'
			}
		}
	});
});

var ideasImportFile = null;
var localisationIdeasImportFile = null;
$('.importIdeas').on('click', function(){
	readIdeasFiles();
});

//Read the ideas file, and parse it to JSON
function readIdeasFiles(){
	var reader = new FileReader();
	reader.readAsText(document.getElementById('import-ideas').files[0], "UTF-8");
	reader.onload = function(evt){
		ideasImportFile = hoiFileToJSON(evt.target.result, 0, 3);
		if(ideasImportFile.hasOwnProperty('ideas')){
			getIdeaLocalisationData();
		}else{
			alert('Invalid ideas file');
		}
	};
}

function getIdeaLocalisationData(){
	var reader = new FileReader();
	reader.readAsText(document.getElementById('import-localisation').files[0], "UTF-8");
	reader.onload = function(evt){
		localisationIdeasImportFile = localisationToJSON(evt.target.result, 0);
		attachLocalisationToImportedIdeas();
	};
}

var ideaImportJSON = null;
function attachLocalisationToImportedIdeas(){
	ideaImportJSON = {};
	Object.keys(ideasImportFile.ideas).forEach(function(key){
		var category = ideasImportFile.ideas[key];
		$.each(category, function(id, vals){
			if(typeof vals == "string"){
		      	return true;
		      }
			var data = {options: {}, group: key};
			$.each(vals, function(n,v){
				if(n != 'picture'){
					data.options[n] = v;
				}else{
					//This will be handled on upload
					data.picture = v;
				}
			});
			if(localisationIdeasImportFile.hasOwnProperty(id)){
				if(localisationIdeasImportFile[id].hasOwnProperty('name')){
					data.name = localisationIdeasImportFile[id].name;
				}
				// if(localisationIdeasImportFile[id].hasOwnProperty('description')){
				// 	data.description = localisationIdeasImportFile[id].description;
				// }
			}else{
				data.name = id;
			}
			ideaImportJSON[id] = data;
		});
	});

	importIdeas();
}

function importIdeas(){
	const config = {
		headers: {
			'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content,
		}
	}
	let formData = new FormData();
	formData.append('id', $('.ideas').attr('id'));
	formData.append('import', JSON.stringify(ideaImportJSON));
	axios.post('/ideas/import', formData, config).then(function (response) {
		window.location.href = window.location.href;
	});
}