window.onload = function(){
	focusConnectors();
	calcContinuousFocusXY();
}

// Figure out which focuses, if any, require a connector
window.focusConnectors = function(){
	$('.focus-connector').remove();

	var focusList = {};
	$('.focus-json').each(function(){
		let focusJson = JSON.parse($(this).text());
		focusList[focusJson.id] = {'top': focusJson.y, 'left': focusJson.x};
		if(focusJson.data.hasOwnProperty('prerequisite')){
			focusList[focusJson.id]['prerequisite'] = focusJson.data.prerequisite;
		}
		if(focusJson.data.hasOwnProperty('mutually_exclusive')){
			focusList[focusJson.id]['mutually_exclusive'] = focusJson.data.mutually_exclusive;
		}
	});
	

	Object.keys(focusList).forEach(function(key){
		let obj = focusList[key];
		if(obj.hasOwnProperty('prerequisite')){
			let preReq = obj.prerequisite.toString().replaceAll('OR', 'AND');
			preReq = preReq.split('AND');
			preReq.forEach(function(id){
				makeConnection('prerequisite', id, key);
			});
		}
		if(obj.hasOwnProperty('mutually_exclusive')){
			let mE = obj.mutually_exclusive.toString().replaceAll('OR', 'AND');
			mE = mE.split('AND');
			mE.forEach(function(id){
				makeConnection('mutual', id, key);
			});
		}
	});
}

// Make the connectors for focuses
window.makeConnection = function(type, from, to){
	var connectionWidth = 220;
	var connectionHeight = 150;
	var connectionOffset = 40;
	if(!$('[data-id="'+from+'"]').length || !$('[data-id="'+to+'"]').length){
		return;
	}
	var fromJSON = JSON.parse($('[data-id="'+from+'"] .focus-json').text());
	var toJSON = JSON.parse($('[data-id="'+to+'"] .focus-json').text());

	if(fromJSON.x > toJSON.x){
		var right = fromJSON;
		var left = toJSON;
	}else{
		var left = fromJSON;
		var right = toJSON;
	}

	if(fromJSON.y > toJSON.y){
		var top = fromJSON;
		var bottom = toJSON;
	}else{
		var top = fromJSON;
		var bottom = toJSON;
	}

	var connector = '';
	if(type == 'prerequisite'){
		if(left == top){
			var verticalLeft = parseInt($('[data-id="'+left.id+'"]').css('left').replace('px', '')) + (connectionWidth / 2);
			var verticalTop = (left.y * connectionHeight) + (left.y * connectionOffset) + (connectionHeight - 30);
			
			var vertical2Left = parseInt($('[data-id="'+right.id+'"]').css('left').replace('px', '')) + (connectionWidth / 2);
			var vertical2Height = parseInt($('[data-id="'+right.id+'"]').css('top').replace('px', '')) - verticalTop;

			var horizontalLeft = verticalLeft;
			var horizontalWidth = vertical2Left - verticalLeft;
			if(horizontalWidth < 0){
				horizontalWidth = 0;
			}
		}else{
			var verticalLeft = parseInt($('[data-id="'+right.id+'"]').css('left').replace('px', '')) + (connectionWidth / 2);
			var verticalTop = (right.y * connectionHeight) + (right.y * connectionOffset) + (connectionHeight - 30);
			
			var vertical2Left = parseInt($('[data-id="'+left.id+'"]').css('left').replace('px', '')) + (connectionWidth / 2);
			var vertical2Height = parseInt($('[data-id="'+left.id+'"]').css('top').replace('px', '')) - verticalTop;

			var horizontalLeft = vertical2Left;
			var horizontalWidth = verticalLeft - vertical2Left;
			if(horizontalWidth < 0){
				horizontalWidth = 0;
			}
		}

		
		connector += '<div data-between="'+left.id+'-'+right.id+'" class="focus-connector '+type+'" data-direction="vertical" style="left: '+verticalLeft+'px; top: '+verticalTop+'px; height: 20px;"></div>';
		connector += '<div data-between="'+left.id+'-'+right.id+'" class="focus-connector '+type+'" data-direction="horizontal" style="left: '+horizontalLeft+'px; top: '+(verticalTop + 20)+'px; width: '+horizontalWidth+'px"></div>';
		connector += '<div data-between="'+left.id+'-'+right.id+'" class="focus-connector '+type+'" data-direction="vertical" style="left: '+vertical2Left+'px; top: '+(verticalTop + 20)+'px; height: '+vertical2Height+'px"></div>';
	}else{
		var horizontalLeft = parseInt($('[data-id="'+left.id+'"]').css('left').replace('px', '')) + connectionWidth;
		var horizontalTop = parseInt($('[data-id="'+left.id+'"]').css('top').replace('px', '')) + (connectionHeight / 2) + 20;
		
		var horizontal2Left = parseInt($('[data-id="'+right.id+'"]').css('left').replace('px', ''));
		var horizontalBetween = horizontal2Left - horizontalLeft;
		horizontal2Left = horizontal2Left - (horizontalBetween / 2);
		var horizontal2Top = parseInt($('[data-id="'+right.id+'"]').css('top').replace('px', '')) + (connectionHeight / 2) + 20;


		var verticalLeft = horizontalLeft;
		var verticalHeight = horizontal2Top - horizontalTop;
		if(verticalHeight <= 20){
			verticalHeight = 20;
		}

		var width = (horizontalBetween / 2);
		// if(width < 0){
		// 	width = width * -1;
		// 	horizontalLeft = horizontalLeft - width;
		// 	horizontal2Left = horizontal2Left - width;
		// }
		connector += '<div data-between="'+left.id+'-'+right.id+'" class="focus-connector left '+type+'" data-direction="horizontal" style="left: '+horizontalLeft+'px; top: '+horizontalTop+'px; width: '+width+'px;"></div>';
		connector += '<div data-between="'+left.id+'-'+right.id+'" class="focus-connector centre '+type+'" data-direction="vertical" style="left: '+(horizontalLeft + (horizontalBetween / 2))+'px; top: '+(horizontalTop - 13)+'px; height: '+verticalHeight+'px">!</div>';
		connector += '<div data-between="'+left.id+'-'+right.id+'" class="focus-connector right '+type+'" data-direction="horizontal" style="left: '+horizontal2Left+'px; top: '+horizontal2Top+'px; width: '+width+'px"></div>';
	}
	$('.focus-connector-container').append(connector);	
}


$('#focus-multi-select').on('change', function(){
	if($(this).prop('checked')){
		$("#all-focuses").selectable();
	}else{
		$("#all-focuses").selectable("destroy");
	}
});

var focusClickPosition = {x:0, y:0}; //Used in scaling
var focusTreeZoom = 1;
var focusGridX = 200;
var focusGridY = 190;

function calcContinuousFocusXY(){
	var left = $('.continuous-focuses').data('x');
	var top = $('.continuous-focuses').data('y');
	var focusWidth = 220;

	var toLeft = -40 + (left * focusGridX);
	if(x < 1){
		toLeft = 0;
	}

	var toTop = (top * focusGridY);

	$('.continuous-focuses').css({top: toTop+'px', left: toLeft+'px'})
}

$( ".continuous-focuses" ).draggable({ 
	grid: [ focusGridX, focusGridY ],
	snap: ".national-focus.edit",
	start: function(event, ui){
		if (ui.helper.hasClass('ui-selected')){ 
		 	focusesToMove = $('.continuous-focuses.ui-selected');
		}else{
	        focusesToMove = $(ui.helper);
	        $('.continuous-focuses.ui-selected').removeClass('ui-selected');
	    }

	    focusClickPosition.x = event.clientX;
		focusClickPosition.y = event.clientY;
	},
	drag: function(event, ui){
		var original = ui.originalPosition;
		var x = $(this).attr('data-x');
		var y = $(this).attr('data-y');
		

		var posX = event.clientX - focusClickPosition.x + original.left;
		var posY = event.clientY - focusClickPosition.y + original.top;
		ui.position = {
            left: Math.round((posX / focusTreeZoom) / focusGridX) * focusGridX,
            top:  Math.round((posY/ focusTreeZoom) / focusGridY) * focusGridY
        };

  //       var xMove = x - Math.floor(ui.position.left / focusGridX);
  //       var yMove = y - Math.floor(ui.position.top / focusGridY);
		// moveSelectedFocuses(xMove, yMove);
	},
	stop: function(){
		var left = $(this).css('left');
		var top = $(this).css('top');
		var parent = $(this);

		left = parseInt(left.replace('px', ''));
		top = parseInt(top.replace('px', ''));
		if(0 > left){
			left = 0;
		}
		if(0 > top){
			top = 0;
		}

		var newX = Math.floor(left / 200);
		var newY = Math.floor(top / 190);

		$(this).attr('data-y', newY);
		$(this).attr('data-x', newX);
		$(this).css({
			left:left+'px',
			top:top+'px'
		})


		const config = {
			headers: {
				'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content,
			}
		}
		let formData = new FormData();
		formData.append('id', $(".national-focus.edit").attr('id'));
		formData.append('x', newX);
		formData.append('y', newY);
		axios.post('/focus-tree/update-continuous', formData, config);
	}
});

$('.focus-zoom-controls .increase').on('click', increaseFocusZoom);
$('.focus-zoom-controls .decrease').on('click', decreaseFocusZoom);
$('.focus-zoom-controls').bind('mousewheel', function(event){
	if(event.originalEvent.wheelDelta >= 0){
		increaseFocusZoom();
	}else{
		decreaseFocusZoom();
	}
});

//Reset zoom on click of a focus so popup appears correctly
var lastClickedFocus;
$('.focus-item').on('click', function(){
	lastClickedFocus = $(this).attr('data-id')
	setTimeout(function(){
		if($('.focus-item[data-id="'+lastClickedFocus+'"]').hasClass('active')){
			$('#all-focuses').attr('style','');
		}
	}, 500);
});
$('[data-help="create_focus"]').on('click', function(){
	$('#all-focuses').attr('style','');
});

function increaseFocusZoom(){
	if(focusTreeZoom < 1.5){
		focusTreeZoom += 0.1;
		focusTreeZoom = Math.round(focusTreeZoom * 100) / 100;
		updateFocusZoom();
	}
}
function decreaseFocusZoom(){
	if(focusTreeZoom > 0.2){
		focusTreeZoom -= 0.1;
		focusTreeZoom = Math.round(focusTreeZoom * 100) / 100;
		updateFocusZoom();
	}
}
window.updateFocusZoom = function(){
	$('.focus-zoom-controls .current-zoom').text(Math.round(focusTreeZoom * 100));
	if(focusTreeZoom != 1){
		$('#all-focuses').css('transform', 'scale('+focusTreeZoom+')');
	}else{
		$('#all-focuses').attr('style','');
	}
}


var focusesToMove;
function moveSelectedFocuses(l, t){
    focusesToMove.each(function(){
        var x = $(this).attr('data-x');
        var y = $(this).attr('data-y');

        $(this).css({
        	left: ((x - l) * focusGridX),
        	top: ((y - t) * focusGridY)
        });
    });
}

window.resetFocusDraggable = function(){
	if($('.national-focus.edit').length){
		disableFocusSelectClick = true;
		$( ".national-focus.edit .focus-item" ).draggable({ 
			snap: ".national-focus.edit",
			start: function(event, ui){
				if (ui.helper.hasClass('ui-selected')){ 
				 	focusesToMove = $('.focus-item.ui-selected');
				}else{
			        focusesToMove = $(ui.helper);
			        $('.focus-item.ui-selected').removeClass('ui-selected');
			    }

			    focusClickPosition.x = event.clientX;
        		focusClickPosition.y = event.clientY;
			},
			drag: function(event, ui){
				var original = ui.originalPosition;
				var x = $(this).attr('data-x');
				var y = $(this).attr('data-y');
				

				var posX = event.clientX - focusClickPosition.x + original.left;
				var posY = event.clientY - focusClickPosition.y + original.top;
				ui.position = {
		            left: Math.round((posX / focusTreeZoom) / focusGridX) * focusGridX,
		            top:  Math.round((posY/ focusTreeZoom) / focusGridY) * focusGridY
		        };

		        var xMove = x - Math.floor(ui.position.left / focusGridX);
		        var yMove = y - Math.floor(ui.position.top / focusGridY);
				moveSelectedFocuses(xMove, yMove);
				focusConnectors();
			},
			stop: function(event, ui){
				disableFocusSelectClick = true;
                setTimeout(function(){
                    disableFocusSelectClick = false;
                }, 200);

				

				if($('#focus-multi-select').prop('checked')){
					var loop = $('.focus-item.ui-selected');
				}else{
					var loop = $('.focus-item[data-id="'+$(this).attr('data-id')+'"]');
				}
				var positionUpdates = [];

				loop.each(function(){
					var left = $(this).css('left');
					var top = $(this).css('top');
					var parent = $(this);
					left = parseInt(left.replace('px', ''));
					top = parseInt(top.replace('px', ''));
					if(0 > left){
						left = 0;
					}
					if(0 > top){
						top = 0;
					}


					var newX = Math.round(left / focusGridX);
					var newY = Math.round(top / focusGridY);

					$(this).attr('data-y', newY);
					$(this).attr('data-x', newX);
					$(this).css({
						left:left+'px',
						top:top+'px'
					})

					var json = JSON.parse(parent.find('.focus-json').text());
					json['x'] = newX;
					json['y'] = newY;
					parent.find('.focus-json').text(JSON.stringify(json));

					positionUpdates.push({id:json['id'], x: newX, y: newY});
				});

				const config = {
					headers: {
						'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content,
					}
				}
				let formData = new FormData();
				formData.append('update', JSON.stringify(positionUpdates));
				axios.post('/focus/update-position', formData, config);

				focusConnectors();
			}
		});
	}
}
resetFocusDraggable();

var focusTreeImportFile = null;
var localisationTreeImportFile = null;
$('.importFocusTree').on('click', function(){
	readNationalFocusFiles();
});

//Read the national focus tree file, and parse it to JSON
function readNationalFocusFiles(){
	var reader = new FileReader();
	reader.readAsText(document.getElementById('import-focus_tree').files[0], "UTF-8");
	reader.onload = function(evt){
		focusTreeImportFile = hoiFileToJSON(evt.target.result, 0);
		//If it is a focus tree - Swap out relative positions to absolute ones
		if(focusTreeImportFile.hasOwnProperty('focus_tree')){
			getFocusTreeAbsolutePositions();
		}else{
			alert('Invalid focus tree');
		}
	};
}

var getFocusTreeAbsolutePositionsComplete = false;
var focusCheckIteration = 0;
var importFocusListPositions = {};
var focusImportMinX = 0;
var focusImportMinY = 0;
//Get absolute positions, from relative focus ID
function getFocusTreeAbsolutePositions(){
	if(!getFocusTreeAbsolutePositionsComplete && !$('body').find('.loader-overlay').length){
		$('body').append('<div class="loader-overlay"><svg class="feather-icon icon-loader"><use xlink:href="#loader"></use></svg></div>');
	}

	//Set to true, so that the loop can set to false if there's a relative ID in there
	getFocusTreeAbsolutePositionsComplete = true;

	if(focusTreeImportFile.focus_tree.hasOwnProperty('focus') && !importFocusListPositions.hasOwnProperty(focus.id)){
		Object.keys(focusTreeImportFile.focus_tree.focus).forEach(function(key){
			var focus = focusTreeImportFile.focus_tree.focus[key];
			if(focus.hasOwnProperty('relative_position_id')){
				//Relative position found and needs sorting - their may be more, so set to false
				getFocusTreeAbsolutePositionsComplete = false;

				
				var rel = focus.relative_position_id;
				//Because some idiot at PDX decided to write the thing in twice...
		        if(Array.isArray(rel)){
		        	rel = rel[rel.length - 1]; //Get the last, as that will be what the game uses
		        }
		        //The relative focus exists in the absolute position list
		        if (importFocusListPositions.hasOwnProperty(rel)) {
		        	var relativePos = importFocusListPositions[rel];

					focus.x = (parseInt(relativePos.x) + parseInt(focus.x));
					focus.y = (parseInt(relativePos.y) + parseInt(focus.y));

					importFocusListPositions[focus.id] = {'x' : focus.x, 'y': focus.y}

					//Update the list
					delete focus.relative_position_id;
					focusTreeImportFile.focus_tree.focus[key] = focus;
				}
			}else{
				//Don't add it again
				if(!importFocusListPositions.hasOwnProperty(focus.id)){
					importFocusListPositions[focus.id] = {'x':focus.x, 'y':focus.y};
				}
			}

			if(parseInt(focus.x) < focusImportMinX){
				focusImportMinX = parseInt(focus.x);
			}
			if(parseInt(focus.y) < focusImportMinY){
				focusImportMinY = parseInt(focus.y);
			}

		});
	}else{
		//No focuses to loop through, so we're done.
		getFocusTreeAbsolutePositionsComplete = true;
	}

	if(getFocusTreeAbsolutePositionsComplete){
		//Everything works perfectly, now to just ensure nothing is offscreen
		if(focusTreeImportFile.focus_tree.hasOwnProperty('focus')){
			var xOffset = (0 - focusImportMinX);
			var yOffset = (0 - focusImportMinX);
			Object.keys(focusTreeImportFile.focus_tree.focus).forEach(function(key){
				var focus = focusTreeImportFile.focus_tree.focus[key];
				if(focusImportMinX < 0){
					focus[x] = xOffset + parseInt(focus.x);
				}
				if(focusImportMinY < 0){
					focus[y] = xOffset + parseInt(focus.y);
				}
				focusTreeImportFile.focus_tree.focus[key] = focus;
			});
		}
		getFocusLocalisationData();
		
	}else{
		getFocusTreeAbsolutePositions();
	}
}

function getFocusLocalisationData(){
	var reader = new FileReader();
	reader.readAsText(document.getElementById('import-localisation').files[0], "UTF-8");
	reader.onload = function(evt){
		localisationImportFile = localisationToJSON(evt.target.result, 0);
		attachLocalisationToImportedFocuses();
	};
}

function attachLocalisationToImportedFocuses(){
	Object.keys(focusTreeImportFile.focus_tree.focus).forEach(function(key){
		var focus = focusTreeImportFile.focus_tree.focus[key];
		if(localisationImportFile.hasOwnProperty(focus.id)){
			if(localisationImportFile[focus.id].hasOwnProperty('name')){
				focus['name'] = localisationImportFile[focus.id].name;
			}
			if(localisationImportFile[focus.id].hasOwnProperty('description')){
				focus['description'] = localisationImportFile[focus.id].description;
			}
		}
		focusTreeImportFile.focus_tree.focus[key] = focus;
	});

	importFocusTree();
}

function importFocusTree(){
	const config = {
		headers: {
			'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content,
		}
	}
	let formData = new FormData();
	formData.append('id', $('.national-focus').attr('id'));
	formData.append('import', JSON.stringify(focusTreeImportFile));
	axios.post('/focus-tree/import', formData, config).then(function (response) {
		window.location.href = window.location.href;
	});
}

//Focus relation builder

//TODO something similar but for tags for will_lead_to_war_with
var focusRelationBuilder = null;
if($('.national-focus').length){
	var focusEditPageLocalisationFile = JSON.parse(document.getElementById("help-text-file").innerHTML);
}
$('.national-focus').on('focus', '[name="data[mutually_exclusive]"],[name="data[prerequisite]"]', function(){
	if(focusRelationBuilder == $(this).attr('name')){
		//In case change tab/unfocus and focus window again
		return;
	}
	if($(this).attr('name') == 'data[allow_branch]'){
		var modalClass = 'singleOnly';
	}else{
		var modalClass = '';
	}
	//Remove old one, if exists
	$('#focusRelationModal').remove();

	//Use name, as just setting to $(this) doesn't work
	focusRelationBuilder = $(this).attr('name');
	var val = $(this).val();
	var relationList = '<div class="focus-relation-list-wrapper">';
	if(val){
		var or = val.split('OR');
		or.forEach(function(arr){
			relationList += '<div class="focus-relation-list">';
				var and = arr.split('AND');
				and.forEach(function(id){
					var rel = $('.focus-item[data-id="'+id+'"]');
					relationList += '<div class="focus-item small" data-id="'+id+'">';
						relationList += '<img class="focus-image" src="'+rel.find('.focus-image').attr('src')+'">';
						relationList += '<div class="focus-title">'+rel.find('.focus-title').text()+'</div>';
					relationList += '</div>';
				});
			relationList += '</div>';
		});
	}
	relationList += '<span class="addNewFocusRelationGroup addToBuilder">+</span>';
	relationList += '</div>';

	var modal = '<div class="modal force-second '+modalClass+'" id="focusRelationModal">';
		modal += '<div class="modal-header">';
			modal += '<h5>'+focusEditPageLocalisationFile.relations.title+'</h5>';
			modal += '<div id="closeRelation" class="close"><svg class="feather-icon icon-x"><use xlink:href="#x"></use></svg></div>';
		modal += '</div>';
		modal += '<div class="modal-content text-centre">';
			modal += '<p>'+focusEditPageLocalisationFile.relations.description+'</p>';
			modal += relationList;
			modal += '<p>&nbsp;</p>';
			modal += '<p><button class="button" id="submitFocusRelation">'+focusEditPageLocalisationFile.relations.update+'</button></p>';
		modal += '</div>';
	modal += '</div>';

	$('body').append(modal);

	$('.addNewFocusRelationGroup').on('click', function(){
		$('<div class="focus-relation-list"></div>').insertBefore($(this));
		setFocusRelationClickHandlers();
	});

	setFocusRelationClickHandlers();
});

//Remove relation builder on focus non-relation field
$('.national-focus').on('focus', '[tool="focus"] input,[tool="focus"] textarea,[tool="focus"] select', function(){
	if($(this).attr('name') == 'data[mutually_exclusive]' || $(this).attr('name') == 'data[prerequisite]'){
		return;
	}
	$('#focusRelationModal').remove();
});

var focusListToAddTo = null;

function setFocusRelationClickHandlers(){
	$('.focus-relation-list .focus-item,.focus-relation-list').unbind('click');
	$('.focus-relation-list .focus-item').on('click', function(){
		var parent = $(this).parent();
		$(this).remove();
		if(!parent.find('.focus-item').length){
			parent.remove();
		}
	});

	$('.focus-relation-list').on('click', function(e){
		if (e.target !== this){ return; }
		var content = '<div class="modal-cover">';
			content += '<div class="input-wrapper">';
				content += '<input class="default-input" id="search-foci" type="text">';
			content += '</div>';
			content += '<div class="focus-search-list">';
				$('.focus-item').each(function(){
					content += '<div class="focus-item small" data-id="'+$(this).attr('data-id')+'">';
						content += '<img class="focus-image" src="'+$(this).find('.focus-image').attr('src')+'">';
						content += '<div class="focus-title">'+$(this).find('.focus-title').text()+'</div>';
					content += '</div>';
				});
			content += '</div>';
			content += '<p>&nbsp;</p>';
			content += '<p class="text-centre"><span class="button invert" id="selectFocusRelation">'+focusEditPageLocalisationFile.relations.add_focus+'</span></p>';
		content += '</div>';

		focusListToAddTo = $(this);
		$('#focusRelationModal').append(content);

		$('.focus-search-list .focus-item').on('click', function(){
			$(this).toggleClass('selected');
		});

		$('#focusRelationModal #search-foci').on('keyup', function(){
			var val = $(this).val();
			if(val.length){
				var re = new RegExp(val, 'i');
				$('.focus-search-list .focus-item').each(function(){
					if($(this).text().match(re)){
						$(this).show();
					}else{
						$(this).hide();
					}
				})
			}else{
				$('.focus-search-list .focus-item').show();
			}
		});

		$('#selectFocusRelation').on('click', function(){
			var newFoci = '';
			$('.focus-search-list .focus-item.selected').each(function(){
				var rel = $(this);
				newFoci += '<div class="focus-item small" data-id="'+rel.attr('data-id')+'">';
					newFoci += '<img class="focus-image" src="'+rel.find('.focus-image').attr('src')+'">';
					newFoci += '<div class="focus-title">'+rel.find('.focus-title').text()+'</div>';
				newFoci += '</div>';
			});
			focusListToAddTo.append(newFoci);
			$('#focusRelationModal').find('.modal-cover').remove();
			setFocusRelationClickHandlers();
		});
	});

	$('#closeRelation').on('click', function(){
		$('#focusRelationModal').remove();
		focusRelationBuilder = null;
	});

	$('#submitFocusRelation').on('click', function(){
		var newVal = '';
		var orCount = 1;
		var orTotal = $('.focus-relation-list').length;
		$('.focus-relation-list').each(function(){
			var andCount = 1;
			var andTotal = $(this).find('.focus-item').length;
			$(this).find('.focus-item').each(function(){
				newVal += $(this).attr('data-id');
				if(andCount < andTotal){
					newVal += 'AND';
				}
				andCount++;
			});
			if(orCount < orTotal){
				newVal += 'OR';
			}
			orCount++;
		});
		
		$('[name="'+focusRelationBuilder+'"]').val(newVal);
		$('#focusRelationModal').remove();
		focusRelationBuilder = null;
	});
}

//TODO state chooser

//TODO continous focus box moving thing

$('.focus-tree-delete').on('click', function(){
	var id = $(this).attr('data-id');
	var list = $(this).closest('.mod-focus-list-item');
	jconfirm({
		title: $('.delete-title').text(),
		content: $('.delete-text').text(),
		buttons: {   
			ok: {
				text: $('.delete-confirm').text(),
				btnClass: 'button',
				keys: ['enter'],
				action: function(){
					const config = {
						headers: {
							'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content,
						}
					}
					let formData = new FormData();
					axios.post('/focus-tree/delete/'+id, formData, config);
					list.remove();
				}
			},
			cancel: {
				text: $('.delete-cancel').text(),
				btnClass: 'button invert white'
			}
		}
	});
});

$('.focus-tree-edit').on('click', function(){
	var id = $(this).attr('data-id');
	$('#help-tree-'+id).addClass('open');
});
$(window).on('load', function(){
	if($('#focusStyle').length){
		$(window).on('resize', function(){
			var zoom = Math.round(window.devicePixelRatio * 100);
			if(zoom < 90 || zoom > 110){
				$('#focusStyle').html('.focus-title{ display: none; }');
			}else{
				$('#focusStyle').html('');
			}
		});
	}
});