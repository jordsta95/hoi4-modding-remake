var ideologyImportFile = null;
var localisationIdeologyImportFile = {};
var factionIdeologyImportFile = {};
var ideologyToImport = null;
$('.importIdeology').on('click', function(){
	readIdeologyFiles();
});

//Read the ideas file, and parse it to JSON
function readIdeologyFiles(){
	var reader = new FileReader();
	reader.readAsText(document.getElementById('import-ideology').files[0], "UTF-8");
	reader.onload = function(evt){
		ideologyImportFile = hoiFileToJSON(evt.target.result, 0, 4);
		if(ideologyImportFile.hasOwnProperty('ideologies')){
			getIdeologyLocalisationData();
			getIdeologyFactionData();
		}else{
			alert('Invalid ideology file');
		}
	};
}

function getIdeologyLocalisationData(){
	var reader = new FileReader();
	reader.readAsText(document.getElementById('import-localisation').files[0], "UTF-8");
	reader.onload = function(evt){
		localisationIdeologyImportFile = localisationToJSON(evt.target.result, 0);
		attachLocalisationToImportedIdeology();
	};
}
function getIdeologyFactionData(){
	var reader = new FileReader();
	reader.readAsText(document.getElementById('import-factions').files[0], "UTF-8");
	reader.onload = function(evt){
		factionIdeologyImportFile = localisationToJSON(evt.target.result, 0);
	};
}

var ideologyImportJSON = null;
function attachLocalisationToImportedIdeology(){
	var buttons = {};
	$.each(ideologyImportFile.ideologies, function(key, value){
		if(localisationIdeologyImportFile.hasOwnProperty(key)){
			buttons[key] = { text: localisationIdeologyImportFile[key].name, btnClass: 'button', action: function(){ prepareImportIdeology(key) } };
		}else{
			buttons[key] = { text: key, btnClass: 'button', action: function(){ prepareImportIdeology(key) } };
		}
	});

	jconfirm({
		title: $('.import-title').text(),
		content: $('.import-desc').text(),
		buttons: buttons
	});
}

var ideologyImportJSON = null;
function prepareImportIdeology(key){
	ideologyImportJSON = {data: {}};
	if(localisationIdeologyImportFile.hasOwnProperty(key)){
		ideologyImportJSON.name = localisationIdeologyImportFile[key].name;
	}else{
		ideologyImportJSON.name = key;
	}
	$.each(ideologyImportFile.ideologies[key], function(key, value){
		if(key == 'color'){
			var c = value.color.color.replace('{', '').replace('}', '').trim();
			var colour = {};
			c = c.split(' ');
			colour.r = c[0];
			colour.g = c[1];
			colour.b = c[2];
			ideologyImportJSON.data.colour = colour;
		}
		else if(key == 'rules'){
			var rules = {};
			$.each(value, function(name, yn){
				rules[name] = (yn == 'yes' ? true : false);
			});
			ideologyImportJSON.rules = rules;
		}
		else if(key == 'modifiers'){
			var modifiers = {};
			$.each(value, function(name, v){
				modifiers[name] = v;
			});
			ideologyImportJSON.modifiers = modifiers;
		}
		else if(key == 'dynamic_faction_names'){
			var factions = [];
			var factionArray = value.dynamic_faction_names.dynamic_faction_names.replace('{', '').replace('}', '').replace(/\t/g, '').split(/\n/);
			var index = 0;
			$.each(factionArray, function(index, v){
				v = v.replace(/"/g, '');
				if(factionIdeologyImportFile.hasOwnProperty(v)){
					factions.push({name:factionIdeologyImportFile[v].name, index: index});
				}
				index++;
			});
			ideologyImportJSON.data.factions = factions;
		}
		else if(key == 'types'){
			var types = [];
			var index = 0;
			$.each(value, function(type, data){
				var t = {index:index, restrited: false};
				if(localisationIdeologyImportFile.hasOwnProperty(type)){
					t.name = localisationIdeologyImportFile[type].name;
				}else{
					t.name = type;
				}
				if(data.hasOwnProperty('can_be_randomly_selected')){
					if(data.can_be_randomly_selected == "no"){
						t.restrited = true;
					}
				}
				types.push(t);
				index++;
			});
			ideologyImportJSON.types = types;
		}
		else{
			ideologyImportJSON.data[key] = value;
		}
	});
	importIdeology();
}

function importIdeology(){
	const config = {
		headers: {
			'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content,
		}
	}
	let formData = new FormData();
	formData.append('id', $('.ideology').attr('id'));
	formData.append('import', JSON.stringify(ideologyImportJSON));
	axios.post('/ideology/import', formData, config).then(function (response) {
		window.location.href = window.location.href;
	});
}

$('.ideology-delete').on('click', function(){
	var id = $(this).attr('data-id');
	var list = $(this).closest('.mod-focus-list-item');
	jconfirm({
		title: $('.delete-title').text(),
		content: $('.delete-text').text(),
		buttons: {   
			ok: {
				text: $('.delete-confirm').text(),
				btnClass: 'button',
				keys: ['enter'],
				action: function(){
					const config = {
						headers: {
							'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content,
						}
					}
					let formData = new FormData();
					axios.post('/ideology/delete/'+id, formData, config);
					list.remove();
				}
			},
			cancel: {
				text: $('.delete-cancel').text(),
				btnClass: 'button invert white'
			}
		}
	});
});