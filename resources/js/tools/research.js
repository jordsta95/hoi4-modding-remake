var researchClickPosition = {x:0, y:0}; //Used in scaling
var researchTreeZoom = 1;
var researchGridX = 80;
var researchGridY = 80;

$('.research-zoom-controls .increase').on('click', increaseResearchZoom);
$('.research-zoom-controls .decrease').on('click', decreaseResearchZoom);
$('.research-zoom-controls').bind('mousewheel', function(event){
	if(event.originalEvent.wheelDelta >= 0){
		increaseResearchZoom();
	}else{
		decreaseResearchZoom();
	}
});

//Reset zoom on click of a research so popup appears correctly
var lastClickedresearch;
$('.research-item').on('click', function(){
	lastClickedresearch = $(this).attr('data-id')
	setTimeout(function(){
		if($('.research-item[data-id="'+lastClickedresearch+'"]').hasClass('active')){
			$('#all-researches').attr('style','');
		}
	}, 500);
});
$('[data-help="create_research"]').on('click', function(){
	$('#all-researches').attr('style','');
});

function increaseResearchZoom(){
	if(researchTreeZoom < 1.5){
		researchTreeZoom += 0.1;
		researchTreeZoom = Math.round(researchTreeZoom * 100) / 100;
		updateResearchZoom();
	}
}
function decreaseResearchZoom(){
	if(researchTreeZoom > 0.2){
		researchTreeZoom -= 0.1;
		researchTreeZoom = Math.round(researchTreeZoom * 100) / 100;
		updateResearchZoom();
	}
}
window.updateResearchZoom = function(){
	$('.research-zoom-controls .current-zoom').text(Math.round(researchTreeZoom * 100));
	if(researchTreeZoom != 1){
		$('#all-researches').css('transform', 'scale('+researchTreeZoom+')');
	}else{
		$('#all-researches').attr('style','');
	}
}


var researchesToMove;
function moveSelectedResearches(l, t){
    researchesToMove.each(function(){
        var x = $(this).attr('data-x');
        var y = $(this).attr('data-y');

        $(this).css({
        	left: ((x - l) * researchGridX),
        	top: ((y - t) * researchGridY)
        });
    });
}

window.resetResearchDraggable = function(){
	if($('.research.edit').length){
		disableresearchSelectClick = true;
		$( ".research.edit .research-item" ).draggable({ 
			snap: ".research.edit",
			start: function(event, ui){
				if (ui.helper.hasClass('ui-selected')){ 
				 	researchesToMove = $('.research-item.ui-selected');
				}else{
			        researchesToMove = $(ui.helper);
			        $('.research-item.ui-selected').removeClass('ui-selected');
			    }

			    researchClickPosition.x = event.clientX;
        		researchClickPosition.y = event.clientY;
			},
			drag: function(event, ui){
				var original = ui.originalPosition;
				var x = $(this).attr('data-x');
				var y = $(this).attr('data-y');
				

				var posX = event.clientX - researchClickPosition.x + original.left;
				var posY = event.clientY - researchClickPosition.y + original.top;
				ui.position = {
		            left: Math.round((posX / researchTreeZoom) / researchGridX) * researchGridX,
		            top:  Math.round((posY/ researchTreeZoom) / researchGridY) * researchGridY
		        };

		        var xMove = x - Math.floor(ui.position.left / researchGridX);
		        var yMove = y - Math.floor(ui.position.top / researchGridY);
				moveSelectedResearches(xMove, yMove);
				//researchConnectors();
			},
			stop: function(event, ui){
				disableResearchSelectClick = true;
                setTimeout(function(){
                    disableResearchSelectClick = false;
                }, 200);

				

				if($('#research-multi-select').prop('checked')){
					var loop = $('.research-item.ui-selected');
				}else{
					var loop = $('.research-item[data-id="'+$(this).attr('data-id')+'"]');
				}
				var positionUpdates = [];

				loop.each(function(){
					var left = $(this).css('left');
					var top = $(this).css('top');
					var parent = $(this);
					left = parseInt(left.replace('px', ''));
					top = parseInt(top.replace('px', ''));
					if(0 > left){
						left = 0;
					}
					if(0 > top){
						top = 0;
					}


					var newX = Math.round(left / researchGridX);
					var newY = Math.round(top / researchGridY);

					$(this).attr('data-y', newY);
					$(this).attr('data-x', newX);
					$(this).css({
						left:left+'px',
						top:top+'px'
					})

					var json = JSON.parse(parent.find('.research-json').text());
					json['x'] = newX;
					json['y'] = newY;
					parent.find('.research-json').text(JSON.stringify(json));

					positionUpdates.push({id:json['id'], x: newX, y: newY});
				});

				const config = {
					headers: {
						'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content,
					}
				}
				let formData = new FormData();
				formData.append('update', JSON.stringify(positionUpdates));
				axios.post('/research/update-position', formData, config);

				//researchConnectors();
			}
		});
	}
}
resetResearchDraggable();