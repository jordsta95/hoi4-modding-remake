$('#modDelete').on('click', function(){
	var id = $(this).attr('data-id');
	jconfirm({
		title: $('.mod-delete-title').text(),
		content: $('.mod-delete-text').text(),
		buttons: {   
			ok: {
				text: $('.mod-delete-confirm').text(),
				btnClass: 'button delete invert',
				keys: ['enter'],
				action: function(){
					const config = {
						headers: {
							'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content,
						}
					}
					let formData = new FormData();
					axios.post('/mod/delete/'+id, formData, config);
					window.location.href="/";
				}
			},
			cancel: {
				text: $('.delete-cancel').text(),
				btnClass: 'button'
			}
		}
	});
});

var downloadModCheckInterval = null;
$('.downloadMod').on('click', function(){
	downloadModCheckInterval = setInterval(function(){
		if(getCookie('downloadedMod')){
			clearInterval(downloadModCheckInterval);
			var errors = eval(getCookie('modDownloadErrors'));
			if(errors){
				generateBugReportModal();
				downloadModCheckInterval = setInterval(function(){
					if(generateBugReportModalImageGenerated){
						clearInterval(downloadModCheckInterval);
						$('.bug-modal-wrapper [name="title"]').val('Error exporting mod: '+$('h1').text());
						$.each(errors, function(key, err){
							$('.bug-modal-wrapper [name="description"]').val($('.bug-modal-wrapper [name="description"]').val()+decodeURIComponent(err.replace(/\+/g, ''))+"\r\n");
						});
					}
				}, 500);
			}
		}
	}, 100);
});