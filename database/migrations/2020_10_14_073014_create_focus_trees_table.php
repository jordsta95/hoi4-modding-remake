<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFocusTreesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('focus_trees', function (Blueprint $table) {
            $table->id();
            $table->foreignId('mod_id');
            $table->string('tag');
            $table->string('name');
            $table->string('tree_id');
            $table->string('lang')->default('english');
            $table->integer('x')->default(0);
            $table->integer('y')->default(8);
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('focus_trees');
    }
}
