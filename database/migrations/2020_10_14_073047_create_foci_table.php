<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFociTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foci', function (Blueprint $table) {
            $table->id();
            $table->foreignId('focus_tree_id');
            $table->string('game_id')->nullable();
            $table->string('name');
            $table->mediumText('description')->nullable();
            $table->integer('gfx');
            $table->integer('x')->default(0);
            $table->integer('y')->default(0);
            $table->mediumText('data')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foci');
    }
}
