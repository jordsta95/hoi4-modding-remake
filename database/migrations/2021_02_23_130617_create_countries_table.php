<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->id();
            $table->foreignId('mod_id');
            $table->string('mod')->default('vanilla'); //What mod preset to use
            $table->string('name'); //How to refer to the country
            $table->string('tag');
            $table->json('colour'); //{"r":1,"b":2,"g":3} - colour for nation
            $table->integer('capital'); //Which state ID the capital belongs in
            $table->string('language')->default('english'); //Language for exported files
            $table->string('culture')->default('European'); //Culture for generic military staff to inherit
            $table->json('general_data')->nullable(); //Data for generals/field marshals
            $table->json('admiral_data')->nullable(); //Data for admirals
            $table->json('ideas_data')->nullable(); //Custom ideas for the nation
            $table->json('names'); //{"male": [], "female": [], "surnames": [], "callsigns": []} - Names for generated advisers/generals/etc. to use
            $table->json('government'); //{"government_slot" : [ {"data"} ]}
            $table->json('misc_data')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
