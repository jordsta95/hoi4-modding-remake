<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStartDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('start_dates', function (Blueprint $table) {
            $table->id();
            $table->foreignId('mod_id');
            $table->string('name');
            $table->mediumText('description')->nullable();
            $table->string('language')->default('english');
            $table->string('date');
            $table->integer('gfx')->default(1);
            $table->json('data')->nullable();
            $table->json('countries')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('start_dates');
    }
}
