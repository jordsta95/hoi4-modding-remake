<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIdeologiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ideologies', function (Blueprint $table) {
            $table->id();
            $table->foreignId('mod_id');
            $table->string('name');
            $table->json('types')->nullable();
            $table->json('rules')->nullable();
            $table->json('modifiers')->nullable();
            $table->json('data')->nullable();
            $table->string('language')->default('english');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ideologies');
    }
}
