<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountryDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('country_dates', function (Blueprint $table) {
            $table->id();
            $table->foreignId('country_id');
            $table->integer('year');
            $table->integer('month');
            $table->integer('day');
            $table->json('states')->nullable();
            $table->json('research')->nullable(); //["option", "option"]
            $table->json('divisions')->nullable(); //[{"name" : "Name", ...}]
            $table->json('ideologies'); //[{"name": .., "type"..}]
            $table->json('misc_data')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('country_dates');
    }
}
