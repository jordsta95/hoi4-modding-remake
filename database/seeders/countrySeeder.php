<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Http\Contracts\Media;
use Storage;

class countrySeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run(){
		$placeholder = Storage::get('seeder/placeholder-image.png');

		$media = new Media();
		$media->setUser(0);

		$ideaIcon = Storage::files('seeder/leaders');
		foreach($ideaIcon as $icon){
			$i = explode('/', explode('.', $icon)[0]);
			$n = array_pop($i);
			$media->uploadImage(Storage::get($icon), ['tool' => 'country', 'extension' => 'png', 'size' => Storage::size($icon), 'name' => $n, 'path' => 'hoi4/leaders'], false);
		}		
	}
}
