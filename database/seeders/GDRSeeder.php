<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Http\Contracts\Media;
use Storage;

class GDRSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $placeholder = Storage::get('seeder/placeholder-image.png');

		$media = new Media();
		$media->setUser(0);

		$leaderIcon = Storage::files('seeder/gdr/leaders');
		foreach($leaderIcon as $icon){
			$i = explode('/', explode('.', $icon)[0]);
			$n = array_pop($i);
			$media->uploadImage(Storage::get($icon), ['tool' => 'country', 'extension' => 'png', 'size' => Storage::size($icon), 'name' => $n, 'path' => 'hoi4/leaders'], false);
		}

		$ideaIcon = Storage::files('seeder/gdr/idea');
		foreach($ideaIcon as $icon){
			$i = explode('/', explode('.', $icon)[0]);
			$n = array_pop($i);
			$media->uploadImage(Storage::get($icon), ['tool' => 'ideas', 'extension' => 'png', 'size' => Storage::size($icon), 'name' => $n, 'path' => 'hoi4/ideas'], false);
		}

		$eventsIcon = Storage::files('seeder/gdr/events');
		foreach($eventsIcon as $icon){
			$i = explode('/', explode('.', $icon)[0]);
			$n = array_pop($i);
			$media->uploadImage(Storage::get($icon), ['tool' => 'events', 'extension' => 'png', 'size' => Storage::size($icon), 'name' => $n, 'path' => 'hoi4/events'], false);
		}

		$focusIcon = Storage::files('seeder/gdr/focus');
		foreach($focusIcon as $icon){
			$i = explode('/', explode('.', $icon)[0]);
			$n = array_pop($i);
			$media->uploadImage(Storage::get($icon), ['tool' => 'events', 'extension' => 'png', 'size' => Storage::size($icon), 'name' => $n, 'path' => 'hoi4/focus'], false);
		}

		$decisionIcon = Storage::files('seeder/gdr/decision');
		foreach($decisionIcon as $icon){
			$i = explode('/', explode('.', $icon)[0]);
			$n = array_pop($i);
			$media->uploadImage(Storage::get($icon), ['tool' => 'decision', 'extension' => 'png', 'size' => Storage::size($icon), 'name' => $n, 'path' => 'hoi4/decision'], false);
		}
    }
}
