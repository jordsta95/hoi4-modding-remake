<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ForumCategory;
use App\Models\Forum;

class forumData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
    	//Localisation key
    	$categories = [
    		'site_talk',
    		'modders_corner',
    		'general_chat',
    		'international'
    	];
    	$categoryIDs = [];
    	foreach($categories as $order => $cat){
    		$categoryIDs[$cat] = ForumCategory::insertGetId(['localisation_key' => $cat, 'order' => $order]);
    	}

    	$forums = [
    		//Category => [
    		//	Localisation key => slug
    		//]
    		'site_talk' => [
    			'bug_report' => 'bug-reports',
    			'suggestions' => 'suggestions',
                'updates' => 'updates'
    		],
    		'modders_corner' => [
    			'team_search' => 'team-search',
    			'mod_discussion' => 'mod-discussion'
    		],
    		'general_chat' => [
    			'general_chat' => 'general-chat'
    		],
    		'international' => [
    			'international' => 'international'
    		]
    	];
    	foreach($forums as $cat => $forumList){
    		$catID = $categoryIDs[$cat];
    		foreach($forumList as $localisation => $slug){
    			Forum::insertGetId(['forum_category_id' => $catID, 'localisation_key' => $localisation, 'slug' => $slug]);
    		}
    	}
    }
}
