<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Http\Contracts\Media;
use Storage;

class mediaSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run(){
		$placeholder = Storage::get('seeder/placeholder-image.png');

		$media = new Media();
		$media->setUser(0);
		$media->uploadImage($placeholder, ['extension' => 'png', 'size' => Storage::size('seeder/placeholder-image.png'), 'name' => 'placeholder-image']);

		$focusIcons = Storage::files('seeder/focus-tree');
		foreach($focusIcons as $icon){
			$media->uploadImage(Storage::get($icon), ['tool' => 'focus', 'extension' => 'png', 'size' => Storage::size($icon), 'name' => str_replace(['seeder/focus-tree/','.png'], '', $icon), 'path' => 'hoi4/focus'], false);
		}

		$eventIcon = Storage::files('seeder/events');
		foreach($eventIcon as $icon){
			$media->uploadImage(Storage::get($icon), ['tool' => 'event', 'extension' => 'png', 'size' => Storage::size($icon), 'name' => str_replace(['seeder/events/','.png'], '', $icon), 'path' => 'hoi4/events'], false);
		}

		$ideaIcon = Storage::files('seeder/ideas');
		foreach($ideaIcon as $icon){
			$media->uploadImage(Storage::get($icon), ['tool' => 'ideas', 'extension' => 'png', 'size' => Storage::size($icon), 'name' => str_replace(['seeder/ideas/','.png'], '', $icon), 'path' => 'hoi4/ideas'], false);
		}		
	}
}
