<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Http\Contracts\Media;
use Storage;

class decisionIconSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $media = new Media();
		$media->setUser(0);

		$decisionIcons = Storage::files('seeder/decision');
		foreach($decisionIcons as $icon){
			$media->uploadImage(Storage::get($icon), ['tool' => 'decision', 'extension' => 'png', 'size' => Storage::size($icon), 'name' => str_replace(['seeder', 'decision/', '/'], '', $icon), 'path' => 'hoi4/decision'], false);
		}
    }
}
