<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Http\Contracts\Media;
use Storage;

class NewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $placeholder = Storage::get('seeder/placeholder-image.png');

		$media = new Media();
		$media->setUser(0);

		$leaderIcon = Storage::files('seeder/country');
		dd($leaderIcon);
		foreach($leaderIcon as $icon){
			$i = explode('/', explode('.', $icon)[0]);
			$n = array_pop($i);
			$media->uploadImage(Storage::get($icon), ['tool' => 'country', 'extension' => 'png', 'size' => Storage::size($icon), 'name' => $n, 'path' => 'hoi4/leaders'], false);
		}

		$ideaIcon = Storage::files('seeder/ideas');
		foreach($ideaIcon as $icon){
			$i = explode('/', explode('.', $icon)[0]);
			$n = array_pop($i);
			$media->uploadImage(Storage::get($icon), ['tool' => 'ideas', 'extension' => 'png', 'size' => Storage::size($icon), 'name' => $n, 'path' => 'hoi4/ideas'], false);
		}

		$eventsIcon = Storage::files('seeder/events');
		foreach($eventsIcon as $icon){
			$i = explode('/', explode('.', $icon)[0]);
			$n = array_pop($i);
			$media->uploadImage(Storage::get($icon), ['tool' => 'events', 'extension' => 'png', 'size' => Storage::size($icon), 'name' => $n, 'path' => 'hoi4/events'], false);
		}

		$focusIcon = Storage::files('seeder/focus-tree');
		foreach($focusIcon as $icon){
			$i = explode('/', explode('.', $icon)[0]);
			$n = array_pop($i);
			$media->uploadImage(Storage::get($icon), ['tool' => 'focus', 'extension' => 'png', 'size' => Storage::size($icon), 'name' => $n, 'path' => 'hoi4/focus'], false);
		}

		$decisionIcon = Storage::files('seeder/decision');
		foreach($decisionIcon as $icon){
			$i = explode('/', explode('.', $icon)[0]);
			$n = array_pop($i);
			$media->uploadImage(Storage::get($icon), ['tool' => 'decision', 'extension' => 'png', 'size' => Storage::size($icon), 'name' => $n, 'path' => 'hoi4/decision'], false);
		}
    }
}
