const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.disableNotifications();
mix.js('resources/js/app.js', 'public/js');
mix.js('resources/js/wysiwyg.js', 'public/js');
mix.sass('resources/sass/app.scss', 'public/css').options({
    autoprefixer: {
    	enabled: true,
        options: {
            browsers: [
                "last 3 versions",
                "not IE 11",
                "not dead"
            ]
        }
    },
    processCssUrls: false
});
