<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use App\Models\UserMod;
use App\Models\Mod;
use App\Http\Contracts\Media;
use Storage;


class RemoveResizedMedia extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hoi4m:removeresizedmedia';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete media files which are resized versions of the originals; export will resize accordingly';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();


        
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
    	$media = new Media();
    	$sizes = array_keys($media->getSizeData());

    	$directories = Storage::directories('uploads');
    	$startPoint = 1;
    	$maxDirectoriesToClear = 500000;
    	$totalItemsDeleted = 0;
    	$totalDirectoriesCleared = 0;
    	foreach($directories as $dir){
    		$parts = explode('/', $dir); //So we can start at a later ID number
    		if($parts[1] < $startPoint){
    			continue;
    		}
    		if($totalDirectoriesCleared >= $maxDirectoriesToClear){
    			break;
    		}

    		echo "Inside directory: {$dir}\n";
    		$customGFX = Storage::files($dir.'/custom-gfx');
    		$custom_gfx = Storage::files($dir.'/custom_gfx');

    		$gfxArray = array_merge($custom_gfx, $customGFX);
    		
    		$filenames = [];
    		foreach($gfxArray as $path){
    			$parts = explode('/', $path);
    			$filenames[] = array_pop($parts);
    		}
    		$toDelete = [];
    		foreach($filenames as $filename){
    			$parts = explode('_', $filename, 2); //Only want two parts
    			if(in_array($parts[0], $sizes) && in_array($parts[1], $filenames)){
    				$toDelete[] = $filename; //Keep the key
    			}
    		}

    		foreach($gfxArray as $path){
    			$parts = explode('/', $path);
    			$filename = array_pop($parts);
    			if(in_array($filename, $toDelete)){
    				Storage::delete($path);
    				$totalItemsDeleted++;
    			}
    		}

    		$totalDirectoriesCleared++;
    		echo "Deleted ".count($toDelete)." files\n----\n";
    	}


        echo "\nTotal items deleted: ".$totalItemsDeleted."\n";
        echo "Across ".$totalDirectoriesCleared." directories \n";
        return 0;
    }
}
