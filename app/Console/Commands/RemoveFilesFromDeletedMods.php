<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\StartDate;
use App\Models\Country;
use App\Models\CountryDate;
use App\Models\Decision;
use App\Models\DecisionCategory;
use App\Models\Event;
use App\Models\Focus;
use App\Models\Idea;
use App\Models\Ideology;
use App\Models\Mod;
use App\Models\User;

use App\Models\Media;

use Storage;


class RemoveFilesFromDeletedMods extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hoi4m:deletefiles';
    //php -d memory_limit=4096M artisan hoi4m:deletefiles

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete unused files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
    	echo "Start deleting\n";

    	$startDate = StartDate::pluck('gfx');
        $country = Country::get();
        echo "Get countries\n";
        $generals = [];
        $admirals = [];
        $governments = [];
        $focus = Focus::pluck('gfx');
        echo "Focus pluck\n";
        $ideas = Idea::pluck('gfx');
        echo "Idea pluck\n";
        $decisions = Decision::pluck('gfx');
        $DecisionCategories = DecisionCategory::pluck('gfx');
        echo "Decision pluck\n";
        $events = Event::pluck('gfx');
        echo "Event pluck\n";
        $ideologies = Ideology::get();
        $ideologyGFX = [];
        echo "Get ideologies\n";

        echo "Start country check\n";
        foreach($country as $c){
        	if(isset($c->general_data)){
	        	foreach($c->general_data as $item){
	        		if(isset($item['gfx'])){
	        			$generals[] = $item['gfx'];
	        		}
	        	}
	        }
	        if(isset($c->admiral_data)){
	        	foreach($c->admiral_data as $item){
	        		if(isset($item['gfx'])){
	        			$admirals[] = $item['gfx'];
	        		}
	        	}
	        }
	        if(isset($c->government)){
	        	foreach($c->government as $item){
	        		if(isset($item['gfx'])){
	        			$governments[] = $item['gfx'];
	        		}
	        	}
	        }
        }

        echo "Start ideology check\n";
        foreach($ideologies as $i){
        	if(isset($i->data['gfx'])){
        		$ideologyGFX[] = $i->data['gfx'];
        	}
        }

        echo "Merge arrays \n";
        $merged = array_merge($startDate->toArray(), $generals, $admirals, $governments, $focus->toArray(), $ideas->toArray(), $decisions->toArray(), $DecisionCategories->toArray(), $events->toArray(), $ideologyGFX);
        
        $unique = array_unique($merged);
        
        echo "\n".count($unique)." unique media IDs\n";

        //$chunk = array_chunk($unique, 100);

        $media = Media::whereIntegerNotInRaw('id', $unique)->where('tool', '=', 'global')->where('user_id', '!=', '0')->orderBy('created_at', 'ASC')->limit(10000)->get();

        $totalItemsDeleted = 0;
        $totalDirsDeleted = 0;

        $sizes = ['focus', 'news-event', 'event', 'leader', 'idea', 'bug', 'national-spirit', 'flag-small', 'flag-medium', 'flag-standard', 'bookmark', 'decision-icon', 'decision-picture', 'ideology'];

        foreach($media as $item){
        	echo "Deleting file: ".$item->path.'/'.$item->filename.'.'.$item->extension."\n";

        	//Delete unused avatar
        	Storage::delete($item->path.'/'.$item->filename.'.'.$item->extension);

        	foreach($sizes as $size){
        		if(Storage::exists($item->path.'/'.$size.'_'.$item->filename.'.'.$item->extension)){
        			Storage::delete($item->path.'/'.$size.'_'.$item->filename.'.'.$item->extension);
        			echo "Deleting file: ".$item->path.'/'.$size.'_'.$item->filename.'.'.$item->extension."\n";
        			$totalItemsDeleted++;
        		}
        	}


        	//Check files in directory
        	$files = Storage::files($item->path);
        	//If directory has no files left in it
        	if(empty($files)){
        		//Delete directory
        		Storage::deleteDirectory($item->path);
        		echo "Deleting dir: ".$item->path."\n";
        		$totalDirsDeleted++;
        	}
        	//Delete entry in media database
        	$item->delete();

        	$totalItemsDeleted++;
        }

        echo "\nDeleted ".$totalDirsDeleted." directories\n";
        echo "Deleted ".$totalItemsDeleted." items\n";


        return 0;
    }
}
