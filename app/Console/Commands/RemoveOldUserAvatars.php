<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use App\Models\Media;
use Storage;


class RemoveOldUserAvatars extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hoi4m:deleteavatars';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete unused avatars';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();


        
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
    	//Get all users which don't use the placeholder media id
        $users = User::where('media_id', '!=', '1')->get();

        $mediaIDs = [];
        foreach($users as $user){
        	$mediaIDs[] = $user->media_id; //Add all USED media IDs to this array
        }

        $totalItemsDeleted = 0;

        $media = Media::where('path', 'LIKE', '%/avatars/%')->whereNotIn('id', $mediaIDs)->get();
        foreach($media as $item){
        	echo "Deleting file: ".$item->path.'/'.$item->filename.'.'.$item->extension."\n";

        	//Delete unused avatar
        	Storage::delete($item->path.'/'.$item->filename.'.'.$item->extension);
        	//Check files in directory
        	$files = Storage::files($item->path);
        	//If directory has no files left in it
        	if(empty($files)){
        		//Delete directory
        		Storage::deleteDirectory($item->path);
        		echo "Deleting dir: ".$item->path."\n";
        	}
        	//Delete entry in media database
        	$item->delete();

        	$totalItemsDeleted++;
        }

        echo "\nTotal items deleted: ".$totalItemsDeleted."\n";
        return 0;
    }
}
