<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Storage;
use Imagick;

class GenerateMapPixels extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'MapPixels:generate';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generate file for pixel locations';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return int
	 */
	public function handle()
	{
		echo "Generating map pixels\n";
		set_time_limit(7200);
		ini_set('memory_limit','16M');

		$i = Storage::get('map-svgs/provinces.bmp');

		$img = new Imagick();
		$img->readimageblob($i);

		$orig_width = $img->getImageWidth();
		$orig_height = $img->getImageHeight();

		if(Storage::has('map-svgs/map-province-colours.json')){
			$json = json_decode(Storage::get('map-svgs/map-province-colours.json'));
		}else{
			$json = false;
		}

		if(!$json){
			//Need to get pixels in the original image, but it's so large that doing anything more than getting the colours which exist crash.
			$allPixels = [];
			for($y = 0; $y < $orig_height; $y++){
			    echo "Pixels in row: ".$y."\n";
			    for($x = 0; $x < $orig_width; $x++){
			        $pixel = $img->getImagePixelColor($x,$y);

			        $colour = rtrim(ltrim($pixel->getColorAsString(), 'srgb('), ')');
			        if(!in_array($colour, $allPixels)){
			            $allPixels[] = $colour;
			        }
			    }
			}
			Storage::write('map-svgs/map-province-colours.json', json_encode($allPixels));
			echo "Checked initial file. Step 2 commence...\n";
		}else{
			$allPixels = $json;
		}
		$i = Storage::get('map-svgs/provinces.png'); //Half size w/h

		$im = new Imagick();
		$im->readimageblob($i);
		$pixels = [];

		$width = $im->getImageWidth();
		$height = $im->getImageHeight();

		$wDiff = $orig_width / $width;
		$hDiff = $orig_height / $height;

		if(Storage::has('map-svgs/small-map-province-colours.json')){
			$json = json_decode(Storage::get('map-svgs/small-map-province-colours.json'));
		}else{
			$json = false;
		}

		if(!$json){
			$colours = 0;
			for($y = 0; $y < $height; $y++){
				echo "Shrunk row: ".$y."\n";
				for($x = 0; $x < $width; $x++){
					$px = $im->getImagePixelColor($x,$y);
					$colour = rtrim(ltrim($px->getColorAsString(), 'srgb('), ')');

					if(in_array($colour, $allPixels) && !isset($pixels[$colour])){
						$pixels[$colour] = [[$x,$y]];
					}else{
						$pixels[$colour][] = [$x,$y];
						$colours++;
					}
				}
			}
			Storage::write('map-svgs/small-map-province-colours.json', json_encode($pixels));
		}else{
			$colours = 0;
			foreach($json as $k => $v){
				$colours++;
			}
			// Yeah, this doesn't work :( $colours = count($json);
			$pixels = $json;
		}

		$locations = [];
		$loc = 0;

		foreach($pixels as $colour => $ls){
			//Set min/max to 9999999 so min will always be smaller
			$xMin = 999999;
			$xMax = -1;
			$yMin = 999999;
			$yMax = -1;

			foreach($ls as $l){
				$x = $l[0];
				$y = $l[1];


				if($x > $xMax){
					$xMax = $x;
				}
				if($x < $xMin){
					$xMin = $x;
				}

				if($y > $yMax){
					$yMax = $y;
				}
				if($y < $yMin){
					$yMin = $y;
				}
			}

			$locations[$colour] = [(((int)(($xMin + $xMax) / 2)) * $wDiff), (((int)(($yMin + $yMax) / 2)) * $hDiff)];
			$loc++;
			echo "Calculated location: ".$loc.'/'.$colours."\n";
		}
		Storage::write('map-svgs/map-centre-positions.json', json_encode($locations));

		echo "Map bullshittery complete!\n";
		return true;
	}
}
