<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

use App\Mail\ForumPostUpdate;

class ForumPostUpdated extends Notification{
	use Queueable;

	public $user;
	public $comment;
	public $post;
	public $link;

	/**
	 * Create a new notification instance.
	 *
	 * @return void
	 */
	public function __construct($user, $comment, $post, $link){
		$this->user = $user;
		$this->comment = $comment;
		$this->post = $post;
		$this->link = $link;
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function via($notifiable){
		return ['database', 'mail'];
	}

	/**
	 * Get the mail representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return \Illuminate\Notifications\Messages\MailMessage
	 */
	public function toMail($notifiable){
		return (new MailMessage)->view('emails/forum.response', ['title' => $this->post->title, 'emailContent' => $this->comment->content, 'link' => env('APP_URL').$this->link, 'user' => $this->user])->subject(translate('tools/forum/emails.newReply.subject', ['title' => $this->post->title]));
	}

	/**
	 * Get the array representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function toArray($notifiable){
		return [
			'link' => $this->link,
			'title' => $this->post->title,
			'username' => $this->user->username
		];
	}
}
