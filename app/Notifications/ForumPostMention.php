<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ForumPostMention extends Notification{
	use Queueable;

	public $user;
	public $post;
	public $link;

	/**
	 * Create a new notification instance.
	 *
	 * @return void
	 */
	public function __construct($user, $post){
		$this->user = $user;
		$this->post = $post;
		$this->link = '/forum/'.$post->forum->slug.'/'.$post->id.'-'.$post->slug;
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function via($notifiable){
		return ['database'];
	}

	/**
	 * Get the array representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function toArray($notifiable){
		return [
			'link' => $this->link,
			'title' => $this->post->title,
			'username' => $this->user->username
		];
	}
}
