<?php 

use App\Models\Media;
use App\Models\Forum;
use App\Models\ForumPost;
use App\Http\Controllers\PatreonController;
use App\Http\Controllers\ModController;

function storagePath($path = null){
	return _ROOT_.'/storage/app'.(!empty($path) ? '/' : '').ltrim($path, '/');
}

/**
* Display Feather Icon SVG
* ===
* icon - name of icon as appears at: https://feathericons.com/
*
*/
function featherIcon($icon){
	echo '<svg class="feather-icon icon-'.$icon.'">';
 	echo '<use xlink:href="#'.$icon.'"></use>';
	echo '</svg>';
}

/**
* Deals with translations for both logged in/logged out users
* ===
* path - array path to translation file, inside of the country code folder e.g. resources/lang/en/alerts/account/warning.php/banned => alerts/account/warning.banned
* replace - key => value array of variables you wish to replace :[variable] to replace
*
*/
function translate($path = null, $replace = []){
	return Lang::get($path, $replace);
}


/**
* Return a list of languages and their lang file codes
*
*/
function getLanguageList(){
	$languages = [
		'en' => 'English',
		'fr' => 'French',
		'de' => 'German',
		'es' => 'Spanish',
		'jp' => 'Japanese',
		'kr' => 'Korean',
		'ru' => 'Russian'
	];

	return $languages;
}

/**
* Return translation file array as a JSON string
* ===
* location - Pass the location of the file to pull in
*
*/

function getLangArray($location){
	return json_encode(Lang::get($location));
}


/**
* Return JSON for media files for use in GFX selector in tools
* ===
* tool - the tool, as set in Media table's "tool" field
*
*/

function getMedia($tool){
	$return = [];
	$return['default'] = Media::where('tool', '=', $tool)->where('user_id', '=', 0)->get();
	if(Auth::check()){
		$return['custom'] = Media::where('image_for_tools', '=', 1)->where('user_id', '=', Auth::user()->id)->get();
	}
	$return['spritemap'] = '/media/spritemap/'.$tool;//generateSpriteMap($tool);

	return json_encode($return);
}

/**
* Convert an array of images into a spritemap
* ===
* tool - tool's images which should be spritemapped
* perRow - how many images should be on a single row
* imageSize - size of each sprite
*
*/
function generateSpriteMap($tool, $perRow = 10, $imageSize = 95){
	if($tool == 'debug'){
		$images = Media::where('user_id', '=', 0)->get();
		$missing = [];
		echo '<textarea>';
		echo "SELECT * FROM media WHERE id IN (";
		foreach($images as $image){
			$filename = $image->path.'/'.$image->filename.'.'.$image->extension;
			if(!Storage::has($filename)){
				$missing[$image->id] = $image->filename;
				echo $image->id.',';
			}
			
		}
		echo '0)';

		echo '</textarea>';
		dd($missing);
	}
	$images = Media::where('tool', '=', $tool)->where('user_id', '=', 0)->get();

	$im = new Imagick();
	$width = $imageSize * $perRow;
	$height = ceil(count($images) / $perRow) * $imageSize;
	$backgroundColour = 'transparent';

	$im->newImage( $width, $height, new ImagickPixel( $backgroundColour ) );

	$column = 0;
	$row = 0;
	$colLimit = $perRow - 1;

	foreach($images as $image){
		$filename = $image->path.'/'.$image->filename.'.'.$image->extension;
		if(Storage::has($filename)){
			$x = $column * $imageSize;
			$y = $row * $imageSize;
			$imageToAdd = Storage::get($filename);

			$img = new Imagick();
			$img->newImage( $imageSize, $imageSize, new ImagickPixel( 'transparent' ) );
			$img->setBackgroundColor(new ImagickPixel('transparent'));
			$img->readimageblob($imageToAdd);
			$img->resizeImage($imageSize,$imageSize, 1, 0, true);

			$im->compositeImage($img, Imagick::COMPOSITE_DEFAULT, $x, $y);
		}
		if($column == $colLimit){
			$column = 0;
			$row++;
		}else{
			$column++;
		}
	}    
	
	$im->setImageFormat( "png" );

	header("Content-Type: image/png");
	echo $im->getImageBlob();
	die();
}


/**
* Return a list of mods, for select values
*
*/
function getModList(){
	$list = [];
	if(Auth::check()){
		if(Auth::user()->mods){
			foreach(Auth::user()->mods as $mod){
				$list[] = [
					'value' => $mod->id,
					'label' => $mod->name
				];
			}
		}
	}
	return $list;
}

function getAdminModList(){
	$list = [];
	if(Auth::check()){
		if(Auth::user()->mods){
			foreach(Auth::user()->mods as $mod){
				if($mod->pivot->administrator){
					$list[] = [
						'value' => $mod->id,
						'label' => $mod->name
					];
				}
			}
		}
	}
	return $list;
}

//Incase they are somehow a creator/designer/developer of a mod
function getUniqueModList(){
	$list = [];
	if(Auth::check()){
		if(Auth::user()->mods){
			foreach(Auth::user()->mods as $mod){
				if(!isset($list[$mod->id])){
					$list[$mod->id] = [
						'value' => $mod->id,
						'label' => $mod->name
					];
				}
			}
		}
	}
	return $list;
}

/**
* Return a list of countries
*
*/
function getCountryArray(){
	$tags = [];

	$data = Storage::get('hoi4-files/localisation/countries_l_english.yml');
	$lines = explode("\n", $data);

	foreach($lines as $line){
		$parts = explode(':', trim($line));
		if(strlen($parts[0]) == 3){
			$name = rtrim(explode('"', $parts[1], 2)[1], '"');
			$tags[$parts[0]] = $name;
		}
	}
	asort($tags);

	return $tags;
}
function getCountryArrayForVue(){
	$list = [];
	foreach(getCountryArray() as $tag => $name){
		$list[] = ['value' => $tag, 'label' => $name];
	}
	return json_encode($list);
}

/**
* Return list of forums with IDs for select-help
*
*/
function getForumList(){
	if(Auth::check() && Auth::user()->role_id == '2'){
		$forums = Forum::get();
	}else{
		$forums = Forum::where('public', '=', '1')->get();
	}
	$arr = [];
	foreach($forums as $f){
		$arr[] = [
			'label' => translate('tools/forum/forums.'.$f->localisation_key.'.title'),
			'value' => $f->id
		];
	}
	return json_encode($arr);
}

/**
* Return last 3 site updates for homepage
*
*/
function getSiteUpdates(){
	return ForumPost::where('forum_id', '=', '3')->orderBy('id', 'DESC')->limit(3)->get();
}

/**
* Simple spacer function for emails
*
*/
function space($multiply = 1){
	return (20 * $multiply).'px; '; 
}

/**
* Get Patreon patrons
*
*/
function getPatrons(){
	$c = new PatreonController();
	return $c->getPatrons();
}

/** 
* Return list of languages the site is available in
*
*/
function getSiteLanguageOptions(){
	$flags = [
		[
			'value' => 'en',
			'label' => '🇬🇧 English'
		],
		[
			'value' => 'de',
			'label' => '🇩🇪 Deutsch'
		]
	];

	return $flags;
}

/** 
* Return list of acceptable languages for the game; select for creating focus trees, countries, etc.
*
*/
function getGameLanguageOptions(){
	return getGameLanuageOptions();
}
function getGameLanuageOptions(){
	$opts = [
		[
			'value' => 'english',
			'label'=> '🇬🇧 English'
		],
		[
			'value'=> 'french',
			'label'=> '🇫🇷 Français'
		],
		[
			'value'=> 'german',
			'label'=> '🇩🇪 Deutsch'
		],
		[
			'value'=> 'polish',
			'label'=> '🇵🇱 Polskie'
		],
		[
			'value'=>'russian',
			'label'=>'🇷🇺 русский'
		],
		[
			'value'=>'spanish',
			'label'=>'🇪🇸 Español'
		],
		[
			'value'=>'braz_por',
			'label'=>'🇧🇷 Português'
		],
		[
			'value' => 'simp_chinese',
			'label' => '🇨🇳 中文'
		]
	];
	return $opts;
}

/* 
* Get the correct map SVG for the parent mod selected
* ===
* mod - string for the available key for the mod
*
*/
function getMapSVG($mod){
	// mod => svg filepath
	$mods = [
		'DEV_TEST' => 'map-svgs/example.svg',
		'vanilla' => 'map-svgs/vanilla.svg',
	];
	if(!isset($mods[$mod])){
		return $mods['vanilla'];
	}
	return Storage::get($mods[$mod]);
}
function getMapSVGURL($mod){
	$mods = [
		'DEV_TEST' => 'example.svg',
		'vanilla' => 'vanilla.svg?v=20241130',
	];
	if(!isset($mods[$mod])){
		return $mods['vanilla'];
	}
	return $mods[$mod];
}

/* 
* Get the correct states for map to use for the parent mod selected
* ===
* mod - string for the available key for the mod
*
*/
function getStateList($mod){
	// mod => svg filepath
	$mods = [
		'DEV_TEST' => 'hoi4-files/history/states',
		'vanilla' => 'hoi4-files/history/states',
	];
	if(!isset($mods[$mod])){
		$location = $mods['vanilla'];
	}else{
		$location = $mods[$mod];
	}
	$noData = true;
	//Check if file exists
	if(Storage::has($location.'/state-data.json')){
		$data = Storage::get($location.'/state-data.json');
		$dataObj = json_decode($data);
		$date = \DateTime::createFromFormat('Y-m-d', $dataObj->date);
		$now = \DateTime::createFromFormat('Y-m-d', date('Y-m-d'));
		//If the file wasn't created today, trash it - states may have updated
		if($date < $now){
			Storage::delete($location.'/state-data.json');
		}else{
			$noData = false;
		}
	}
	if($noData){
		$states = Storage::files($location);
		$stateData = ['date' => date('Y-m-d'), 'states' => []];
		foreach($states as $file){
			$content = json_decode(Storage::get($file));
			if(isset($content->state)){
				$prov = trim(ltrim(rtrim($content->state->provinces, '}'),'{')); //Remove { } from start/end, and remove line breaks/spaces before/after province list;
				$provinces = explode(' ', $prov);
				$stateData['states'][$content->state->id] = $provinces;
			}
		}
		$stateData = json_encode($stateData);
		Storage::write($location.'/state-data.json', $stateData);
	}else{
		$stateData = $data;
	}
	return $stateData;
}

/* 
* Get specific localisation terms from language file that has been JSONd
* ===
* file - name of file before _l_
* language - lanague after _l_
* list - list of terms to return
*
*/
function getHOILocalisationData($file, $language = null, $list = [], $location = 'hoi4-files/localisation'){
	if(Storage::has($location.'/'.$file.'_l_'.$language.'.yml')){
		$file = Storage::get($location.'/'.$file.'_l_'.$language.'.yml');
	}elseif(Storage::has($location.'/'.$file.'_l_english.yml')){
		$file = Storage::get($location.'/'.$file.'_l_english.yml');
	}else{
		return;
	}
	$return = [];
	$json = json_decode($file);
	foreach($json as $key => $item){
		if(empty($list)){
			$return[$key] = $item;
		}else{
			if(in_array($key, $list) || (isset($item->name) && in_array($item->name, $list))){
				$return[$key] = $item;
			}
		}
	}

	return json_encode($return);
}

function getLandUnitDivisionBreakdown($file, $location = 'unit-delegations/default'){
	if(Storage::has($location.'/'.$file)){
		$data = json_decode(Storage::get($location.'/'.$file));
		$data->all_units = array_merge($data->support, $data->regiment);
		return $data;
	}
	return;
}

function getResearch($mod){
	if(Storage::has('research/'.$mod.'.json')){
		return json_decode(Storage::get('research/'.$mod.'.json'));
	}
	return json_decode(Storage::get('research/vanilla.json'));
}

function getTraits($lang = 'english', $modID = null){
	$unitLeader = json_decode(Storage::get('hoi4-files/common/unit_leader/00_traits.txt'));
	$countryLeader = json_decode(Storage::get('hoi4-files/common/country_leader/00_traits.txt'));
	$localisationFile = localisationKeyValue(Storage::get('hoi4-files/localisation/traits_l_'.$lang.'.yml'));
	//When trait mod support added; append the mod's custom traits to these lists
	if(isset($_GET['combined'])){
		$allTraits = (object) array_merge((array) $unitLeader->leader_traits, (array) $countryLeader->leader_traits);
		return ['traits' => $allTraits, 'localisation' => $localisationFile, 'language' => $lang];
	}else{
		return ['unit' => $unitLeader, 'country' => $countryLeader, 'localisation' => $localisationFile, 'language' => $lang];
	}
}

function getSubIdeologies($mod, $t){
	$ideologies = getLangArray('tools/country/ideologies');
	$json = json_decode($ideologies);
	$return = $json->{$t};
	$modController = new ModController();
	if($mod->ideologies->count() > 0){
		foreach($mod->ideologies as $index => $ideology){
			$name = $modController->nameToId($ideology->name, $index.'_n');
			$return->{$name} = $ideology->name;
			$subs = [];
			if(!empty($ideology->types)){
				foreach($ideology->types as $type){
					$subs[$modController->nameToId($type['name'], $index.'_n')] = $type['name'];
				}
			}
			$return->{$name.'_sub'} = $subs;
		}
	}
	$json->{$t} = $return;
	return json_encode($json);
}

function tabbedLine($content, $tabs = 0){
	return str_repeat("\t", $tabs).$content."\r\n";
}

function idify($string){
	$tl = \Transliteration::clean_filename($string);
	if(str_replace(' ', '_', $string) != $tl){
		$tl = str_replace('_', '', $tl);
	}
	$string = $tl;
	$return = strtolower(preg_replace("/[^A-Za-z0-9_]/", '', str_replace(' ', '_', $string)));
	return $return; 
}

function tabbedBlock($content, $tabs = 0){
	$tabsPerLine = str_repeat("\t", $tabs);
	$lines = explode("\n", $content);
	$block = implode($tabsPerLine."\n", $lines);
	return $tabsPerLine.$block."\r\n";
}

function getIdeologyList(){
	$vanillaIdeologies = json_decode('[{"value":"conservatism","label":"Conservatism"},{"value":"liberalism","label":"Liberalism"},{"value":"socialism","label":"Socialism"},{"value":"marxism","label":"Marxism"},{"value":"leninism","label":"Leninism"},{"value":"stalinism","label":"Stalinism"},{"value":"anti_revisionism","label":"Anti-Revisionism"},{"value":"anarchist_communism","label":"Anarchist Communism"},{"value":"nazism","label":"Nazism"},{"value":"gen_nazism","label":"Nazism"},{"value":"fascism_ideology","label":"Fascism"},{"value":"falangism","label":"Falangism"},{"value":"rexism","label":"Rexism"},{"value":"neutrality","label":"Non-Aligned"},{"value":"neutrality_noun","label":"Non-Aligned"},{"value":"neutrality_desc","label":"Authoritarian Regime"},{"value":"despotism","label":"Despotic"},{"value":"oligarchism","label":"Oligarchic"},{"value":"moderatism","label":"Moderatism"},{"value":"centrism","label":"Centrism"},{"value":"anarchism","label":"Anarchism"}]');

	return $vanillaIdeologies;
}

function getAutonomyLevels($lang){
	$content = localisationKeyValue(Storage::get('hoi4-files/localisation/autonomy_l_'.$lang.'.yml'));
	$returnContent = [];
	foreach($content as $key => $value){
		if(strtolower($key) == $key){
			//Ignore the non-autonomy-level things
			$returnContent[$key] = $value;
		}
	}
	return json_decode(json_encode($returnContent));
}

function localisationKeyValue($localisation){
	$lines = explode("\n", $localisation);
	$return = [];

	foreach($lines as $line){
		$parts = explode(":", trim($line), 2);
		if(!isset($parts[1])){
			continue;
		}

		$split = explode('"', trim($parts[1]), 2);
		if(!isset($split[1])){
			continue;
		}
		$value = rtrim($split[1], '"');

		$return[$parts[0]] = $value; 
	}

	return json_decode(json_encode($return));
}

function preprocess($s) {
    $lines = preg_split("/\r?\n/", $s);
    $result = "";
    foreach ($lines as $line) {
        $index = strpos($line, "#");
        if ($index !== false) {
            $line = substr($line, 0, $index);
        }
        $result .= $line . "\n";
    }
    return $result;
}

function hoiFileToJSON($s, $level = 0, $maxLevel = 2){
	return json_decode(json_encode(hoiFileToArray($s, $level, $maxLevel)));
}

function hoiFileToArray($s, $level = 0, $maxLevel = 2, $hoiJsonImportGlobalKey = '') {
    $s = preprocess($s);
    $key = "";
    $value = "";
    $buildingKey = true;
    $braceCount = 0;
    $hadBraces = false;
    $json = [];
    $fuckyKeys = ['provinces', 'color', 'traits', 'dynamic_faction_names', 'has_dlc', 'victory_points'];
    $forceComplete = false;

    if (in_array($hoiJsonImportGlobalKey, $fuckyKeys)) {
        $buildingKey = false;
        $key = $hoiJsonImportGlobalKey;
    }

    for ($i = 0; $i < strlen($s); $i++) {
        $c = $s[$i];

        if ($buildingKey) {
            if ($c === "{" || $c === "}") {
                continue;
            }

            if ($c !== '=') {
                $key .= $c;
            } else {
                $buildingKey = false;
                $key = trim($key);
                $hoiJsonImportGlobalKey = $key;
            }
        } else {
        	if(in_array($key, $fuckyKeys)){
        		if($c === '{'){
        			continue;
        		}
        		if($c === '}'){
        			continue;
        		}
        		if($s[($i-1)] === '}'){
        			$forceComplete = true;
        		}
        	}
            $value .= $c;

            if ($c === "{") {
                $braceCount++;
                $hadBraces = true;
            } elseif ($c === "}") {
                $braceCount--;
            }

            $isNoSpaceCheck = in_array($key, ['name', 'surnames']);
            if(!$isNoSpaceCheck){
            	$isNoSpaceCheck = in_array($key, $fuckyKeys);
            }
            if ($braceCount === 0 && preg_match("/\S/", $value) && ((preg_match('/"/', $c) || (preg_match('/\t/', $c) && !in_array($key, $fuckyKeys))) && strlen($value) > 2 && $isNoSpaceCheck || preg_match('/\s/', $c) && !$isNoSpaceCheck) || $forceComplete) {
                $value = trim($value);

                if (array_key_exists($key, $json)) {
                    if (!is_array($json[$key])) {
                        $json[$key] = [$json[$key]];
                    }

                    if($key == 'victory_points'){
                		$value = "{ \n".tabbedLine($value, 3).tabbedLine('}', 2); //State's victory points don't get their braces back for some reason
                	}

                    if ($hadBraces && $level < $maxLevel) {
                        $json[$key] = array_merge($json[$key], hoiFileToArray($value, $level + 1, $maxLevel, $hoiJsonImportGlobalKey));
                        $hadBraces = false;
                    } else {
                        $json[$key][] = $value;
                    }
                } else {
                    if ($hadBraces && $level < $maxLevel) {
                        $json[$key] = hoiFileToArray($value, $level + 1, $maxLevel, $hoiJsonImportGlobalKey);
                        $hadBraces = false;
                    } else {
                    	if($key == 'victory_points'){
                    		$value = "{ \n".tabbedLine($value, 3).tabbedLine('}', 2); //State's victory points don't get their braces back for some reason
                    	}
                        $json[$key] = $value;
                    }
                }

                $buildingKey = true;
                $key = "";
                $value = "";
                $forceComplete = false;
            }
        }
    }

    return $json;
}