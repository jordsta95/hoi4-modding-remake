<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;

class checkUserIsAdmin{

	public function handle(Request $request, Closure $next){
		if(!Auth::check()){
			return redirect('/')->with('error', translate('auth.unauthorised'));
		}
		if(Auth::user()->role_id != 2){
			return redirect('/')->with('error', translate('auth.unauthorised'));
		}
		return $next($request);
	}
}
