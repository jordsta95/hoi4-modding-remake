<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;
use App;

class CheckAndSetLanguage{

	public function handle(Request $request, Closure $next){
		if(Auth::check()){
			App::setLocale(Auth::user()->language);
		}
		return $next($request);
	}
}
