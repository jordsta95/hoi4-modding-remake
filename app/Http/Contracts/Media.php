<?php

namespace App\Http\Contracts;

use Imagick;
use Storage;
use App\Models\Media as Model;
use stdClass;
use Auth;
use ImagickDraw;
use ImagickPixel;


class Media{
	protected $user;
	protected $routesFile;

	/**
	* Add/edit width/height for different sizes, and their names, for all image uploads to confine to (none square images fit to width)
	*
	*/
	public $sizes;

	private $images = ['gif', 'png', 'jpg', 'jpeg', 'tga', 'dds'];

	public function __construct($id = 0){
		$this->setSizesData();
        $this->routesFile = 'media-routes/media-json-'.$id.'.json';
	}

	public function setSizesData(){
		$this->sizes = Media::getSizeData();
	}

	public static function getSizeData(){
		return [
			'focus' => ['width' => 95, 'height' => 85],
			'news-event' => ['width' => 397, 'height' => 153],
			'event' => ['width' => 210, 'height' => 176],
			'leader' => ['width' => 156, 'height' => 210],
			'idea' => ['width' => 65, 'height' => 67],
			'bug' => ['width' => 400, 'height' => 400],
			'national-spirit' => ['width' => 60, 'height' => 68],
			'flag-small' => ['width' => 10, 'height' => 7],
			'flag-medium' => ['width' => 41, 'height' => 26],
			'flag-standard' => ['width' => 82, 'height' => 52],
			'bookmark' => ['width' => 180, 'height' => 104],
			'decision-icon' => ['width' => 33, 'height' => 33],
			'decision-picture' => ['width' => 114, 'height' => 101],
			'ideology' => ['width' => 66, 'height' => 66]
		];
	}

	/**
	* Generic upload function, nothing fancy here
	* ===
	* request - Post request 
	*
	*/
	public function upload($request){
		$this->setUser();

		if(empty($request->image)){
			return null;
		}

		$options = [];

		if(!empty($request->path)){
			$options['path'] = $request->path;
		}

		if(!empty($request->filename)){
			$options['name'] = $request->filename;
		}

		$return = $this->handleUpload($request->file('image'), $options);

		if(isset($_GET['url'])){
			if(isset($_GET['asJson'])){
				return json_encode(['url'=> '/media/find/'.$return->id.'/preview', 'success' => true]);
			}else{
				return '/media/find/'.$return->id;
			}
		}else{
			return $return;
		}
	}

	public function uploadCustomGFX($request){
		$this->setUser();

		if(empty($request->custom_gfx)){
			return null;
		}

		$options = [];

		if(!isset($request->path)){
			$options['path'] = 'uploads/'.$this->user->id.'/custom-gfx';
		}

		if(isset($request->filename)){
			$options['name'] = $request->filename;
		}

		$options['tool'] = 'global';

		echo $this->handleUpload($request->file('custom_gfx'), $options);
	}

	public function mediaManagerUpload($request){
		$this->setUser();
		
		if(empty($request->image)){
			return null;
		}

		$options = [];

		if(!empty($request->path)){
			$options['path'] = $request->path;
		}

		if(!empty($request->filename)){
			$options['name'] = $request->filename;
		}

		if(!is_array($request->image)){
			return '<script> var json = '.$this->handleUpload($request->file('image'), $options).'; parent.mediaJson(json);</script>';
		}else{
			foreach($request->file('image') as $img){
				$json[] = $this->handleUpload($img, $options);
			}
			return '<script> var json = '.json_encode($json).'; parent.mediaJsonMultiple(json);</script>';
		}
	}

	public function setUser($id = null){
		if(empty($id) && !isset($id)){
			$this->user = Auth::user();
		}else{
			$thing = new stdClass();
			$thing->id = $id;
			$this->user = $thing;
		}
	}

	/**
	* Function to handle whether upload is an image, and should be treated as such, or whether it's another file type
	* ===
	* file - $request which is ->file('name');
	* options - array to override default data passed ['name' => 'name wanted', 'path' => 'file path']
	* resize - Whether the image should be resized or not
	*
	*/
	public function handleUpload($file, $options = [], $resize = false){
		if(empty($this->user) && !isset($this->user)){
			return redirect()->back()->withErrors(['notLoggedIn', 'Must be logged in to upload files']);
		}

		

		$extension = $file->extension();
		if($extension == 'bin'){
			$name = $file->getClientOriginalName();
			$e = explode('.', $name);
			$extension = array_pop($e);
		}

		if(in_array(strtolower($extension), $this->images)){
			return $this->uploadImage($file, $options, $resize);
		}else{
			//Not an image. Should we be doing anything with this?
			return $this->uploadFile($file, $options);
		}
	}

	/**
	* Function to handle uploading non-image files
	* ===
	* file - $request which is ->file('name');
	* options - array to override default data passed ['name' => 'name wanted', 'path' => 'file path']
	*
	*/
	public function uploadFile($file, $options = []){
		if(empty($this->user) && !isset($this->user)){
			return redirect()->back()->withErrors(['notLoggedIn', 'Must be logged in to upload files']);
		}

		$path = '/uploads/default';
		$forTool = 1;
		$tool = '';

		if(!empty($options['extension'])){
			$extension = $options['extension'];
		}else{
			$extension = $file->extension();
		}

		if(!empty($options['size'])){
			$filesize = $options['size'];
		}else{
			$filesize = $file->getSize();
		}

		if(!empty($options['name'])){
			$name = $options['name'];
		}else{
			$name = $file->getClientOriginalName();
		}

		if(!empty($options['path'])){
			//Trim / off the start, if passed accross, and add if not
			$path = '/'.ltrim(strtolower($options['path']), '/');
		}

		if(!empty($options['tool'])){
			$tool = $options['tool'];
		}else{
			$forTool = 0;
		}

		$path = rtrim($path, '/');
		if(strpos($name, '.') !== false){
			$name = preg_replace('/\.'.$extension.'$/', '', $name);
			//$name = rtrim($name, '.'.$extension);
		}

		$filename = $this->getValidFileName($path.'/'.$name, $extension);


		if(!empty($options['size']) && !empty($options['extension'])){
			$toUpload = $file;
		}else{
			$toUpload = file_get_contents($file);
		}

		Storage::write($filename, $toUpload);

		$betterFilename = explode('.'.$extension, $filename)[0];

		$media = Model::create([
			'filename' => str_replace($path.'/', '', $betterFilename),//rtrim(str_replace($path.'/', '', $filename), '.'.$extension),
			'path' => $path,
			'extension' => $extension,
			'size' => $filesize,
			'user_id' => $this->user->id,
			'image_for_tools' => $forTool,
			'tool' => $tool,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s'),
		]);

		return $media;
	}

	/**
	* Handle uploading and resizing of images
	* ===
	* file - $request which is ->file('name');
	* options - array to override default data passed ['name' => 'name wanted', 'path' => 'file path']
	* resize - Whether the image should be resized or not
	*
	*/
	public function uploadImage($image, $options = [], $resize = false){
		if(empty($this->user) && !isset($this->user)){
			return redirect()->back()->withErrors(['notLoggedIn', 'Must be logged in to upload files']);
		}

			
		$path = '/uploads/default';
		$forTool = 1;
		$tool = '';

		if(!empty($options['extension'])){
			$extension = $options['extension'];
		}else{
			$extension = $image->extension();
		}

		if($extension == 'bin'){
			$name = $image->getClientOriginalName();
			$e = explode('.', $name);
			$extension = array_pop($e);
		}


		if(!empty($options['size'])){
			$filesize = $options['size'];
		}else{
			$filesize = $image->getSize();
		}

		if(!empty($options['name'])){
			$name = $options['name'];
		}else{
			$name = $image->getClientOriginalName();
		}

		if(!empty($options['path'])){
			//Trim / off the start, if passed accross, and add if not
			$path = '/'.ltrim(str_replace('-','_',strtolower($options['path'])), '/');
		}

		if(!empty($options['tool'])){
			$tool = $options['tool'];
		}else{
			$forTool = 0;
		}

		if($extension == 'tga' || $extension == 'dds'){
			$imagick = new Imagick();
			$setExtension = $imagick->setformat($extension);
			$originalImage = $imagick->readimageblob(file_get_contents($image));
			if($extension == 'tga'){
				$i = $imagick->setImageType(Imagick::IMGTYPE_TRUECOLORMATTE);
				$i = $imagick->setImageOrientation(Imagick::ORIENTATION_LEFTTOP );
			}
			$reformattedImage = $imagick->setformat('png');
			if($extension == 'tga'){
				$i = $imagick->flipImage();
			}
			$image = $imagick->getImageBlob();

			$extension = 'png';
			$options['extension'] = 'png';
			$options['size'] = $filesize;
		}

		$path = rtrim($path, '/');
		//$path = str_replace(['-', ' '], '_', $path); //So - can be used for folder separation
		if(strpos($name, '.') !== false){
			//$name = rtrim($name, '.'.$extension);
			$name = preg_replace('/\.'.$extension.'$/', '', $name);
		}

		$filename = $this->getValidFileName($path.'/'.$name, $extension);


		if(!empty($options['size']) && !empty($options['extension'])){
			$toUpload = $image;
		}else{
			$toUpload = file_get_contents($image);
		}

		Storage::write($filename, $toUpload);

		if(!in_array($extension, $this->images)){
			$resize = false;
		}

		$media = Model::create([
			'filename' => preg_replace('/\.'.$extension.'$/', '', str_replace($path.'/', '', $filename)),
			'path' => $path,
			'extension' => $extension,
			'size' => $filesize,
			'user_id' => $this->user->id,
			'image_for_tools' => $forTool,
			'tool' => $tool,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s'),
		]);

		if($resize){
			foreach($this->sizes as $sizeName => $size){
				$imagick = new Imagick();
				$originalImage = $imagick->setSize($size['width'],$size['height']);
				$originalImage = $imagick->readimageblob($toUpload);
				$resizeImage = $imagick->resizeImage($size['width'], $size['height'], 1, 0, false);
				$resizedImage = $imagick->getImageBlob();
				$filename = $this->getValidFileName($path.'/'.$sizeName.'_'.$name, $extension);
				Storage::write($filename, $resizedImage);
			}
		}

		return $media;
	}

	public function getImageSizes(){
		return $this->sizes;
	}

	public function getUserMedia($id){
		return Model::where('user_id', '=', $id)->get();
	}

	public function generateResizedImage($media, $sizeName){
		if(Storage::has($media->path.'/'.$sizeName.'_'.$media->filename.'.'. $media->extension)){
			return; //It's already there, no need to regenerate
		}

		$size = $this->sizes[$sizeName];

		$imagick = new Imagick();
		$originalImage = $imagick->readimageblob(Storage::get($media->path.'/'.$media->filename.'.'.$media->extension));
		$resizeImage = $imagick->resizeImage($size['width'], $size['height'], 1, (strpos($sizeName, 'flag') !== false ? 1 : 0), true);
		$resizedImage = $imagick->getImageBlob();
		$filename = $media->path.'/'.$sizeName.'_'.$media->filename.'.'. $media->extension;
		Storage::write($filename, $resizedImage);
	}

	/**
	* Make sure the file being uploaded doesn't already exist
	* ===
	* filename - The desired filename
	* extension - extension of the file (because uploading untitled.png is fine if untitled.jpg exists)
	* num - Number of files already uploaded with that name
	*
	*/
	public function getValidFileName($filename, $extension, $num = 0){
		if($num > 0){
			$name = $filename.'_'.$num.'.'.$extension;
		}else{
			$name = $filename.'.'.$extension;
		}

		if(Storage::exists($name)){
			$name = $this->getValidFileName($filename, $extension, ($num + 1));
		}

		return $name;
	}

	/**
	* Avoid making media database calls unless absolutely necessary (media never been accessed before)
	* ===
	* id - id of the file in the database
	*
	*/
	public function mediaFileCheck($id, $json = false){
		if(!$json){
			if(!Storage::has($this->routesFile)){
				$media = Model::find(1);
				$js = ['extension' => $media->extension, 'filename' => $media->filename, 'path' => $media->path];
				Storage::write($this->routesFile, '{"media":{"1": '.json_encode($js).'}}');
			}
			$json = Storage::get($this->routesFile);
			$json = json_decode($json);
		}

		//Add the info, if it isn't already in cache file
		if(!isset($json->media->{$id})){
			$media = Model::find($id);
			if(!empty($media)){
				if(!is_object($json->media)){
					$json->media = json_decode(json_encode($json->media));
				}
				if(!is_object($json->media)){ //It empty
					$json->media = new \stdClass();
				}

				$json->media->{$id} = ['extension' => $media->extension, 'filename' => $media->filename, 'path' => $media->path];
				$json = json_encode($json);
				Storage::delete($this->routesFile);
				Storage::write($this->routesFile, $json);

				//Decode again, just for consistency between array/object
				$json = json_decode($json);
			}else{
				return null;
			}
			
		}
		return $json->media->{$id};
	}

	public static function getMediaJSON($id){
		if(Storage::has('media-routes/media-json-'.$id.'.json')){
			return json_decode(Storage::get('media-routes/media-json-'.$id.'.json'));
		}else{
			return json_decode('[]');
		}
	}

	/**
	* Get the file, and resized versions, based on the id
	* ===
	* id - id of the file in the database
	* size - desired size of the file
	*
	*/
	public function find($id, $size = ''){
		$media = $this->mediaFileCheck($id);

		if(!empty($media)){
			if(strtolower($media->extension) == 'gif'){
				//Resized gifs cannot animate - So we always want to pull out full size gif images
				$size = '';
			}
			if(!in_array($media->extension, $this->images)){

				if(!empty($size)){
					return $this->outputFilePreview($media);
				}else{
					return $this->outputFile($media);
				}
			}
		}

		if($size == 'preview_'){
			$size = '';
		}

		$this->outputImage($media, $size);
	}

	public function outputFilePreview($media){
		$draw = new ImagickDraw();

		$placeholder = Storage::get('file-placeholder.png');
		$im = new Imagick();
		$im->readimageblob($placeholder);
		$draw = new ImagickDraw();
		$draw->setFontSize( 160 );
		$im->annotateImage($draw, 90, 450, 0, $media->extension);
		$im->setImageFormat('png');
		header('Content-type: image/png');
		echo $im;
	}
	public function outputFile($media){
		$file = Storage::get($media->path.'/'.$media->filename.'.'.$media->extension);
		echo $file;
	}

	/**
	* Get file from readable url; /media/path-to/media.png
	* ===
	* path - path to where it is stored on the server
	* filename - actual name of the file
	* extension - extension of the file
	*
	*/
	public function findByName($path, $filename, $extension){
		$media = new stdClass();
		$media->path = str_replace('-', '/', $path);
		$media->filename = $filename;
		$media->extension = $extension;
		$this->outputImage($media);
	}

	/**
	* Create image blob for browser to render image
	* ===
	* media - object containing media attributes
	* size - if looking for a specific resized variant
	*
	*/
	public function outputImage($media, $size = ''){
		$filename = '';
		if(!empty($media)){
			$filename = $media->path.'/'.$size.$media->filename.'.'.$media->extension;
		}
		if(!Storage::has($filename)){
			$filename = $media->path.'/'.$media->filename.'.'.$media->extension;
			if(!Storage::has($filename)){
				$media = $this->mediaFileCheck(1);
				$filename = $media->path.'/'.$media->filename.'.'.$media->extension;
				header("Content-Type: image/".$media->extension);
				echo Storage::get($filename);
			}else{
				$sizes = self::getSizeData();
				if(Storage::has($media->path.'/'.$media->filename.'.'.$media->extension)){
					$size = str_replace('_', '', $size);
					$toUpload = Storage::get($media->path.'/'.$media->filename.'.'.$media->extension);
					$imagick = new Imagick();
					$originalImage = $imagick->setSize($sizes[$size]['width'],$sizes[$size]['height']);
					$originalImage = $imagick->readimageblob($toUpload);
					$resizeImage = $imagick->resizeImage($sizes[$size]['width'], $sizes[$size]['height'], 1, 0, false);
					$resizedImage = $imagick->getImageBlob();
					$size = $size.'_';
					header("Content-Type: image/".$media->extension);
					echo $resizedImage;
					// $filename = $media->path.'/'.$size.$media->filename.'.'.$media->extension;
					// if(!Storage::has($filename)){
					// 	Storage::write($filename, $resizedImage);
					// }
				}
			}

		}else{
			header("Content-Type: image/".$media->extension);
			echo Storage::get($filename);
		}
		die();
	}

	/**
	* Delete image
	*
	*/
	public function deleteImage($id, $return = true){
		$media = Model::findOrFail($id);

		foreach($this->sizes as $size => $dimensions){
			if(Storage::has($media->path.'/'.$size.'_'.$media->filename.'.'.$media->extension)){
				Storage::delete($media->path.'/'.$size.'_'.$media->filename.'.'.$media->extension);
			}
		}

		Storage::delete($media->path.'/'.$media->filename.'.'.$media->extension);
		$media->delete();
		if($return){
			return redirect()->back()->withSuccess('Image deleted');
		}
	}

	public function get($path = null){
		if($path){
			if(Storage::has('public/'.$path)){
				$f = Storage::get('public/'.$path);
				$parts = explode('.', $path);
				$last = array_pop($parts);
				header("Content-Type: image/".$last);
				echo $f;
				die();
			}
		}
		return null;
	}
	public static function copyImageToDirectoryWithJSON($json, $id, $copyTo, $rename = null, $size = '', $format = 'dds'){
		self::copyImageToDirectory($id, $copyTo, $rename, $size, $format, $json);
	}
	//Size is full path, including filename/extension, when copying core file.
	public static function copyImageToDirectory($id, $copyTo, $rename = null, $size = '', $format = 'dds', $json = false){
		if($id > 0){
			if(!empty($size)){
				$size = $size.'_';
			}
			$m = new Media();
			$media = $m->mediaFileCheck($id, $json);
			if(empty($media)){
				$media = $m->mediaFileCheck(1);
			}
			if(is_array($media)){ //Because it returns array occasionally?
				$media = json_decode(json_encode($media));
			}
			if(strtolower($media->extension) == 'gif'){
				//Resized gifs cannot animate - So we always want to pull out full size gif images
				$size = '';
			}
			$filename = $media->path.'/'.$size.$media->filename.'.'.$media->extension;
			if(!Storage::has($filename)){
				if($media->extension == 'gif'){
					$filename = $media->path.'/'.$media->filename.'.'.$media->extension;
				}else{
					$sizes = self::getSizeData();
					if(Storage::has($media->path.'/'.$media->filename.'.'.$media->extension)){
						$size = str_replace('_', '', $size);
						$toUpload = Storage::get($media->path.'/'.$media->filename.'.'.$media->extension);
						$imagick = new Imagick();
						$originalImage = $imagick->setSize($sizes[$size]['width'],$sizes[$size]['height']);
						$originalImage = $imagick->readimageblob($toUpload);
						$resizeImage = $imagick->resizeImage($sizes[$size]['width'], $sizes[$size]['height'], 1, 0, false);
						$resizedImage = $imagick->getImageBlob();
						$size = $size.'_';
						$filename = $media->path.'/'.$size.$media->filename.'.'.$media->extension;
						if(!Storage::has($filename)){
							Storage::write($filename, $resizedImage);
						}
					}

				}
			}
			if(!Storage::has($filename)){
				$media = $m->mediaFileCheck(1);
				$filename = $media->path.'/'.$size.$media->filename.'.'.$media->extension;
				if(!Storage::has($filename = $media->path.'/'.$size.$media->filename.'.'.$media->extension)){
					$filename = $media->path.'/'.$media->filename.'.'.$media->extension;
				}
			}
			$moveTo = rtrim($copyTo, '/');
			$extension = $media->extension;
			if(!empty($rename)){
				$moveTo .= '/'.$rename;
			}else{
				$moveTo .= '/'.$size.$media->filename;
			}
			if(!Storage::has($filename)){
				return;
			}
		}else{
			if(!Storage::has($size)){
				return;
			}
			$filename = $size;
			$file = Storage::get($filename);
			$path = explode('/', $filename);
			$last = end($path);
			$ex = explode('.', $last);
			$extension = end($ex);
			

			$moveTo = rtrim($copyTo, '/');
			if(!empty($rename)){
				$moveTo .= '/'.$rename;
			}else{
				$rename = str_replace('.'.$extension, '', $last);
				$moveTo .= '/'.$rename;
			}

		}

		if(Storage::has($moveTo.'.'.$format)){
			if(Storage::lastModified($moveTo.'.'.$format) > strtotime("yesterday")){
				Storage::delete($moveTo.'.'.$format);
			}
		}

		if(Storage::has($filename.'.'.$format) && !Storage::has($moveTo.'.'.$format)){
			Storage::write($moveTo.'.'.$format, Storage::get($filename.'.'.$format));
			return;
		}

		$imagick = new Imagick();
		$originalImage = $imagick->readimageblob(Storage::get($filename));
		$reformatImage = $imagick->setformat($format);
		if($format == 'tga'){
			$imagick->setImageType(Imagick::IMGTYPE_TRUECOLORMATTE);
			$imagick->setImageOrientation(Imagick::ORIENTATION_LEFTTOP);
		}
		$reformattedImage = $imagick->getImageBlob();
		if(!Storage::has($filename.'.'.$format)){
			Storage::write($filename.'.'.$format, $reformattedImage);
		}
		if(!Storage::has($moveTo.'.'.$format)){
			Storage::write($moveTo.'.'.$format, $reformattedImage);
			Storage::copy($filename, $moveTo.'.'.$extension);
		}
	}

	public function createAdvisorGFXFromPortrait($gfx, $directory, $filename){
		$media = Model::findOrFail($gfx);

		$advisorImage = new Imagick();
		$advisorImage->readimageblob(Storage::get($media->path.'/'.$media->filename.'.'.$media->extension));
		$advisorImage->scaleImage(36, 0);
		$advisorImage->rotateImage(new ImagickPixel('none'), -4);

		$compositeCanvas = new Imagick();
		$compositeCanvas->newImage(
			65,
			67,
		    new ImagickPixel('none') // Transparent background
		);
		$compositeCanvas->compositeImage(
		    $advisorImage,
		    Imagick::COMPOSITE_OVER,
		    5, // X offset
		    6  // Y offset
		);

		$advisorOverlay = new Imagick();
		$advisorOverlay->readimageblob(Storage::get("advisor-overlay.png"));

		$imagick = new Imagick();
		$imagick->newImage(
			65,
			67,
		    new ImagickPixel('none') // Transparent background
		);
		$imagick->compositeImage(
			$compositeCanvas,
			Imagick::COMPOSITE_OVER,
			0,
			0
		);
		$imagick->compositeImage(
			$advisorOverlay,
			Imagick::COMPOSITE_OVER,
			0,
			0
		);


		$format = 'dds';
		$imagick->setImageFormat($format);
		$reformattedImage = $imagick->getImageBlob();

		Storage::write($directory.'/'.$filename.'.'.$format, $reformattedImage);
	}


	public function getTraitSprite($num){
		if(!Storage::has('hoi4-files/interface/ideas/trait-sprite-'.$num.'.png')){
			$spriteList = Storage::get('/game-files/gfx/interface/ideas/idea_traits_strip.dds');
			$imagick = new Imagick();
			$imagick->readimageblob($spriteList);

			$squareSize = $imagick->getImageHeight();

			if (!$imagick->cropImage($squareSize, $squareSize, $squareSize * ($num - 1), 0)){
		        throw new Exception('Failed to crop the image');
		    }

			$imagick->setImageFormat('png');
			$reformattedImage = $imagick->getImageBlob();
			Storage::write('hoi4-files/interface/ideas/trait-sprite-'.$num.'.png', $reformattedImage);
		}
		
		header("Content-Type: image");
		echo Storage::get('hoi4-files/interface/ideas/trait-sprite-'.$num.'.png');
	}
}