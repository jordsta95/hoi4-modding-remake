<?php

namespace App\Http\Controllers;

use Patreon\API;
use Patreon\OAuth;
use Storage;
use App\Models\User;

class PatreonController extends Controller{
	private $clientID = '0VTRKDtpXWOFGe7FIGImayqSI4P4SRddngBC6uk8C1qu-274FjxBtUk4OYapz9g6';
	private $clientSecret = 'odt5-BgH4hzxCLDT5CPIWv6FFnQl69XQVs90gdx-7ZbrtqyRAmQcooheoJ3NRi1K';
	private $campaignID = '2313707';
	private $redirect = null;
	private $accessToken = 'ynUgyu0hfqThw5lTQDYreQ-VujkHCkZbibt9l7fJcRY';

	public function __construct(){
		$this->redirect = env('APP_URL');
		if(empty($this->accessToken)){
			$oauth_client = new OAuth($this->clientID, $this->clientSecret);
			$tokens = $oauth_client->get_tokens($this->token, $this->redirect);
			$access_token = $tokens['access_token'];
			$refresh_token = $tokens['refresh_token'];

			$this->accessToken = $access_token;
		}
	}


	public function getPatrons(){
		if(Storage::has('patreonPatrons.json')){
			$json = json_decode(Storage::get('patreonPatrons.json'));
		}else{
			$json = json_decode(json_encode(['updated_on' => date('2000-00-00')]));
		}
		$date = \DateTime::createFromFormat('Y-m-d', $json->updated_on);
		$now = \DateTime::createFromFormat('Y-m-d', date('Y-m-d'));
		if($date < $now) {
			$api_client = new API($this->accessToken);
			$cursor = null;
			$l = urlencode('[');
			$r = urlencode(']');
			$patron_response = $api_client->get_data(str_replace(['[', ']'], [$l, $r], 'campaigns/2313707/members?include=currently_entitled_tiers,address&fields[member]=last_charge_status,lifetime_support_cents,currently_entitled_amount_cents,patron_status,email'));
			$emailAddresses = [];
			foreach($patron_response['data'] as $item){
				$atts = $item['attributes'];
				if($atts['patron_status'] == 'active_patron'){
					$emailAddresses[] = $atts['email'];
				}
			}

			$users = User::whereIn('email', $emailAddresses)->get();

			$info = [];

			foreach($users as $u){
				$info[$u->id] = ['username' => $u->username, 'avatar' => $u->media_id];
			}
			$json->updated_on = date('Y-m-d');
			$json->data = $info;
			$json = json_encode($json);

			Storage::delete('patreonPatrons.json');
			Storage::write('patreonPatrons.json', $json);
			$json = json_decode($json);
		}


		return $json->data;
	}

}
