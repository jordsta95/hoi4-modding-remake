<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Str;
use Storage;

use App\Models\Decision;
use App\Models\DecisionCategory;

class DecisionController extends Controller
{
	protected $decisionGFXList;
	/*
	* Show list of all decisions
	*
	*/
	public function index(){
		if(!Auth::check()){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}
		return view('tools/decision.index', ['mods' => Auth::user()->mods]);
	}

	/*
	* Create new decision category
	*
	*/
	public function create(Request $request){
		$id = DecisionCategory::insertGetId(
			[
				'mod_id' => $request->mod_id,
				'name' => $request->name,
				'language' => $request->lang,
				'gfx' => 1,
				'description_gfx' => 1,
				'description' => '',
				'data' => json_encode(['visible' => '', 'allowed' => '']),
				'created_at' => date('Y-m-d G:i:s'),
				'updated_at' => date('Y-m-d G:i:s')
			]
		);

		return redirect('/decision/edit/'.$id);
	}

	/*
	* View decision list
	*
	*/
	public function view($id){
		$events = DecisionCategory::find($id);
		$can_edit = false;
		if(Auth::user()){
			foreach($events->mod->users as $user){
				if($user->id == Auth::user()->id){
					$can_edit = true;
				}
			}
			if(Auth::user()->role_id == 2){
				$can_edit = true;
			}
		}

		return view('tools/decision.view', ['events' => $events, 'can_edit' => $can_edit]);
	}

	/*
	* Edit decision category
	*
	*/
	public function edit($id){
		$cat = DecisionCategory::find($id);
		$can_edit = false;
		if(Auth::user()){
			foreach($cat->mod->users as $user){
				if($user->id == Auth::user()->id){
					$can_edit = true;
				}
			}
			if(Auth::user()->role_id == 2){
				$can_edit = true;
			}
		}
		if(!$can_edit){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}

		return view('tools/decision.edit', ['category' => $cat, 'can_edit' => $can_edit]);
	}

	public function update(Request $request){
		$cat = DecisionCategory::find($request->id);
		foreach($request->request as $name => $value){
			if(!in_array($name, ['id', 'data'])){
				$cat->{$name} = $value;
			}
			if($name == 'data'){
				$cat->data = json_decode($value);
			}
		}
		$cat->save();
		return $cat;
	}

	public function updateOption(Request $request){
		if(strpos($request->id, 'new-') !== false){
			$decision = new Decision();
			$decision->decision_category_id = $request->category_id;
		}else{
			$decision = Decision::find($request->id);
		}
		$decision->name = $request->name;
		$decision->gfx = $request->gfx;
		$data = [];
		foreach(json_decode($request->data) as $name => $value){
			if(!empty($value)){
				$data[$name] = $value;
			}
		}
		$decision->data = json_decode(json_encode($data));
		$decision->save();
		return $decision;
	}

	public function deleteOption(Request $request){
		$decision = Decision::find($request->id);
		$decision->delete();
		return $decision;
	}

	/*
	* Delete decision category
	*
	*/
	public function delete(Request $request, $id){
		$decision = DecisionCategory::find($id);
		$can_edit = false;
		if(Auth::user()){
			foreach($decision->mod->users as $user){
				if($user->id == Auth::user()->id){
					$can_edit = true;
				}
			}
			if(Auth::user()->role_id == 2){
				$can_edit = true;
			}
		}

		if($can_edit){
			foreach($decision->decisions as $d){
				$d->delete();
			}
			$decision->delete();
		}

	}


	public function updateMedia(Request $request){
		$event = Event::findOrFail($request->id);
		$event->gfx = $request->media_id;
		$event->save();
	}

	public function updateGroup(Request $request, $id){
		$group = EventGroup::findOrFail($id);
		$group->prefix = $request->prefix;
		$group->name = $request->name;
		$group->filename = Str::slug($request->name);
		$group->language = $request->lang;
		$group->save();
		return redirect()->back()->with('success', translate('tools/mod/view.updated_ideas_group', ['name' => $group->name]));
	}

	public function import(Request $request){
		$json = json_decode($request->import);
		$category = DecisionCategory::find($request->id);
		$category->name = $json->category->name;
		if(isset($json->category->description)){
			$category->description = $json->category->description;
		}
		$category->save();
		foreach($json->list as $item){
			if(isset($item->name)){
				if(gettype($item->name) == "object"){
					if(isset($item->name->name)){
						$item->name = $item->name->name;
					}else{
						$name = '';
						foreach($item->name as $n){
							if(empty($name)){
								$name = $n;
							}
						}
						$item->name = $name;
					}
				}
			}else{
				$item->name = "??";
			}
			if(isset($item->description)){
				$item->data->description = $item->description;
				unset($item->description);
			}
			if(isset($item->icon)){
				$item->gfx = $this->getDecisionGFX($item->icon);
				unset($item->icon);
			}else{
				$item->gfx = '445';
			}

			foreach($item->data as $opt => $val){
				if(gettype($val) == "object"){
					$r = "";
					foreach($val as $k => $v){
						if(gettype($v) == "string"){
							$r .= $k.' = '.$v."\n";
						}else{
							$r .= $k.' = {'."\n";
							foreach($v as $i => $o){
								$r .= $i . ' = '.$o."\n";
							}
							$r .= "}\n";
						}
					}
					$val = $r;
				}
				$v = rtrim(ltrim(trim($val), '{'), '}');
				$v = trim($v);
				$item->data->{$opt} = $v;
			}

			$decision = new Decision();
			$decision->decision_category_id = $request->id;
			foreach($item as $name => $value){
				$decision->{$name} = $value;
			}

			$decision->save();

		}
		return true;
	}

	/*
	* Convert game GFX name to internal file ID - If file does not exist, set ID to 1
	*
	*/
	public function getDecisionGFX($gfx){
		if(empty($this->decisionGFXList)){
			$json = [];

			//Get media route list
			$mediaFile = Storage::get('media-routes.json');
			$mediaFile = json_decode($mediaFile);
			//Get goals JSON file
			$gfxFile = Storage::get('hoi4-files/interface/decisions.gfx');
			$gfxFile = json_decode($gfxFile);

			$gfxList = [];

			foreach($gfxFile->spriteTypes->spriteType as $file){
				$textureFile = str_replace(['\"', 'gfx/interface/decisions/', '.dds', '"'], '', $file->texturefile);
				$name = str_replace(['\"', '"'], '', $file->name);
				$gfxList[$textureFile] = $name;
			}

			foreach($mediaFile->media as $id => $media){
				if(isset($gfxList[$media->filename])){
					$json[$gfxList[$media->filename]] = $id; 
				}
			}

			$json = json_encode($json);

			$this->decisionGFXList = $json;
		}

		$json = json_decode($this->decisionGFXList);
		if(isset($json->$gfx)){
			return $json->$gfx;
		}

		return '445';
	}
}