<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;

class Admin extends Controller{
	public function uploadFiles(Request $request){
		$files = [];
		foreach($request->file('file') as $file){
			$files[] = $file;
		}
		$location = !empty($request->location) ? $request->location : '/converted-files';
		foreach($files as $file){
			$filename = $file->getClientOriginalName();
			$path = rtrim($location, '/').'/'.$filename;
			if(Storage::has($path)){
				Storage::delete($path);
			}
			Storage::write($path, $request->fileJson[$filename]);
		}
		return redirect()->back()->with('success', 'Files uploaded');
	}

	public function createMapSVG(Request $request){
		$path = $request->location;
		if(Storage::has($path)){
			Storage::delete($path);
		}
		Storage::write($path, $request->svg_code);
		return redirect()->back()->with('success', 'Map uploaded');
	}
}
