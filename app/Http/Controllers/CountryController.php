<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Storage;
use Imagick;

use App\Models\Mod;
use App\Models\Country;
use App\Models\CountryDate;
use App\Models\Media as MediaModel;

use App\Http\Contracts\Media;
use App\Http\Controllers\ModController;


class CountryController extends Controller{
	public $mods = [
			'vanilla' => [
				'start_dates' => [
					['year' => 1936, 'month' => 1, 'day' => 1],
					['year' => 1939, 'month' => 1, 'day' => 1]
				],
				'ideologies' => [
					[
						'overall' => 'democratic',
						'sub' => 'liberalism'
					],
					[
						'overall' => 'fascism',
						'sub' => 'nazism'
					],
					[
						'overall' => 'communism',
						'sub' => 'stalinism'
					],
					[
						'overall' => 'neutrality',
						'sub' => 'despotism'
					]
				]
			]
		];
	public $createError = null;
	/*
	* Show list of all countries
	*
	*/
	public function index(){
		if(!Auth::check()){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}
		return view('tools/country.index', ['mods' => Auth::user()->mods]);
	}

	public function list(){
		if(Auth::check()){
			$user = Auth::user();
			return $user->mods()->with('countries')->get();
		}
	}

	public function api($id){
		return Country::with('dates')->findOrFail($id);
	}

	public function checkIfCountryIsEditable($id){
		$editable = false;
		$userID = 0;
		if(Auth::check()){
			$userID = Auth::user()->id;
			$country = Country::findOrFail($id);
			$editable = $country->mt->userHasAccess($userID);
		}
		return json_decode(json_encode(['editable' => $editable, 'user_id' => $userID]));
	}

	public function getListOfAvailableIdeologiesForCountry(Request $request){
		$mod = Mod::findOrFail($request->mod_id);
		$country = Country::findOrFail($request->country_id);
		$modType = $country->mod;
		$available = $this->getAvailableIdeologiesStartDates($mod, $this->mods, $modType);
		if(empty($available)){
			return;
		}
		header('Content-Type: application/json');
		$return = ['available' => $available[$modType]['ideologies']];
		$current = [];
		foreach($country->dates[0]['ideologies'] as $i){
			$current[] = ['overall' => $i['ideology'], 'sub' => isset($i['sub_ideology']) ? $i['sub_ideology'] : ''];
		}
		$return['current'] = $current;
		return json_encode($return);
	}

	public function getListOfAvailableStartDatesForCountry(Request $request){
		$mod = Mod::findOrFail($request->mod_id);
		$country = Country::findOrFail($request->country_id);
		$modType = $country->mod;
		$available = $this->getAvailableIdeologiesStartDates($mod, $this->mods, $modType);
		if(empty($available)){
			return;
		}
		header('Content-Type: application/json');
		$return = ['available' => $available[$modType]['start_dates']];
		$current = [];
		foreach($country->dates as $i){
			$current[] = ['year' => $i->year, 'month' => $i->month, 'day' => $i->day];
		}
		$return['current'] = $current;
		return json_encode($return);
	}

	public function getAvailableIdeologiesStartDates($mod, $mods, $modType){
		$modController = new ModController();
		$json = json_decode($mod->data);
		
		if($json){
			if(isset($json->start_date_overhaul) && !empty($json->start_date_overhaul)){
				if($mod->startDates->count() > 0){
					$mods[$modType]['start_dates'] = [];
					
				}else{
					$this->createError = translate('tools/country/index.no_start_date');
					return;
				}
			}
			if(isset($json->ideology_overhaul) && !empty($json->ideology_overhaul)){
				if($mod->ideologies->count() > 0){
					$mods[$modType]['ideologies'] = [];
				}else{
					$this->createError = translate('tools/country/index.no_ideology');
					return;
				}
			}
		}

		if($mod->startDates->count() > 0){
			foreach($mod->startDates as $date){
				$e = explode('.', $date->date);
				if(count($e) == 4 || count($e) == 3){
					$mods[$modType]['start_dates'][] = ['year' => $e[0], 'month' => $e[1], 'day' => $e[2]];
				}else{ //Invalid
					$mods[$modType]['start_dates'][] = ['year' => 1936, 'month' => 1, 'day' => 1];
				}
			}
		}
		if($mod->ideologies->count() > 0){
			foreach($mod->ideologies as $index => $ideology){
				if(!empty($ideology->types)){
					$sub = $ideology->types[array_rand($ideology->types, 1)]['name'];
					$mods[$modType]['ideologies'][] = ['overall' => $modController->nameToId($ideology->name, $index.'_n'), 'sub' => $modController->nameToId($sub, $index.'_s')];
				}
			}
		}
		
		$startDates = [];
		foreach($mods[$modType]['start_dates'] as $startDate){
			$startDates[$startDate['year'].$startDate['month'].$startDate['day']] = $startDate;
		}

		ksort($startDates);
		$dates = [];
		$i = 0;
		foreach($startDates as $d){
			$dates[$i] = $d;
			$i++;
		}
		$mods[$modType]['start_dates'] = $dates;
		return $mods;
	}

	public function updateIdeologies(Request $request, $id){
		$country = Country::findOrFail($id);
		
		foreach($country->dates as $date){
			$ideologies = $date->ideologies;
			if(!empty($request->add)){
				
				foreach($request->add as $name => $value){
					$ideologies[] = [
						'ideology' => $name,
						'sub_ideology' => '',
						'name' => $country->name,
						'definition' => 'The '.$country->name,
						'nationality' => $country->name.' citizen',
						'flag' => 1,
						'leader' => '???',
						'leader_gfx' => 1,
						'traits' => '',
						'popularity' => 0
					];
				}

			}
			if(!empty($request->remove)){
				foreach($request->remove as $name => $value){
					$ideologies = $date->ideologies;
					foreach($ideologies as $index => $data){
						if($data['ideology'] == $name){
							unset($ideologies[$index]);
						}
					}
					
				}
			}
			$date->ideologies = $ideologies;
			$date->save();
		}

		return redirect()->back()->with(['success' => translate('tools/mod/view.updated_single', ['name' => $country->name])]);
	}

	public function updateStartDates(Request $request, $id){
		$country = Country::findOrFail($id);
		$mod = Mod::findOrFail($country->mt->id);
		$modType = $country->mod;
		$mods = $this->getAvailableIdeologiesStartDates($mod, $this->mods, $modType);
		if(isset($country->dates[0])){
			$startDate = $country->dates[0];
		}else{
			if(isset($this->mods[$modType])){
				$startDate = $this->mods[$modType]['start_dates'][0]; //Some idiot deleted all their start dates
			}else{
				$startDate = $this->mods['vanilla']['start_dates'][0]; //And their mod isn't listed
			}
		}

		$alreadyAdded = [];
		foreach($country->dates as $date){
			$alreadyAdded[] = [$date->year, $date->month, $date->day];
		}
		if($country->dates->count() === 0){
			$alreadyAdded[] = [$startDate['year'], $startDate['month'], $startDate['day']];
		}

		$addedNow = [];


		if(!empty($request->add)){
			foreach($request->add as $name => $value){
				$e = explode('.', $name);
				$earlier = false;
				if($e[0] < $alreadyAdded[0][0]){
					$earlier = true;
				}

				if($e[0] == $alreadyAdded[0][0] && $e[1] < $alreadyAdded[0][1]){
					$earlier = true;
				}

				if($e[0] == $alreadyAdded[0][0] && $e[1] == $alreadyAdded[0][1] && $e[2] < $alreadyAdded[0][2]){
					$earlier = true;
				}
				if($earlier){
					$d = new CountryDate();
					foreach($startDate->attributesToArray() as $n => $v){
						if($n != 'id'){
							$d->{$n} = $v;
						}
					}
					$d->year = $e[0];
					$d->month = $e[1];
					$d->day = $e[2];
					$d->save();
				}else{
					$date = ['year' => $e[0], 'month' => $e[1], 'day' => $e[2]];
					$d = $this->addStartDate($date, $country, $mods, $modType);
				}

				$addedNow[] = $d->id;
			}
		}

		if(!empty($request->remove)){
			foreach($request->remove as $name => $value){
				$e = explode('.', $name);
				$cd = CountryDate::where('year', '=', $e[0])->where('month', '=', $e[1])->where('day', '=', $e[2])->where('country_id', '=', $country->id)->whereNotIn('id', $addedNow)->get();
				foreach($cd as $c){
					$c->delete();
				}
			}
		}


		return redirect()->back()->with(['success' => translate('tools/mod/view.updated_single', ['name' => $country->name])]);
	}

	/*
	* Create a new country
	*
	*/
	public function create(Request $request){
		$government = [
			//First to last
			'mobilization_laws' => [
				'scraping_the_barrel' => [
					'is_default' => true,
					'is_active' => true,
					'icon' => 1903,
					'data' => []
				],
				'all_adults_serve' => [
					'is_default' => true,
					'is_active' => true,
					'icon' => 1114,
					'data' => []
				],
				'extensive_conscription' => [
					'is_default' => true,
					'is_active' => true,
					'icon' => 1308,
					'data' => []
				],
				'limited_conscription' => [
					'is_default' => true,
					'is_active' => true,
					'icon' => 1739,
					'data' => []
				],
				'volunteer_only' => [
					'is_default' => true,
					'is_active' => true,
					'icon' => 2066,
					'data' => []
				],
				'disarmed_nation' => [
					'is_default' => true,
					'is_active' => false,
					'icon' => 1262,
					'data' => []
				]
			],
			'economy' => [
				'tot_economic_mobilisation' => [
					'is_default' => true,
					'is_active' => false,
					'icon' => 1936,
					'data' => []
				],
				'war_economy' => [
					'is_default' => true,
					'is_active' => false,
					'icon' => 2071,
					'data' => []
				],
				'partial_economic_mobilisation' => [
					'is_default' => true,
					'is_active' => false,
					'icon' => 1820,
					'data' => []
				],
				'low_economic_mobilisation' => [
					'is_default' => true,
					'is_active' => false,
					'icon' => 1743,
					'data' => []
				],
				'civilian_economy' => [
					'is_default' => true,
					'is_active' => true,
					'icon' => 1247,
					'data' => []
				]
			],
			'political_advisor' => [
			],
			'tank_manufacturer' => [
			],
			'naval_manufacturer' => [
			],
			'aircraft_manufacturer' => [
			],
			'materiel_manufacturer' =>[
			],
			'industrial_concern'=>[
			],
			'theorist' =>[
			],
			'army_chief' => [
			],
			'navy_chief' => [
			],
			'air_chief' => [
			],
			'high_command' => [
			]
		];

		$mods = $this->mods;

		$mod = Mod::findOrFail($request->mod_id);
		$mods = $this->getAvailableIdeologiesStartDates($mod, $mods, 'vanilla');
		if(!empty($this->createError)){
			return redirect()->back()->with('error', $this->createError);
		}


		if(empty($mods['vanilla']['ideologies'])){
			return redirect()->back()->with(['error' => 'Ideology type not set for any custom ideologies']);
		}


		$country = new Country();
		foreach($request->all() as $name => $value){
			if($name != '_token'){
				$country->{$name} = $value;
			}
		}
		//Everything is json encoded, then decoded, to ensure that it saves to JSON field properly
		$country->colour = json_decode(json_encode(['r' => 0, 'g' => 0, 'b' => 0]));
		//$country->general_data = json_decode(json_encode('{}'));
		//$country->admiral_data = json_decode(json_encode('{}'));
		$country->capital = 1;
		$country->culture = 'european';
		//$country->ideas_data = json_decode(json_encode('{}'));
		$country->names = json_decode(json_encode(['male' => [],'female' => [],'surnames' => [],'callsigns' => []]));
		$country->government = json_decode(json_encode($government));
		$country->misc_data = json_decode(json_encode(
			[
				'set_war_support' => 0.5, 
				'set_stability' => 0.5, 
				'set_convoys' => 0, 
				'add_to_faction' => [], 
				'set_research_slots' => 3, 
				'starting_train_buffer' => 0,
				'add_command_power' => 0,
				'set_politics' => [
					'election_frequency' => 60, 
					'elections_allowed' => 'yes', 
					'last_election' => ($mods['vanilla']['start_dates'][0]['year'] - 1).".".rand(1, 11).".".rand(1, 27),
					'ruling_party' => 'democratic',
				],
				"give_military_access" => [], 
				"created_faction" => false,
				"add_breakthrough_progress" => [],
				"complete_special_project" => []
			]
		));
		$country->save();

		foreach($mods['vanilla']['start_dates'] as $date){
			$this->addStartDate($date, $country, $mods, 'vanilla');
		}

		return redirect('/country/edit/'.$country->id);
	}

	public function addStartDate($date, $country, $mods, $modType){
		$d = new CountryDate();
		$d->country_id = $country->id;
		$d->year = $date['year'];
		$d->month = $date['month'];
		$d->day = $date['day'];
		//$d->research = json_decode(json_encode('{}'));
		//Locations is province_id => number of divisions there
		$d->divisions = json_decode(json_encode([['name' => 'Example Division', 'locations' => []]]));
		//$d->states = json_decode(json_encode('{}'));
		$ideologies = [];
		$iNum = $mods[$modType]['ideologies'];
		if(empty($iNum)){
			if(isset($this->mods[$modType])){
				$iNum = $this->mods[$modType]['ideologies']; //Some idiot deleted all the data
			}else{
				$iNum = $this->mods['vanilla']['ideologies']; //And it wasn't an initially listed mod
			}
		}
		$ideologyCount = number_format((100 / count($iNum)), 2, '.', '');
		foreach($mods[$modType]['ideologies'] as $i){
			$ideologies[] = [
				'ideology' => $i['overall'],
				'name' => $country->name,
				'definition' => 'The '.$country->name,
				'nationality' => $country->name.' citizen',
				'flag' => 1,
				'popularity' => $ideologyCount
			];
		}
		$d->ideologies = json_decode(json_encode($ideologies));
		$d->save();
		return $d;
	}

	/*
	* Get edit page for country
	*
	*/
	public function edit($id){
		$country = Country::find($id);
		$dates = [];

		$canEdit = false;
		foreach(Auth::user()->mods as $mod){
			if($mod->id == $country->mod_id){
				$canEdit = true;
			}
		}
		if(Auth::user()->role_id == 2){
			$canEdit = true;
		}

		if(!$canEdit){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}

		foreach($country->dates as $date){
			$dates[] = $date;
		}
		$country->dates = $dates;
		$mod = $country->mt;

		return view('tools/country.view', ['country' => $country, 'mod' => $mod, 'ideas' => $mod->ideas]);
	}

	/*
	* Get view page for country
	*
	*/
	public function view($id){
		$country = Country::find($id);
		$dates = [];
		foreach($country->dates as $date){
			$dates[] = $date;
		}
		$country->dates = $dates;

		return view('tools/country.view', ['country' => $country]);
	}

	/*
	* Update country data
	*
	*/
	public function update(Request $request, $id){
		$country = Country::find($id);
		$data = json_decode($request->data);
		$this->setCountryData($data, $country);
	}

	public function updateName(Request $request, $id){
		$country = Country::find($id);
		$country->name = $request->name;
		$country->save();
		return redirect()->back()->with('success', 'Successfully updated country name');
	}

	/*
	* Upload a new flag for a country
	*
	*/
	public function addFlag(Request $request){

		$media = new Media();
		if(Auth::check()){
			$media->setUser(Auth::user()->id);
		}else{
			$media->setUser(0);
		}
		$options = [];

		if(!isset($request->path)){
			$options['path'] = 'uploads/'.Auth::user()->id.'/custom-gfx';
		}else{
			$options['path'] = $request->path;
		}

		if(isset($request->filename)){
			$options['name'] = $request->filename;
		}

		$options['tool'] = 'global';

		echo $media->handleUpload($request->file('flag'), $options);
		die();
	}

	/*
	* Import country information from files
	*
	*/
	public function import(Request $request, $id){
		$country = Country::find($id);
		$data = json_decode($request->data);

		foreach(['admiral_data', 'general_data'] as $type){
			if(isset($data->{$type})){
				foreach($data->{$type} as $general){
					if(isset($general->getGFX)){
						$g = explode('/', $general->getGFX);
						$picture = array_pop($g);
						$filename = explode('.', $picture)[0];
						$general->gfx = $this->getImportGFX($filename);
						unset($general->getGFX);
					}
				}
			}
		}

		foreach($data->dates as $date){
			foreach($date->ideologies as $i){
				if(isset($i->getLeaderGFX)){
					$g = explode('/', $i->getLeaderGFX);
					$picture = array_pop($g);
					$filename = explode('.', $picture)[0];

					$i->leader_gfx = $this->getImportGFX($filename);
					
					unset($i->getLeaderGFX);
				}

				if(isset($i->leader_description)){
					if('GET_LOC_DESC' == explode('||', $i->leader_description)[0]){
						$i->leader_description = '';
					}
				}
			}
		}
		
		return $this->setCountryData($data, $country);
	}

	/*
	* Search for media when importing files, hope to get a match
	*
	*/
	public function getImportGFX($filename){
		$model = MediaModel::where('filename', 'LIKE', $filename)->first();
		if(!empty($model) && isset($model->id)){
			return $model->id; 
		}
		return 1;
	}

	/*
	* Set country data from creation/update
	*
	*/
	public function setCountryData($data, $country){
		$dates = $data->dates;
		//Fields that don't want to update normally
		unset($data->dates);
		unset($data->created_at);
		unset($data->updated_at);
		unset($data->id);
		if(isset($data->mt)){
			unset($data->mt);
		}

		foreach($data as $name => $value){
			$country->{$name} = $value;
		}

		$country->save();


		foreach($country->dates as $date){
			foreach($dates as $d){
				if($date->id == $d->id){
					foreach($d as $name => $value){
						if(!in_array($name, ['id', 'country_id', 'created_at', 'updated_at'])){
							$date->{$name} = $value;
						}
					}
					$date->save();
				}
			}
		}

		return $country;
	}


	/*
	* Delete a country
	*
	*/
	public function delete(Request $request, $id){
		if(!Auth::check()){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}
		$canEdit = false;
		$country = Country::find($id);

		foreach(Auth::user()->mods as $mod){
			if($mod->id == $country->mod_id && $mod->pivot->administrator){
					$canEdit = true;
			}
		}
		if(Auth::user()->role_id == 2){
			$canEdit = true;
		}
		if(!$canEdit){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}

		foreach($country->dates as $date){
			$date->delete();
		}
		
		$country->delete();
		return redirect()->back()->with('success', translate('tools/country/help.country_deleted'));
	}


	/*
	Save country data from country tool
	*/
	public function save(Request $request){
		$model = Country::findOrFail($request->id);

		$country = json_decode($request->country);
		foreach($country as $key => $value){
			if(!in_array($key, ['id', 'mod_id', 'created_at', 'updated_at', 'dates'])){
				$model->{$key} = $value;
			}
		}
		$model->save();

		foreach($country->dates as $index => $date){
			$dateModel = CountryDate::findOrFail($date->id);
			foreach($date as $key => $value){
				if(!in_array($key, ['id', 'country_id', 'created_at', 'updated_at'])){
					$dateModel->{$key} = $value;
				}
			}
			$dateModel->save();
		}
	}

	/*
	Files for export
	*/
	private $exportDirectory;
	private $stateJSONs = [];
	private $modLanguage;
	private $countryIdeas = [];
	private $modCharacters;
	private $modIdeas;
	private $countryMedia;
	private $modCountries = [];
	private $mod;

	public function export($country, $directory){
		$this->exportDirectory = $directory;
		
		$localisation = $this->generateLocalisationFile($country);
		$history = $this->generateHistoryFile($country);
		$common = $this->generateCommonFile($country);
		$names = $this->generateNamesFile($country);
		$divisionsAndNamesLists = $this->generateDivisionsFile($country);
		$ideas = $this->generateIdeasFile($country);
		$this->addToStates($country);

		$this->modCountries[$country->tag] = $country->name;

		//Export files
		$cNameID = idify($country->name);
		$this->generateCountryGFX($country);
		$this->addDivisionsToExportDirectory($divisionsAndNamesLists);
		Storage::write($this->exportDirectory.'/common/countries/'.$country->name.'.txt', $common);
		Storage::write($this->exportDirectory.'/common/ideas/'.$cNameID.'_ideas.txt', $ideas);
		Storage::write($this->exportDirectory.'/history/countries/'.$country->tag.' - '.$country->name.'.txt', $history);
		Storage::write($this->exportDirectory.'/localisation/'.$this->modLanguage.'/country_'.$cNameID.'_l_'.$this->modLanguage.'.yml', $localisation);
	}

	//Function to generate the base files all countries should add to
	public function prepareExportFiles($mod){
		$json = json_decode($mod->data);
		if(isset($json->language)){
			$modLanguage = $json->language;
		}else{
			$modLanguage = 'english';
		}

		//Add empty states if overhauling
		if(isset($json->country_overhaul) && !empty($json->country_overhaul)){
			$stateFiles = \Storage::files('game-files/history/states');
			foreach($stateFiles as $file){
				$data = hoiFileToJSON(Storage::get($file), 0, 4);
				if(isset($data->state->history)){
					if(isset($data->state->history->owner)){
						$data->state->history->owner = '';
					}
					if(isset($data->state->history->add_core_of)){
						$data->state->history->add_core_of = [];
					}
				}
				$this->stateJSONs[$data->state->id] = $data;
			}
		}

		//Get all character IDs/internal names for the mod; as they may be needed
		$cc = new CharacterListController();
		$ic = new IdeaController();
		$this->modCharacters = $cc->getAllCharacterIdsAndNamesForMod($mod);
		$this->modIdeas = $ic->getAllIdeasAndNamesForMod($mod);
		$this->modLanguage = $modLanguage;
		$this->mod = $mod;
	}

	//Function called after all countries have been looped over; writing the files which overwrite default
	public function writeExportFiles(){
		$stateFiles = \Storage::files('game-files/history/states');
		foreach($stateFiles as $file){
			$parts = explode('history/states/', $file);
			$filename = $parts[1];
			$split = explode('-', $filename);
			$id = trim($split[0]);

			if(isset($this->stateJSONs[$id])){
				$newContent = $this->writeStateFile($this->stateJSONs[$id]);
				Storage::write($this->exportDirectory.'/history/states/'.$filename, $newContent);
			}
		}

		$countryFile = "";
		foreach($this->modCountries as $tag => $name){
			$countryFile .= tabbedLine($tag.' = "countries/'.$name.'.txt"');
		}
		Storage::write($this->exportDirectory.'/common/country_tags/custom_countries.txt', $countryFile);
		$this->generateCountryColours($this->mod);

		//Unset the data for the state files et al. to hopefully speed up exports
		unset($this->stateJSONs);
		unset($this->countryIdeas);
		unset($this->modIdeas);
		unset($this->modCharacters);
		unset($this->mod);
	}

	private function writeStateFile($file, $indent = 0){
		$export = '';
		foreach($file as $key => $value){
			if(!is_array($value) && !is_object($value)){
				if($key == 'provinces'){
					$export .= tabbedLine($key.' = { '.$value.' }', $indent);
				}else{
					$export .= tabbedLine($key.' = '.$value, $indent);
				}
			}
			if(is_array($value)){
				if($key == 'provinces'){
					$province = '';
				}
				foreach($value as $v){
					if($key == 'provinces'){
						$province .= $v.' ';
					}else{
						$export .= tabbedLine($key.' = '.$v, $indent);
					}
				}

				if($key == 'provinces'){
					$export .= tabbedLine($key . ' = { '.$province.'}', $indent);
				}
			}
			if(is_object($value)){
				if($key == 'provinces'){
					$province = '';
					foreach($value as $v){
						$province .= $v.' ';
					}
					$value = $province;
				}

				$export .= tabbedLine($key.' = {', $indent);
					$export .= $this->writeStateFile($value, ($indent + 1));
				$export .= tabbedLine('}', $indent);
			}
		}

		return $export;
	}

	//Generate all of the localisation stuff for this country
	private function generateLocalisationFile($country){
		//Compile the files
		$localisationFile = chr(239) . chr(187) . chr(191) ."l_".$this->modLanguage.":\n";

		$tag = $country->tag;
		foreach($country->dates as $index => $date){
			if($index == 0){
				foreach($date->ideologies as $ideology){
					$localisationFile .= tabbedLine($tag.'_'.$ideology['ideology'].':5 "'.$ideology['name'].'"');
					$localisationFile .= tabbedLine($tag.'_'.$ideology['ideology'].'_DEF:5 "'.$ideology['definition'].'"');
					$localisationFile .= tabbedLine($tag.'_'.$ideology['ideology'].'_ADJ:5 "'.$ideology['nationality'].'"');
					$localisationFile .= tabbedLine($tag.'_'.$ideology['ideology'].'_party'.':0 "'.(isset($ideology['party']) ? $ideology['party'] : '' ).'"');
					$localisationFile .= tabbedLine($tag.'_'.$ideology['ideology'].'_party_long'.':0 "'.(isset($ideology['party_long']) ? $ideology['party_long'] : '' ).'"');
				}
			}
		}

		foreach($country->government as $type => $ideas){
			foreach($ideas as $index => $idea){
				if(!$idea['is_default']){ //Default already have localisation in-game
					if(!isset($idea['is_character']) || !$idea['is_character']){ //Character localisation added via Character tool
						$name = $this->getValidIdeaID($tag, $idea['data']['name'], $country->id.'_'.$type.'_'.$index);
						$localisationFile .= tabbedLine($name.':0 "'.$idea['data']['name'].'"');
					}
				}
			}
		}

		return $localisationFile;
	}

	private function generateHistoryFile($country){
		$startDate = $country->dates[0];

		$historyFile = chr(239) . chr(187) . chr(191) .'';
		$historyFile .= tabbedLine('capital = '.$country->capital);

		$historyFile .= tabbedLine('# Recruit Characters');

		$recruited = [];

		//Leaders first, so that we have the right people in charge
		foreach($country->dates as $date){
			foreach($date->ideologies as $ideology){
				if(isset($ideology['leader_list'])){
					foreach($ideology['leader_list'] as $id){
						if(isset($this->modCharacters[$id]) && !in_array($id, $recruited)){
							$char = $this->modCharacters[$id];
							$historyFile .= tabbedLine('recruit_character = '.$char);
							$recruited[] = $char;
						}
					}
				}
			}
		}

		foreach($country->government as $type => $ideas){
			foreach($ideas as $index => $idea){
				if(isset($idea['is_character']) && $idea['is_character']){
					$id = explode(':', $index)[1];
					if(isset($this->modCharacters[$id]) && !in_array($id, $recruited)){
						$char = $this->modCharacters[$id];
						$historyFile .= tabbedLine('recruit_character = '.$char);
						$recruited[] = $char;
					}
				}
			}
		}

		if(!empty($country->general_data)){
			foreach($country->general_data as $id){
				if(is_numeric($id)){
					if(isset($this->modCharacters[$id]) && !in_array($id, $recruited)){
						$char = $this->modCharacters[$id];
						$historyFile .= tabbedLine('recruit_character = '.$char);
						$recruited[] = $char;
					}
				}
			}
		}

		if(!empty($country->admiral_data)){
			foreach($country->admiral_data as $id){
				if(is_numeric($id)){
					if(isset($this->modCharacters[$id]) && !in_array($id, $recruited)){
						$char = $this->modCharacters[$id];
						$historyFile .= tabbedLine('recruit_character = '.$char);
						$recruited[] = $char;
					}
				}
			}
		}
		
		$historyFile .= $this->getHistoryDateData($startDate, $country, 0, true);

		$historyFile .= tabbedLine('# Government/National Spirits');
		$historyFile .= tabbedLine('add_ideas = {');
		foreach($country->government as $type => $ideas){
			foreach($ideas as $index => $idea){
				if($idea['is_active']){
					if(!$idea['is_default']){
						if(!isset($idea['is_character']) || !$idea['is_character']){
							$name = $this->getValidIdeaID($country->tag, $idea['data']['name'], $country->id.'_'.$type.'_'.$index);
							$historyFile .= tabbedLine($name, 1);
						}else{
							$id = explode(':', $index)[1];
							if(isset($this->modCharacters[$id])){
								$char = $this->modCharacters[$id];
								$historyFile .= tabbedLine($char, 1);
							}
						}
					}else{
						$historyFile .= tabbedLine($index, 1);
					}
				}
			}
		}
		if(isset($startDate->misc_data['national_spirits'])){
			foreach($startDate->misc_data['national_spirits'] as $idea){
				if(!is_numeric($idea)){
					$idea = isset($idea->id) ? $idea->id : $idea['id'];
				}
				if(isset($this->modIdeas[$idea])){
					$historyFile .= tabbedLine($this->modIdeas[$idea], 1);
				}
			}
		}

		$historyFile .= tabbedLine('}');
		$historyFile .= tabbedLine('');

		foreach($country->dates as $index => $startDate){
			if($index != 0){
				$historyFile .= $this->getHistoryDateData($startDate, $country, 1, false);
			}
		}

		


		return $historyFile;
	}

	private function getHistoryDateData($startDate, $country, $indent, $isFirstDateInList = false){
		$historyFile = '';

		if(!$isFirstDateInList){
			$historyFile .= tabbedLine($startDate->year.'.'.$startDate->month.'.'.$startDate->day.' = {');
		}
		$historyFile .= tabbedLine('oob = "'.$country->tag.'_'.$startDate->year.'"', $indent);

		$historyFile .= tabbedLine('starting_train_buffer = '.$this->getMiscDataOrDefault('starting_train_buffer', $country, $startDate, 0), $indent);
		$historyFile .= tabbedLine('set_research_slots = '.$this->getMiscDataOrDefault('set_research_slots', $country, $startDate, 2), $indent);
		$historyFile .= tabbedLine('set_stability = '.$this->getMiscDataOrDefault('set_stability', $country, $startDate, 0.5), $indent);
		$historyFile .= tabbedLine('set_war_support = '.$this->getMiscDataOrDefault('set_war_support', $country, $startDate, 0.2), $indent);
		$historyFile .= tabbedLine('add_command_power = '.$this->getMiscDataOrDefault('add_command_power', $country, $startDate, 0), $indent);
		$historyFile .= tabbedLine('set_convoys = '.$this->getMiscDataOrDefault('set_convoys', $country, $startDate, 0), $indent);

		$historyFile .= tabbedLine('');

		if(isset($startDate->misc_data->subjects) && !empty($startDate->misc_data->subjects)){
			$historyFile .= tabbedLine('# Subjects');
			foreach($startDate->misc_data->subjects as $subject){
				$historyFile .= tabbedLine('set_autonomy = {', $indent);
					$historyFile .= tabbedLine('target = '.$subject->tag, ($indent + 1));
					$historyFile .= tabbedLine('autonomous_state = '.$subject->tag, ($indent + 1));
				$historyFile .= tabbedLine('}', $indent);
			}
			$historyFile .= tabbedLine('');
		}

		$faction = $this->getMiscDataOrDefault('create_faction', $country, $startDate, '');
		if(!empty($faction)){
			$historyFile .= tabbedLine('# Faction', $indent);
			$historyFile .= tabbedLine('create_faction = "'.$faction.'"', $indent);

			$addTo = $this->getMiscDataOrDefault('add_to_faction', $country, $startDate, $indent);
			if(!empty($addTo)){
				if(is_array($addTo)){ //No idea why it's an array; but just to be safe from holdover data
					if(is_array($addTo[0])){
						$n = '';
						foreach($addTo as $index => $i){
							if($index > 0){
								$n .= ',';
							}
							$n .= implode(',', $i);
						}
						$addTo = $n;
					}else{
						$addTo = implode(',', $addTo);
					}
				}
				$g = explode(',', $addTo);
				foreach($g as $tag){
					$t = trim($tag);
					if(!empty($t)){
						$historyFile .= tabbedLine('add_to_faction = '.$t, $indent);
					}
				}
			}

			$historyFile .= tabbedLine('');
		}

		$milAccess = $this->getMiscDataOrDefault('give_military_access', $country, $startDate, '');
		if(!empty($milAccess)){
			$historyFile .= tabbedLine('# Military Access', $indent);
			if(is_array($milAccess)){ //No idea why it's an array; but just to be safe from holdover data
				if(is_array($milAccess[0])){
					$n = '';
					foreach($milAccess as $index => $i){
						if($index > 0){
							$n .= ',';
						}
						$n .= implode(',', $i);
					}
					$milAccess = $n;
				}else{
					$milAccess = implode(',', $milAccess);
				}
			}
			$g = explode(',', $milAccess);
			foreach($g as $tag){
				$t = trim($tag);
				if(!empty($t)){
					$historyFile .= tabbedLine('give_military_access = '.$t, $indent);
				}
			}

			$historyFile .= tabbedLine('');
		}

		$guarantees = $this->getMiscDataOrDefault('give_guarantee', $country, $startDate, '');
		if(!empty($guarantees)){
			$historyFile .= tabbedLine('# Guarantees', $indent);
			$g = explode(',', $guarantees);
			foreach($g as $tag){
				$t = trim($tag);
				if(!empty($t)){
					$historyFile .= tabbedLine('give_guarantee = '.$t, $indent);
				}
			}

			$historyFile .= tabbedLine('');
		}

		if(!$isFirstDateInList){
			if(!empty($startDate->misc_data->national_spirits)){
				$historyFile .= tabbedLine('# National Spirits', $indent);
				$historyFile .= tabbedLine('add_ideas = {', $indent);
				foreach($startDate->misc_data->national_spirits as $idea){
					if(!is_numeric($idea)){
						$idea = $idea->id;
					}
					if(isset($this->modIdeas[$idea])){
						$historyFile .= tabbedLine($this->modIdeas[$idea], ($indent + 1));
					}
				}

				$historyFile .= tabbedLine('}', $indent);
				$historyFile .= tabbedLine('');
			}
		}

		if(!empty($startDate->research)){
			$historyFile .= tabbedLine('# Research', $indent);
			$historyFile .= tabbedLine('set_technology = {', $indent);
				foreach($startDate->research as $item){
					$historyFile .= tabbedLine($item['value'].' = 1', ($indent + 1));
				}
			$historyFile .= tabbedLine('}', $indent);
		}

		$historyFile .= tabbedLine('# Politics for '.$startDate->year.'.'.$startDate->month.'.'.$startDate->day, $indent);
		$historyFile .= tabbedLine('set_politics = {', $indent);
		$defaultIdeology = '';
		if(isset($country->dates[0]->ideologies[0]['ideology'])){
			$defaultIdeology = $country->dates[0]->ideologies[0]['ideology'];
		}
			$historyFile .= tabbedLine('ruling_party = '.$this->getMiscDataOrDefault('set_ruling_party', $country, $startDate, $defaultIdeology),($indent + 1));
			$historyFile .= tabbedLine('last_election = "'.$this->getMiscDataOrDefault('last_election', $country, $startDate, '1934.1.1').'"', ($indent + 1));
			$historyFile .= tabbedLine('election_frequency = '.$this->getMiscDataOrDefault('election_frequency', $country, $startDate, '48'), ($indent + 1));
			$allowed = $this->getMiscDataOrDefault('elections_allowed', $country, $startDate, true);
			$historyFile .= tabbedLine('elections_allowed = '.(($allowed === 'yes' || $allowed === true || $allowed === 1) ? 'yes' : 'no'), ($indent + 1));
		$historyFile .= tabbedLine('}', $indent);

		if($isFirstDateInList){
			$historyFile .= tabbedLine('set_popularities = {');
				foreach($startDate->ideologies as $i){
					$historyFile .= tabbedLine($i['ideology'].' = '.intval($i['popularity']), 1);
				}
			$historyFile .= tabbedLine('}');
		}

		if(!$isFirstDateInList){
			$historyFile .= tabbedLine('}');
		}

		return $historyFile;
	}

	private function generateCommonFile($country){
		$commonFile = '';
		$commonFile .= tabbedLine('graphical_culture = '.$country->culture.'_gfx');
		$commonFile .= tabbedLine('graphical_culture_2d = '.$country->culture.'_2d');
		$commonFile .= tabbedLine('color = { '.$country->colour['r'].' '.$country->colour['g'].' '.$country->colour['b'].' }');

		return $commonFile;
	}

	private function generateNamesFile($country){
		$names = '';
		if(isset($country->names) && !empty($country->names)){
			$names = tabbedLine($country->tag.' = {');
			foreach($country->names as $type => $nameList){
				$names .= tabbedLine($type.' = {', 1);
					$indent = 2;
					if(in_array($type, ['male', 'female'])){
						$indent++;
					}
					if($indent == 3){
						$names .= tabbedLine('names = {', 2);
					}
						$allNames = '';
						if(is_string($nameList)){
							$nameList = explode(',', $nameList);
						}
						foreach($nameList as $name){
							$allNames .= '"'.$name.'" ';
						}
						$names .= tabbedLine($allNames, $indent);
					if($indent == 3){
						$names .= tabbedLine('}', 2);
					}
				$names .= tabbedLine('}', 1);
			}
			$names .= tabbedLine('}');
		}
		

		return $names;
	}

	private function generateDivisionsFile($country){
		$n = 1; //For unit numbering
		$divArr = [];
		$nameArr = [];
		foreach($country->dates as $dateIndex => $date){
			if(!empty($date->divisions)){
				$divFile = '';
				$nameList = '';

				//Add division templates
				$divFile .= tabbedLine('#Create units');
				foreach($date->divisions as $index => $division){
					$divFile .= tabbedLine("division_template = {");
						$divFile .= tabbedLine('name = "'.$division['name'].'"', 1);
						$divFile .= tabbedLine('division_names_group = "'.$country->tag.'_DIV_'.$index.'_'.$dateIndex.'"', 1);

						$nameList .= tabbedLine($country->tag.'_DIV_'.$index.'_'.$dateIndex.' = {');
							$nameList .= tabbedLine('name = "'.$division['name'].'"', 1);
							$nameList .= tabbedLine('for_countries = { '.$country->tag.' }', 1);
							$nameList .= tabbedLine('can_use = { always = yes }', 1);
							$divisionList = [];
							if(isset($division['regiments'])){
								foreach($division['regiments'] as $x => $data){
									foreach($data as $y => $unit){
										if(!in_array($unit, $divisionList)){
											$divisionList[] = $unit;
										}
									}
								}
							}else{
								$divisionList[] = 'infantry';
							}
							$nameList .= tabbedLine('division_types = { ', 1);
							$divList = '';
							foreach($divisionList as $d){
								$divList .= '"'.$d.'" ';
							}
							$divList .= '';
							$nameList .= tabbedLine($divList, 2);
							$nameList .= tabbedLine('}', 1);
							$nameList .= tabbedLine('fallback_name = "%d. '.$division['name'].'"', 1);
						$nameList .= tabbedLine('}');
						
						if(isset($division['regiments'])){
							$divFile .= tabbedLine('regiments = {', 1);
							foreach($division['regiments'] as $x => $data){
								foreach($data as $y => $unit){
									$divFile .= tabbedLine($unit.' = { x = '.$x.' y = '.$y.'}', 2);
								}
							}
							$divFile .= tabbedLine('}', 1);
						}
					
						if(isset($division['support'])){
							$divFile .= tabbedLine('support = {', 1);
								foreach($division['support'] as $y => $unit){
									$divFile .= tabbedLine($unit.' = { x = 0 y = '.$y.'}', 2);
								}
							$divFile .= tabbedLine('}', 1);
						}
					$divFile .= "\n}\n";
				}
				$divFile .= tabbedLine('');
				$divFile .= tabbedLine('#Spawn units');
				//Add templates to their location(s)
				$divFile .= tabbedLine('units = {');
					foreach($date->divisions as $index => $division){
						foreach($division['locations'] as $province => $amount){
							if(!empty($amount)){ //Not 0, null...
								$amount = (int) $amount;
								//Add all of the units for this province
								while($amount > 0){
									$divFile .= tabbedLine('division= {', 1);
										$ordinal = date( 'S', mktime( 1, 1, 1, 1, ( (($n>=10)+($n%100>=20)+($n==0))*10 + $n%10) ));

										$divFile .= tabbedLine('name = "'.$n.$ordinal.' '.$division['name'].'"', 2);
										$divFile .= tabbedLine('location = '.$province, 2);
										$divFile .= tabbedLine('division_template = "'.$division['name'].'"', 2);
									$divFile .= tabbedLine('}', 1);

									$n++;

									$amount--;
								}
							}
						}
					}
				$divFile .= tabbedLine('}');

				$divArr[$country->tag.'_'.$date->year.'.txt'] = $divFile;
				$nameArr[$country->tag.'_'.$date->year.'_names_divisions.txt'] = $nameList;
			}
		}

		return ['divisions' => $divArr, 'names' => $nameArr];
	}

	private function addDivisionsToExportDirectory($lists){
		if(!empty($lists)){
			foreach($lists['divisions'] as $filename => $contents){
				Storage::write($this->exportDirectory.'/history/units/'.$filename, $contents);
			}
			foreach($lists['names'] as $filename => $contents){
				Storage::write($this->exportDirectory.'/common/units/names_divisions/'.$filename, $contents);
			}
		}
	}

	private function generateIdeasFile($country){
		$ideasFile = '';
		$tag = $country->tag;
		$ideasFile .= tabbedLine('ideas = {');
			foreach($country->government as $type => $ideas){
				$ideasFile .= tabbedLine($type.' = {', 1);
					foreach($ideas as $index => $idea){
						if(!$idea['is_default'] && (!isset($idea['is_character']) || $idea['is_character'] == false)){
							$name = $this->getValidIdeaID($tag, $idea['data']['name'], $country->id.'_'.$type.'_'.$index);

							$ideasFile .= tabbedLine($name.' = {', 2);
								$ideasFile .= tabbedLine('allowed = {', 3);
									$ideasFile .= tabbedLine('tag = '.$tag, 4);
								$ideasFile .= tabbedLine('}', 3);

								if(is_array($idea['data']['traits'])){
									$idea['data']['traits'] = implode(' ', $idea['data']['traits']);
								}

								$ideasFile .= tabbedLine('traits = {'.$idea['data']['traits'].'}', 3);

								if(isset($idea['data']['ledger']) && !empty($idea['data']['ledger'])){
									$ideasFile .= tabbedLine('ledger = '.$idea['data']['ledger'], 3);
								}

								//$ideasFile .= tabbedLine('picture = "'.$fileName.'"', 3); - Not necessary?
							$ideasFile .= tabbedLine('}', 2);
						}
					}
				$ideasFile .= tabbedLine('}', 1);
			}
		$ideasFile .= tabbedLine('}');

		return $ideasFile;
	}

	private function addToStates($country){
		$tag = $country->tag;
		foreach($country->dates as $index => $d){
			if(!empty($d->states)){
				foreach($d->states as $stateID => $state){
					$date = null;
					if($index > 0){
						$date = $d->year.'.'.$d->month.'.'.$d->day;
					}
					$this->updateStateJSON($stateID, $tag, $state['core'], $state['control'], $date);
				}
			}
		}
	}

	private function updateStateJSON($stateID, $tag, $core = false, $control = false, $date = null){
		if(!isset($this->stateJSONs[$stateID])){
			$stateFiles = \Storage::files('game-files/history/states');
			foreach($stateFiles as $file){
				$parts = explode('history/states/', $file);
				$filename = $parts[1];
				$split = explode('-', $filename);
				$id = trim($split[0]);
				if($id == $stateID){
					$data = hoiFileToJSON(Storage::get($file), 0, 4);
					$this->stateJSONs[$id] = $data;
				}
			}
		}

		$data = $this->stateJSONs[$stateID];
		if($control){
			if($date == null){
				$data->state->history->owner = $tag;
				if(isset($data->state->history->controller)){
					$data->state->history->controller = $tag;
				}
			}else{
				if(!isset($data->state->history->{$date})){
					$data->state->history->{$date} = new \StdClass();
				}
				$data->state->history->{$date}->owner = $tag;
				if(isset($data->state->history->{$date}->controller)){
					$data->state->history->{$date}->controller = $tag;
				}
			}
		}
		if($core){
			if($date == null){
				if(!isset($data->state->history->add_core_of)){
					$data->state->history->add_core_of = [];
				}
				if(is_string($data->state->history->add_core_of)){
					$data->state->history->add_core_of = [$data->state->history->add_core_of];
				}
				$data->state->history->add_core_of[] = $tag;
			}else{
				if(!isset($data->state->history->{$date})){
					$data->state->history->{$date} = new \StdClass();
					$data->state->history->{$date}->add_core_of = [];
				}
				if(!isset($data->state->history->{$date}->add_core_of)){
					$data->state->history->{$date}->add_core_of = [];
				}
				$data->state->history->{$date}->add_core_of[] = $tag;
			}
		}
		$this->stateJSONs[$stateID] = $data;
	}

	private function generateCountryGFX($country){
		$customGfx = '';

		$customGfx .= tabbedLine('spriteTypes = { ');
		foreach($country->government as $type => $ideas){
			foreach($ideas as $index => $idea){
				if(!$idea['is_default'] && (!isset($idea['is_character']) || $idea['is_character'] == false)){
					$name = $this->getValidIdeaID($country->tag, $idea['data']['name'], $country->id.'_'.$type.'_'.$index);

					Media::copyImageToDirectoryWithJSON($this->countryMedia,(int) $idea['icon'], $this->exportDirectory.'/gfx/interface/ideas/'.$country->tag, $name, 'idea');
					$customGfx .= tabbedLine('##Icon For: '.$idea['data']['name'], 1);
					$customGfx .= tabbedLine('spriteType = { ', 1);
					$customGfx .= tabbedLine('name = "GFX_idea_'.$name.'"', 2);
					$customGfx .= tabbedLine('texturefile = "gfx/interface/ideas/'.$country->tag.'/'.$name.'.dds"', 2);
					$customGfx .= tabbedLine('}', 1);
					$customGfx .= tabbedLine('');
				}
			}
		}
		$customGfx .= tabbedLine('}');

		foreach($country->dates[0]->ideologies as $i){
			$flag = $i['flag'];
			if($flag != 1 && $flag != "1"){
				$fileName = $country->tag.'_'.$i['ideology'];
				Media::copyImageToDirectoryWithJSON($this->countryMedia, $flag, $this->exportDirectory.'/gfx/flags', $fileName, 'flag-standard', 'tga');
				Media::copyImageToDirectoryWithJSON($this->countryMedia, $flag, $this->exportDirectory.'/gfx/flags/medium', $fileName, 'flag-medium', 'tga');
				Media::copyImageToDirectoryWithJSON($this->countryMedia, $flag, $this->exportDirectory.'/gfx/flags/small', $fileName, 'flag-small', 'tga');
			}
		}

		Storage::write($this->exportDirectory.'/interface/ideas/country-'.idify($country->name).'_ideas_customicons.gfx', $customGfx);
	}

	private function generateCountryColours($mod){
		$countryColoursFile = Storage::get('/game-files/common/countries/colors.txt');
		$json = json_decode($mod->data);
		if($json){
			if(isset($json->country_overhaul) && !empty($json->country_overhaul)){
				$countryColoursFile = '';
			}
		}
		$countryColoursFile .= tabbedLine('');
		$countryColoursFile .= tabbedLine('#Custom colours start here');

		foreach($mod->countries as $i => $c){
			$countryColoursFile .= tabbedLine($c->tag.' = {');
			$countryColoursFile .= tabbedLine('color = rgb { '.$c->colour['r'].' '.$c->colour['g'].' '.$c->colour['b'].' }', 1);
			$countryColoursFile .= tabbedLine('color_ui = rgb { '.$c->colour['r'].' '.$c->colour['g'].' '.$c->colour['b'].' }', 1);
			$countryColoursFile .= tabbedLine('}');
		}
			
		Storage::write($this->exportDirectory.'/common/countries/colors.txt', $countryColoursFile);
	}


	/*
	Utility functions
	*/
	private function getValidIdeaID($tag, $name, $index){
		if(isset($this->countryIdeas[$index])){
			return $this->countryIdeas[$index];
		}

		$name = $tag.'_idea_'.idify($name);

		//If name has already been added once, let's add a number to the end to make the ID unique
		if(in_array($name, $this->countryIdeas)){
			$num = 1;
			$newName = $name;
			while(in_array($newName, $this->countryIdeas)){
				$newName = $name.'_'.$num;
				$num++;
			}
			$name = $newName;
		}

		$this->countryIdeas[$index] = $name;

		return $name;
	}

	private function getMiscDataOrDefault($selector, $country, $date, $default = '', $arraySelector = false){
		if(isset($date->misc_data[$selector])){
			$data = $date->misc_data[$selector];
			if($arraySelector != false){

				if(isset($data[$arraySelector])){
					return $data[$arraySelector];
				}
			}else{
				return $data;
			}
		}
		if(isset($country->misc_data->{$selector})){
			$data = $country->misc_data->{$selector};
			if($arraySelector != false){
				if(isset($data[$arraySelector])){
					return $data[$arraySelector];
				}
			}else{
				return $data;
			}
		}

		if(isset($country->misc_data[$selector])){
			$data = $country->misc_data[$selector];
			if($arraySelector != false){
				if(isset($data[$arraySelector])){
					return $data[$arraySelector];
				}
			}else{
				return $data;
			}
		}
		return $default;
	}

	private function getCountryMediaJSON($country){
		if(empty($this->countryMedia)){
			$ids = [];
			foreach($country->dates as $date){
				foreach($date->ideologies as $i){
					$ids[] = $i['flag'];
				}
				foreach($date->government as $type){
					foreach($type as $item){
						if(!$item['is_default'] && (!isset($item['is_character']) || $item['is_character'] == false)){
							$ids[] = $item['icon'];
						}
					}
				}
			}
			$media = MediaModel::whereIn('id', $ids)->get();
			$array = [];
			foreach($media as $item){
				$array[$item->id] = $item;
			}

			$this->countryMedia = json_decode(json_encode(['media' => json_decode(json_encode($array))]));
		}

		return $this->countryMedia;
	}
}
