<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Models\FocusTree;
use App\Models\Focus;

use Auth;
use Storage;


class FocusTreeController extends Controller{
	protected $focusGFXList = null;

	/*
	* Show list of all focus trees
	*
	*/
	public function index(){
		if(!Auth::check()){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}
		return view('tools/focus-tree.index', ['mods' => Auth::user()->mods]);
	}

	/*
	* Show view page for focus tree
	* ===
	* id - ID of the focus tree being edited
	*
	*/
	public function view($id){
		$focustree = FocusTree::find($id);
		
		if(empty($focustree)){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}

		return view('tools/focus-tree.view', ['focustree' => $focustree]);
	}

	/*
	* Show edit page for focus tree
	* ===
	* id - ID of the focus tree being edited
	*
	*/
	public function edit($id){
		$focustree = FocusTree::find($id);
		
		if(empty($focustree)){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}

		if(!Auth::check()){
			return redirect('/focus-tree/view/'.$id)->with('error', translate('auth.unauthorised'));
		}

		$canEdit = false;
		foreach(Auth::user()->mods as $mod){
			if($mod->id == $focustree->mod_id){
				$canEdit = true;
			}
		}
		if(Auth::user()->role_id == 2){
			$canEdit = true;
		}

		if(!$canEdit){
			return redirect('/focus-tree/view/'.$id)->with('error', translate('auth.unauthorised'));
		}
		
		
		return view('tools/focus-tree.edit', ['focustree' => $focustree]);
	}

	/*
	* Create a new focus trees
	*
	*/
	public function create(Request $request){
		$tree_id = $request->name;
		if(!empty($request->tree_id)){
			$tree_id = $request->tree_id;
		}
		$id = FocusTree::insertGetID([
			'tag' => $request->tag,
			'name' => $request->name,
			'tree_id' => Str::slug($tree_id),
			'mod_id' => $request->mod_id,
			'lang' => $request->lang 
		]);
		return redirect()->back()->with('success', translate('tools/focus-tree/help.focus_tree_created'));
	}

	/*
	* Delete a focus tree
	*
	*/
	public function delete(Request $request, $id){
		if(!Auth::check()){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}
		$canEdit = false;
		$focusTree = FocusTree::find($id);
		foreach(Auth::user()->mods as $mod){
			if($mod->id == $focusTree->mod_id && $mod->pivot->administrator){
					$canEdit = true;
			}
		}
		if(!$canEdit){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}

		
		$focusTree->delete();
		return redirect()->back()->with('success', translate('tools/focus-tree/help.focus_tree_deleted'));
	}

	/*
	* Update a focus tree
	*
	*/
	public function update(Request $request, $id){
		if(!Auth::check()){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}
		$canEdit = false;
		$focusTree = FocusTree::find($id);
		foreach(Auth::user()->mods as $mod){
			if($mod->id == $focusTree->mod_id && $mod->pivot->administrator){
					$canEdit = true;
			}
		}
		if(Auth::user()->role_id == 2){
			$canEdit = true;
		}
		if(!$canEdit){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}

		
		$focusTree->name = $request->name;
		$focusTree->tag = $request->tag;
		$focusTree->tree_id = $request->tree_id;
		$focusTree->lang = $request->lang;
		$focusTree->save();
		return redirect()->back()->with('success', translate('tools/focus-tree/help.focus_tree_updated'));
	}

	/*
	* Import focus tree from existing file
	*
	*/
	public function import(Request $request){
		$tree_id = $request->id;
		
		$insert = [];

		$nonDataAttributes = ['name', 'description', 'gfx', 'x', 'y', 'id', 'icon'];
		$json = json_decode($request->import);
		foreach($json->focus_tree->focus as $focus){
			$toAdd = ['focus_tree_id' => $tree_id];

			$toAdd['name'] = (isset($focus->name)) ? $focus->name : 'undefined';
			$toAdd['description'] = (isset($focus->description)) ? $focus->description : 'undefined';
			$toAdd['gfx'] = $this->getFocusGFX($focus->icon);
			$toAdd['x'] = $focus->x;
			$toAdd['y'] = $focus->y;
			$toAdd['game_id'] = $focus->id;

			$data = [];
			foreach($focus as $key => $value){
				if(!in_array($key, $nonDataAttributes)){
					$data[$key] = $this->importData($key, $value);
				}
			}
			$toAdd['data'] = json_encode($data);


			$insert[] = $toAdd;
		}

		$focuses = Focus::insert($insert);

		$focusList = Focus::where('focus_tree_id', '=', $tree_id)->get();

		$idList = [];
		foreach($focusList as $f){
			$idList[$f->game_id] = $f->id;
		}

		krsort($idList);


		foreach($focusList as $f){
			$data = json_decode($f->data);
			$me = isset($data->mutually_exclusive) ? $data->mutually_exclusive : '';
			$p = isset($data->prerequisite) ? $data->prerequisite : '';

			foreach($idList as $game_id => $id){
				if(isset($data->mutually_exclusive)){
					$data->mutually_exclusive = str_replace($game_id, $id, $data->mutually_exclusive);
				}
				if(isset($data->prerequisite)){
					$data->prerequisite = str_replace($game_id, $id, $data->prerequisite);
				}
			}

			if((!empty($me) && $me != $data->mutually_exclusive) || (!empty($p) && $p != $data->prerequisite)){
				var_dump('Updated');
				$f->data = json_encode($data);
				$f->save();
			}
		}

		return json_encode($focusList);
	}

	/*
	* Convert game GFX name to internal file ID - If file does not exist, set ID to 1
	*
	*/
	public function getFocusGFX($gfx){
		if(empty($this->focusGFXList)){
			$json = [];

			//Get media route list
			$mediaFile = Storage::get('media-routes.json');
			$mediaFile = json_decode($mediaFile);
			//Get goals JSON file
			$gfxFile = Storage::get('hoi4-files/interface/goals.gfx');
			$gfxFile = json_decode($gfxFile);

			$gfxList = [];

			foreach($gfxFile->spriteTypes->SpriteType as $file){
				$textureFile = str_replace(['\"', 'gfx/interface/goals/', '.dds', '"'], '', $file->texturefile);
				$name = str_replace(['\"', '"'], '', $file->name);
				$gfxList[$textureFile] = $name;
			}

			foreach($mediaFile->media as $id => $media){
				if(isset($gfxList[$media->filename])){
					$json[$gfxList[$media->filename]] = $id; 
				}
			}

			$json = json_encode($json);

			$this->focusGFXList = $json;
		}

		$json = json_decode($this->focusGFXList);
		if(isset($json->$gfx)){
			return $json->$gfx;
		}

		return '445';
	}

	/*
	* Ensure data is formatted correctly
	*
	*/
	public function importData($key, $value){
		if(method_exists($this, 'format_'.$key.'_ForImport')){
			return $this->{'format_'.$key.'_ForImport'}($value);
		}else{
			return $value;
		}
	}

	/*
	* Format Search Filters for import
	*
	*/
	public function format_search_filters_ForImport($value){
		return str_replace(['{', '}', ' '], '', $value);
	}

	/*
	* Format Reward for import
	*
	*/
	public function format_completion_reward_ForImport($value){
		if(is_array($value)){
			$val = '';
			foreach($value as $index => $v){
				if($index == 0){
					$var = rtrim($v, '}');
				}else{
					$var = ltrim($v, '{');
				}
				$val .= $var;
			}
			$value = $val;
		}
		return rtrim(ltrim($value, '{'), '}');
	}

	/*
	* Format Bypass for import
	*
	*/
	public function format_bypass_ForImport($value){
		if(gettype($value) == "array"){
			$r = '';
			foreach($value as $v){
				$r .= rtrim(ltrim($v, '{'), '}'). '} bypass = {';
			}
			$r = rtrim($r, '} bypass = {');
			return $r;
		}
		return rtrim(ltrim($value, '{'), '}');
	}

	/*
	* Format Available for import
	*
	*/
	public function format_available_ForImport($value){
		if(is_array($value)){
			$val = '';
			foreach($value as $index => $v){
				if($index == 0){
					$var = rtrim($v, '}');
				}else{
					$var = ltrim($v, '{');
				}
				$val .= $var;
			}
			$value = $val;
		}
		return rtrim(ltrim($value, '{'), '}');
	}

	/*
	* Format AI will do for import
	*
	*/
	public function format_ai_will_do_ForImport($value){
		if(!is_numeric($value)){
			$factor = '1';
		}else{
			$factor = str_replace(['{', '}', 'factor', '=', ' '], '', $value);
			$factor = trim($factor);
			if(!is_numeric($factor)){
				$factor = '1';
			}
		}
		return $factor;
	}

	/*
	* Format Mutually Exclusive for import
	*
	*/
	public function format_mutually_exclusive_ForImport($value){
		$output = '';
		if(is_array($value)){
			foreach($value as $v){
				$output .= $this->importFocusListFixer($v);
				$output .= 'OR';
			}
			$output = rtrim($output, 'OR');
		}else{
			$output .= $this->importFocusListFixer($value);
		}
		return $output;
	}

	/*
	* Format Prerequisite for import
	*
	*/
	public function format_prerequisite_ForImport($value){
		$output = '';
		if(is_array($value)){
			foreach($value as $v){
				$output .= $this->importFocusListFixer($v);
				$output .= 'OR';
			}
			$output = rtrim($output, 'OR');
		}else{
			$output .= $this->importFocusListFixer($value);
		}
		return $output;
	}


	/*
	* Create && list for focus list on import.
	* e.g. 
	* { focus = focus_1 focus = focus_2 }
	* becomes
	* focus_1&&focus_2 - To be converted later to internal IDs (1&&2)
	*
	*/
	public function importFocusListFixer($list){
		$return = '';
		$fixed = rtrim(ltrim($list, '{'), '}');
		$arr = explode(' ', $fixed);
		foreach($arr as $a){
			if($a != 'focus' && $a != '=' && !empty($a)){
				$return .= $a.'AND';
			}
		}
		$return = rtrim($return, 'AND');
		return $return;
	}


	public function updateContinuousFocusesBox(Request $request){
		$tree = FocusTree::findOrFail($request->id);
		$tree->x = $request->x;
		$tree->y = $request->y;
		$tree->save();
	}


	/* 
	Export files
	*/
	public function export($dir, $tree){
		
	}
}
