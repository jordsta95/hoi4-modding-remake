<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**
* Contacts
*/
use App\Http\Contracts\Media;
use App\Http\Contracts\Admin\MediaManager;

use Auth;

class MediaController extends Controller
{


    protected $mediaContract;
    protected $mediaManager;
    protected $userId;

    public function __construct(){
    	$this->middleware(function (Request $request, $next) {
            if (Auth::check()) {
            	$this->setUserID(Auth::user()->id); // you can access user id here
            }else{
            	$this->setUserID(0);
            }
            return $next($request);
        });
    }

    public function setUserID($id){
    	$this->mediaContract = new Media($id);
        $this->mediaManager = new MediaManager();
    }

    public function upload(Request $request){
        return method_exists($this->mediaContract, 'upload') ? $this->mediaContract->upload($request) : null;
    }

    public function uploadCustomGFX(Request $request){
        return method_exists($this->mediaContract, 'uploadCustomGFX') ? $this->mediaContract->uploadCustomGFX($request) : null;
    }

    public function find($id){
        return method_exists($this->mediaContract, 'find') ? $this->mediaContract->find($id) : null;
    }

    public function findSized($id, $size){
        return method_exists($this->mediaContract, 'find') ? $this->mediaContract->find($id, $size.'_') : null;
    }

    public function findByName($path, $filename, $extension){
        return method_exists($this->mediaContract, 'findByName') ? $this->mediaContract->findByName($path, $filename, $extension) : null;
    }

    public function renderMediaManager(){
        return method_exists($this->mediaManager, 'renderMediaManager') ? $this->mediaManager->renderMediaManager() : null;
    }

    public function renderMediaManagerPath($path){
        return method_exists($this->mediaManager, 'renderMediaManagerPath') ? $this->mediaManager->renderMediaManagerPath($path) : null;
    }

    public function deleteImage($id){
        return method_exists($this->mediaContract, 'deleteImage') ? $this->mediaContract->deleteImage($id) : null;
    }

    public function mediaManagerUpload(Request $request){
        return method_exists($this->mediaContract, 'mediaManagerUpload') ? $this->mediaContract->mediaManagerUpload($request) : null;
    }

    public function get($path){
        return method_exists($this->mediaContract, 'get') ? $this->mediaContract->get($path) : null;
    }

    public function getTraitSprite($num){
        return method_exists($this->mediaContract, 'getTraitSprite') ? $this->mediaContract->getTraitSprite($num) : null;
    }

}
