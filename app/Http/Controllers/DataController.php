<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;

class DataController extends Controller{
	public function locator($data){
		//dd($data);
		if(method_exists($this, 'get'.ucfirst($data))){
			return $this->{'get'.ucfirst($data)}();
		}else{
			return response(view('errors/404'),404);
		}
	}

	public function getTags(){
		return getCountryArray();
	}

	public function getBuilder(){
		$builderInfo = json_decode(\Storage::get('builder.json'));
		$builderText = getLangArray('tools/builder');

		foreach($builderInfo as $type => $option){
			if($type != 'version'){
				foreach($option as $key => $info){
					if(isset($builderText->{$info->game_id})){
						$option->{$key}->description = $builderText->{$info->game_id};
					}
				}
			}
		}
		return json_encode($builderInfo);
	}

	public function getStates(){
		$date = \Storage::lastModified('states.json');
		$now = strtotime(date('Y-m-d'));

		if($date < $now){
			$localistion = localisationKeyValue(\Storage::get('game-files/localisation/english/state_names_l_english.yml'));
			$compiledData = [];

			$return = [];
			foreach($localistion as $key => $name){
				$stateID = str_replace('STATE_', '', $key);
				if(is_numeric($stateID)){
					$return[$stateID] = $name;
				}
			}

			$nameTags = [];
			$files = \Storage::files('game-files/common/country_tags');
			foreach($files as $file){
				$data = Storage::get($file);
				$lines = explode("\n", $data);

				foreach($lines as $mainLine){
					$rLines = explode("\r", $mainLine);
					foreach($rLines as $line){
						$split = explode('=', $line);
						$tag = trim($split[0]);
						if(isset($split[1])){
							$filename = explode('/', $split[1]);
							if(isset($filename[1])){
								$name = explode('.', $filename[1])[0];
								$nameTags[$tag] = $name;
							}
						}
					}
				}
			}

			$colours = [];
			$countryFiles = \Storage::files('game-files/common/countries');
			foreach($countryFiles as $file){
				$country = str_replace(['game-files/common/countries/', '.txt'], '', $file);
				$tag = array_search($country, $nameTags);
				
				$data = hoiFileToJSON(Storage::get($file));
				
				if(isset($data->color)){
					$colour = 'rgb('.str_replace(' ', ',', str_replace('  ', ' ',trim($data->color))).')';
					$colours[$tag] = $colour;
				}
			}

			$stateData = [];
			$stateFiles = \Storage::files('game-files/history/states');
			foreach($stateFiles as $file){
				$data = hoiFileToJSON(Storage::get($file), 0, 4);
				$owner = $data->state->history->owner;

				$stateData[] = [
					'id' => $data->state->id,
					'name' => $return[$data->state->id],
					'colour' => isset($colours[$owner]) ? $colours[$owner] : 'rgb(0,0,0)',
					'provinces' => explode(' ', trim($data->state->provinces))
				];
			}

			$data = json_encode($stateData);
			\Storage::put('states.json', $data);
			
		}

		return json_decode(\Storage::get('states.json'));
	}

	public function getTraits(){
		$lang = 'english';
		if(isset($_GET['lang'])){
			$lang = $_GET['lang'];
		}
		return getTraits($lang);
	}

	public function getAutonomy(){
		$lang = 'english';
		if(isset($_GET['lang'])){
			$lang = $_GET['lang'];
		}
		return getAutonomyLevels($lang);
	}

	public function getProvinces(){
		$mod = 'vanilla';
		if(isset($_GET['mod'])){
			$mod = $_GET['mod'];
		}

		$csv = Storage::get('game-files/map/definition.csv');

		$data = [];
		$lines = explode("\r\n", $csv);
		foreach($lines as $line){
			$col = explode(";",$line);
			$data[] = ['r' => $col[1], 'g' => $col[2], 'b' => $col[3], 'hex' => strtoupper(sprintf("#%02x%02x%02x", $col[1], $col[2], $col[3]))];
		}
		return $data;
	}

	public function getUnits(){
		$localistion = localisationKeyValue(\Storage::get('game-files/localisation/english/unit_l_english.yml'));

		$return = [];

		$startKey = 'SERVICE_MANPOWER_HEADER';
		$started = false;

		foreach($localistion as $key => $value){
			if(!$started){
				$started = $key == $startKey;
			}else{
				if(strtolower($key) == $key){ //Need to make sure it's actually a thing that _could_ be added
					$keyParts = explode('_', $key);
					if(array_pop($keyParts) == 'desc' || (isset($keyParts[0]) && in_array($keyParts[0], ['category', 'division', 'marines', 'damaged', 'on', 'broken', 'rudder', 'naval', 'destroyed', 'strat']))  || !isset($keyParts[0]) || $key == 'super_heavy_railway_gun'){
						continue;
					}
					$return[$key] = $value;
				}
			}
		}

		return $return;
	}

	public function getMapBMP(){
		return Storage::get('game-files/map/provinces.bmp');
	}

	public function getResearch(){
		$localistion = localisationKeyValue(\Storage::get('game-files/localisation/english/research_l_english.yml'));
		$return = [];
		foreach($localistion as $key => $value){
			if(strtolower($key) == $key){ //Need to make sure it's actually a thing that _could_ be added
				$keyParts = explode('_', $key);
				$last = array_pop($keyParts);
				if(in_array($last, ['folder', 'desc', 'tt', 'or', 'and', 'tech']) || (isset($keyParts[0]) && in_array($keyParts[0], ['click', 'mio', 'cat']))  || !isset($keyParts[0]) || $key == 'super_heavy_railway_gun'){
					continue;
				}
				$return[$key] = $value;
			}
		}

		return $return;
	}
}
