<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CountryLeaderTrait;
use App\Models\UnitLeaderTrait;
use App\Models\Mod;
use Auth;
use Storage;
use App\Models\Media as MediaModel;
use App\Http\Contracts\Media;

class TraitController extends Controller
{
	public function list(){
		if(!Auth::check()){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}
		return view('tools/traits.list', ['mods' => Auth::user()->mods]);
	}

	public function getLists(){
		if(Auth::check()){
			$user = Auth::user();
			$return = $user->mods()->get();
			$traits = [
				[
					'id' => '',
					'name' => 'Unit Leader Traits'
				],
				[
					'id' => '',
					'name' => 'Country Leader Traits'
				]
			];


			foreach($return as $index => $r){
				$traits[0]['id'] = $r->id.'/unit';
				$traits[1]['id'] = $r->id.'/country';

				$return[$index]->traits = json_decode(json_encode($traits));
			}

			return $return;
		}
	}

	public function view($id, $type){
		if(!Auth::check()){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}
		$mod = Mod::findOrFail($id);
		return view('tools/traits.view', ['mod' => $mod, 'type' => $type]);
	}

	public function getData($id, $type){
		if($type == "country"){
			$data = CountryLeaderTrait::where('mod_id', $id)->get();
		}else{
			//Will default to unit leader if someone tries to change the URL
			$data = UnitLeaderTrait::where('mod_id', $id)->get();
		}
		return $data;
	}

	public function checkTraitsEditable($id){
		$editable = false;
		$userID = 0;
		if(Auth::check()){
			$userID = Auth::user()->id;
			$mod = Mod::findOrFail($id);
			$editable = $mod->userHasAccess($userID);
		}
		return json_decode(json_encode(['editable' => $editable, 'user_id' => $userID]));
	}
}
