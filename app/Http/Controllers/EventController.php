<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Str;
use Storage;

use App\Models\Event;
use App\Models\EventGroup;

class EventController extends Controller
{
    private $eventGFXList = null;
	/*
	* Show list of all events
	*
	*/
	public function index(){
		if(!Auth::check()){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}
		return view('tools/event.index', ['mods' => Auth::user()->mods]);
	}

	/*
	* Create new events group
	*
	*/
	public function create(Request $request){
		$id = EventGroup::insertGetId(
			[
				'mod_id' => $request->mod_id,
				'name' => $request->name,
				'filename' => str_replace('-','_',Str::slug($request->name)),
				'language' => $request->lang,
				'prefix' => $request->prefix,
				'created_at' => date('Y-m-d G:i:s'),
				'updated_at' => date('Y-m-d G:i:s')
			]
		);
		return redirect('/events/edit/'.$id);
	}

	/*
	* View events group
	*
	*/
	public function view($id){
		$events = EventGroup::find($id);
		$can_edit = false;
		if(Auth::user()){
			foreach($events->mod->users as $user){
				if($user->id == Auth::user()->id){
					$can_edit = true;
				}
			}
			if(Auth::user()->role_id == 2){
				$can_edit = true;
			}
		}

		return view('tools/event.view', ['events' => $events, 'can_edit' => $can_edit]);
	}

	/*
	* Edit event group
	*
	*/
	public function edit($id){
		$events = EventGroup::find($id);
		$can_edit = false;
		if(Auth::user()){
			foreach($events->mod->users as $user){
				if($user->id == Auth::user()->id){
					$can_edit = true;
				}
			}
			if(Auth::user()->role_id == 2){
				$can_edit = true;
			}
		}
		if(!$can_edit){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}

		return view('tools/event.edit', ['events' => $events, 'can_edit' => $can_edit]);
	}

	public function update(Request $request){
		if($request->id == 'new'){
			return $this->addEvent($request);
		}else{
			return $this->editEvent($request);
		}
	}

	public function updateOption(Request $request){
		$event = Event::find($request->id);
		$options = $event->options;
		if(empty($options)){
			$options = json_decode("{}", true);
		}
		if($request->option_index == 'new'){
			$options[time()] = json_decode(json_encode(['name' => $request->name, 'data' => $request->data]));
		}else{
			$options[$request->option_index] = json_decode(json_encode(['name' => $request->name, 'data' => $request->data]));
		}
		$event->options = $options;
		$event->save();
		return $event;
	}

	public function deleteOption(Request $request){
		$event = Event::find($request->id);
		$options = $event->options;
		unset($options[$request->option_index]);
		$event->options = $options;
		$event->save();
		return $event;
	}

	/*
	* Add new event
	*
	*/
	public function addEvent($request){
		$event = new Event();
		$event->event_group_id = $request->group_id;
		$event = $this->setEventData($event, $request);
		$event->save();
		return $event;
	}

	/*
	* Edit existing event
	*
	*/
	public function editEvent($request){
		$event = Event::find($request->id);
		$event = $this->setEventData($event, $request);
		$event->save();
		return $event;
	}

	/*
	* Update event's data
	*
	*/
	public function setEventData($event, $request){
		$event->name = $request->name;
		$event->type = $request->type;
		$event->gfx = $request->gfx;

		

		$data = [];
		foreach(json_decode($request->data) as $name => $value){
			if(!empty($value)){
				$data[$name] = $value;
			}
		}

		$event->data = $data;

		return $event;
	}

	/*
	* Delete event
	*
	*/
	public function delete(Request $request){
		$event = Event::find($request->id);
		$can_edit = false;
		if(Auth::user()){
			foreach($event->eventGroup->mod->users as $user){
				if($user->id == Auth::user()->id){
					$can_edit = true;
				}
			}
			if(Auth::user()->role_id == 1){
				$can_edit = true;
			}
		}

		if($can_edit){
			$event->delete();
		}

	}

	public function deleteGroup(Request $request, $id){
		$group = EventGroup::find($id);
		$can_edit = false;
		if(Auth::user()){
			foreach($group->mod->users as $user){
				if($user->id == Auth::user()->id){
					$can_edit = true;
				}
			}
			if(Auth::user()->role_id == 1){
				$can_edit = true;
			}
		}

		if($can_edit){
			foreach($group->events as $event){
				$event->delete();
			}
			$name = $group->name;
			$group->delete();
			return redirect()->back()->with('success', translate('tools/mod/view.delete_ideas_group', ['name' => $name]));
		}

	}

	public function updateMedia(Request $request){
		$event = Event::findOrFail($request->id);
		$event->gfx = $request->media_id;
		$event->save();
	}

	public function updateGroup(Request $request, $id){
		$group = EventGroup::findOrFail($id);
		$group->prefix = $request->prefix;
		$group->name = $request->name;
		$group->filename = Str::slug($request->name);
		$group->language = $request->lang;
		$group->save();
		return redirect()->back()->with('success', translate('tools/mod/view.updated_ideas_group', ['name' => $group->name]));
	}

	public function import(Request $request){
		$json = json_decode($request->import);
		foreach($json as $item){
			if(isset($item->name)){
				if(gettype($item->name) == "object"){
					if(isset($item->name->name)){
						$item->name = $item->name->name;
					}else{
						$name = '';
						foreach($item->name as $n){
							if(empty($name)){
								$name = $n;
							}
						}
						$item->name = $name;
					}
				}
			}else{
				$item->name = "??";
			}
			if(isset($item->data->description)){
				if(gettype($item->data->description) == "object"){
					$item->data->description = $item->data->description->name;
				}
			}
			if(isset($item->picture)){
				$item->gfx = $this->getEventGFX($item->picture);
				unset($item->picture);
			}else{
				$item->gfx = '445';
			}

			foreach($item->data as $opt => $val){
				if(gettype($val) == "object"){
					$r = "";
					foreach($val as $k => $v){
						if(gettype($v) == "string"){
							$r .= $k.' = '.$v."\n";
						}else{
							$r .= $k.' = {'."\n";
							foreach($v as $i => $o){
								$r .= $i . ' = '.$o."\n";
							}
							$r .= "}\n";
						}
					}
					$val = $r;
				}
				$v = rtrim(ltrim(trim($val), '{'), '}');
				$v = trim($v);
				$item->data->{$opt} = $v;
			}

			if(empty($options)){
				$options = json_decode("{}", true);
			}
			foreach($item->options as $opt => $val){
				if(isset($val->name)){
					if(gettype($val->name) == "object"){
						$val->name = $val->name->name;
					}
				}else{
					$val->name = '??';
				}
				$options["import_".$opt] = json_decode(json_encode(['name' => $val->name, 'data' => json_encode($val->data)]));
			}
			$item->options = json_decode(json_encode($options));

			$event = new Event();
			$event->event_group_id = $request->id;
			foreach($item as $name => $value){
				$event->{$name} = $value;
			}

			$event->save();

		}
		return true;
	}

	/*
	* Convert game GFX name to internal file ID - If file does not exist, set ID to 1
	*
	*/
	public function getEventGFX($gfx){
		if(empty($this->eventGFXList)){
			$json = [];

			//Get media route list
			$mediaFile = Storage::get('media-routes.json');
			$mediaFile = json_decode($mediaFile);
			//Get goals JSON file
			$gfxFile = Storage::get('hoi4-files/interface/eventpictures.gfx');
			$gfxFile = json_decode($gfxFile);

			$gfxList = [];

			foreach($gfxFile->spriteTypes->spriteType as $file){
				$textureFile = str_replace(['\"', 'gfx/interface/event_pictures/', '.dds', '"'], '', $file->texturefile);
				$name = str_replace(['\"', '"'], '', $file->name);
				$gfxList[$textureFile] = $name;
			}

			foreach($mediaFile->media as $id => $media){
				if(isset($gfxList[$media->filename])){
					$json[$gfxList[$media->filename]] = $id; 
				}
			}

			$json = json_encode($json);

			$this->eventGFXList = $json;
		}

		$json = json_decode($this->eventGFXList);
		if(isset($json->$gfx)){
			return $json->$gfx;
		}

		return '445';
	}
}
