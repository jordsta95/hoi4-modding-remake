<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Storage;
use App\Models\User;
use App\Models\UserMod;
use App\Models\Media as MediaModel;
use App\Http\Contracts\Media;

class UserController extends Controller{
	public function index(){
		if(!Auth::check()){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}
		return view('auth.user', ['user' => Auth::user()]);
	}

	public function getUser($id){
		if(is_numeric($id)){
			$user = User::find($id);
		}else{
			$user = User::where('username', '=', urldecode($id))->first();
		}
		if(empty($user)){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}
		return view('auth.user', ['user' => $user]);
	}


	public function addUserToMod(Request $request, $id){
		$role = !empty($request->role) ? $request->role : translate('tools/user/profile.team_member');
		$insert = UserMod::insert(['mod_id' => $request->mod_id, 'user_id' => $id, 'role' => $role]);

		if($insert){
			return redirect()->back()->with('success', translate('tools/user/profile.team_member_added'));
		}else{
			return redirect()->back()->with('error', translate('tools/user/profile.team_member_failed'));
		}
	}

	public function updateUserData(Request $request){
		if(!Auth::check()){
			return redirect('/')->with('error', translate('auth.unauthorised'));
		}

		$user = User::find(Auth::user()->id);

		if(!empty($request->file('avatar'))){
			$media = new Media();
			$media->setUser(Auth::user()->id);
			$options = [];
			$options['path'] = '/user/'.Auth::user()->id.'/avatars/'.date('Y-m-d');
			$upload = $media->uploadImage($request->avatar, $options, false);

			$user->media_id = $upload->id;
		}
		$user->language = $request->language;
		$user->description = $request->description;
		$user->links = json_encode($request->link);
		$user->updated_at = date('Y-m-d H:i:s');
		
		if(isset($request->style)){
			$user->styles = json_encode($request->theme[$request->style['theme']]);
		}

		$user->save();

		return redirect('/user/'.$user->id)->with('success', translate('tools/user/profile.profile_updated'));
	}

	public function findUser(Request $request){
		$users = User::where('can_post', '=', 1);
		if(!isset($request->search) || (isset($request->search) && empty($request->search))){
			if(isset($request->first_letter)){
				switch($request->first_letter){
					case '0':
						$users->where('username', 'regexp', '^[0-9]+');
						break;
					case '-':
						$users->where('username', 'regexp', '^[^0-9A-Za-z]');
						break;
					default:
						$users->where('username', 'like', $request->first_letter.'%');
						break;
				}
			}
		}
		if(isset($request->search) && !empty($request->search)){
			$users->where('username', 'like', '%'.$request->search.'%');
		}
		$users->inRandomOrder(date('Ymd'));
		$users = $users->paginate(51);
		return view('auth.search', ['users' => $users]);
	}

	public function getMedia($id){
		$media = new Media();
		$userMedia = $media->getUserMedia($id);
		$return = [];
		if($userMedia->count() == 0){
			return json_encode($return);
		}

		$sizes = $media->getImageSizes();
		foreach($userMedia as $gfx){
			$item = ['id' => $gfx->id, 'sizes' => []];
			foreach($sizes as $sizeName => $size){
				$item['sizes'][$sizeName] = Storage::has($gfx->path.'/'.$sizeName.'_'.$gfx->filename.'.'. $gfx->extension);
			}
			$return[] = $item;
		}
		return json_encode($return);
	}

	public function updateMedia(Request $request){
		$id = $request->id;
		$media = new Media();
		if(isset($request->delete) && !empty($request->delete)){
			$media->deleteImage($id, false);
			return;
		}
		$model = MediaModel::find($id);
		$data = json_decode($request->data);
		foreach($data as $size => $set){
			if($set){
				$media->generateResizedImage($model, $size);
			}
		}
	}

	public function deleteUser(Request $request){
		if(!isset($request->delete) || $request->delete != 'CONFIRM'){
			return redirect()->back()->with(['error' => 'Confirmation phrase incorrect']);
		}
		$user = Auth::user();
		$id = $user->id;
		$allModsDeleted = true;

		if(count($user->mods) > 0){
			foreach($user->mods as $mod){
				$shouldModBeDeleted = true;
				//Check the users in the mod first
				if(count($mod->roles) > 1){
					$shouldModBeDeleted = false;
					$allModsDeleted = false;
					foreach($mod->roles as $role){
						if($role->user_id == $id){
							$role->delete();
						}else{
							$role->administrator = 1;
							$role->save();
						}
					}
				}
				if($shouldModBeDeleted){
					if(count($mod->focus_trees) > 0){
						foreach($mod->focus_trees as $tree){
							$treeID = $tree->id;
							\DB::table('foci')->where('focus_tree_id', '=', $treeID)->delete();
							$tree->delete();
						}
					}
					if(count($mod->events) > 0){
						foreach($mod->events as $event){
							$eventID = $event->id;
							\DB::table('events')->where('event_group_id', '=', $eventID)->delete();
							$event->delete();
						}
					}
					if(count($mod->ideas) > 0){
						foreach($mod->ideas as $idea){
							$ideaID = $idea->id;
							\DB::table('ideas')->where('idea_group_id', '=', $ideaID)->delete();
							$idea->delete();
						}
					}
					if(count($mod->countries) > 0){
						foreach($mod->countries as $country){
							$countryID = $country->id;
							\DB::table('country_dates')->where('country_id', '=', $countryID)->delete();
							$country->delete();
						}
					}
					if(count($mod->decisions) > 0){
						foreach($mod->decisions as $decision){
							$decisionID = $decision->id;
							\DB::table('decisions')->where('decision_category_id', '=', $decisionID)->delete();
							$decision->delete();
						}
					}
					if(count($mod->start_dates) > 0){
						foreach($mod->start_dates as $start_date){
							$start_date->delete();
						}
					}
					if(count($mod->ideologies) > 0){
						foreach($mod->ideologies as $ideology){
							$ideology->delete();
						}
					}
				}
			}
		}

		if(count($user->forum_posts) > 0){
			\DB::table('forum_posts')->where('user_id', '=', $id)->update(['user_id' => '0']);
		}

		if(count($user->forum_comments) > 0){
			\DB::table('forum_comments')->where('user_id', '=', $id)->update(['user_id' => '0']);
		}

		if($allModsDeleted){
			if(count($user->media) > 0){
				$media = new Media();
				foreach($user->media as $item){
					$media->deleteImage($item->id, false);
				}
			}
		}

		$user = User::find($id);

	    Auth::logout();

	    if ($user->delete()) {

	         return redirect('/')->with(['success', 'Your account has been deleted!']);
	    }
		
	}
}
