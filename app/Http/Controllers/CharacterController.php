<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Character;

class CharacterController extends Controller
{
    public function addOrUpdate(Request $request){
    	if($request->id == 'new'){
    		$character = new Character();
    		$character->character_list_id = $request->listID;
    	}else{
    		$character = Character::findOrFail($request->id);
    	}

    	$data = json_decode(json_encode(['name' => $request->name, 'gfx' => $request->gfx, 'type' => $request->type]));

    	$character->data = $data;
    	$character->save();

    	return $character;
    }

    public function remove(Request $request){
    	$character = Character::findOrFail($request->id);
    	$character->delete();
    }
}
