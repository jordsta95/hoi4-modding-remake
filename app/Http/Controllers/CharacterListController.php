<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Storage;
use App\Models\CharacterList;
use App\Models\Media as MediaModel;

use App\Http\Contracts\Media;

class CharacterListController extends Controller
{
	private $characterNameList = [];
	private $characterList;
	private $characterMedia;
	private $exportDirectory;

	public function list(){
		if(!Auth::check()){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}
		return view('tools/character.list', ['mods' => Auth::user()->mods]);
	}

	public function getLists(){
		if(Auth::check()){
			$user = Auth::user();
			return $user->mods()->with('characters')->get();
		}
	}

	public function createList(Request $request){
		$list = new CharacterList();
		$list->name = $request->name;
		$list->mod_id = $request->mod_id;
		$list->save();

		return redirect(route('viewCharacterList', $list->id));
	}

	public function view($id){
		$list = CharacterList::findOrFail($id);
		return view('tools/character.view', ['list' => $list]);
	}

	public function getList($id){
		return CharacterList::findOrFail($id)->characters;
	}

	public function checkIfListIsEditable($id){
		$editable = false;
		$userID = 0;
		if(Auth::check()){
			$userID = Auth::user()->id;
			$list = CharacterList::findOrFail($id);
			$editable = $list->mod->userHasAccess($userID);
		}
		return json_decode(json_encode(['editable' => $editable, 'user_id' => $userID]));
	}

	public function getListsInMod($mod){
		$lists = CharacterList::where('mod_id', '=', $mod)->with('characters')->get();
		return $lists;
	}

	public function deleteList(Request $request, $id){
		if(!Auth::check()){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}

		$canEdit = false;
		$list = CharacterList::find($id);

		foreach(Auth::user()->mods as $mod){
			if($mod->id == $list->mod_id && $mod->pivot->administrator){
				$canEdit = true;
			}
		}
		if(Auth::user()->role_id == 2){
			$canEdit = true;
		}
		if(!$canEdit){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}

		foreach($list->characters as $char){
			$char->delete();
		}
		$list->delete();

		return redirect()->back()->with('success', "Character list successfully deleted");
	}

	/* 
	Files for mod export
	*/
	public function export($list, $directory){
		$this->characterList = $list;
		$this->exportDirectory = $directory;

		$characterFile = $this->generateCharacterFile();
		$gfxFile = $this->generateGFXFile();
		$localisationFile = $this->generateLocalisationFile();

		$listName = idify($list->name);
		$language = "english";
		if(!empty($list->mod->data)){
			$d = json_decode($list->mod->data);
			if(isset($d->language)){
				$language = $d->language;
			}
		}

		Storage::write($directory.'/common/characters/'.$listName.'.txt', $characterFile);
		Storage::write($directory.'/localisation/'.$language.'/characters_'.$listName.'_l_'.$language.'.yml', $localisationFile);
		Storage::write($directory.'/interface/characters_'.$listName.'.gfx', $gfxFile);
	}

	public function generateCharacterFile(){
		$file = "characters = {\n";

		foreach($this->characterList->characters as $item){
			$char = json_decode(json_encode($item->data));
			$name = $this->getValidCharacterNameID($item);

			//Add character block
			$file .= tabbedLine('#'.$char->name, 1);
			$file .= tabbedLine($name.' = {', 1);
				$file .= tabbedLine('name = '.$name, 2);
				$file .= $this->generateCharacterPortraits($char->type, $name);
				if(isset($char->type->civilian->country_leader->selected)){
					$file .= $this->generateCountryLeader($char->type, $name);
				}
				if(isset($char->type->civilian->advisor->selected)){
					$file .= $this->generateAdvisor($char->type, $name);
				}
				if(isset($char->type->army->selected)){
					$file .= $this->generateArmy($char->type, $name);
				}
				if(isset($char->type->navy->selected)){
					$file .= $this->generateNavy($char->type, $name);
				}

			$file .= tabbedLine('}', 1);
		}

		$file .= "}";

		return $file;
	}

	public function generateLocalisationFile(){
		$language = "english";
		$json = json_decode($this->characterList->mod->data);
		if(isset($json->language)){
			$language = $json->language;
		}

		$file = chr(239) . chr(187) . chr(191) ."l_".$language.":\n";

		foreach($this->characterList->characters as $item){
			$name = $this->getValidCharacterNameID($item);
			$file .= tabbedLine($name.':0 "'.$item->data['name'].'"');
		}

		return $file;
	}

	public function generateGFXFile(){
		$folderName = idify($this->characterList->name);
		$directoryPath = 'gfx/characters/'.$folderName;

		$characterGFX = $this->getCharacterGFXMedia();

		$file = tabbedLine('spriteTypes = {');
			foreach($this->characterList->characters as $item){
				$name = $this->getValidCharacterNameID($item);
				$char = json_decode(json_encode($item->data));
				$gfx = $char->gfx;

				$largeFilename = $name.'_large';
				$smallFilename = $name.'_small';

				$file .= tabbedLine('spriteType = {', 1);
					$file .= tabbedLine('name = "GFX_'.$name.'_large"', 2);
					$file .= tabbedLine('texturefile = "'.$directoryPath.'/'.$largeFilename.'.dds"', 2);
				$file .= tabbedLine('}', 1);
				$file .= tabbedLine('spriteType = {', 1);
					$file .= tabbedLine('name = "GFX_'.$name.'_small"', 2);
					$file .= tabbedLine('texturefile = "'.$directoryPath.'/'.$smallFilename.'.dds"', 2);
				$file .= tabbedLine('}', 1);


				if(isset($char->type->civilian->advisor->selected)){
					$file .= tabbedLine('spriteType = {', 1);
						$file .= tabbedLine('name = "GFX_'.$name.'_small"', 2);
						$file .= tabbedLine('texturefile = "'.$directoryPath.'/'.$name.'_advisor.dds"', 2);
					$file .= tabbedLine('}', 1);

					$m = new Media();
					$m->createAdvisorGFXFromPortrait($gfx, $this->exportDirectory.'/'.$directoryPath, $name.'_advisor');
				}


				Media::copyImageToDirectoryWithJSON($characterGFX, $gfx, $this->exportDirectory.'/'.$directoryPath, $largeFilename, 'leader', 'dds');
				Media::copyImageToDirectoryWithJSON($characterGFX, $gfx, $this->exportDirectory.'/'.$directoryPath, $smallFilename, 'idea', 'dds');
			}
		$file .= tabbedLine('}');
		return $file;
	}

	/*
	For individual sections of files, to keep main functions cleaner
	*/
	private function generateCharacterPortraits($char, $name){
		$file = tabbedLine('portraits = {', 2);

			if(isset($char->civilian->selected)){
				if(isset($char->civilian->country_leader->selected) || isset($char->civilian->advisor->selected)){
					$file .= tabbedLine('civilian = {', 3);
						if(isset($char->civilian->country_leader->selected)){
							$file .= tabbedLine('large = GFX_'.$name.'_large', 4);
						}

						if(isset($char->civilian->advisor->selected)){
							$file .= tabbedLine('small = GFX_'.$name.'_advisor', 4);
						}
					$file .= tabbedLine('}', 3);
				}
			}

			if(isset($char->army->selected)){
				$file .= tabbedLine('army = {', 3);
					$file .= tabbedLine('large = GFX_'.$name.'_large', 4);
					$file .= tabbedLine('small = GFX_'.$name.'_small', 4);
				$file .= tabbedLine('}', 3);
			}

			if(isset($char->navy->selected)){
				$file .= tabbedLine('army = {', 3);
					$file .= tabbedLine('large = GFX_'.$name.'_large', 4);
					$file .= tabbedLine('small = GFX_'.$name.'_small', 4);
				$file .= tabbedLine('}', 3);
			}

		$file .= tabbedLine('}', 2);

		return $file;
	}

	private function generateCountryLeader($char, $name){
		$file = tabbedLine('country_leader = {', 2);
			$file .= tabbedLine('ideology = '.$char->civilian->country_leader->ideology, 3);
			$file .= tabbedLine('traits = { '.$char->civilian->country_leader->traits.' }', 3);
			$file .= tabbedLine('expire = "'.$char->civilian->country_leader->expire.'"', 3);
		$file .= tabbedLine('}', 2);
		return $file;
	}

	private function generateAdvisor($char, $name){
		if(isset($char->civilian->advisor->slot)){
			$file = tabbedLine('advisor = {', 2);
				$file .= tabbedLine('slot = '.$char->civilian->advisor->slot, 3);
				$file .= tabbedLine('idea_token = '.$name, 3);
				$file .= tabbedLine('cost = '.$char->civilian->advisor->cost, 3);
				$file .= tabbedLine('traits = { '.$char->civilian->advisor->traits.' }', 3);
				if(!empty($char->civilian->advisor->available)){
					$file .= tabbedLine('available = {', 3);
						$file .= tabbedBlock($char->civilian->advisor->available, 4);
					$file .= tabbedLine('}', 3);
				}
				if(!empty($char->civilian->advisor->allowed)){
					$file .= tabbedLine('allowed = {', 3);
						$file .= tabbedBlock($char->civilian->advisor->allowed, 4);
					$file .= tabbedLine('}', 3);
				}
				if(!empty($char->civilian->advisor->ai_will_do)){
					$file .= tabbedLine('ai_will_do = {', 3);
						$file .= tabbedBlock($char->civilian->advisor->ai_will_do, 4);
					$file .= tabbedLine('}', 3);
				}
			$file .= tabbedLine('}', 2);
		}else{
			$file = tabbedLine('# Error adding '.$name.' - No slot defined');
		}
		return $file;
	}

	private function generateArmy($char, $name){
		$position = 'corps_commander';
		if(isset($char->army->position)){
			$position = $char->army->position;
		}
		$file = tabbedLine($position.' = {', 2);
			$file .= tabbedLine('traits = { '.$char->army->traits.' }', 3);
			foreach($char->army->stats as $name => $value){
				$file .= tabbedLine($name.' = '.$value, 3);
			}
		$file .= tabbedLine('}', 2);
		return $file;
	}

	private function generateNavy($char, $name){
		$file = tabbedLine('navy_leader = {', 2);
			$file .= tabbedLine('traits = { '.$char->navy->traits.' }', 3);
			foreach($char->navy->stats as $name => $value){
				$file .= tabbedLine($name.' = '.$value, 3);
			}
		$file .= tabbedLine('}', 2);
		return $file;
	}

	/* Utility functions */
	private function getValidCharacterNameID($item){
		if(isset($this->characterNameList[$item->id])){
			return $this->characterNameList[$item->id];
		}

		$char = json_decode(json_encode($item->data));
		$prefix = idify($this->characterList->name).'_';

		$name = $prefix.idify($char->name);

		//If name has already been added once, let's add a number to the end to make the ID unique
		if(in_array($name, $this->characterNameList)){
			$num = 1;
			$newName = $name;
			while(in_array($newName, $this->characterNameList)){
				$newName = $name.'_'.$num;
				$num++;
			}
			$name = $newName;
		}

		$this->characterNameList[$item->id] = $name;

		return $name;
	}

	private function getCharacterGFXMedia(){
		if(empty($this->characterMedia)){
			$gfx = $this->characterList->characters()->selectRaw('JSON_EXTRACT(data, "$.gfx") as gfx')->pluck('gfx');
			$ids = explode(',', str_replace('"', '', implode(',', $gfx->toArray())));

			$media = MediaModel::whereIn('id', $ids)->get();
			$characterMedia = [];
			foreach($media as $item){
				$characterMedia[$item->id] = $item;
			}

			$this->characterMedia = json_decode(json_encode(['media' => json_decode(json_encode($characterMedia))]));
		}

		return $this->characterMedia;
	}

	public function getAllCharacterIdsAndNamesForMod($mod){
		foreach($mod->characters as $list){
			$this->characterList = $list;
			foreach($list->characters as $item){
				$this->getValidCharacterNameID($item);
			}
		}
		return $this->characterNameList;
	}

}
