<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Str;
use Storage;

use App\Models\Idea;
use App\Models\IdeaGroup;

class IdeaController extends Controller{
	private $ideaGFXList = null;
	/*
	* Show list of all ideas
	*
	*/
	public function index(){
		if(!Auth::check()){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}
		return view('tools/idea.index', ['mods' => Auth::user()->mods]);
	}

	/*
	* Create new ideas group
	*
	*/
	public function create(Request $request){
		$id = IdeaGroup::insertGetId(
			[
				'mod_id' => $request->mod_id,
				'name' => $request->name,
				'filename' => Str::slug($request->name),
				'language' => $request->lang,
				'prefix' => $request->prefix,
				'created_at' => date('Y-m-d G:i:s'),
				'updated_at' => date('Y-m-d G:i:s')
			]
		);
		return redirect('/ideas/edit/'.$id);
	}

	/*
	* View ideas group
	*
	*/
	public function view($id){
		$idea = IdeaGroup::find($id);
		$can_edit = false;
		if(Auth::user()){
			foreach($idea->mod->users as $user){
				if($user->id == Auth::user()->id){
					$can_edit = true;
				}
			}
			if(Auth::user()->role_id == 2){
				$can_edit = true;
			}
		}

		return view('tools/idea.view', ['ideas' => $idea, 'can_edit' => $can_edit]);
	}

	/*
	* Edit ideas group
	*
	*/
	public function edit($id){
		$idea = IdeaGroup::find($id);
		$can_edit = false;
		if(Auth::user()){
			foreach($idea->mod->users as $user){
				if($user->id == Auth::user()->id){
					$can_edit = true;
				}
			}
			if(Auth::user()->role_id == 2){
				$can_edit = true;
			}
		}
		if(!$can_edit){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}

		return view('tools/idea.edit', ['ideas' => $idea, 'can_edit' => $can_edit, 'is_editable' => true]);
	}

	public function update(Request $request){
		if($request->id == 'new'){
			return $this->addIdea($request);
		}else{
			return $this->editIdea($request);
		}
	}

	/*
	* Add new idea
	*
	*/
	public function addIdea($request){
		$idea = new Idea();
		$idea->idea_group_id = $request->group_id;
		$idea = $this->setIdeaData($idea, $request);
		$idea->save();
		return $idea;
	}

	/*
	* Edit existing idea
	*
	*/
	public function editIdea($request){
		$idea = Idea::find($request->id);
		$idea = $this->setIdeaData($idea, $request);
		$idea->save();
		return $idea;
	}

	/*
	* Update idea's data
	*
	*/
	public function setIdeaData($idea, $request){
		$idea->name = $request->name;
		$idea->group = $request->group;
		$idea->gfx = $request->gfx;

		

		$data = [];
		foreach(json_decode($request->data) as $name => $value){
			if(!empty($value)){
				$data[$name] = $value;
			}
		}

		$idea->options = $data;

		return $idea;
	}

	/*
	* Delete idea
	*
	*/
	public function delete(Request $request){
		$idea = Idea::find($request->id);
		$can_edit = false;
		if(Auth::user()){
			foreach($idea->ideaGroup->mod->users as $user){
				if($user->id == Auth::user()->id){
					$can_edit = true;
				}
			}
			if(Auth::user()->role_id == 1){
				$can_edit = true;
			}
		}

		if($can_edit){
			Idea::find($request->id)->delete();
		}

	}

	public function deleteGroup(Request $request, $id){
		$group = IdeaGroup::find($id);
		$can_edit = false;
		if(Auth::user()){
			foreach($group->mod->users as $user){
				if($user->id == Auth::user()->id){
					$can_edit = true;
				}
			}
			if(Auth::user()->role_id == 1){
				$can_edit = true;
			}
		}

		if($can_edit){
			foreach($group->ideas as $idea){
				$idea->delete();
			}
			$name = $group->name;
			$group->delete();
			return redirect()->back()->with('success', translate('tools/mod/view.delete_ideas_group', ['name' => $name]));
		}

	}

	public function updateMedia(Request $request){
		$idea = Idea::findOrFail($request->id);
		$idea->gfx = $request->media_id;
		$idea->save();
	}

	public function updateGroup(Request $request, $id){
		$group = IdeaGroup::findOrFail($id);
		$group->prefix = $request->prefix;
		$group->name = $request->name;
		$group->filename = Str::slug($request->name);
		$group->language = $request->lang;
		$group->save();
		return redirect()->back()->with('success', translate('tools/mod/view.updated_ideas_group', ['name' => $group->name]));
	}

	public function import(Request $request){
		$json = json_decode($request->import);

		foreach($json as $item){
			if(isset($item->picture)){
				$item->gfx = $this->getIdeaGFX($item->picture);
				unset($item->picture);
			}else{
				$item->gfx = 1;
			}
			foreach($item->options as $opt => $val){
				$v = rtrim(ltrim($val, '{'), '}');
				$v = trim($v);
				$item->options->{$opt} = $v;
			}

			$idea = new Idea();
			$idea->idea_group_id = $request->id;
			foreach($item as $name => $value){
				$idea->{$name} = $value;
			}
			$idea->save();

		}
		return true;
	}

	public function list($id){
		return Idea::where('idea_group_id', '=', $id)->get();
	}

	public function getForMod($id){
		return IdeaGroup::where('mod_id', $id)->with('ideas')->get();
	}

	/*
	* Convert game GFX name to internal file ID - If file does not exist, set ID to 1
	*
	*/
	public function getIdeaGFX($gfx){
		if(empty($this->ideaGFXList)){
			$json = [];

			//Get media route list
			$mediaFile = Storage::get('media-routes.json');
			$mediaFile = json_decode($mediaFile);
			//Get goals JSON file
			$gfxFile = Storage::get('hoi4-files/interface/ideas.gfx');
			$gfxFile = json_decode($gfxFile);

			$gfxList = [];

			foreach($gfxFile->spriteTypes->SpriteType as $file){
				$textureFile = str_replace(['\"', 'gfx/interface/ideas/', '.dds', '"'], '', $file->texturefile);
				$name = str_replace(['\"', '"'], '', $file->name);
				$gfxList[$textureFile] = $name;
			}

			foreach($mediaFile->media as $id => $media){
				if(isset($gfxList[$media->filename])){
					$json[$gfxList[$media->filename]] = $id; 
				}
			}

			$json = json_encode($json);

			$this->ideaGFXList = $json;
		}

		$json = json_decode($this->ideaGFXList);
		if(isset($json->$gfx)){
			return $json->$gfx;
		}

		return '445';
	}

	/*
	Utility functions
	*/

	private $ideaList = [];
	private $prefix;
	public function getAllIdeasAndNamesForMod($mod){
		foreach($mod->ideas as $list){
			$this->prefix = $list->prefix;
			foreach($list->ideas as $idea){
				$this->getValidIdeaNameID($idea);
			}
		}
		return $this->ideaList;
	}

	private function getValidIdeaNameID($item){
		if(isset($this->ideaList[$item->id])){
			return $this->ideaList[$item->id];
		}

		$prefix = !empty($this->prefix) ? $this->prefix.'_' : '';

		$name = $prefix.idify($item->name);

		//If name has already been added once, let's add a number to the end to make the ID unique
		if(in_array($name, $this->ideaList)){
			$num = 1;
			$newName = $name;
			while(in_array($newName, $this->ideaList)){
				$newName = $name.'_'.$num;
				$num++;
			}
			$name = $newName;
		}

		$this->ideaList[$item->id] = $name;

		return $name;
	}
}
