<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Storage;

use App\Models\StartDate;

class StartDateController extends Controller{

	public function index(){
		if(!Auth::check()){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}
		return view('tools/start-date.index', ['mods' => Auth::user()->mods]);
	}

	public function create(Request $request){
		if(!Auth::check()){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}

		$date = new StartDate();
		$date->name = $request->name;
		$date->language = $request->language;
		$date->date = $request->date;
		$date->mod_id = $request->mod_id;
		$date->save();

		return redirect('/start-date/edit/'.$date->id);
	}

	public function edit($id){
		if(!Auth::check()){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}
		$date = StartDate::findOrFail($id);
		$canEdit = false;
		foreach(Auth::user()->mods as $mod){
			if($mod->id == $date->mod_id){
				$canEdit = true;
			}
		}
		if(Auth::user()->role_id == 2){
			$canEdit = true;
		}

		if(!$canEdit){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}
		return view('tools/start-date/edit', ['date' => $date]);
	}

	public function updateMedia(Request $request){
		if(!Auth::check()){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}
		$date = StartDate::findOrFail($request->id);
		$date->gfx = $request->media_id;
		$date->save();
		return $date;
	}

	public function update(Request $request){
		if(!Auth::check()){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}
		$data = json_decode($request->data);

		$date = StartDate::findOrFail($data->id);
		$ignore = ['id', 'mod_id', 'updated_at', 'created_at'];
		foreach($data as $key => $item){
			if(!in_array($key, $ignore)){
				$date->{$key} = $item;
			}
		}
		$date->save();
	}

	public function import(Request $request){
		if(!Auth::check()){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}
		$data = json_decode($request->import);

		$date = StartDate::findOrFail($request->id);
		$ignore = ['id', 'mod_id', 'updated_at', 'created_at'];
		foreach($data as $key => $item){
			if(!in_array($key, $ignore)){
				$date->{$key} = $item;
			}
		}
		$date->save();
	}

	public function delete(Request $request, $id){
		if(!Auth::check()){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}
		$date = StartDate::findOrFail($id);
		$name = $date->name;
		$date->delete();
		return redirect()->back()->with('success', translate('tools/mod/view.delete_single', ['name' => $name]));
	}
}
