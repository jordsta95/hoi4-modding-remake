<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Models\ForumCategory;
use App\Models\Forum;
use App\Models\ForumPost;
use App\Models\ForumComment;
use App\Models\Media;

use Auth;
use Imagick;
use Storage;
use Mail;

use App\Notifications\ForumPostUpdated;
use App\Http\Contracts\Media as MediaContract;


class ForumController extends Controller{
	private $postsPerPage = 10;
	public function index(){
		if(isset($_GET['markAllNotificationsAsRead'])){
			if(Auth::check()){
				foreach(Auth::user()->unreadNotifications as $n){
					$n->delete();
				}
			}
		}
		if(isset($_GET['search'])){
			$posts = ForumPost::where('title', 'LIKE', '%'.$_GET['search'].'%')->orWhereHas('comments', function ($query) { 
		        $query->where('content', 'LIKE', '%'.$_GET['search'].'%');
		    })->orderBy('updated_at', 'DESC')->paginate(10);

		    return view('forum.search', ['posts' => $posts]);
		}
		return view('forum.index', ['categories' => ForumCategory::orderBy('order')->get()]);
	}

	public function category($group){
		$forum = Forum::where('slug', '=', $group)->limit(1)->get();
		return view('forum.category', ['forum' => $forum[0], 'posts' => ForumPost::where('forum_id', '=', $forum[0]->id)->paginate($this->postsPerPage)]);
	}

	public function create(Request $request){
		$slug = Str::slug($request->title);
		$id = ForumPost::insertGetId([
			'title' => $request->title,
			'slug' => $slug,
			'forum_id' => $request->forum,
			'user_id' => Auth::user()->id,
			'public' => 1,
			'open' => 1,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s'),
		]);

		$comment = $this->addComment($id, $request);

		$forum = Forum::where('id', '=', $request->forum)->limit(1)->get();

		Mail::send('emails.forum.create', ['user' => Auth::user(), 'link' => env('APP_URL').'/forum/'.$forum[0]->slug.'/'.$id.'-'.$slug, 'content' => $request->text], function ($m) use ($request, $forum) {
				$m->to('i-am@jordsta95.co.uk')->subject('[HOI4 Modding]<'.$forum[0]->slug.'>: '.$request->title);
			});

		return redirect('/forum/'.$forum[0]->slug.'/'.$id.'-'.$slug);
	}

	public function respond(Request $request, $cat, $id, $slug){
		$comment = $this->addComment($id, $request);

		$commentCheck = ForumComment::where('forum_post_id','=',$id)->paginate($this->postsPerPage);
		if($commentCheck->lastPage() >= 2){
			$urlAppend = '?page='.$commentCheck->lastPage().'#'.$comment->id;
		}else{
			$urlAppend = '#'.$comment->id;
		}

		$link = '/forum/'.$cat.'/'.$id.'-'.$slug.$urlAppend;

		$post = ForumPost::find($id);
		$post->updated_at = date('Y-m-d H:i:s');
		$post->save();
		
		$users = $post->getUsers();

		foreach($users as $u){
			//Don't need to update the person who comments
			if($u->id != Auth::user()->id){
				$u->notify(new ForumPostUpdated(Auth::user(), $comment, $post, $link));
			}
		}

		return redirect($link);
	}

	public function addComment($parentID, $request){
		$comment = ForumComment::create([
			'forum_post_id' => $parentID,
			'user_id' => Auth::user()->id,
			'content' => $request->text,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s'),
		]);

		return $comment;
	}

	public function bugReport(Request $request){
		$path = '/bug-reports/'.Auth::user()->id;
		$filename = 'bug-'.date('YmdHis');
		$extension = 'jpeg';

		$body = str_replace("\n", '<br>', $request->description) . '<hr>';

		$im = new Imagick();
		$img = str_replace('data:image/png;base64,', '', $request->bug_image);
		$filesize = strlen($img);
		$im->readImageBlob(base64_decode($img));
		$im->setImageFormat('jpeg');
		$im->setImageCompressionQuality(90);
		$img = $im->getImageBlob();
		Storage::write($path.'/'.$filename.'.jpeg', $img);

		$media = Media::create([
			'filename' => $filename,
			'path' => $path,
			'extension' => $extension,
			'size' => $filesize,
			'user_id' => Auth::user()->id,
			'image_for_tools' => 0,
			'tool' => '',
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s'),
		]);

		$body .= '<p><a href="'.$request->url.'" target="_blank">'.$request->url.'</a></p><p>&nbsp;</p>';

		$body .= '<a href="/media/find/'.$media->id.'" target="_blank" class="forum-bug-image"><img src="/media/find/'.$media->id.'/preview"></a>';
		$contract = new MediaContract();
		$contract->setUser();

		if(!empty($request->file('files'))){
			foreach($request->file('files') as $index => $file){
				$media = $contract->handleUpload($file, ['tool' => '', 'name' => $filename.'-extra-'.$index, 'path' => $path], false);

				$body .= '<a href="/media/find/'.$media->id.'" target="_blank" class="forum-bug-image"><img src="/media/find/'.$media->id.'/preview"></a>';
			}
		}

		$slug = Str::slug($request->title);
		$id = ForumPost::insertGetId([
			'title' => $request->title,
			'slug' => $slug,
			'forum_id' => '1',
			'user_id' => Auth::user()->id,
			'public' => 1,
			'open' => 1,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s'),
		]);

		$comment = ForumComment::create([
			'forum_post_id' => $id,
			'user_id' => Auth::user()->id,
			'content' => $body,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s'),
		]);


		return redirect('/forum/bug-reports/'.$id.'-'.$slug);
	}

	public function editComment(Request $request, $commentID){
		$comment = ForumComment::find($commentID);
		$comment->content = $request->text;
		$comment->updated_at = date('Y-m-d H:i:s');
		$comment->save();

		$post = ForumPost::find($comment->forum_post_id);
		$post->updated_at = date('Y-m-d H:i:s');
		$post->save();

		return redirect()->back()->with('success', translate('forum.post_updated'));
	}

	//Only care about the id, the rest is superfulous
	public function post($group, $id, $slug){
		if(isset($_GET['notification'])){
			foreach(Auth::user()->unreadNotifications as $n){
				if($n->id == $_GET['notification']){
					$n->delete();
				}
			}
		}

		return view('forum.post', ['post' => ForumPost::find($id)]);
	}
}
