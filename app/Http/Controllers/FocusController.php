<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Focus;

class FocusController extends Controller{

	public function update(Request $request){
		if($request->id == 'new'){
			$focus = new Focus();
		}else{
			$focus = Focus::findOrFail($request->id);
		}
		foreach($request->all() as $name => $value){
			if(!in_array($name, ['_token', 'id'])){
				$focus->{$name} = $value;
			}
		}
		$focus->save();
		return $focus;
	}

	public function delete(Request $request){
		$focus = Focus::findOrFail($request->id);
		$focus->delete();
	}

	public function updateMedia(Request $request){
		$focus = Focus::findOrFail($request->id);
		$focus->gfx = $request->media_id;
		$focus->save();
	}

	public function updatePosition(Request $request){
		$focuses = [];
		foreach(json_decode($request->update) as $item){
			$focuses[$item->id] = ['x' => $item->x, 'y' => $item->y];
		}
		$get = Focus::whereIn('id', array_keys($focuses))->get();
		foreach($get as $focus){
			$focus->x = $focuses[$focus->id]['x'];
			$focus->y = $focuses[$focus->id]['y'];
			$focus->save();
		}
	}
}
