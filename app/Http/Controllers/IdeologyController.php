<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Storage;

use App\Models\Ideology;
use App\Models\Mod;

class IdeologyController extends Controller{
	public function index(){
		if(!Auth::check()){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}
		return view('tools/ideology.index', ['mods' => Auth::user()->mods]);
	}

	public function create(Request $request){
		if(!Auth::check()){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}
		$ideology = new Ideology();
		$ideology->mod_id = $request->mod_id;
		$ideology->name = $request->name;
		$ideology->language = $request->language;
		$ideology->data = json_decode(json_encode(['colour' => ['r' => rand(0,255), 'g' => rand(0,255),'b' => rand(0,255)], 'factions' => []]));
		$ideology->rules = json_decode('{}');
		$ideology->modifiers = json_decode('{}');
		$ideology->save();
		return redirect('/ideology/edit/'.$ideology->id);
	}

	public function edit($id){
		if(!Auth::check()){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}
		$ideology = Ideology::findOrFail($id);
		if(Auth::user()){
			foreach($ideology->mod->users as $user){
				if($user->id == Auth::user()->id){
					$can_edit = true;
				}
			}
			if(Auth::user()->role_id == 2){
				$can_edit = true;
			}
		}
		if(!$can_edit){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}

		return view('tools/ideology.edit', ['ideology' => $ideology]);
	}

	public function update(Request $request){
		if(!Auth::check()){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}
		$json = json_decode($request->data);
		$ideology = Ideology::findOrFail($json->id);

		if(Auth::user()){
			foreach($ideology->mod->users as $user){
				if($user->id == Auth::user()->id){
					$can_edit = true;
				}
			}
			if(Auth::user()->role_id == 2){
				$can_edit = true;
			}
		}
		if(!$can_edit){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}
		$updatable = ['name', 'types', 'rules', 'modifiers', 'data'];
		foreach($updatable as $u){
			$ideology->{$u} = $json->{$u};
		}
		$ideology->save();
	}

	public function import(Request $request){
		if(!Auth::check()){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}
		$ideology = Ideology::findOrFail($request->id);
		if(Auth::user()){
			foreach($ideology->mod->users as $user){
				if($user->id == Auth::user()->id){
					$can_edit = true;
				}
			}
			if(Auth::user()->role_id == 2){
				$can_edit = true;
			}
		}
		if(!$can_edit){
			return;
		}
		$json = json_decode($request->import);
		$updatable = ['name', 'types', 'rules', 'modifiers', 'data'];
		foreach($updatable as $u){
			$ideology->{$u} = $json->{$u};
		}
		$ideology->save();
	}

	public function delete(Request $request, $id){
		if(!Auth::check()){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}
		$ideology = Ideology::findOrFail($id);
		if(Auth::user()){
			foreach($ideology->mod->users as $user){
				if($user->id == Auth::user()->id){
					$can_edit = true;
				}
			}
			if(Auth::user()->role_id == 2){
				$can_edit = true;
			}
		}
		if(!$can_edit){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}
		$name = $ideology->name;
		$ideology->delete();
		return redirect()->back()->with('success', translate('tools/mod/view.delete_single', ['name' => $name]));
	}

	public function getIdeologyList($id){
		$mod = Mod::find($id);
		if(isset($_GET['parent'])){
			$array = [
				[
					'value' => 'democratic',
					'label' => 'Democratic',
				],
				[
					'value' => 'fascism',
					'label' => 'Fascism',
				],
				[
					'value' => 'communism',
					'label' => 'Communism',
				],
				[
					'value' => 'neutrality',
					'label' => 'Non-Aligned'
				]
			];
			$ideologies = json_decode(json_encode($array));
		}else{
			$ideologies = getIdeologyList();
		}

		if($mod->ideologies->count() > 0){
			if(isset(json_decode($mod->data)->ideology_overhaul)){
				$ideologies = [];
			}

			foreach($mod->ideologies as $i){
				if(isset($_GET['parent'])){
					$json = json_decode(json_encode(['value' => idify($i->name), 'label' => $i->name]));
					$ideologies[] = $json;
				}else{
					foreach($i->types as $sub){
						$name = idify($sub['name']);
						$json = json_decode(json_encode(['value' => $name, 'label' => $sub['name']]));
						$ideologies[] = $json;
					}
				}
			}
		}

		return $ideologies;
	}
}
