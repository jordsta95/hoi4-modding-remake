<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

use Auth;
use ZipArchive;
use Imagick;
use Storage;
use Throwable;

use App\Models\Mod;
use App\Models\UserMod;
use App\Http\Contracts\Media;

/*
Controllers for each tool; for new export functionality
*/
use App\Http\Controllers\CharacterListController;
use App\Http\Controllers\CountryController;
use App\Http\Controllers\FocusTreeController;

class ModController extends Controller{
	private $modDirectory = '';
	private $focusGFX = [];
	private $focusTreeFocuses = [];
	private $stateFiles = [];
	private $editedStateFiles = [];
	private $errors = [];
	private $mediaJSON;

	public function __construct(){
	}

	public function setMediaJSON($id){
		$this->mediaJSON = Media::getMediaJSON($id);
	}


	/*
	* Show a list of all mods that the user has
	*
	*/
	public function index(){
		$mods = [];
		if(Auth::check()){
			$mods = Auth::user()->mods;
		}
		return view('tools/mod.index', ['mods' => $mods]);
	}

	public function getModsList(){
		if(Auth::check()){
			return Auth::user()->mods;
		}
	}

	/*
	* Create a new mod and assign current user as an administrator
	*
	*/
	public function create(Request $request){
		if(!Auth::check()){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}

		$media = 1;
		if(isset($request->mod_image) && !empty($request->mod_image)){

			$media = new Media();
			$media->setUser(Auth::user()->id);
			$options = [];
			$options['path'] = '/user/'.Auth::user()->id.'/mod/'.Str::slug($request->name).'/'.date('Y-m-d');
			$upload = $media->uploadImage($request->mod_image, $options, false);
			$media = $upload->id;
		}


		$mod = Mod::create([
			'name' => $request->name,
			'description' => $request->description,
			'media_id' => $media,
			'data' => json_encode($request->data)
		]);

		UserMod::create([
			'user_id' => Auth::user()->id,
			'mod_id' => $mod->id,
			'role' => translate('tools/mod/index.creator'),
			'administrator' => 1
		]);

		return redirect()->back()->with('success', translate('tools/mod/index.created'));
	}

	public function removeFromTeam(Request $request, $modID, $userID){
		$mod = Mod::find($modID);
		$isAdmin = false;

		foreach($mod->roles as $role){
			if($role->user_id == $userID){
				$isAdmin = $role->administrator;
			}
		}

		foreach($mod->roles as $role){
			if($role->user_id == $userID){
				$role->delete();
			}else{
				if($isAdmin){
					$role->administrator = 1;
					$role->save();
				}
			}
		}

		$msg = 'User successfully deleted from mod.';
		if($isAdmin){
			$msg .= ' Remaining user(s) have been given ability to add/remove team members.';
		}

		return redirect()->back()->with(['success' => $msg]);
	}

	/*
	* Delete mod
	*
	*/
	public function delete(Request $request, $id){
		$mod = Mod::find($id);
		if(!Auth::check()){
			return redirect('/')->with('error', translate('auth.unauthorised'));
		}

		$canEdit = false;
		foreach(Auth::user()->mods as $m){
			if($m->id == $id){
				$canEdit = true;
			}
		}
		if(Auth::user()->role_id == 2){
			$canEdit = true;
		}

		if(!$canEdit){
			return redirect('/')->with('error', translate('auth.unauthorised'));
		}

		$mod->delete();

		return redirect()->back()->with('success', translate('tools/mod/index.deleted'));
	}

	/*
	* View a specific mod
	*
	*/
	public function view($id){
		$mod = Mod::find($id);
		if(empty($mod)){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}

		return view('tools/mod.view', ['mod' => $mod]);
	}

	/*
	* Update a specific mod
	*
	*/
	public function update(Request $request, $id){
		$mod = Mod::find($id);
		if(empty($mod)){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}

		if(!Auth::check()){
			return redirect('/')->with('error', translate('auth.unauthorised'));
		}

		$canEdit = false;
		foreach(Auth::user()->mods as $m){
			if($m->id == $id){
				$canEdit = true;
			}
		}
		if(Auth::user()->role_id == 2){
			$canEdit = true;
		}

		if(!$canEdit){
			return redirect('/')->with('error', translate('auth.unauthorised'));
		}

		$mod->name = $request->name;
		$mod->description = $request->description;
		$data = [];

		if(isset($request->data)){
			foreach($request->data as $name => $value){
				if(!empty($value)){
					$data[$name] = $value;
				}
			}
		}
		$mod->data = json_encode($data);
		if(isset($request->mod_image) && !empty($request->mod_image)){
			$media = new Media();
			$media->setUser(Auth::user()->id);
			$options = [];
			$options['path'] = '/user/'.Auth::user()->id.'/mod/'.Str::slug($request->name).'/'.date('Y-m-d');
			$upload = $media->uploadImage($request->mod_image, $options, false);
			$media = $upload->id;

			$mod->media_id = $media;
		}
		$mod->save();

		return redirect()->back()->with('success', translate('tools/mod/view.updated_single', ['name' => $mod->name]));
	}


	protected $idArrays = [];

	/*
	* Export a specific mod
	*
	*/
	public function export($id){
		ini_set('max_execution_time', '120');
		ini_set("memory_limit","512M");

		 if (Auth::check()) {
        	$this->setMediaJSON(Auth::user()->id); // you can access user id here
        }else{
        	$this->setMediaJSON(0);
        }

		$export = false;
		$mod = Mod::find($id);
		
		if(empty($mod)){
			return redirect()->back()->with('error', translate('auth.unauthorised'));
		}

		$this->modDirectory = 'exports/mod/'.$id.'/'.date('Ymdhis');
		if($mod->focus_trees->count() > 0){
			$this->setFocusGFX();
		}

		//Generate focus tree info start
		foreach($mod->focus_trees as $tree){
			if(env('APP_ENV') == "local"){
				$tree = new FocusTreeController();
				$tree->exportFiles($this->modDirectory, $tree);
			}else{
				//File return ['tree' => '', 'localisation' => '', 'customgfx' => '']
				try{
					$file = $this->generateFocusFile($tree);
					Storage::write($this->modDirectory.'/common/national_focus/'.$tree->tree_id.'.txt', $file['tree']);
					Storage::write($this->modDirectory.'/localisation/focustree_'.$tree->tree_id.'_l_'.$tree->lang.'.yml', $file['lang']);
					if(!empty($file['customgfx'])){
						Storage::write($this->modDirectory.'/interface/focus-'.$tree->tag.'_customicons.gfx', $file['customgfx']);
					}
					$export = true;
				}
				catch (Throwable $t){
					$this->errors[] = translate('tools/mod/index.export.error.focus_tree', ['tag' => $tree->tag]);
				}
			}
		}
		//Generate focus tree end

		//Generate ideas start
		if($mod->ideas->count() > 0){
			try{
				$this->generateIdeasFiles($mod);
				$export = true;
			}
			catch (Throwable $t){
				$this->errors[] = translate('tools/mod/index.export.error.ideas');
			}
		}
		//generate ideas end

		//Generate events start
		if($mod->events->count() > 0){
			try{
				$this->generateEventsFiles($mod);
				$export = true;
			}
			catch (Throwable $t){
				$this->errors[] = translate('tools/mod/index.export.error.events');
			}
		}
		//generate events end

		//Generate Start Dates start
		if($mod->startDates->count() > 0){
			try{
				$this->generateBookmarkFiles($mod);
				$export = true;
			}
			catch (Throwable $t){
				$this->errors[] = translate('tools/mod/index.export.error.start_date');
			}
		}
		//generate Start Dates end

		//Generate ideologies start
		if($mod->ideologies->count() > 0){
			try{
				$this->generateIdeologiesFiles($mod);
				$export = true;
			}
			catch (Throwable $t){
				$this->errors[] = translate('tools/mod/index.export.error.ideology');
			}
		}
		//generate ideologies end

		//Generate ideologies start
		if($mod->decisions->count() > 0){
			try{
				$this->generateDecisionFiles($mod);
				$export = true;
			}
			catch (Throwable $t){
				$this->errors[] = translate('tools/mod/index.export.error.decision');
			}
		}
		//generate ideologies end

		if($mod->countries->count() > 0){
			$export = true;
			$coun = new CountryController();
			$coun->prepareExportFiles($mod);
			foreach($mod->countries as $country){
				//try{
					$coun->export($country, $this->modDirectory);
				// }
				// catch (Throwable $t){
				// 	$this->errors[] = translate('tools/mod/index.export.error.country', ['tag' => $country->tag]);
				// }
			}
			$coun->writeExportFiles();
		}

		/*
		$json = json_decode($mod->data);
		$override = false;
		if($json){
			if(isset($json->country_overhaul) && !empty($json->country_overhaul)){
				// Storage::write($this->modDirectory.'/common/country_tags/00_countries.txt', '');
				// Storage::write($this->modDirectory.'/common/country_tags/01_countries.txt', '');
				$this->removeDefaultCountriesFromFiles();
			}
		}
		

		//Generate countries start
		foreach($mod->countries as $country){
			//File return ['tree' => '', 'localisation' => '', 'customgfx' => '']
			try{
				$localisationFile = chr(239) . chr(187) . chr(191) ."l_".$country->language.":\n";
				$files = $this->generateCountryFile($country, $localisationFile);
				$common = $this->generateCountryCommonFile($country);
				$ideas = $this->generateCountryIdeasFile($country, $files['localisation']);
				$cNameID = $this->nameToId($country->name, 'cExport_'.$country->id, $country->tag);

				Storage::write($this->modDirectory.'/common/countries/'.$country->name.'.txt', $common);
				Storage::write($this->modDirectory.'/common/ideas/'.$cNameID.'_ideas.txt', $ideas['ideas']);
				Storage::write($this->modDirectory.'/history/countries/'.$country->tag.' - '.$country->name.'.txt', $files['history']);
				Storage::write($this->modDirectory.'/localisation/country_'.$cNameID.'_l_'.$country->language.'.yml', $ideas['localisation']);
				if(!empty($ideas['customGFX'])){
					Storage::write($this->modDirectory.'/interface/country-'.$cNameID.'_ideas_customicons.gfx', $ideas['customGFX']);
				}

				$this->generateDivisions($country);

				$export = true;
			}
			catch (Throwable $t){
				$this->errors[] = translate('tools/mod/index.export.error.country', ['tag' => $country->tag]);
			}
		}

		
		
		
		//Generate countries end

		

		//Only do this after all countries have been checked over, and the state files edited accordingly
		if(!empty($this->editedStateFiles)){
			try{
				$this->generateEditedStateFiles();
				$export = true;
			}
			catch (Throwable $t){
				$this->errors[] = translate('tools/mod/index.export.error.country_states');
			}
		}

		if($json){
			if(isset($json->country_overhaul) && !empty($json->country_overhaul)){
				// Storage::write($this->modDirectory.'/common/country_tags/00_countries.txt', '');
				// Storage::write($this->modDirectory.'/common/country_tags/01_countries.txt', '');
				$this->removeDefaultCountriesFromFiles();
			}
			if(isset($json->ideology_overhaul) && !empty($json->ideology_overhaul)){
				$this->replaceDefaultIdeologiesInFiles($mod);
			}
		}


		if($mod->countries->count() > 0){
			try{
				$countryTagsFile = '';
				$countryColoursFile = Storage::get('/hoi4-files/common/countries/colors.txt');
				if($json){
					if(isset($json->country_overhaul) && !empty($json->country_overhaul)){
						$countryColoursFile = '';
					}
				}
				$countryColoursFile .= $this->addLine('');
				$countryColoursFile .= $this->addLine('#Custom colours start here');

				foreach($mod->countries as $i => $c){
					$countryTagsFile .= $this->addLine($c->tag.' = "countries/'.$c->name.'.txt"');

					$countryColoursFile .= $this->addLine($c->tag.' = {');
					$countryColoursFile .= $this->addLine('color = rgb { '.$c->colour['r'].' '.$c->colour['g'].' '.$c->colour['b'].' }', 1);
					$countryColoursFile .= $this->addLine('color_ui = rgb { '.$c->colour['r'].' '.$c->colour['g'].' '.$c->colour['b'].' }', 1);
					$countryColoursFile .= $this->addLine('}');
				}
					
				Storage::write($this->modDirectory.'/common/country_tags/'.$mod->id.'_countries.txt', $countryTagsFile);
				Storage::write($this->modDirectory.'/common/countries/colors.txt', $countryColoursFile);
			}
			catch (Throwable $t){
				$this->errors[] = translate('tools/mod/index.export.error.country_colour');
			}
		}
		*/

		if($mod->characters->count() > 0){
			$export = true;
			$char = new CharacterListController();
			foreach($mod->characters as $list){
				$char->export($list, $this->modDirectory);
			}
		}


		if(!$export){
			return redirect()->back()->with('error', translate('tools/mod/view.nothing'));
		}

		if(isset($_GET['test'])){
			die();
		}

		$zip_file_name = preg_replace("/[^A-Za-z0-9 ]/", '', $mod->name).'.zip';
		$directory = storagePath('/'.$this->modDirectory);
		$za = new FlxZipArchive;
		$res = $za->open($zip_file_name, ZipArchive::CREATE);
		if($res === TRUE) {
			$za->addDir($directory, basename($directory));
			$za->close();
		}
		ob_get_clean();
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private", false);
		header("Content-Type: application/zip");
		header("Content-Disposition: attachment; filename=" . basename($zip_file_name) . ";" );
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: " . filesize($zip_file_name));
		setcookie('downloadedMod', true, time() + 100);
		if(!empty($this->errors)){
			//setcookie('modDownloadErrors', json_encode($this->errors), time() + 100);
		}
		readfile($zip_file_name);
		unlink($zip_file_name);
		$delete = Storage::deleteDirectory($this->modDirectory);
		dd($delete);
	}

	/*
	* Add line with necessary number of tabs in front
	*
	*/
	public function addLine($content, $tabs = 0){
		return str_repeat("\t", $tabs).$content."\r\n";
	}

	/*
	* Convert passed string to a unique ID the game can use
	* ---
	* string - The string (usually a title) that needs to be converted to an ID
	* id - A unique internal id to allow the id to be called again if need be
	* tag - To allow different focus trees to have the same game id
	*/
	public function nameToId($string, $id = null, $tag = null){
		$parts = explode(' ', $string);
		$parts[count($parts) - 1] = str_replace('|', 'I', $parts[count($parts) - 1]);
		$string = implode(' ', $parts);
		
		$tl = \Transliteration::clean_filename($string);
		if(str_replace(' ', '_', $string) != $tl){
			$tl = str_replace('_', '', $tl);
		}
		$string = $tl;
		$return = strtolower(preg_replace("/[^A-Za-z0-9_]/", '', str_replace(' ', '_', $string)));
		$returnTag = strtolower(preg_replace("/[^A-Za-z0-9_]/", '', (!empty($tag) ? str_replace(' ', '_', $tag).'_' : '' ).str_replace(' ', '_', $string)));

		if(empty($return) || (in_array($return, $this->idArrays) && empty($tag)) || (in_array($returnTag, $this->idArrays) && !empty($tag))){

			if(!isset($this->idArrays[$id])){
				$randomString = $this->generateRandomString();
				$this->idArrays[$id] = $randomString;
			}

			$return = $this->idArrays[$id];
		}
		$this->idArrays[$id] = $return;
		return $return; 
	}

	public function generateRandomString(){
		$permitted_chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$id = substr(str_shuffle($permitted_chars), 0, 8);
		if(in_array($id, $this->idArrays)){
			$id = $this->generateRandomString();
		}else{
			return $id;
		}

	}

	/*
	* Generate array of GFX used in base game focus IDs
	* 
	*/
	public function setFocusGFX(){
		$media = json_decode(getMedia('focus'));
		$file = json_decode(Storage::get('hoi4-files/interface/goals.gfx'));
		$output = [];
		foreach($media->default as $m){
			foreach($file->spriteTypes->SpriteType as $icon){
				if('"GFX_'.$m->filename.'"' == $icon->name){
					$output[$m->id] = $icon->name;
				}
			}
		}

		$this->focusGFX = $output;
	}


	/*
	* Generate file for focus tree passed
	* =
	* tree - Focus Tree
	*/
	public function generateFocusFile($tree){
		$tag = $tree->tag;

		$customGfx = "spriteTypes = {";

		$lang = chr(239) . chr(187) . chr(191) .'l_'.$tree->lang.": \r\n";

		$focusTree = $this->addLine('focus_tree = {');
		$continuousX = -40 + ($tree->x * 190);
		$continuousY = 10 + ($tree->y * 130);

			if(strtolower($tree->tree_id) == 'generic'){
				$focusTree .= $this->addLine("id = generic_focus", 1);
				$focusTree .= $this->addLine("country = {", 1);
					$focusTree .= $this->addLine("factor = 1", 2);
				$focusTree .= $this->addLine("}", 1);
				$focusTree .= $this->addLine("default = yes", 1);
				$focusTree .= $this->addLine("continuous_focus_position = { x = ".$continuousX." y = ".$continuousY." } ", 1);
			}else{
				$focusTree .= $this->addLine("id = ".$tree->tree_id, 1);
				
				$focusTree .= $this->addLine("continuous_focus_position = { x = ".$continuousX." y = ".$continuousY." } ", 1);
				$focusTree .= $this->addLine("country = {", 1);
					$focusTree .= $this->addLine("factor = 0", 2);
					$focusTree .= $this->addLine("modifier = {", 2);
						$focusTree .= $this->addLine("add = 10", 3);
						$focusTree .= $this->addLine("tag = ".$tree->tag, 3);
					$focusTree .= $this->addLine("}", 2);
				$focusTree .= $this->addLine("}", 1);
			}
			
		$mediaJSON = $this->mediaJSON;

		//Loop over focuses
		foreach($tree->focuses as $focus){
			$this->focusTreeFocuses[$focus->id] = $tag.'_'.$this->nameToId($focus->name, 'f_'.$focus->id, $tag);
		}
		foreach($tree->focuses as $focus){
			$focusID = $tag.'_'.$this->nameToId($focus->name, 'f_'.$focus->id, $tag);
			$fortree = '';

			//Add focus data to lang file
			$lang .= $focusID.':0 "'.$focus->name.'"'."\r\n";
			$lang .= $focusID.'_desc:0 "'.str_replace("\n", '\n', $focus->description).'"'."\r\n";

			$icon = '';
			if(isset($this->focusGFX[$focus->gfx])){
				$icon = $this->focusGFX[$focus->gfx];
			}else{
				$fileName = "GFX_".$focusID.'-'.$focus->gfx;
				$icon = $fileName;
				if(isset($mediaJSON->media->{$focus->gfx})){
					$mJ = $mediaJSON->media->{$focus->gfx};
				}else{
					$mJ = $mediaJSON->media->{'1'};
				}
				Media::copyImageToDirectoryWithJSON($this->mediaJSON, $focus->gfx, $this->modDirectory.'/gfx/interface/goals/', $fileName, 'focus_', 'dds', $mJ);
				$customGfx .= $this->addLine('##Icon For: '.$focus->focus_title, 1);
				$customGfx .= $this->addLine('SpriteType = { ', 1);
				$customGfx .= $this->addLine('name = "'.$fileName.'"', 2);
				$customGfx .= $this->addLine('texturefile = "gfx/interface/goals/'.$fileName.'.dds"', 2);
				$customGfx .= $this->addLine('}', 1);
				$customGfx .= $this->addLine('');
				$customGfx .= $this->addLine('##Icon Shine For: '.$focus->focus_title, 1);
				$customGfx .= $this->addLine('SpriteType = { ', 1);
				$customGfx .= $this->addLine('name = "'.$fileName.'_shine"', 2);
				$customGfx .= $this->addLine('texturefile = "gfx/interface/goals/'.$fileName.'.dds"', 2);
				$customGfx .= $this->addLine('effectFile = "gfx/FX/buttonstate.lua"', 2);
				//Animation 1
				$customGfx .= $this->addLine('animation = {', 2);
				$customGfx .= $this->addLine('animationmaskfile = "gfx/interface/goals/'.$fileName.'.dds"', 3);
				$customGfx .= $this->addLine('animationtexturefile = "gfx/interface/goals/shine_overlay.dds"', 3);
				$customGfx .= $this->addLine('animationrotation = -90.0', 3);
				$customGfx .= $this->addLine('animationlooping = no', 3);
				$customGfx .= $this->addLine('animationtime = 0.75', 3);
				$customGfx .= $this->addLine('animationdelay = 0', 3);
				$customGfx .= $this->addLine('animationblendmode = "add"', 3);
				$customGfx .= $this->addLine('animationtype = "scrolling"', 3);
				$customGfx .= $this->addLine('animationrotationoffset = { x = 0.0 y = 0.0 }', 3);
				$customGfx .= $this->addLine('animationtexturescale = { x = 1.0 y = 1.0 } ', 3);
				$customGfx .= $this->addLine('}', 2);
				//Animation 2
				$customGfx .= $this->addLine('animation = {', 2);
				$customGfx .= $this->addLine('animationmaskfile = "gfx/interface/goals/'.$fileName.'.dds"', 3);
				$customGfx .= $this->addLine('animationtexturefile = "gfx/interface/goals/shine_overlay.dds"', 3);
				$customGfx .= $this->addLine('animationrotation = 90.0', 3);
				$customGfx .= $this->addLine('animationlooping = no', 3);
				$customGfx .= $this->addLine('animationtime = 0.75', 3);
				$customGfx .= $this->addLine('animationdelay = 0', 3);
				$customGfx .= $this->addLine('animationblendmode = "add"', 3);
				$customGfx .= $this->addLine('animationtype = "scrolling"', 3);
				$customGfx .= $this->addLine('animationrotationoffset = { x = 0.0 y = 0.0 }', 3);
				$customGfx .= $this->addLine('animationtexturescale = { x = 1.0 y = 1.0 } ', 3);
				$customGfx .= $this->addLine('}', 2);
				$customGfx .= $this->addLine('legacy_lazy_load = no', 2);
				$customGfx .= $this->addLine('}', 1);
				$customGfx .= $this->addLine('');
			}

			//Create new focus
			$fortree .= $this->addLine('#'.$focus->name, 1);
			$fortree .= $this->addLine("focus = {", 1);
				//Focus content
				$fortree .= $this->addLine('id = '.$focusID, 2);
				$fortree .= $this->addLine('icon = '.str_replace('"', '', $icon), 2);
				$fortree .= $this->addLine('x = '.$focus->x, 2);
				$fortree .= $this->addLine('y = '.$focus->y, 2);

				$json = json_decode($focus->data);
				foreach($json as $key => $value){
					if(!empty($value) && (!is_array($value) && !empty(trim($value)))){
						if($key == 'will_lead_to_war_with' && !is_array($value)){
							$value = explode(',', $value);
						}
						switch ($key) {
							case 'prerequisite':
							case 'mutually_exclusive':
								$fortree .= $this->focusInternalToExternalConvert($key, $value, $key);
								break;
							case 'ai_will_do':
								if(is_numeric($value)){
									$fortree .= $this->addLine($key.' = { factor = '.$value.' }', 2);
								}else{
									$fortree .= $this->addLine($key.' = {', 2);
									$lines = explode("\n", $value);
									foreach($lines as $line){
										$fortree .= $this->addLine($line, 3);
									}
									$fortree .= $this->addLine('}', 2);
								}
								break;
							case 'cost':
							case 'will_lead_to_war_with':
							case 'available_if_capitulated':
							case 'cancel':
							case 'cancel_if_invalid':
							case 'complete_tooltip':
							case 'historical_ai':
							case 'select_effect':
							case 'continue_if_invalid':
							case 'dynamic':
								if(!empty($value)){
									if($key == 'select_effect'){ //This one sometimes imports with { } and sometimes without
										$trimmedValue = trim($value);
										if(!empty($trimmedValue)){
											if($trimmedValue[0] != '{'){
												$value = "{\n".$value;
											}
										}
										if(substr_count($value, '}') < substr_count($value, '{')){
											$value .= "\n}";
										}
									}
									if(is_array($value)){
										foreach($value as $v){
											$fortree .= $this->addLine($key.' = '.$v, 2);
										}
									}else{
										$fortree .= $this->addLine($key.' = '.$value, 2);
									}
								}
								break;
							case 'offset':
							case '[object SVGSymbolElement]':
							case 'text':
							break;
							default:
								$fortree .= $this->addLine($key.' = {', 2);
								if(gettype($value) == 'array'){
									$v = '';
									foreach($value as $i){
										$v .= htmlspecialchars_decode($i);
									}
									$value = $v;
								}
									$lines = explode("\n", $value);
									foreach($lines as $line){
										$fortree .= $this->addLine(htmlspecialchars_decode($line), 3);
									}
								$fortree .= $this->addLine('}', 2);
								break;
						}
					}
				}

			$fortree .= $this->addLine('}', 1);

			//Add focus to focus tree
			$focusTree .= $fortree."\r\n";
		}

		$focusTree .= $this->addLine('}');

		$customGfx .= '}';

		return ['tree' => $focusTree, 'lang' => $lang, 'customgfx' => $customGfx];
	}

	/*
	* Generate files for country upon export
	* ---
	* country - Individual country from the mod
	*
	*/
	public function generateCountryFile($country, $localisationFile){
		$language = $country->language;
		try{
			$files = $this->generateCountryHistoryFile($country, $localisationFile);
		}
		catch (Throwable $t){
			$this->errors[] = translate('tools/mod/index.export.error.country_history', ['tag' => $country->tag]);
		}

		$localisationFile = $files['localisation'];

		$tag = $country->tag;
		foreach($country->dates[0]->ideologies as $ideology){
			$localisationFile .= $this->addLine($tag.'_'.$ideology['ideology'].':5 "'.$ideology['name'].'"');
			$localisationFile .= $this->addLine($tag.'_'.$ideology['ideology'].'_DEF:5 "'.$ideology['definition'].'"');
			$localisationFile .= $this->addLine($tag.'_'.$ideology['ideology'].'_ADJ:5 "'.$ideology['nationality'].'"');
		}
		
		$files['localisation'] = $localisationFile;
		return $files;
		
	}

	private function generateCountryHistoryFile($country, $localisationFile){
		$tag = $country->tag;
		$startDate = $country->dates[0];

		$historyFile = chr(239) . chr(187) . chr(191) .'';
		$id = $this->nameToId(strtolower(preg_replace("/[^A-Za-z]/", '', $country->name)), 'c_'.$country->id, $country->tag);

		//Start of file
		$historyFile .= $this->addLine('oob = "'.$country->tag.'_'.$startDate->year.'"');
		$historyFile .= $this->addLine('capital = '.$country->capital);
		$historyFile .= $this->addLine('set_research_slots = '.$country->misc_data['set_research_slots']);
		$historyFile .= $this->addLine('set_stability = '.$country->misc_data['set_stability']);
		$historyFile .= $this->addLine('set_war_support = '.$country->misc_data['set_war_support']);
		$historyFile .= $this->addLine('set_convoys = '.$country->misc_data['set_convoys']);
		if(!empty($country->misc_data['create_faction'])){
			$historyFile .= $this->addLine('create_faction = "'.$country->misc_data['create_faction'].'"');
		}
		$historyFile .= $this->addLine('');
		if(!empty($country->misc_data['add_to_faction'])){
			foreach($country->misc_data['add_to_faction'] as $f){
				if(!is_array($f)){
					$f = explode(',', $f);
				}
				if(!empty($f) && !is_array($f)){
					$historyFile .= $this->addLine('add_to_faction = '.$f);
				}else{
					if(is_array($f)){
						foreach($f as $po){
							if(!empty($po)){
								$historyFile .= $this->addLine('add_to_faction = '.$po);
							}
						}
					}
				}
			}
		}
		if(!empty($country->misc_data['give_military_access'])){
			foreach($country->misc_data['give_military_access'] as $f){
				if(!empty($f)  && !is_array($f)){
					$historyFile .= $this->addLine('give_military_access = '.$f);
				}else{
					if(is_array($f)){
						foreach($f as $po){
							if(!empty($po)){
								$historyFile .= $this->addLine('give_military_access = '.$po);
							}
						}
					}
				}
			}
		}
		$historyFile .= $this->addLine('');
		$historyFile .= $this->addLine('add_ideas = {');
		$mD = json_decode(json_encode($startDate->misc_data));
			foreach($country->government as $type => $ideas){
				if(!in_array($type, ['political_advisor', 'high_command', 'air_chief', 'navy_chief', 'army_chief', 'theorist'])){
					foreach($ideas as $gID => $idea){
						if($idea['is_active']){
							if(!$idea['is_default']){
									$historyFile .= $this->addLine($this->nameToId($idea['data']['name'], 'i_'.$id), 1);
							}else{
								$historyFile .= $this->addLine($gID, 1);
							}
						}
					}
				}
			}
			if(isset($startDate->misc_data) && !empty($startDate->misc_data)){
				
				if(isset($mD->national_spirits)){
					foreach($mD->national_spirits as $idea){
						if(isset($this->idArrays['i_'.$idea->id])){
							$historyFile .= $this->addLine((!empty($idea->prefix) ? $idea->prefix.'_' : '').$this->idArrays['i_'.$idea->id]);
						}
					}
				}
			}

		$historyFile .= $this->addLine('}');
		$historyFile .= $this->addLine('');
			foreach($country->government as $type => $ideas){
				if(in_array($type, ['political_advisor', 'high_command', 'air_chief', 'navy_chief', 'army_chief', 'theorist'])){
					foreach($ideas as $gID => $idea){
						if(!$idea['is_default']){
								$historyFile .= $this->addLine('recruit_character = '.$this->nameToId($idea['data']['name'], 'i_'.$id), 1);
						}
					}
				}
			}
		$historyFile .= $this->addLine('');
		if(isset($startDate->misc_data) && !empty($startDate->misc_data)){
			if(isset($mD->subjects)){
				foreach($mD->subjects as $subject){
					if(!empty($subject->tag)){
						$historyFile .= $this->addLine('set_autonomy = {');
							$historyFile .= $this->addLine('target = '.$subject->tag, 1);
							$historyFile .= $this->addLine('autonomous_state = '.$subject->autonomyLevel, 1);
						$historyFile .= $this->addLine('}');
						$historyFile .= $this->addLine('');
					}
				}
			}
		}

		if(!empty($startDate->research)){
			$historyFile .= $this->addLine('#Starting tech');
			$historyFile .= $this->addLine('set_technology = {');
				foreach($startDate->research as $r){
						if(empty(trim($r['value']))){
							continue;
						}
					$historyFile .= $this->addLine($r['value'].' = 1', 1);
				}
			$historyFile .= $this->addLine('}');
		}

		//Expand upon start of file for later dates
		foreach($country->dates as $index => $date){
			//Ignore the start date, as that is set above
			if($index == 0){
				continue;
			}
			$historyFile .= $this->addLine('');
			
			
			

			$historyFile .= $this->addLine('');
			$historyFile .= $this->addLine('#For start date '.$date->year.'.'.$date->month.'.'.$date->day);
			$historyFile .= $this->addLine($date->year.'.'.$date->month.'.'.$date->day.' = {');
				$historyFile .= $this->addLine('add_political_power = 1000', 1);
				$historyFile .= $this->addLine('oob = "'.$country->tag.'_'.$date->year.'"', 1);
				// $historyFile .= $this->addLine('');
				// $historyFile .= $this->addLine('#complete_national_focus = YOUR_FOCUS_ID', 1);
				// $historyFile .= $this->addLine('#complete_national_focus = YOUR_FOCUS_ID', 1);
				// $historyFile .= $this->addLine('');
				//TODO: Research
				if(!empty($date->research)){
					$historyFile .= $this->addLine('set_technology = {', 1);
						foreach($date->research as $r){
								if(empty(trim($r['value']))){
									continue;
								}
							$historyFile .= $this->addLine($r['value'].' = 1', 2);
						}
					$historyFile .= $this->addLine('}', 1);
				}
				if(isset($date->misc_data) && !empty($date->misc_data)){
					$mD = json_decode(json_encode($date->misc_data));
					if(isset($mD->national_spirits)){
						$historyFile .= $this->addLine('add_ideas = {');
							foreach($mD->national_spirits as $idea){
								if(isset($this->idArrays['i_'.$idea->id])){
									$historyFile .= $this->addLine((!empty($idea->prefix) ? $idea->prefix.'_' : '').$this->idArrays['i_'.$idea->id], 1);
								}
							}
						$historyFile .= $this->addLine('}');
						$historyFile .= $this->addLine('');
					}
					if(isset($mD->subjects)){
						foreach($mD->subjects as $subject){
							$historyFile .= $this->addLine('set_autonomy = {', 1);
								$historyFile .= $this->addLine('target = '.$subject->tag, 2);
								$historyFile .= $this->addLine('autonomous_state = '.$subject->autonomyLevel, 2);
							$historyFile .= $this->addLine('}', 1);
							$historyFile .= $this->addLine('');
						}
					}
				}

			$historyFile .= $this->addLine('}');
		}

		//Set politics
		$historyFile .= $this->addLine('');
		$historyFile .= $this->addLine('#Politics for '.$startDate->year.'.'.$startDate->month.'.'.$startDate->day);
		$historyFile .= $this->addLine('set_politics = {');
			$popularityTotal = 0;
			foreach($startDate->ideologies as $i){
				$popularityTotal += intval($i['popularity']);
			}
			if($popularityTotal > 100){
				$this->errors[] = $country->name." has total party popularity greater than 100% for date: ".$startDate->year.'.'.$startDate->month.'.'.$startDate->day;
			}
			$historyFile .= $this->addLine('ruling_party = '.$country->misc_data['set_politics']['ruling_party'], 1);
			$historyFile .= $this->addLine('last_election = "'.$country->misc_data['set_politics']['last_election'].'"', 1);
			$historyFile .= $this->addLine('election_frequency = '.$country->misc_data['set_politics']['election_frequency'], 1);
			$historyFile .= $this->addLine('elections_allowed = '.(($country->misc_data['set_politics']['elections_allowed'] === 'yes' || $country->misc_data['set_politics']['elections_allowed'] === true || $country->misc_data['set_politics']['elections_allowed'] === 1) ? 'yes' : 'no'), 1);
		$historyFile .= $this->addLine('}');

		$historyFile .= $this->addLine('set_popularities = {');
			foreach($startDate->ideologies as $i){
				$historyFile .= $this->addLine($i['ideology'].' = '.intval($i['popularity']), 1);
			}
		$historyFile .= $this->addLine('}');

		//Set politics for later dates
		$characters = 'characters = {'; //Start character file
		foreach($country->dates as $index => $date){
			if($index == 0){
				continue;
			}

			$historyFile .= $this->addLine('');
			$historyFile .= $this->addLine('#Politics for '.$date->year.'.'.$date->month.'.'.$date->day);
			$historyFile .= $this->addLine($date->year.'.'.$date->month.'.'.$date->day.' = {');
				//$historyFile .= $this->addLine('set_politics = {', 1);
				$popularityTotal = 0;
				foreach($date->ideologies as $i){
					$popularityTotal += intval($i['popularity']);
				}
				if($popularityTotal > 100){
					$this->errors[] = $country->name." has total party popularity greater than 100% for date: ".$date->year.'.'.$date->month.'.'.$date->day;
				}
				//$historyFile .= $this->addLine('}', 1);
				$historyFile .= $this->addLine('set_popularities = {', 1);
					foreach($date->ideologies as $i){
						$historyFile .= $this->addLine($i['ideology'].' = '.intval($i['popularity']), 2);
					}
				$historyFile .= $this->addLine('}', 1);
			$historyFile .= $this->addLine('}');
		}

		//Add the country leaders
		$leadersList = ''; //These need to be reversed
		foreach($country->dates as $index => $d){
			$popularity = [];
			foreach($d->ideologies as $i){
				$pop = (float) $i['popularity'];
				while(isset($popularity[$pop])){
					$pop -= 0.1;
				}
				if(isset($i['party']) && !empty($i['party'])){
					$localisationFile .= $this->addLine($country->tag.'_'.$i['ideology'].'_party'.':0 "'.$i['party'].'"');
				}
				if(isset($i['party_long']) && !empty($i['party_long'])){
					$localisationFile .= $this->addLine($country->tag.'_'.$i['ideology'].'_party_long'.':0 "'.$i['party_long'].'"');
				}
				$popularity[$pop] = $i;
			}
			
			krsort($popularity);
			$historyFile .= $this->addLine('');
			foreach($popularity as $indx => $i){
				if($i['leader'] != '???' && !empty($i['leader'])){
					$leaders = $this->addLine('');
					$leaderID = $this->nameToId($i['leader'], 'leader_'.$country->id.$indx, $country->tag);
					$historyFile .= $this->addLine('recruit_character = '.$leaderID);
					$localisationFile .= $this->addLine($leaderID.':0 "'.str_replace("\n", '\n', $i['leader']).'"');

					$leaders .= $this->addLine('#Create country leader - '.$i['leader']);
					$leaders .= $this->addLine($leaderID.' = {');
						$leaders .= $this->addLine('name = '.$leaderID, 1);
						if(isset($i['leader_description']) && !empty($i['leader_description'])){
							$leaderID = strtoupper($leaderID.'_DESC');
							$localisationFile .= $this->addLine($leaderID.'_desc:0 "'.str_replace(["\n", '"'], ['\n', '\"'], $i['leader_description']).'"');
							//$leaders .= $this->addLine('desc = "'.$leaderID.'"', 1);
						}
						$leaders .= $this->addLine('portraits = {', 1);
							$leaders .= $this->addLine('civilian = {', 2);
								if(!is_numeric($i['leader_gfx'])){
									$leaders .= $this->addLine('large = "'.$i['leader_gfx'].'"', 3);
								}else{
									$fileName = $this->nameToId($i['leader'], 'country_leader_gfx_'.$country->id.'_'.$i['ideology']).'-'.$i['leader_gfx'];
									Media::copyImageToDirectoryWithJSON($this->mediaJSON,$i['leader_gfx'], $this->modDirectory.'/gfx/leaders/'.$country->tag, $fileName, 'leader');
									$leaders .= $this->addLine('#Custom image for '.$i['leader'], 3);
									$leaders .= $this->addLine('large = "/gfx/leaders/'.$country->tag.'/'.$fileName.'.dds"', 3);
								}
							$leaders .= $this->addLine('}', 2);
						$leaders .= $this->addLine('}', 1);
					
						$leaders .= $this->addLine('country_leader = {', 1);
							$leaders .= $this->addLine('expire = "'. ((isset($i['leader_expire']) && !empty($i['leader_expire'])) ? $i['leader_expire'] : '1965.1.1').'"', 2);
							$leaders .= $this->addLine('ideology = '.$i['sub_ideology'], 2);
							if(isset($i['leader_traits'])){
								$leaders .= $this->addLine('traits = { '.$i['leader_traits'].' }', 2);
							}
						$leaders .= $this->addLine('}', 1);
					$leaders .= $this->addLine('}');

					$leadersList = $leaders . $leadersList;
				}

				if($index == 0){ //Very first start date
					$flag = $i['flag'];
					//Custom flag set

					if($flag != 1 && $flag != "1"){
						$fileName = $country->tag.'_'.$i['ideology'];
						Media::copyImageToDirectoryWithJSON($this->mediaJSON,$flag, $this->modDirectory.'/gfx/flags', $fileName, 'flag-standard', 'tga');
						Media::copyImageToDirectoryWithJSON($this->mediaJSON,$flag, $this->modDirectory.'/gfx/flags/medium', $fileName, 'flag-medium', 'tga');
						Media::copyImageToDirectoryWithJSON($this->mediaJSON,$flag, $this->modDirectory.'/gfx/flags/small', $fileName, 'flag-small', 'tga');
					}
				}
				
			}
		}
		$characters .= $leadersList; //This should be reverse order

		//For general/admiral ID
		$idIns = rand(1, 100).$country->id;
		//Setup generals
		if(!empty($country->general_data)){
			foreach($country->general_data as $index => $general){
				$leader = $this->addLine('');
				$generalID = $this->nameToId($general['name'], 'country_leader_gfx_'.$general['name']);
				$historyFile .= $this->addLine('recruit_character = '.$generalID);
				$leader .= $this->addLine('#Create military leader - '.$general['name']);

				$leaderId = '1'.$idIns.$index; //To try to ensure it won't conflict with anything in game or in mod/other mods
				$leader .= $this->addLine($generalID . ' = {', 1);
				// if($general['is_field_marshal']){
				// 	$historyFile .= $this->addLine('create_field_marshal = {');
				// }else{															//Old way of doing things
				// 	$historyFile .= $this->addLine('create_corps_commander = {');
				// }
				$leader .= $this->addLine('name = "'.$generalID.'"', 2);
				$localisationFile .= $this->addLine($generalID.':0 "'.$general['name'].'"');
				//Check if the GFX was as the import expected
				if(!isset($general['gfx'])){ $general['gfx'] = 1; }
				$leader .= $this->addLine('portraits = {', 2);
					$leader .= $this->addLine('army = {', 3);
						if(!is_numeric($general['gfx'])){
							$leader .= $this->addLine('large = "'.$general['gfx'].'"', 4);
						}else{
							$fileName = $generalID.'-'.$general['gfx'];
							Media::copyImageToDirectoryWithJSON($this->mediaJSON,$general['gfx'], $this->modDirectory.'/gfx/leaders/'.$country->tag, $fileName, 'leader');
							Media::copyImageToDirectoryWithJSON($this->mediaJSON,$general['gfx'], $this->modDirectory.'/gfx/interface/ideas/'.$country->tag, $fileName, 'idea');
							
							$leader .= $this->addLine('#Custom images for '.$general['name'], 4);
							$leader .= $this->addLine('large = "/gfx/leaders/'.$country->tag.'/'.$fileName.'.dds"', 4);
							$leader .= $this->addLine('small = "/gfx/interface/ideas/'.$country->tag.'/'.$fileName.'.dds"', 4);
						}
					$leader .= $this->addLine('}', 3);
				$leader .= $this->addLine('}', 2);

				if($general['is_field_marshal']){
					$leader .= $this->addLine('field_marshal = {', 2);
				}else{
					$leader .= $this->addLine('corps_commander = {', 2);
				}
					$leader .= $this->addLine('traits = {'.implode(' ', $general['traits']).' }', 3);
					$leader .= $this->addLine('skill = '.$general['overall'], 3);
					$leader .= $this->addLine('attack_skill = '.$general['attack'], 3);
					$leader .= $this->addLine('defense_skill = '.$general['defence'], 3);
					$leader .= $this->addLine('planning_skill = '.$general['planning'], 3);
					$leader .= $this->addLine('logistics_skill = '.$general['logistics'], 3);
					$leader .= $this->addLine('legacy_id = '.$leaderId, 2);
				$leader .= $this->addLine('}', 2);

				$leader .= $this->addLine('}', 1);
				$characters .= $leader; 
			}
		}

		if(!empty($country->admiral_data)){
			foreach($country->admiral_data as $index => $general){
				$leader = $this->addLine('');
				$leaderId = '3'.$idIns.$index;
				$generalID = $this->nameToId($general['name'], 'country_leader_gfx_'.$general['name']);
				$historyFile .= $this->addLine('recruit_character = '.$generalID);
				$leader .= $this->addLine($generalID.' = {', 1);

				$leader .= $this->addLine('name = "'.$generalID.'"', 2);
				$localisationFile .= $this->addLine($generalID.':0 "'.$general['name'].'"');

				//Check if the GFX was as the import expected
				if(!isset($general['gfx'])){ $general['gfx'] = 1; }
				$leader .= $this->addLine('portraits = {', 2);
					$leader .= $this->addLine('army = {', 3);
						if(!is_numeric($general['gfx'])){
							$leader .= $this->addLine('large = "'.$general['gfx'].'"', 4);
						}else{
							$fileName = $generalID.'-'.$general['gfx'];
							Media::copyImageToDirectoryWithJSON($this->mediaJSON,$general['gfx'], $this->modDirectory.'/gfx/leaders/'.$country->tag, $fileName, 'leader');
							Media::copyImageToDirectoryWithJSON($this->mediaJSON,$general['gfx'], $this->modDirectory.'/gfx/interface/ideas/'.$country->tag, $fileName, 'idea');
							
							$leader .= $this->addLine('#Custom images for '.$general['name'], 4);
							$leader .= $this->addLine('large = "/gfx/leaders/'.$country->tag.'/'.$fileName.'.dds"', 4);
							$leader .= $this->addLine('small = "/gfx/interface/ideas/'.$country->tag.'/'.$fileName.'.dds"', 4);
						}
					$leader .= $this->addLine('}', 3);
				$leader .= $this->addLine('}', 2);

				$leader .= $this->addLine('navy_leader = {', 2);
					$leader .= $this->addLine('traits = {'.implode(' ', $general['traits']).' }', 3);
					$leader .= $this->addLine('skill = '.$general['overall'], 3);
					$leader .= $this->addLine('attack_skill = '.$general['attack'], 3);
					$leader .= $this->addLine('defense_skill = '.$general['defence'], 3);
					$leader .= $this->addLine('maneuvering_skill = '.$general['maneuvering'], 3);
					$leader .= $this->addLine('coordination_skill = '.$general['coordination'], 3);
					$leader .= $this->addLine('legacy_id = '.$leaderId, 3);
				$leader .= $this->addLine('}', 2);

				$leader .= $this->addLine('}', 1);

				$characters .= $leader; 
			}
		}
		$historyFile .= $this->addLine('');

		$characters .= $this->addLine('}');

		Storage::write($this->modDirectory.'/common/characters/'.$country->tag.'.txt', $characters);

		foreach($country->dates as $index => $d){
			if(!empty($d->states)){
				foreach($d->states as $stateID => $state){
					$date = null;
					if($index > 0){
						$date = $d->year.'.'.$d->month.'.'.$d->day;
					}
					$this->editStateFileContents($stateID, $tag, $state['core'], $state['control'], $date);
				}
			}
		}

		return ['history' => $historyFile, 'localisation' => $localisationFile];
	}


	private function generateCountryCommonFile($country){
		$commonFile = '';
		$commonFile .= $this->addLine('graphical_culture = '.$country->culture.'_gfx');
		$commonFile .= $this->addLine('graphical_culture_2d = '.$country->culture.'_2d');
		$commonFile .= $this->addLine('color = { '.$country->colour['r'].' '.$country->colour['g'].' '.$country->colour['b'].' }');

		$names = '';
		if(isset($country->names) && !empty($country->names)){
			$names = $this->addLine($country->tag.' = {');
				foreach($country->names as $type => $nameList){
					$names .= $this->addLine($type.' = {', 1);
						$indent = 2;
						if(in_array($type, ['male', 'female'])){
							$indent++;
						}
						if($indent == 3){
							$names .= $this->addLine('names = {', 2);
						}
							$allNames = '';
							foreach($nameList as $name){
								$allNames .= '"'.$name.'" ';
							}
							$names .= $this->addLine($allNames, $indent);
						if($indent == 3){
							$names .= $this->addLine('}', 2);
						}
					$names .= $this->addLine('}', 1);
				}
			$names .= $this->addLine('}');
			Storage::write($this->modDirectory.'/common/names/'.$country->name.'.txt', $names);
		}

		return $commonFile;
	}

	private function generateCountryIdeasFile($country, $localisationFile){
		$ideasFile = '';
		$id = $this->nameToId(strtolower(preg_replace("/[^A-Za-z]/", '', $country->name)), 'c_'.$country->id, $country->tag);
		$customGfx = '';
		$characters = 'characters = {';

		$ideasFile .= $this->addLine('ideas = {');
			foreach($country->government as $type => $ideas){
				$ideasFile .= $this->addLine($type.' = {', 1);
					foreach($ideas as $idea){
						if(!$idea['is_default']){
							$name = $this->nameToId($idea['data']['name'], 'i_'.$id);

							$ideasFile .= $this->addLine($name.' = {', 2);
								$ideasFile .= $this->addLine('allowed = {', 3);
									$ideasFile .= $this->addLine('tag = '.$country->tag, 4);
								$ideasFile .= $this->addLine('}', 3);
								$ideasFile .= $this->addLine('traits = {'.implode(' ', $idea['data']['traits']).'}', 3);

								if(isset($idea['data']['ledger']) && !empty($idea['data']['ledger'])){
									$ideasFile .= $this->addLine('ledger = '.$idea['data']['ledger'], 3);
								}

								$fileName = $this->nameToId($idea['data']['name'], 'country_idea_gfx_'.$idea['data']['name']).'-'.$idea['icon'];

								Media::copyImageToDirectoryWithJSON($this->mediaJSON,(int) $idea['icon'], $this->modDirectory.'/gfx/interface/ideas/'.$country->tag, $fileName, 'idea');
								$customGfx .= $this->addLine('##Icon For: '.$idea['data']['name'], 1);
								$customGfx .= $this->addLine('SpriteType = { ', 1);
								$customGfx .= $this->addLine('name = "'.$fileName.'"', 2);
								$customGfx .= $this->addLine('texturefile = "gfx/interface/ideas/'.$country->tag.'/'.$fileName.'.dds"', 2);
								$customGfx .= $this->addLine('}', 1);
								$customGfx .= $this->addLine('');

								$ideasFile .= $this->addLine('picture = "'.$fileName.'"', 3);
							$ideasFile .= $this->addLine('}', 2);

							$localisationFile .= $this->addLine($name.':0 "'.$idea['data']['name'].'"');

							if(in_array($type, ['political_advisor', 'high_command', 'air_chief', 'navy_chief', 'army_chief', 'theorist'])){
								$characters .= $this->addLine($name . ' = {', 1);
									$characters .= $this->addLine('name = '.$name, 2);
									$characters .= $this->addLine('portraits = {', 2);
										$characters .= $this->addLine('army = {', 3);
											$characters .= $this->addLine('small = "gfx/interface/ideas/'.$country->tag.'/'.$fileName.'.dds"', 3);
										$characters .= $this->addLine('}', 3);
									$characters .= $this->addLine('}', 2);
									$characters .= $this->addLine('advisor = {', 2);
										$characters .= $this->addLine('slot = '.$type, 3);
										if(isset($idea['data']['ledger']) && !empty($idea['data']['ledger'])){
											$characters .= $this->addLine('ledger = '.$idea['data']['ledger'], 3);
										}
										$characters .= $this->addLine('idea_token = '.strtolower($name), 3);
										$characters .= $this->addLine('allowed = {', 3);
											$characters .= $this->addLine('original_tag = '.$country->tag, 4);
										$characters .= $this->addLine('}', 3);
										$characters .= $this->addLine('traits = {'.implode(' ', $idea['data']['traits']).'}', 3);
									$characters .= $this->addLine('}', 2);
								$characters .= $this->addLine('}', 1);
							}
						}
					}
				$ideasFile .= $this->addLine('}');
			}
		$ideasFile .= $this->addLine('}');

		$characters .= $this->addLine('}');

		Storage::write($this->modDirectory.'/common/characters/'.$country->tag.'_ideas.txt', $characters);

		//Wrap it if icons are needed
		if(!empty($customGfx)){
			$customGfx = "spriteTypes = { \r\n" .$customGfx ." \r\n }";
		}

		return ['ideas' => $ideasFile, 'localisation' => $localisationFile, 'customGFX' => $customGfx];
	}

	public function setStateFileData(){
		$states = Storage::files('/hoi4-files/history/states');

		foreach($states as $filePath){
			$arr = explode('-', str_replace('hoi4-files', 'hoi4files', $filePath), 2);

			$this->stateFiles[trim(str_replace('hoi4files/history/states/','',$arr[0]))] = $filePath;
		}
	}

	public function editStateFileContents($stateID, $tag, $core = false, $controls = false, $date = null){
		if(empty($this->stateFiles)){
			$this->setStateFileData();
		}

		if(!isset($this->editedStateFiles[$stateID])){
			$json = json_decode(Storage::get($this->stateFiles[$stateID]));
			// if(isset($_GET['debug'])){
			// 	var_dump($json);
			// }
			foreach($json->state->history as $key => $value){
				if(substr($key, 0, 2) == '19'){
					unset($json->state->history->{$key});
				}
			}
			// if(isset($_GET['debug'])){
			// 	var_dump($json);
			// }
		}else{
			$json = $this->editedStateFiles[$stateID];
		}


		//Only want to update state files if they have been altered
		$updated = false;
		if(empty($date)){
			$info = $this->updateStateJSON($json->state->history, $tag, $core, $controls, $json);
			if($info['updated']){
				$json->state->history = $info['json'];
				$updated = true;
			}
		}else{
			if(!isset($json->state->history->{$date})){
				$json->state->history->{$date} = json_decode(json_encode(['owner' => '']));
			}else{
				//If it hasn't been edited yet, this is what we need
				if(!json_decode($json->state->history->{$date})){
					$makeJSON = [];
					$lines = explode("\n", $json->state->history->{$date});
					foreach($lines as $l){
						if(!empty($l)){
							$kv = explode('=', $l);
							if(isset($kv[1])){
								$makeJSON[trim($kv[0])] = trim($kv[1]);
							}
						}
					}
					$json->state->history->{$date} = json_decode(json_encode($makeJSON));
				}
			}
			$info = $this->updateStateJSON($json->state->history->{$date}, $tag, $core, $controls, $json);
			if($info['updated']){
				$json->state->history->{$date} = $info['json'];
				$updated = true;
			}
		}

		if($updated){
			$this->editedStateFiles[$stateID] = $json;
		}
	}

	public function updateStateJSON($json, $tag, $core, $control, $full){
		$updated = false;

		//Add it if it's not there
		if(!isset($json->add_core_of)){
			$json->add_core_of = [];
		}
		$coreData = $json->add_core_of;
		if($core){
			//The core has been set

			//Check if it is an array, and if it's not already in it
			if(is_array($coreData)){
				if(!in_array($tag, $coreData)){
					//It's not already there, so add it
					$json->add_core_of[] = $tag;
					$updated = true;
				}
			}else{
				if($tag != $coreData){
					//The string that exists isn't the tag
					$json->add_core_of = [$json->add_core_of, $tag];
					$updated = true;
				}
			}
		}else{
			//The core has not been set/removed
			if(is_array($coreData)){
				if(in_array($tag, $coreData)){
					//Remove it from the array
					unset($json->add_core_of[array_search($tag, $coreData)]);
					$updated = true;
				}
			}else{
				if($tag == $coreData){
					//The tag is the only one in the list. Set it as an empty array
					$json->add_core_of = [];
					$updated = true;
				}
			}
		}

		//Add it if it's not there
		if(!isset($json->owner)){
			$json->owner = '';
		}
		if(!isset($json->controller)){
			$json->controller = '';
		}
		$owner = $json->owner;
		$controller = $json->controller;
		if($control){
			//The core has been set

			//Check if it is an array, and if it's not already in it
			if(is_array($owner)){
				$json->owner = $tag;
				$json->controller = $tag;
				$updated = true;
			}else{
				if($tag != $owner){
					//The string that exists isn't the tag
					$json->owner = $tag;
					$json->controller = $tag;
					$updated = true;
				}
			}
		}else{
			//The core has not been set/removed
			if(is_array($owner)){
				if(in_array($tag, $owner)){
					//Remove it from the array
					$json->owner = '';
					$json->controller = '';
					$updated = true;
				}
			}else{
				if($tag == $owner){
					//The tag is the only one in the list. Set it as an empty array
					$json->owner = '';
					$json->controller = '';
					$updated = true;
				}
			}
		}

		if(empty($json->owner)){
			if(isset($full->state->history->owner)){
				$json->owner = $full->state->history->owner;
			}
		}

		if(empty($json->controller) && !empty($json->owner)){
			$json->controller = $json->owner;
		}
		if(empty($json->controller) && empty($json->owner)){
			unset($json->controller);
			unset($json->owner);
		}

		if(isset($_GET['debug'])){
			var_dump($json);
		}

		return ['json' => $json, 'updated' => $updated];
	}

	/*
	* Convert edited state file JSONs to HOI4 format, and save to export folder
	* 
	*/
	public function generateEditedStateFiles(){
		foreach($this->editedStateFiles as $id => $data){
			//Should always be set, but better safe than sorry
			if(isset($this->stateFiles[$id])){
				//If this isn't there, then the JSON is fucked. Skip it
				if(isset($data->state)){
					$filename = $this->stateFiles[$id];
					$contents = '';
					$contents .= "state = {\n";
						$contents .= $this->getStateFileData($data->state);
					$contents .= "\n}";
					Storage::write($this->modDirectory.'/history/states/'.str_replace('hoi4-files/history/states', '',$filename), $contents);
				}
			}
		}
	}

	public function getStateFileData($loop, $indent = 0){
		$contents = '';
		foreach($loop as $key => $value){
			if(!is_array($value) && !is_object($value)){
				$contents .= $this->addLine($key.' = '.$value, ($indent + 1));
			}else{
				if(is_object($value)){
					$contents .= $this->addLine($key.' = {', ($indent + 1));
						$contents .= $this->getStateFileData($value, ($indent + 1));
					$contents .= $this->addLine('}', ($indent + 1));
				}
				if(is_array($value)){
					foreach($value as $v){
						$contents .= $this->addLine($key.' = '.$v, ($indent + 1));
					}
				}
			}
		}
		return $contents;
	}

	public function generateDivisions($country){
		$n = 1; //For unit numbering
		foreach($country->dates as $dateIndex => $date){
			if(!empty($date->divisions)){
				$divFile = '';
				$nameList = '';

				//Add division templates
				$divFile .= $this->addLine('#Create units');
				foreach($date->divisions as $index => $division){
					$divFile .= "division_template = {\n";
						$divFile .= $this->addLine('name = "'.$division['name'].'"', 1);
						$divFile .= $this->addLine('division_names_group = "'.$country->tag.'_DIV_'.$index.'_'.$dateIndex.'"', 1);

						$nameList .= $country->tag.'_DIV_'.$index.'_'.$dateIndex.' = {';
							$nameList .= $this->addLine('name = "'.$division['name'].'"', 1);
							$nameList .= $this->addLine('for_countries = { '.$country->tag.' }', 1);
							$nameList .= $this->addLine('can_use = { always = yes }', 1);
							$divisionList = [];
							if(isset($division['regiments'])){
								foreach($division['regiments'] as $x => $data){
									foreach($data as $y => $unit){
										if(!in_array($unit, $divisionList)){
											$divisionList[] = $unit;
										}
									}
								}
							}else{
								$divisionList[] = 'infantry';
							}
							$nameList .= $this->addLine('division_types = { ', 1);
							$divList = '';
							foreach($divisionList as $d){
								$divList .= '"'.$d.'" ';
							}
							$divList .= '}';
							$nameList .= $this->addLine($divList, 2);
							$nameList .= $this->addLine('}', 1);
							$nameList .= $this->addLine('fallback_name = "%d. '.$division['name'].'"', 1);
						$nameList .= $this->addLine('}');
						
						if(isset($division['regiments'])){
							$divFile .= $this->addLine('regiments = {', 1);
							foreach($division['regiments'] as $x => $data){
								foreach($data as $y => $unit){
									$divFile .= $this->addLine($unit.' = { x = '.$x.' y = '.$y.'}', 2);
								}
							}
							$divFile .= $this->addLine('}', 1);
						}
					
						if(isset($division['support'])){
							$divFile .= $this->addLine('support = {', 1);
								foreach($division['support'] as $y => $unit){
									$divFile .= $this->addLine($unit.' = { x = 0 y = '.$y.'}', 2);
								}
							$divFile .= $this->addLine('}', 1);
						}
					$divFile .= "\n}\n";
				}
				$divFile .= $this->addLine('');
				$divFile .= $this->addLine('#Spawn units');
				//Add templates to their location(s)
				$divFile .= $this->addLine('units = {');
					foreach($date->divisions as $index => $division){
						foreach($division['locations'] as $province => $amount){
							if(!empty($amount)){ //Not 0, null...
								$amount = (int) $amount;
								//Add all of the units for this province
								while($amount > 0){
									$divFile .= $this->addLine('division= {', 1);
										$ordinal = date( 'S', mktime( 1, 1, 1, 1, ( (($n>=10)+($n%100>=20)+($n==0))*10 + $n%10) ));

										$divFile .= $this->addLine('name = "'.$n.$ordinal.' '.$division['name'].'"', 2);
										$divFile .= $this->addLine('location = '.$province, 2);
										$divFile .= $this->addLine('division_template = "'.$division['name'].'"', 2);
									$divFile .= $this->addLine('}', 1);

									$n++;

									$amount--;
								}
							}
						}
					}
				$divFile .= $this->addLine('}');

				$filename = $country->tag.'_'.$date->year.'.txt';
				Storage::write($this->modDirectory.'/history/units/'.$filename, $divFile);

				$filename = $country->tag.'_'.$date->year.'_names_divisions.txt';
				Storage::write($this->modDirectory.'/common/unit_names/names_divisions/'.$filename, $nameList);
			}
		}

	}

	public function generateIdeasFiles($mod){
		foreach($mod->ideas as $group){
			if($group->ideasOrdered->count() == 0){
				continue;
			}
			$prefix = $group->prefix;
			if(!empty($prefix)){
				$prefix .= '_';
			}
			$filename = $group->filename;
			$language = $prefix.$filename.'_l_'.$group->language.'.yml';

			$file = 'ideas = {';
			$ideaType = '';
			$characters = 'characters = {';

			$lang = chr(239) . chr(187) . chr(191) .'l_'.$group->language.':'."\n";
			$customGfx = "spriteTypes = { \r\n";
			foreach($group->ideasOrdered as $idea){
				if($idea->group != $ideaType){
					$file .= $this->addLine('');
					if(!empty($ideaType)){
						$file .= $this->addLine('}', 1);
					}
					$ideaType = $idea->group;
					$file .= $this->addLine($idea->group.' = {', 1);
				}
				$name = $prefix.$this->nameToId(preg_replace("/[^A-Za-z0-9_]/", '', str_replace(' ', '_',$idea->name)), 'i_'.$idea->id, $group->prefix);

				$lang .= $this->addLine($name.':0 "'.$idea->name.'"');
				$noBraceKeyValues = ['removal_cost', 'ledger', 'cost'];
				$file .= $this->addLine('');
				$file .= $this->addLine($name.' = {', 2);
					$gfxID = $idea->gfx;
					$iconName = $name.'-'.$gfxID;
					$type = 'idea';
					$file .= $this->addLine('picture = '.$iconName, 3);
					Media::copyImageToDirectoryWithJSON($this->mediaJSON,$gfxID, $this->modDirectory.'/gfx/interface/ideas/'.$group->filename.'-ideas/', $iconName, $type);
					$customGfx .= $this->addLine('##Icon For: '.$idea->name, 1);
					$customGfx .= $this->addLine('SpriteType = { ', 1);
					$customGfx .= $this->addLine('name = "GFX_idea_'.$iconName.'"', 2);
					$customGfx .= $this->addLine('texturefile = "gfx/interface/ideas/'.$group->filename.'-ideas/'.$iconName.'.dds"', 2);
					$customGfx .= $this->addLine('}', 1);
					$customGfx .= $this->addLine('');

					foreach($idea->options as $key => $value){
						$key = str_replace(['offence', 'defence'], ['offense', 'defense'], $key);
						if(in_array($key, $noBraceKeyValues)){
							$file .= $this->addLine($key.' = '.$value, 3);
						}else{
							if($key == 'description'){
								$lang .= $this->addLine($name.'_desc:0 "'.str_replace("\n", '\n', $value).'"');
							}else{
								if($key == 'allowed_civil_war' && (trim($value) == 'yes' || trim($value) == 'no')){
									$value = 'always = '.$value;
								}
								if($key == 'ai_will_do'){
									$value = 'factor = '.$value;
								}
								$file .= $this->addLine($key.' = {', 3);
								$file .= $this->addLine($value, 4);
								$file .= $this->addLine('}', 3);
							}
						}
					}
				$file .= $this->addLine('}', 2);
				

				if(in_array($ideaType, ['political_advisor', 'high_command', 'air_chief', 'navy_chief', 'army_chief', 'theorist'])){
					$characters .= $this->addLine($name . ' = {', 1);
						$characters .= $this->addLine('name = '.$name, 2);
						$characters .= $this->addLine('portraits = {', 2);
							$characters .= $this->addLine('army = {', 3);
								$characters .= $this->addLine('small = "gfx/interface/ideas/'.$group->filename.'-ideas/'.$iconName.'.dds"', 3);
							$characters .= $this->addLine('}', 3);
						$characters .= $this->addLine('}', 2);
						$characters .= $this->addLine('advisor = {', 2);
							$characters .= $this->addLine('slot = '.$ideaType, 3);
							if(isset($idea->options['ledger']) && !empty($idea->options['ledger'])){
								$characters .= $this->addLine('ledger = '.$idea->options['ledger'], 3);
							}
							$characters .= $this->addLine('idea_token = '.strtolower($name), 3);
							foreach($idea->options as $key => $value){
								$key = str_replace(['offence', 'defence'], ['offense', 'defense'], $key);
								if(in_array($key, $noBraceKeyValues)){
									$characters .= $this->addLine($key.' = '.$value, 3);
								}else{
									if($key == 'allowed_civil_war' && (trim($value) == 'yes' || trim($value) == 'no')){
										$value = 'always = '.$value;
									}
									if($key == 'ai_will_do'){
										$value = 'factor = '.$value;
									}
									$characters .= $this->addLine($key.' = {', 3);
									$characters .= $this->addLine($value, 4);
									$characters .= $this->addLine('}', 3);
								}
							}
						$characters .= $this->addLine('}', 2);
					$characters .= $this->addLine('}', 1);
				}
				

			}
			$file .= $this->addLine('}', 1);
			$file .=  $this->addLine('}');
			$customGfx .= " \r\n }";
			Storage::write($this->modDirectory.'/common/characters/ideas_'.$prefix.$filename.'.txt', $characters);
			Storage::write($this->modDirectory.'/common/ideas/'.$prefix.$filename.'.txt', $file);
			Storage::write($this->modDirectory.'/localisation/ideas_'.$prefix.$filename.'_l_'.$group->language.'.yml', $lang);
			Storage::write($this->modDirectory.'/interface/ideas-'.$prefix.$filename.'_customicons.gfx', $customGfx);
		}
	}

	public function generateEventsFiles($mod){
		foreach($mod->events as $group){
			if($group->eventsOrdered->count() == 0){
				continue;
			}
			$prefix = $group->prefix;
			if(!empty($prefix)){
				$prefix .= '_';
			}
			$filename = $group->filename;
			$language = $prefix.$filename.'_l_'.$group->language.'.yml';

			$namespace = $prefix.$filename;

			$file = $this->addLine('add_namespace = '.$namespace);


			$lang = chr(239) . chr(187) . chr(191) .'l_'.$group->language.':'."\n";
			$customGfx = "spriteTypes = { \r\n";
			$num = 0;
			foreach($group->events as $event){
				$num++;
				$name = $prefix.$this->nameToId(preg_replace("/[^A-Za-z0-9_]/", '', str_replace(' ', '_',$event->name)), 'i_'.$event->id, $group->prefix);
				$file .= $this->addLine('');
				$file .= $this->addLine('#'.$event->name);
				$file .= $this->addLine($event->type.'_event = {');

				$file .= $this->addLine('id = '.$namespace.'.'.$num, 1);
				$file .= $this->addLine('title = '.$namespace.'.'.$num.'.t', 1);
				$file .= $this->addLine('desc = '.$namespace.'.'.$num.'.d', 1);

				$lang .= $this->addLine($namespace.'.'.$num.'.t'.':0 "'.$event->name.'"');
				$lang .= $this->addLine($namespace.'.'.$num.'.d'.':0 "'.(isset($event->data['description']) ? str_replace("\n", '\n', $event->data['description']) : '').'"');
				
				$noBraceKeyValues = ['timeout_days', 'mean_time_to_happen', 'ai_chance'];
				$booleanKeys = ['fire_only_once', 'is_triggered_only', 'fire_for_sender', 'hidden', 'major'];
				$file .= $this->addLine('');
				$gfxID = $event->gfx;
				$iconName = $name.'-'.$gfxID;
				$type = 'event';

				if($event->type == 'news'){
					$type = 'news-event';
				}

				$file .= $this->addLine('picture = '.$iconName, 1);
				Media::copyImageToDirectoryWithJSON($this->mediaJSON,$gfxID, $this->modDirectory.'/gfx/interface/events/'.$group->filename.'-event/', $iconName, $type);
				$customGfx .= $this->addLine('##Icon For: '.$event->name, 1);
				$customGfx .= $this->addLine('SpriteType = { ', 1);
				$customGfx .= $this->addLine('name = "'.$iconName.'"', 2);
				$customGfx .= $this->addLine('texturefile = "gfx/interface/events/'.$group->filename.'-event/'.$iconName.'.dds"', 2);
				$customGfx .= $this->addLine('}', 1);
				$customGfx .= $this->addLine('');

				foreach($event->data as $key => $value){
					if($key != 'description'){
						if(in_array($key, $noBraceKeyValues) || in_array($key, $booleanKeys)){
							if(in_array($key, $noBraceKeyValues)){
								if($key == 'mean_time_to_happen'){
									$key .= ' = { days';
									$value .= ' }';
								}
								$file .= $this->addLine($key.' = '.$value, 1);
							}
							if(in_array($key, $booleanKeys)){
								$file .= $this->addLine($key.' = '.($value == '1' ? 'yes' : 'no'), 1);
							}
						}else{
							$file .= $this->addLine($key.' = {', 1);
							$file .= $this->addLine($value, 2);
							$file .= $this->addLine('}', 1);
						}
					}
				}
				$optNum = 'a';
				if(isset($event->options)){
					foreach($event->options as $option){
						$file .= $this->addLine('option = {', 1);
							$file .= $this->addLine('name = '.$namespace.'.'.$num.'.'.$optNum, 2);
							$lang .= $this->addLine($namespace.'.'.$num.'.'.$optNum.':0 "'.$option['name'].'"');
							foreach(json_decode($option['data']) as $key => $value){
								if(in_array($key, $noBraceKeyValues)){
									switch ($key) {
										case 'ai_chance':
											if(empty($value)){
												$value = 1;
											}
											$file .= $this->addLine($key.' = { factor = '.$value.' }', 2);
											break;
										
										default:
											$file .= $this->addLine($key.' = '.$value, 2);
											break;
									}
										
								}else{
									if($key == 'effect'){
										$file .= $this->addLine($value, 2);
									}else{
										$file .= $this->addLine($key.' = {', 2);
										$file .= $this->addLine($value, 3);
										$file .= $this->addLine('}', 2);
									}
								}
							}
						$file .= $this->addLine('}', 1);

						$optNum++;
					}
				}
				else{
					//TODO: Throw error - No event options for $event->name
				}
				$file .= $this->addLine('}');


				

			}

			$customGfx .= " \r\n }";

			Storage::write($this->modDirectory.'/events/'.$prefix.$filename.'.txt', $file);
			Storage::write($this->modDirectory.'/localisation/events_'.$prefix.$filename.'_l_'.$group->language.'.yml', $lang);
			Storage::write($this->modDirectory.'/interface/events-'.$prefix.$filename.'_customicons.gfx', $customGfx);
		}
	}

	/*
	* Convert internal focus list to external focus list
	* ---
	* key - The key to for the values to adhere to (prereq. mutually_excl.)
	* value - List of internal IDs to be parsed (1AND2OR3)
	*
	*/
	public function focusInternalToExternalConvert($key, $value, $type){
		$val = '';
		$or = explode('AND', $value);
		foreach($or as $item){
			if(!empty($val)){
				$val .= ' '.$type.' = ';
			}
			$val .= '{';
			$and = explode('OR', $item);
			foreach($and as $id){
				if(isset($this->focusTreeFocuses[$id])){
					$val .= ' focus = '.$this->focusTreeFocuses[$id].' ';
				}
			}
			$val .= ' }';
		}
		return $this->addLine($key.' = '.$val, 2);
	}


	public function generateBookmarkFiles($mod){
		$json = json_decode($mod->data);
		$override = false;
		if($json){
			if(isset($json->start_date_overhaul) && !empty($json->start_date_overhaul)){
				$override = true;
			}
		}

		if($override){
			Storage::write($this->modDirectory.'/common/bookmarks/blitzkrieg.txt', '');
			Storage::write($this->modDirectory.'/common/bookmarks/the_gathering_storm.txt', '');
		}


		foreach($mod->startDates as $startDate){
			$filename = $this->nameToId($startDate->name);
			$file = '';
			$localisationFile = chr(239) . chr(187) . chr(191) .'l_'.$startDate->language.":\n";
			$localisationFile .= $this->addLine($filename.'_name:0 "'.$startDate->name.'"');
			$localisationFile .= $this->addLine($filename.'_desc:0 "'.str_replace(["\n","\r"], '\n', $startDate->description).'"');

			$customGfx = "spriteTypes = { \r\n";

			$file .= $this->addLine('bookmarks = {');
				$file .= $this->addLine('bookmark = {', 1);
					$file .= $this->addLine('name = "'.$filename.'_name"', 2);
					$file .= $this->addLine('desc = "'.$filename.'_desc"', 2);

					$date = explode('.',$startDate->date);
					if(count($date) == 4){ //Has hour
						$date = $startDate->date;
					}elseif(count($date) == 3){ //No hour
						$date = $startDate->date.'.12';
					}else{ //Invalid
						$date = '1936.1.1.12';
					}

					$file .= $this->addLine('date = '.$date, 2);

					$iconName = $filename.'-'.$startDate->gfx;

					//Fix image
					$file .= $this->addLine('picture = "'.$iconName.'"', 1);
					Media::copyImageToDirectoryWithJSON($this->mediaJSON,$startDate->gfx, $this->modDirectory.'/gfx/interface/bookmarks/'.$filename.'-bookmark/', $iconName, 'bookmark');
					$customGfx .= $this->addLine('##Icon For: '.$startDate->name, 1);
					$customGfx .= $this->addLine('SpriteType = { ', 1);
					$customGfx .= $this->addLine('name = "'.$iconName.'"', 2); //TODO - Fix image not working
					$customGfx .= $this->addLine('texturefile = "gfx/interface/bookmarks/'.$filename.'-bookmark/'.$iconName.'.dds"', 2);
					$customGfx .= $this->addLine('}', 1);
					$customGfx .= $this->addLine('');

					$file .= $this->addLine('default_country = "'.$startDate->data['default_country'].'"', 2);
					if(isset($startDate->data['default'])){
						$file .= $this->addLine('default = yes', 2);
					}

					foreach($startDate->countries as $country){
						$file .= $this->addLine('"'.$country['tag'].'" = {', 2);
							$localisationFile .= $this->addLine($filename.'_'.$country['tag'].':0 "'.$country['data']['history'].'"');
							$file .= $this->addLine('history = "'.$filename.'_'.$country['tag'].'"', 3);
							if(isset($country['data']['ideology']) && !empty($country['data']['ideology'])){
								$file .= $this->addLine('ideology = '.$country['data']['ideology'], 3);
							}
							if(!empty($country['data']['ideas'])){
								$file .= $this->addLine('ideas = {', 3);
								foreach(explode(',', $country['data']['ideas']) as $idea){
									$file .= $this->addLine($idea, 4);
								}
								$file .= $this->addLine('}', 3);
							}
							if(!empty($country['data']['focuses'])){
								$file .= $this->addLine('focuses = {', 3);
								foreach(explode(',', $country['data']['focuses']) as $focus){
									$file .= $this->addLine($focus, 4);
								}
								$file .= $this->addLine('}', 3);
							}
							if(isset($country['data']['minor'])){
								$file .= $this->addLine('minor = yes', 3);
							}
						$file .= $this->addLine('}', 2);
					}

				$file .= $this->addLine('}', 1);
			$file .= $this->addLine('}');

			Storage::write($this->modDirectory.'/common/bookmarks/'.$filename.'.txt', $file);
			Storage::write($this->modDirectory.'/localisation/bookmark_'.$filename.'_l_'.$startDate->language.'.yml', $localisationFile);
			Storage::write($this->modDirectory.'/interface/bookmark-'.$filename.'_customicons.gfx', $customGfx);
		}
	}

	public function generateIdeologiesFiles($mod){
		$json = json_decode($mod->data);
		$override = false;
		$customGfx = 'spriteTypes = {';
		if($json){
			if(isset($json->ideology_overhaul) && !empty($json->ideology_overhaul)){
				$override = true;
			}
		}

		if($override){
			Storage::write($this->modDirectory.'/common/ideologies/00_ideologies.txt', '');
			//Storage::copy('/hoi4-files/events/PoliticalEvents.txt', $this->modDirectory.'/events/PoliticalEvents.txt');
		}
		
		foreach($mod->ideologies as $ideology){
			$file = '';
			$file .= $this->addLine('ideologies = {');
				$localisationFile = chr(239) . chr(187) . chr(191) .'l_'.$ideology->language.":\n";
				$id = $this->nameToId($ideology->name, 'ideology_'.$ideology->id);

				if(isset($ideology->data['gfx']) && !empty($ideology->data['gfx'])){
					$fileName = $id.'_icon';
					Media::copyImageToDirectoryWithJSON($this->mediaJSON,$ideology->data['gfx'], $this->modDirectory.'/gfx/interface/ideologies/', $fileName, 'ideology');
					$customGfx .= $this->addLine('##Icon For: '.$ideology->name, 1);
					$customGfx .= $this->addLine('SpriteType = { ', 1);
					$customGfx .= $this->addLine('name = "GFX_ideology_'.$fileName.'_group"', 2);
					$customGfx .= $this->addLine('texturefile = "gfx/interface/ideologies/'.$fileName.'.dds"', 2);
					$customGfx .= $this->addLine('}', 1);
					$customGfx .= $this->addLine('');
				}

				$localisationFile .= $this->addLine($id.':0 "'.$ideology->name.'"');
				if(isset($ideology->data['description'])){
					$localisationFile .= $this->addLine($id.'_desc:0 "'.str_replace("\n", '\n', $ideology->data['description']).'"');
				}
				$file .= $this->addLine($id.' = {', 1);
					$file .= $this->addLine('types = {', 2);
					foreach($ideology->types as $type){
						$typeID = $this->nameToId($type['name'], 'ideology_'.$ideology->id.'_'.$type['index']);
						$localisationFile .= $this->addLine($typeID.':0 "'.$type['name'].'"');
						if(isset($type['description'])){
							$localisationFile .= $this->addLine($typeID.'_desc:0 "'.str_replace("\n", '\n', $type['description']).'"');
						}

						$file .= $this->addLine($typeID .' = { '. ((isset($type['restricted']) && !empty($type['restricted'])) ? 'can_be_randomly_selected = no' : '') .' }', 3);
					}
					$file .= $this->addLine('}', 2);

					$file .= $this->addLine('dynamic_faction_names = {', 2);
					foreach($ideology->data['factions'] as $faction){
						$factionID = 'faction_'.$id.'_'.$this->nameToId($faction['name'], 'ideology_'.$ideology->id.'_'.$faction['index']);
						$localisationFile .= $this->addLine($factionID.':0 "'.$faction['name'].'"');

						$file .= $this->addLine('"'. $factionID .'"', 3);
					}
					$file .= $this->addLine('}', 2);

					$file .= $this->addLine('color = { '.$ideology->data['colour']['r'].' '.$ideology->data['colour']['g'].' '.$ideology->data['colour']['b'].' }', 2);

					foreach($ideology->data as $key => $value){
						if(in_array($key, ['colour', 'factions', 'description', 'gfx'])){
							continue;
						}
						if(strpos($key, 'faction_') !== false){
							continue;
						}
						if(!empty($value)){
							if($value === true){ // For checkbox answer
								$value = 'yes';
							}
							$file .= $this->addLine($key . ' = '. $value, 2);
						}
					}

					$file .= $this->addLine('faction_modifiers = {', 2);
						foreach($ideology->data as $key => $value){
							if(strpos($key, 'faction_') !== false){
								if(!empty($value)){
									$file .= $this->addLine($key . ' = '. $value, 3);
								}
							}
						}
					$file .= $this->addLine('}', 2);

					$file .= $this->addLine('rules = {', 2);
					foreach($ideology->rules as $rule => $value){
						if($value){
							$file .= $this->addLine($rule.' = yes', 3);
						}
					}
					$file .= $this->addLine('}', 2);

					$file .= $this->addLine('modifiers = {', 2);
					foreach($ideology->modifiers as $modifier => $value){
						if($value){
							$file .= $this->addLine($modifier.' = '. $value, 3);
						}
					}
					$file .= $this->addLine('}', 2);

					$file .= $this->addLine('ai_neutral = yes', 2);
				$file .= $this->addLine('}', 1);

			$file .= $this->addLine('}');

			

			Storage::write($this->modDirectory.'/common/ideologies/'.$id.'.txt', $file);
			Storage::write($this->modDirectory.'/localisation/ideology_'.$id.'_l_'.$ideology->language.'.yml', $localisationFile);

		}
		$customGfx .= $this->addLine('}');
		Storage::write($this->modDirectory.'/interface/ideologies_customicons.gfx', $customGfx);
		
	}

	public function generateDecisionFiles($mod){
		foreach($mod->decisions as $category){
			$decisionCategoryID = $this->nameToId($category->name, 'decision_category_'.$category->id, 'dc_'.$category->id);

			$localisationFile = chr(239) . chr(187) . chr(191) .'l_'.$category->language.":\n";
			$localisationFile .= $this->addLine($decisionCategoryID.':0 "'.$category->name.'"');
			$localisationFile .= $this->addLine($decisionCategoryID.'_desc:0 "'.str_replace(["\n","\r"], '\n', $category->description).'"');

			$customGfx = "spriteTypes = { \r\n";

			$catFile = $this->addLine($decisionCategoryID.' = {');
				
				if($category->gfx > 1){
					$iconName = 'decision_category_icon_'.$decisionCategoryID;
					$catFile .= $this->addLine('icon = '.$iconName, 1);
					Media::copyImageToDirectoryWithJSON($this->mediaJSON,$category->gfx, $this->modDirectory.'/gfx/interface/decisions', $iconName, 'decision-icon');
					$customGfx .= $this->addLine('##Icon For Category: '.$category->name, 1);
					$customGfx .= $this->addLine('SpriteType = { ', 1);
					$customGfx .= $this->addLine('name = "'.$iconName.'"', 2);
					$customGfx .= $this->addLine('texturefile = "gfx/interface/decisions/'.$iconName.'.dds"', 2);
					$customGfx .= $this->addLine('}', 1);
					$customGfx .= $this->addLine('');

				}
				if($category->description_gfx > 1){
					$iconName = 'decision_category_picture_'.$decisionCategoryID;
					$catFile .= $this->addLine('picture = '.$iconName, 1);
					Media::copyImageToDirectoryWithJSON($this->mediaJSON,$category->description_gfx, $this->modDirectory.'/gfx/interface/decisions', $iconName, 'decision-picture');
					$customGfx .= $this->addLine('##Picture For: '.$category->name, 1);
					$customGfx .= $this->addLine('SpriteType = { ', 1);
					$customGfx .= $this->addLine('name = "'.$iconName.'"', 2);
					$customGfx .= $this->addLine('texturefile = "gfx/interface/decisions/'.$iconName.'.dds"', 2);
					$customGfx .= $this->addLine('}', 1);
					$customGfx .= $this->addLine('');
				}

				if(isset($category->data['allowed']) && !empty($category->data['allowed'])){
					$catFile .= $this->addLine('allowed = {', 1);
						$catFile .= $this->addLine($category->data['allowed'], 2);
					$catFile .= $this->addLine('}', 1);
				}
				if(isset($category->data['visible']) && !empty($category->data['visible'])){
					$catFile .= $this->addLine('visible = {', 1);
						$catFile .= $this->addLine($category->data['visible'], 2);
					$catFile .= $this->addLine('}', 1);
				}
			$catFile .= $this->addLine('}');

			$file = $this->addLine($decisionCategoryID.' = {');
				foreach($category->decisions as $decision){
					$decisionID = $this->nameToId($decision->name, 'decision_'.$decision->id, 'dn_'.$decision->id);
					$localisationFile .= $this->addLine($decisionID.':0 "'.$decision->name.'"');
					
					$file .= $this->addLine($decisionID.' = {', 1);
						if($decision->gfx > 1){
							$iconName = 'decision_icon_'.$decisionID;
							$file .= $this->addLine('icon = '.$iconName, 2);
							Media::copyImageToDirectoryWithJSON($this->mediaJSON,$decision->gfx, $this->modDirectory.'/gfx/interface/decisions', $iconName, 'decision-icon');
							$customGfx .= $this->addLine('##Icon For: '.$decision->name, 1);
							$customGfx .= $this->addLine('SpriteType = { ', 1);
							$customGfx .= $this->addLine('name = "'.$iconName.'"', 2);
							$customGfx .= $this->addLine('texturefile = "gfx/interface/decisions/'.$iconName.'.dds"', 2);
							$customGfx .= $this->addLine('}', 1);
							$customGfx .= $this->addLine('');

						}

						$noBraceKeyValues = ['days_remove', 'days_re_enable', 'days_mission_timeout', 'cost'];
						foreach($decision->data as $key => $value){
							if($key == 'description'){
								$localisationFile .= $this->addLine($decisionID.'_desc:0 "'.str_replace(["\n","\r"], '\n', $value).'"');
							}elseif(in_array($key, ['fire_only_once', 'is_good', 'selectable_mission'])){
								$file .= $this->addLine($key.' = yes', 2);
							}elseif(in_array($key, $noBraceKeyValues)){
								$file .= $this->addLine($key.' = '.$value, 2);
							}else{
								$file .= $this->addLine($key.' = {', 2);
									$file .= $this->addLine($value, 3);
								$file .= $this->addLine('}', 2);
							}
						}
					$file .= $this->addLine('}', 1);
				}
			$file .= $this->addLine('}');

			$customGfx .= $this->addLine('}');

			Storage::write($this->modDirectory.'/common/decisions/categories/'.$decisionCategoryID.'.txt', $catFile);
			Storage::write($this->modDirectory.'/common/decisions/'.$decisionCategoryID.'_decisions.txt', $file);
			Storage::write($this->modDirectory.'/localisation/decisions-'.$decisionCategoryID.'_l_'.$category->language.'.yml', $localisationFile);
			Storage::write($this->modDirectory.'/interface/decisions-'.$decisionCategoryID.'_customicons.gfx', $customGfx);
		}
	}


	/*
	* Vanilla file interactions
	*
	*/
	private function replaceDefaultIdeologiesInFiles($mod){
		$modIdeologies = [];
		foreach($mod->ideologies as $ideology){
			if(isset($this->idArrays['ideology_'.$ideology->id])){
				$modIdeologies[] = $this->idArrays['ideology_'.$ideology->id];
			}
		}

		$ideologyBind = ['democratic' => $modIdeologies[0], 'democracy' => $modIdeologies[0]];
		if(count($modIdeologies) > 1){
			$ideologyBind['communism'] = $modIdeologies[1];
			$ideologyBind['communist'] = $modIdeologies[1];
		}else{
			$ideologyBind['communism'] = $modIdeologies[0];
			$ideologyBind['communist'] = $modIdeologies[0];
		}
		if(count($modIdeologies) > 2){
			$ideologyBind['fascism'] = $modIdeologies[2];
			$ideologyBind['fascist'] = $modIdeologies[2];
		}else{
			$ideologyBind['fascism'] = $modIdeologies[0];
			$ideologyBind['fascist'] = $modIdeologies[0];
		}
		if(count($modIdeologies) > 3){
			$ideologyBind['neutrality'] = $modIdeologies[3];
		}else{
			$ideologyBind['neutrality'] = $modIdeologies[0];
		}

		$replaceIdeologies = ['/events', '/common/national_focus', '/common/on_actions', '/common/ideas', '/common/operations', '/common/country_leader', '/common/modifiers', '/common/units/names_divisions', '/common/characters', '/common/decisions', '/history/countries', '/common/ai_strategy', '/music', '/common/ai_peace', '/common/ai_equipment', '/common/scripted_localisation', '/common/scripted_triggers', '/common/units/names_ships', '/common/technologies', '/common/autonomous_states', '/common/technology_sharing','/common/intelligence_agencies', '/common/decisions/categories', '/common/scripted_effects', '/common/ai_strategy_plans'];
		foreach($replaceIdeologies as $location){
			$files = Storage::files('/game-files'.$location);
			foreach($files as $file){
				$fileData = Storage::get($file);
				$data = $fileData;
				foreach($ideologyBind as $old => $new){
					$data = str_replace($old, $new, $data);
				}
				if($data != $fileData){
					if(!Storage::has($this->modDirectory.str_replace('game-files', '',$file))){
						Storage::write($this->modDirectory.str_replace('game-files', '',$file), $data);
					}
				}
			}
		}


	}

	private function removeDefaultCountriesFromFiles(){
		//May be used in future
		// $filesToEmpty = ['/common/national_focus'];
		// $filesToIgnore = [];
		// foreach($filesToEmpty as $location){
		// 	$files = Storage::files('/game-files'.$location);
		// 	foreach($files as $file){
		// 		if(strpos($file, '/generic.') !== false){
		// 			continue;
		// 		}
		// 		Storage::write($this->modDirectory.str_replace('game-files', '',$file), '');
		// 	}
		// }
		// $commentOutTags = ['/common/on_actions'];
		// foreach($commentOutTags as $location){
		// 	$files = Storage::files('/game-files'.$location);
		// 	foreach($files as $file){
		// 		$fileData = Storage::get($file);
		// 		$replacable = [
		// 			"original_tag =",
		// 			"original_tag=",
		// 			"tag = ", 
		// 			"tag=", 
		// 			"is_owned_by =",
		// 			"is_owned_by=",
		// 			"is_core_of =",
		// 			"is_core_of=",
		// 			"has_war_with =",
		// 			"has_war_with="
		// 		];
		// 		$data = str_replace($replacable, "\n#This tag has been disabled - ", $fileData);
		// 		$data = str_replace(["}"], "\n}", $data);
		// 		Storage::write($this->modDirectory.str_replace('game-files', '',$file), $data);
		// 	}
		// }

		if(empty($this->stateFiles)){
			$this->setStateFileData();
		}
		foreach($this->stateFiles as $id => $file){
			if($file == 'hoi4-files/history/states/state-data.json'){
				continue;
			}
			if(!isset($this->editedStateFiles[$id])){
				$json = json_decode(Storage::get($file));
			}else{
				$json = $this->editedStateFiles[$id];
			}
			if(!isset($this->editedStateFiles[$id])){
				if(isset($json->state->history->add_core_of)){
					unset($json->state->history->add_core_of);
				}
				if(isset($json->state->history->owner)){
					unset($json->state->history->owner);
				}
				if(isset($json->state->history->controller)){
					unset($json->state->history->controller);
				}
			}

			if(empty($json)){
				continue;

				if(isset($_GET['test'])){
					var_dump("JSON TEST");
					var_dump($json);
					var_dump($id);
					var_dump($file);
					echo Storage::get($file);
				}
			}
			foreach($json->state->history as $key => $value){
				// if(substr($key, 0, 2) == '19'){
				// 	unset($json->state->history->{$key});
				// 	continue;
				// }

				
				if(gettype($json->state->history->{$key}) == 'object'){
					if(!isset($this->editedStateFiles[$id])){
						if(isset($json->state->history->{$key}->add_core_of)){
							unset($json->state->history->{$key}->add_core_of);
						}
						if(isset($json->state->history->{$key}->owner)){
							unset($json->state->history->{$key}->owner);
						}
						if(isset($json->state->history->{$key}->controller)){
							unset($json->state->history->{$key}->controller);
						}
					}

					foreach($json->state->history->{$key} as $sub => $val){
						if(substr($sub, 0, 2) == '19'){
							unset($json->state->history->{$key}->{$sub});
						}
					}
				}
				else{
					if(!isset($this->editedStateFiles[$id])){
						if(gettype($json->state->history->{$key}) == 'string'){
							if(strpos($json->state->history->{$key}, 'owner') !== false){
								$json->state->history->{$key} = str_replace('owner', '#owner', $json->state->history->{$key});
							}
							if(strpos($json->state->history->{$key}, 'controller') !== false){
								$json->state->history->{$key} = str_replace('controller', '#controller', $json->state->history->{$key});
							}
							if(strpos($json->state->history->{$key}, 'set_province_') !== false){
								$json->state->history->{$key} = str_replace('set_province_', '#set_province_', $json->state->history->{$key});
							}
							if(strpos($json->state->history->{$key}, 'add_core_of') !== false){
								$json->state->history->{$key} = str_replace('add_core_of', '#add_core_of', $json->state->history->{$key});
							}
						}
					}
				}

				
			}
			$this->editedStateFiles[$id] = $json;
			
		}
	}
}


//$delete_file_after_download= true; doesnt work!!
class FlxZipArchive extends ZipArchive{
	/** Add a Dir with Files and Subdirs to the archive;;;;; @param string $location Real Location;;;;  @param string $name Name in Archive;;; @author Nicolas Heimann;;;; @access private  **/
	public function addDir($location, $name){
		$this->addEmptyDir($name);
		$this->addDirDo($location, $name);
	 } // EO addDir;
	/**  Add Files & Dirs to archive;;;; @param string $location Real Location;  @param string $name Name in Archive;;;;;; @author Nicolas Heimann
	 * @access private   **/
	private function addDirDo($location, $name){
		$name .= '/';
		$location .= '/';
		// Read all Files in Dir
		$dir = opendir ($location);
		while ($file = readdir($dir))
		{
			if ($file == '.' || $file == '..') continue;
			// Rekursiv, If dir: FlxZipArchive::addDir(), else ::File();
			$do = (filetype( $location . $file) == 'dir') ? 'addDir' : 'addFile';
			$this->$do($location . $file, $name . $file);
		}
	} // EO addDirDo();
}