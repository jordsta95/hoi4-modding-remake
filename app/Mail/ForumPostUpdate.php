<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ForumPostUpdate extends Mailable{
	use Queueable, SerializesModels;

	public $emailContent;
	public $user;
	public $title;
	public $link;

	/**
	 * Create a new message instance.
	 *
	 * @return void
	 */
	public function __construct($user, $reply, $post, $link){
		$this->user = $user;
		$this->emailContent = $reply->content;
		$this->title = $post->title;
		$this->link = env('APP_URL').$link;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build(){
		return $this->view('emails/forum.response')
		->subject(translate('tools/forum/emails.newReply.subject', ['title' => $this->title]));
	}
}
