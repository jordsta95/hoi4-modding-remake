<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IdeaGroup extends Model
{
    use HasFactory;
    public function mod(){
    	return $this->belongsTo(Mod::class);
    }
    public function ideas(){
		return $this->hasMany(Idea::class);
	}
	public function ideasOrdered(){
		return $this->hasMany(Idea::class)->orderBy('group', 'ASC');
	}
}
