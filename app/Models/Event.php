<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;
    protected $casts = [
        'options' => 'array',
        'data' => 'array'
    ];
    public function eventGroup(){
    	return $this->belongsTo(EventGroup::class);
    }
}
