<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\User;

class ForumPost extends Model{
    use HasFactory;
    protected $guarded  = ['id'];
    protected $with = ['user'];

    public function comments(){
    	return $this->hasMany(ForumComment::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function forum(){
        return $this->belongsTo(Forum::class);
    }

    public function initialComment(){
    	return $this->hasOne(ForumComment::class)->oldest();
    }

    public function latestComment(){
        return $this->hasOne(ForumComment::class)->latest()->first();
    }

    public function getUsers(){
    	$results = [];
    	$query = $this->comments()->select('user_id')->groupBy('user_id')->get();
    	foreach($query as $q){
    		$results[] = $q->user_id;
    	}
    	$users = User::whereIn('id', $results)->get();
    	return $users;
    }
}
