<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventGroup extends Model
{
    use HasFactory;
     public function mod(){
    	return $this->belongsTo(Mod::class);
    }
    public function events(){
		return $this->hasMany(Event::class)->orderBy('created_at', 'ASC');
	}
    public function eventsOrdered(){
        return $this->hasMany(Event::class)->orderBy('type', 'ASC');
    }
}
