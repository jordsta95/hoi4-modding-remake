<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use HasFactory;
    protected $guarded  = ['id'];
    protected $casts = [
        'government' => 'array',
        'names' => 'array',
        'general_data' => 'array',
        'admiral_data' => 'array',
        'colour' => 'array',
        'misc_data' => 'array'
    ];

    public function dates(){
    	return $this->hasMany(CountryDate::class)->orderBy('year')->orderBy('month')->orderBy('day');
    }

    public function mt(){
    	return $this->belongsTo(Mod::class, 'mod_id', 'id');
    }
}
