<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mod extends Model{
	use HasFactory;
	use SoftDeletes;
	
	protected $guarded  = ['id'];

	public function users(){
		return $this->belongsToMany(User::class, 'user_mods', 'mod_id', 'user_id')->withPivot('role', 'administrator');
	}

	public function roles(){
		return $this->hasMany(UserMod::class, 'mod_id');
	}

	public function focusTrees(){
		return $this->hasMany(FocusTree::class);
	}

	public function focus_trees(){
		return $this->hasMany(FocusTree::class);
	}

	public function events(){
		return $this->hasMany(EventGroup::class);
	}

	public function countries(){
		return $this->hasMany(Country::class);
	}

	public function ideas(){
		return $this->hasMany(IdeaGroup::class);
	}

	public function startDates(){
		return $this->hasMany(StartDate::class);
	}

	public function start_dates(){
		return $this->hasMany(StartDate::class);
	}

	public function ideologies(){
		return $this->hasMany(Ideology::class);
	}

	public function decisions(){
		return $this->hasMany(DecisionCategory::class);
	}

	public function characters(){
		return $this->hasMany(CharacterList::class);
	}

	public function userHasAccess($user){
		$userID = $user;
		if(!is_numeric($user)){
			$userID = $user->id;
		}
		$access = $this->users()->where('user_id', $userID)->exists();

		if(!$access){
			$user = User::find($userID);
			if($user->role_id == 2){
				$access = true;
			}
		}

		return $access; 
	}
}
