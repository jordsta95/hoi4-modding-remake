<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ideology extends Model
{
    use HasFactory;
    protected $casts = [
        'rules' => 'array',
        'modifiers' => 'array',
        'data' => 'array',
        'types' => 'array',
    ];
    public function mod(){
        return $this->belongsTo(Mod::class);
    }
}
