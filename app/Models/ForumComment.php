<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Notifications\ForumPostMention;
use Auth;

class ForumComment extends Model{
    use HasFactory;
    protected $guarded  = ['id'];
    protected $with = ['user'];

    public function user(){
    	return $this->belongsTo(User::class);
    }

    public function post(){
    	return $this->belongsTo(ForumPost::class, 'forum_post_id');
    }


	public function setContentAttribute($body){
		$this->attributes['content'] = preg_replace('/@([\w\-]+)/','<a href="/user/$1">$0</a>',$body);
		$user = strip_tags(preg_replace('/@([\w\-]+)/','$1',$body));
		if(!empty($user) && strpos($body, '@') !== false){
			$inThread = false;
			$post = $this->post;
			foreach($post->getUsers() as $u){
				if($u->username == $user){
					$inThread = true;
				}
			}
			if(!$inThread){
				$u = User::where('username', '=', $user)->first();
				if(!empty($u)){
					$u->notify(new ForumPostMention(Auth::user(), $post));
				}
			}
		}
	}
}
