<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
	use HasFactory, Notifiable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $guarded  = ['id'];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'email_verified_at' => 'datetime',
	];

	public function mods(){
		return $this->belongsToMany(Mod::class,'user_mods', 'user_id', 'mod_id')->withPivot('role', 'administrator');
	}

	public function forum_posts(){
		return $this->hasMany(ForumPost::class);
	}

	public function forum_comments(){
		return $this->hasMany(ForumComment::class);
	}

	public function media(){
		return $this->hasMany(Media::class);
	}
}
