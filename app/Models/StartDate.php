<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StartDate extends Model
{
    use HasFactory;
    protected $casts = [
        'countries' => 'array',
        'data' => 'array'
    ];
    public function mod(){
        return $this->belongsTo(Mod::class);
    }
}
