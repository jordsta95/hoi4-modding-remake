<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CountryDate extends Model
{
    use HasFactory;
    protected $guarded  = ['id'];
    protected $casts = [
        'ideologies' => 'array',
        'research' => 'array',
        'divisions' => 'array',
        'misc_data' => 'array',
        'states' => 'array',
        
    ];
}
