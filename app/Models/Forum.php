<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Forum extends Model{
    use HasFactory;
    protected $guarded  = ['id'];

    public function posts(){
    	return $this->hasMany(ForumPost::class);
    }

    public function latestPost(){
    	return $this->hasMany(ForumPost::class)->latest()->where('public', '=', 1)->first();
    }
}
