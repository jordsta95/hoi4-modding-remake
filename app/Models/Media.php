<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
	use HasFactory;
	protected $guarded  = ['id'];

	/**
	* Make a path attribute which can be used for media/<path> route & media manager
	*
	*/
	public function getUrlFriendlyPathAttribute(){
		return str_replace('/', '-', ltrim($this->path, '/'));
	}

	/**
	* media->url to get a nice-name url instead of /media/find/id
	*
	*/
	public function getUrlAttribute(){
		$path = str_replace('/', '-', ltrim($this->path, '/'));
		return '/media/'.(!empty($path) ? $path : 'root').'/'.$this->filename.'.'.$this->extension;
	}
}
