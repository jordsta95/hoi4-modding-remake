<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DecisionCategory extends Model
{
    use HasFactory;
    protected $casts = [
        'data' => 'array'
    ];
    public function mod(){
    	return $this->belongsTo(Mod::class);
    }
    public function decisions(){
		return $this->hasMany(Decision::class);
	}
}
